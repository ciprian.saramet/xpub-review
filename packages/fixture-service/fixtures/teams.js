const Chance = require('chance')
const { TeamMember } = require('./team-members')
const { assign, remove } = require('lodash')

const chance = new Chance()
const teams = []

function Team(props) {
  return {
    id: chance.guid(),
    role: props.role || null,
    manuscriptId: props.manuscriptId || null,
    journalId: props.journalId || null,
    members: props.members || [],
    async save() {
      if (!this.id) {
        this.id = chance.guid()
      }
      const existingTeam = teams.find(t => t.id === this.id)
      if (existingTeam) {
        assign(existingTeam, this)
      } else {
        teams.push(this)
      }
      return Promise.resolve(this)
    },
    addMember(user, invitationOptions) {
      this.members = this.members || []
      const existingMember = this.members.some(
        member => member.userId === user.id,
      )
      if (existingMember) {
        const defaultIdentity = user.getDefaultIdentity()

        if (existingMember.status === 'removed') {
          throw new Error(
            `${this.role} invitation for ${
              defaultIdentity.email
            } was removed and can't be invited again`,
          )
        }
        throw new ValidationError(
          `User ${defaultIdentity.email} is already invited as ${this.role}`,
        )
      }

      const newMember = new TeamMember({
        ...invitationOptions,
        userId: user.id,
        teamId: this.id,
        position: this.members.length,
      })
      newMember.linkUser(user)
      newMember.team = this

      this.members.push(newMember)

      return newMember
    },
    removeMember(memberId) {
      this.members = this.members || []
      const member = this.members.find(member => member.id === memberId)

      if (!member) {
        throw new NotFoundError(
          `The specified user is not invited as a ${this.role}`,
        )
      }
      if (member.isSubmitting) {
        throw new ValidationError(
          `Submitting authors can't be deleted from the team`,
        )
      }

      this.members = this.members.filter(member => member.id !== memberId)
    },
    async delete() {
      remove(teams, f => f.id === this.id)
    },
    saveRecursively() {
      this.save()
      if (this.members.length > 0) {
        this.members.map(member => member.saveRecursively())
      }
    },
  }
}

Team.Role = {
  author: 'author',
  admin: 'admin',
  editorInChief: 'editorInChief',
  reviewer: 'reviewer',
  handlingEditor: 'handlingEditor',
  editorialAssistant: 'editorialAssistant',
}

Team.GlobalRoles = [
  Team.Role.admin,
  Team.Role.editorInChief,
  Team.Role.handlingEditor,
  Team.Role.editorialAssistant,
]

Team.createOrFind = async function createOrFind({
  queryObject,
  eagerLoadRelations,
  options,
}) {
  let team = await this.findOneBy({
    queryObject,
    eagerLoadRelations,
  })

  if (!team) {
    team = new Team(options)
  }

  return team
}

const generateTeam = properties => {
  const team = new Team(properties || {})

  teams.push(team)

  return team
}

const getTeamByRole = role => teams.find(team => team.role === role)
const getTeamByManuscriptId = id => teams.find(team => team.manuscriptId === id)
const getTeamsByManuscriptId = id =>
  teams.filter(team => team.manuscriptId === id)
const getTeamByRoleAndJournalId = ({ id, role }) =>
  teams.find(team => team.role === role && team.journalId === id)
const getTeamByRoleAndManuscriptId = ({ id, role }) =>
  teams.find(team => team.role === role && team.manuscriptId === id)
const getGlobalTeam = () =>
  teams.find(team => team.journalId === null && team.manuscriptId === null)
const getGlobalTeamByRole = role =>
  teams.find(
    team =>
      team.role === role &&
      team.manuscriptId === null &&
      team.journalId === null,
  )

module.exports = {
  Team,
  teams,
  generateTeam,
  getTeamByRole,
  getTeamByManuscriptId,
  getTeamByRoleAndJournalId,
  getTeamByRoleAndManuscriptId,
  getTeamsByManuscriptId,
  getGlobalTeam,
  getGlobalTeamByRole,
}
