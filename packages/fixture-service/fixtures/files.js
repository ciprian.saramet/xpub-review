const Chance = require('chance')
const { assign, remove } = require('lodash')

const chance = new Chance()

const files = []
function File(props) {
  return {
    id: chance.guid(),
    type: props.type || 'manuscript',
    fileName: props.fileName || 'test-file',
    size: props.size || '123',
    originalName: props.originalName || 'test-file',
    manuscriptId: props.manuscriptId,
    position: props.position || 0,
    mimeType: props.mimeType || 'application/pdf',
    async save() {
      files.push(this)
      return Promise.resolve(this)
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },
    async delete() {
      remove(files, f => f.id === this.id)
    },
    saveRecursively() {
      this.save()
    },
  }
}

File.Types = {
  manuscript: 'manuscript',
  supplementary: 'supplementary',
  coverLetter: 'coverLetter',
  reviewComment: 'reviewComment',
  responseToReviewers: 'responseToReviewers',
}

const generateFile = properties => {
  const file = new File(properties || {})

  files.push(file)

  return file
}

module.exports = { File, files, generateFile }
