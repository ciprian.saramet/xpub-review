const { chain, assign, has, get } = require('lodash')
const config = require('config')
const Chance = require('chance')
const { Team } = require('./teams')

const chance = new Chance()

function Journal(props) {
  const isActive = has(props, 'isActive') ? get(props, 'isActive') : false

  return {
    id: chance.guid(),
    name: props.name || config.get('journal.name'),
    code: props.code || chance.word().toUpperCase(),
    apc: props.apc || 0,
    email: props.email || chance.email(),
    issn:
      props.issn ||
      `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
        min: 999,
        max: 9999,
      })}`,
    isActive,
    teams: [],
    journalArticleTypes: [],
    created: props.created || Date.now(),
    activationDate: props.activationDate || null,
    async save() {
      return Promise.resolve(this)
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },
    toDTO() {
      const eicMember = chain(this.teams)
        .find(t => t.role === Team.Role.editorInChief)
        .get('members.0')
        .value()

      return {
        ...this,
        editorInChief: eicMember ? eicMember.toDTO() : undefined,
        articleTypes: this.journalArticleTypes
          ? this.journalArticleTypes.map(jat => jat.articleType.toDTO())
          : undefined,
      }
    },
    getEditorInChief() {
      const eicTeam = this.teams.find(t => t.role === Team.Role.editorInChief)
      if (!eicTeam) {
        throw new Error('Could not find Editor in Chief team')
      }
      return eicTeam.members[0]
    },
    getEditorialAssistant() {
      const eaTeam = this.teams.find(
        t => t.role === Team.Role.editorialAssistant,
      )
      if (!eaTeam) {
        throw new Error('Could not find Editorial Assistant team')
      }
      return eaTeam ? eaTeam.members[0] : undefined
    },
    assignTeam(team) {
      this.teams = this.teams || []
      this.teams.push(team)
    },
  }
}

const journals = [
  new Journal({
    name: config.get('journal.name'),
    code: chance.word().toUpperCase(),
    email: chance.email(),
    issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`,
    apc: chance.natural(),
    created: Date.now(),
    activationDate: null,
  }),
]

const generateJournal = properties => {
  const journal = new Journal(properties || {})
  journals.push(journal)

  return journal
}

module.exports = { journals, Journal, generateJournal }
