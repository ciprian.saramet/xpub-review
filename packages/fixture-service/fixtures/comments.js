const Chance = require('chance')
const { assign, remove } = require('lodash')

const chance = new Chance()

const comments = []

function Comment(props) {
  return {
    id: chance.guid(),
    reviewId: props.reviewId,
    content: props.content,
    type: props.type,
    files: props.files,
    async save() {
      const existingComment = comments.find(m => m.id === this.id)
      if (existingComment) {
        assign(existingComment, this)
      } else {
        comments.push(this)
      }
      return Promise.resolve(this)
    },
    async delete() {
      remove(comments, comm => comm.id === this.id)
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },
    toDTO() {
      return { ...this }
    },
  }
}

const generateComment = properties => {
  const comment = new Comment(properties || {})
  comments.push(comment)

  return comment
}

Comment.Types = {
  public: 'public',
  private: 'private',
}

module.exports = { Comment, comments, generateComment }
