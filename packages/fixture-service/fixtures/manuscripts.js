const Chance = require('chance')
const {
  get,
  map,
  pick,
  assign,
  sortBy,
  chain,
  groupBy,
  last,
  remove,
} = require('lodash')
const config = require('config')

const chance = new Chance()
const manuscripts = []
const statuses = config.get('statuses')
const { Team } = require('./teams')
const { TeamMember } = require('./team-members')
const { Review } = require('./reviews')

function Manuscript(props) {
  return {
    id: chance.guid(),
    journalId: props.journalId || null,
    submissionId: props.submissionId || chance.guid(),
    ended: props.ended || null,
    created: props.created || new Date(),
    updated: props.updated || new Date(),
    status: props.status || 'draft',
    formState: props.formState || null,
    decision: props.decision || null,
    customId: props.customId || null,
    version: props.version || 1,
    title: props.title || chance.sentence(),
    articleType: props.articleType || null,
    articleIds: props.articleIds || null,
    abstract: props.abstract || chance.paragraph(),
    subjects: props.subjects || null,
    history: props.history || null,
    publicationDates: props.publicationDates || [],
    notes: props.notes || null,
    technicalCheckToken: props.technicalCheckToken || null,
    hasPassedEqa: props.hasPassedEqa,
    hasPassedEqs: props.hasPassedEqs,
    agreeTc: props.agreeTc || null,
    files: props.files || [],
    teams: props.teams || [],
    reviews: props.reviews || [],
    conflicts: props.conflicts || {
      hasConflicts: false,
      message: '',
      hasDataAvailability: true,
      dataAvailabilityMessage: '',
      hasFunding: true,
      fundingMessage: '',
    },
    logs: props.logs || [],
    journal: props.journal || [],
    async save() {
      if (!this.id) {
        this.id = chance.guid()
      }
      const existingManuscript = manuscripts.find(m => m.id === this.id)
      if (existingManuscript) {
        assign(existingManuscript, this)
      } else {
        manuscripts.push(this)
      }
      return Promise.resolve(this)
    },
    assignTeam(team) {
      this.teams = this.teams || []
      this.teams.push(team)
      team.manuscriptId = this.id
    },
    assignFile(file) {
      this.files = this.files || []
      this.files.push(file)
    },
    assignReview(review) {
      this.reviews = this.reviews || []
      this.reviews.push(review)
    },
    submitManuscript() {
      this.status = Manuscript.Statuses.technicalChecks
      this.technicalCheckToken = chance.string()
      this.publicationDates.push({
        type: Manuscript.Statuses.technicalChecks,
        date: Date.now(),
      })
    },
    hasHandlingEditor() {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }

      return !!chain(this.teams)
        .find(t => t.role === Team.Role.handlingEditor)
        .get('members')
        .find(member => member.status === 'accepted')
        .value()
    },
    getHandlingEditor() {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }

      const handlingEditor = chain(this.teams)
        .find(t => t.role === Team.Role.handlingEditor)
        .get('members')
        .find(member => member.status === 'accepted')
        .value()

      return handlingEditor
    },
    getHandlingEditorByStatus(status) {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }

      const handlingEditor = chain(this.teams)
        .find(t => t.role === Team.Role.handlingEditor)
        .get('members')
        .find(member => member.status === status)
        .value()

      return handlingEditor
    },
    getSubmittingAuthor() {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }

      const submittingAuthorTeam = this.teams.find(
        t => t.role === Team.Role.author,
      )

      if (!submittingAuthorTeam) {
        throw new Error('Could not find author team')
      }

      if (submittingAuthorTeam.members.length === 0) {
        throw new Error('Members are required.')
      }

      const submittingAuthor = submittingAuthorTeam.members.find(
        tm => tm.isSubmitting,
      )

      return submittingAuthor
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },

    async linkAuditLog(auditLog) {
      this.logs.push(auditLog)
      await this.saveRecursively()
    },
    toDTO() {
      const membersPerTeam = get(this, 'teams', []).reduce((acc, team) => {
        acc[team.role] = map(sortBy(team.members, 'position'), member =>
          member.toDTO(),
        )
        return acc
      }, {})

      const manuscript = pick(this, [
        'id',
        'submissionId',
        'ended',
        'created',
        'updated',
        'status',
        'formState',
        'decision',
        'version',
        'hasPassedEQS',
        'hasPassedEQA',
        'technicalCheckToken',
        'teams',
        'files',
        'reviews',
        'role',
        'comment',
      ])

      manuscript.customId =
        manuscript.role === 'author' &&
        ['draft', 'technicalChecks'].includes(manuscript.status)
          ? undefined
          : this.customId

      const meta = pick(this, [
        'title',
        'articleType',
        'articleIds',
        'abstract',
        'subjects',
        'history',
        'publicationDates',
        'notes',
        'conflicts',
        'agreeTc',
      ])

      const status = get(manuscript, 'status', 'draft')
      manuscript.visibleStatus = get(
        statuses,
        `${status}.${manuscript.role || 'admin'}.label`,
      )

      return {
        ...manuscript,
        authors: membersPerTeam.author,
        reviewers: membersPerTeam.reviewer,
        handlingEditor: chain(membersPerTeam)
          .get('handlingEditor', [])
          .find(
            he =>
              he.status !== TeamMember.Statuses.declined &&
              he.status !== TeamMember.Statuses.removed &&
              he.status !== TeamMember.Statuses.expired,
          )
          .value(),
        meta: {
          ...meta,
          title: meta.title || '',
        },
        files: sortBy(this.files, ['type', 'position']) || [],
      }
    },
    async saveRecursively() {
      await this.save()
      if (this.files.length > 0) {
        await Promise.all(this.files.map(async files => files.save()))
      }
      if (this.teams.length > 0) {
        await Promise.all(this.teams.map(async team => team.saveRecursively()))
      }
    },
    delete() {
      remove(manuscripts, f => f.id === this.id)
    },
    updateStatus(status) {
      this.status = status
    },
    getAuthors() {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }

      const authorTeam = this.teams.find(t => t.role === 'author')

      if (!authorTeam) {
        throw new Error('Could not find author team')
      }

      if (authorTeam.members.length === 0) {
        throw new Error('Members are required.')
      }

      return authorTeam.members
    },
    setComment() {
      if (!this.reviews) {
        throw new Error('Reviews are required.')
      }

      const responseToRevisionRequest = this.reviews.find(
        review =>
          review.recommendation === Review.Recommendations.responseToRevision,
      )
      if (!responseToRevisionRequest) {
        throw new ValidationError('There has been no request to revision')
      }

      this.comment = responseToRevisionRequest.comments[0].toDTO()
    },
    getLatestEditorReview() {
      if (!this.reviews) {
        throw new Error('Reviews are required.')
      }

      const editorReviews = this.reviews.filter(review =>
        [
          Team.Role.admin,
          Team.Role.editorInChief,
          Team.Role.handlingEditor,
        ].includes(review.member.team.role),
      )

      return last(sortBy(editorReviews, 'updated'))
    },
    async getLatestHERecommendation() {
      const editorReviews = this.reviews.filter(
        review => get(review, 'member.team.role') === Team.Role.handlingEditor,
      )

      return last(sortBy(editorReviews, 'updated'))
    },
    getReviewers() {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }

      const reviewerTeam = this.teams.find(t => t.role === Team.Role.reviewer)

      if (!reviewerTeam) {
        return []
      }

      if (reviewerTeam.members.length === 0) {
        throw new Error('Members are required.')
      }

      return reviewerTeam.members
    },

    addReviewers(reviewers) {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }

      let reviewerTeam = this.teams.find(t => t.role === Team.Role.reviewer)
      if (reviewerTeam && !reviewerTeam.members) {
        throw new Error('Members are required.')
      }

      if (!reviewerTeam) {
        reviewerTeam = new Team({
          manuscriptId: this.id,
          role: Team.Role.reviewer,
        })

        this.teams.push(reviewerTeam)
      }

      reviewers.forEach(reviewer => {
        if (!reviewer.user) {
          throw new Error('User is required.')
        }
        reviewerTeam.addMember(reviewer.user, {
          status: TeamMember.Statuses.pending,
          reviewerNumber: reviewer.reviewerNumber,
        })
      })
    },
    getReviewersByStatus(status) {
      if (!this.teams) {
        throw new Error('Teams are required.')
      }
      const reviewerTeam = this.teams.find(t => t.role === Team.Role.reviewer)

      if (!reviewerTeam.members.length === 0) {
        throw new Error('Members are required.')
      }
      return reviewerTeam.members.filter(m => m.status === status)
    },
  }
}

const generateManuscript = properties => {
  const manuscript = new Manuscript(properties || {})
  manuscripts.push(manuscript)

  return manuscript
}

Manuscript.Statuses = {
  draft: 'draft',
  technicalChecks: 'technicalChecks',
  submitted: 'submitted',
  heInvited: 'heInvited',
  heAssigned: 'heAssigned',
  reviewersInvited: 'reviewersInvited',
  underReview: 'underReview',
  reviewCompleted: 'reviewCompleted',
  revisionRequested: 'revisionRequested',
  pendingApproval: 'pendingApproval',
  rejected: 'rejected',
  inQA: 'inQA',
  accepted: 'accepted',
  withdrawalRequested: 'withdrawalRequested',
  withdrawn: 'withdrawn',
  deleted: 'deleted',
  olderVersion: 'olderVersion',
}

Manuscript.filterOlderVersions = manuscripts => {
  const submissions = groupBy(manuscripts, 'submissionId')
  const latestVersions = Object.values(submissions).map(versions => {
    if (versions.length === 1) {
      return versions[0]
    }

    const sortedVersions = sortBy(versions, 'version')
    const latestManuscript = last(sortedVersions)

    if (latestManuscript.status === Manuscript.Statuses.draft) {
      return sortedVersions[sortedVersions.length - 2]
    }

    return latestManuscript
  })

  return latestVersions
}
module.exports = { manuscripts, Manuscript, generateManuscript }
