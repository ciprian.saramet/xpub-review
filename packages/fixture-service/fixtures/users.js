const Chance = require('chance')
const identities = require('./identities')
const { assign, has } = require('lodash')

const chance = new Chance()
const users = []

function User(props) {
  return {
    id: chance.guid(),
    isActive: props.isActive === undefined ? true : props.isActive,
    isSubscribedToEmails: props.isSubscribedToEmails,
    defaultIdentity: props.defaultIdentity || 'local',
    confirmationToken: props.confirmationToken || chance.guid(),
    invitationToken: props.invitationToken || null,
    passwordResetTimestamp: props.passwordResetTimestamp || null,
    passwordResetToken: props.passwordResetToken || null,
    unsubscribeToken: props.unsubscribeToken || chance.guid(),
    agreeTc: props.agreeTc,
    identities: props.identities || [],
    teamMemberships: props.teamMemberships || [],
    async save() {
      users.push(this)
      return Promise.resolve(this)
    },
    getDefaultIdentity() {
      if (!this.identities) {
        throw new Error('Identities are required.')
      }

      return this.identities.find(
        identity => identity.type === this.defaultIdentity,
      )
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },
    removeIdentity(identityId) {
      if (!this.identities) {
        throw new Error('Identities are required.')
      }

      const identity = this.identities.find(
        identity => identity.id === identityId,
      )
      if (!identity) {
        throw new Error(`Identity does not exist.`)
      }

      if (identity.type === 'local') {
        throw new ValidationError(`The default identity cannot be removed.`)
      }
      this.identities = this.identities.filter(
        identity => identity.id !== identityId,
      )
    },
    async linkTeamMemberships(teamMember) {
      this.teamMemberships.push(teamMember)
      await this.saveRecursively()
    },
    toDTO() {
      return {
        ...this,
        identities: this.identities.map(identity => identity.toDTO()),
      }
    },
    assignIdentity(identity) {
      this.identities = this.identities || []
      this.identities.push(identity)
    },
    async saveRecursively() {
      await this.save()
      if (this.identities.length > 0) {
        await Promise.all(
          this.identities.map(async identity => identity.save()),
        )
      }
    },
    getGlobalRole() {
      if (!this.teamMemberships) {
        throw new Error('TeamMemberships are required.')
      }

      const globalMember = this.teamMemberships.find(
        member => !member.team.manuscriptId,
      )
      return globalMember ? globalMember.team.role : undefined
    },
  }
}

const generateUser = (properties, identityProperties) => {
  const user = new User(properties || {})
  const identity = identities.generateIdentity({
    userId: user.id,
    ...(identityProperties || {}),
  })
  identity.user = user

  if (has(properties, ['isConfirmed'])) {
    assign(identity, properties)
  }

  user.identities.push(identity)

  users.push(user)

  return user
}

module.exports = { users, generateUser, User }
