const Chance = require('chance')

const chance = new Chance()
const journalArticleTypes = []

function JournalArticleType(props) {
  return {
    id: chance.guid(),
    journalId: props.journalId || null,
    articleTypeId: props.articleTypeId || null,
    created: props.created || Date.now(),
    async save() {
      return Promise.resolve(this)
    },
  }
}
const generateJournalArticleType = properties => {
  const journalArticleType = new JournalArticleType(properties || {})

  journalArticleTypes.push(journalArticleType)

  return journalArticleType
}
module.exports = { JournalArticleType, generateJournalArticleType }
