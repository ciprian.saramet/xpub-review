const Chance = require('chance')

const chance = new Chance()

function ArticleType(props) {
  return {
    id: chance.guid(),
    name: props.name || '',
    created: props.created || Date.now(),
    async save() {
      return Promise.resolve(this)
    },
    toDTO() {
      return {
        ...this,
      }
    },
  }
}
const articleTypes = [
  new ArticleType({ name: 'Review Article' }),
  new ArticleType({ name: 'Research Article' }),
  new ArticleType({ name: 'Letter to the Editor' }),
]

module.exports = { ArticleType, articleTypes }
