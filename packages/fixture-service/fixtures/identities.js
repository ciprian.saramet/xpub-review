const Chance = require('chance')
const bcrypt = require('bcrypt')
const { assign } = require('lodash')

const chance = new Chance()
const identities = []

function Identity(props) {
  return {
    id: chance.guid(),
    userId: props.userId,
    type: props.type || 'local',
    isConfirmed: props.isConfirmed === undefined ? true : props.isConfirmed,
    email: props.email || chance.email(),
    passwordHash:
      '$2a$12$BrbCU5BzSWycO0ykn84hH.bFgk.lrmZQ6CgQiD9p6Yy1Y0DQy1maW', // the hash for 'password'
    surname: props.surname || chance.last(),
    givenNames: props.givenNames || chance.first(),
    aff: props.aff || chance.company(),
    country: props.country || chance.country(),
    title: props.title || 'mr',
    async validPassword(password) {
      return bcrypt.compare(password, this.passwordHash)
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },
    async save() {
      identities.push(this)
      return Promise.resolve(this)
    },
    async saveRecursively() {
      this.save()
    },
    toDTO() {
      return {
        email: this.email,
        aff: this.aff,
        name: {
          surname: this.surname,
          givenNames: this.givenNames,
          title: this.title,
        },
        identifier: this.identifier,
      }
    },
  }
}

Identity.hashPassword = password => bcrypt.hash(password, 1)

const generateIdentity = properties => {
  const identity = new Identity(properties || {})

  identities.push(identity)

  return identity
}

module.exports = { identities, generateIdentity, Identity }
