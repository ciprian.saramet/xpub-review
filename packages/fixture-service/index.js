const users = require('./fixtures/users')
const teams = require('./fixtures/teams')
const teamMembers = require('./fixtures/team-members')
const identities = require('./fixtures/identities')
const models = require('./models/models')
const services = require('./services')
const journals = require('./fixtures/journals')
const manuscripts = require('./fixtures/manuscripts')
const files = require('./fixtures/files')
const comments = require('./fixtures/comments')
const reviews = require('./fixtures/reviews')
const auditLogs = require('./fixtures/audit-logs')
const reviewerSuggestions = require('./fixtures/reviewer-suggestions')
const articleTypes = require('./fixtures/article-types')
const journalArticleTypes = require('./fixtures/journal-article-types')

module.exports = {
  models,
  services,
  fixtures: {
    ...users,
    ...teams,
    ...teamMembers,
    ...identities,
    ...journals,
    ...manuscripts,
    ...files,
    ...comments,
    ...reviews,
    ...auditLogs,
    ...reviewerSuggestions,
    ...articleTypes,
    ...journalArticleTypes,
  },
}
