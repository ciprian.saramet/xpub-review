const { orderBy } = require('lodash')
const { filter } = require('lodash')
const bcrypt = require('bcrypt')

const { User } = require('../fixtures/users')
const { Identity } = require('../fixtures/identities')
const { Team } = require('../fixtures/teams')
const { Manuscript } = require('../fixtures/manuscripts')
const { File } = require('../fixtures/files')
const { Review } = require('../fixtures/reviews')
const { Comment } = require('../fixtures/comments')
const { AuditLog } = require('../fixtures/audit-logs')
const { ReviewerSuggestion } = require('../fixtures/reviewer-suggestions')
const { TeamMember } = require('../fixtures/team-members')
const { Journal } = require('../fixtures/journals')
const { ArticleType } = require('../fixtures/article-types')
const { JournalArticleType } = require('../fixtures/journal-article-types')

const build = fixtures => {
  User.find = id => findMock(id, 'users', fixtures)
  User.findOneBy = values => findOneByMock(values, 'users', fixtures)
  User.findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('users', fixtures, orderByField, order, queryObject)

  Identity.findOneBy = values => findOneByMock(values, 'identities', fixtures)
  Identity.findOneByEmail = value => findOneByEmailMock(value, fixtures)
  Identity.hashPassword = password => bcrypt.hash(password, 1)

  Team.findOneBy = values => findOneByMock(values, 'teams', fixtures)
  Team.find = id => findMock(id, 'teams', fixtures)
  Team.findIn = (field, options) =>
    findInMock(field, options, 'teams', fixtures)

  Manuscript.find = (id, related) => findMock(id, 'manuscripts', fixtures)
  Manuscript.findBy = values => findByMock(values, 'manuscripts', fixtures)
  Manuscript.findByField = (field, value) =>
    findByFieldMock(field, value, 'manuscripts', fixtures)
  Manuscript.findManuscriptsBySubmissionId = ({
    submissionId,
    excludedStatus,
  }) =>
    findManuscriptsBySubmissionIdMock(
      'manuscripts',
      fixtures,
      submissionId,
      excludedStatus,
    )
  Manuscript.findOneBy = values =>
    findOneByMock(values, 'manuscripts', fixtures)
  Manuscript.findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('manuscripts', fixtures, orderByField, order, queryObject)
  Manuscript.all = () => allMock('manuscripts', fixtures)

  File.find = id => findMock(id, 'files', fixtures)

  AuditLog.findByField = (field, value) =>
    findByFieldMock(field, value, 'logs', fixtures)
  AuditLog.findBy = values => findByMock(values, 'logs', fixtures)
  Review.find = id => findMock(id, 'reviews', fixtures)
  Review.findOneBy = values => findOneByMock(values, 'reviews', fixtures)

  ReviewerSuggestion.findOneBy = values =>
    findOneByMock(values, 'reviewerSuggestions', fixtures)

  TeamMember.findByField = (field, value) =>
    findByFieldMock(field, value, 'teamMembers', fixtures)
  TeamMember.findBy = values => findByMock(values, 'teamMembers', fixtures)
  TeamMember.find = id => findMock(id, 'teamMembers', fixtures)
  TeamMember.findOneBy = values => findOneByMock(values, 'teams', fixtures)

  Journal.findOneByEmail = value => findOneByEmailMock(value, fixtures)
  Journal.find = id => findMock(id, 'journals', fixtures)
  Journal.findUniqueJournal = ({ email, code, name }) =>
    findUniqueJournalMock(email, code, name, fixtures.journals)
  Journal.findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('journals', fixtures, orderByField, order, queryObject)
  Journal.findOneBy = values => findOneByMock(values, 'journals', fixtures)

  ArticleType.all = () => allMock('articleTypes', fixtures)

  return {
    User,
    Identity,
    Team,
    Manuscript,
    File,
    Review,
    Comment,
    AuditLog,
    ReviewerSuggestion,
    TeamMember,
    Journal,
    ArticleType,
    JournalArticleType,
  }
}

const findMock = async (id, type, fixtures) => {
  const foundObj = Object.values(fixtures[type]).find(
    fixtureObj => fixtureObj.id === id,
  )
  if (!foundObj)
    throw new NotFoundError(`Object not found: ${type} with 'id' ${id}`)

  return Promise.resolve(foundObj)
}

const allMock = (type, fixtures) => Object.values(fixtures[type])

const findManuscriptsBySubmissionIdMock = (
  type,
  fixtures,
  submissionId,
  excludedStatus,
) => {
  const foundObjs = Object.values(fixtures[type]).filter(
    fixtureObj => fixtureObj.submissionId === submissionId,
  )
  const filteredData = foundObjs.filter(obj => obj.status !== excludedStatus)
  return filteredData
}

const findAllMock = (type, fixtures, orderByField, order, queryProps) => {
  const filteredData = filter(fixtures[type], queryProps)
  return orderBy(filteredData, orderByField, order)
}

const findOneByMock = (values, type, fixtures) => {
  delete values.eagerLoadRelations
  return filter(fixtures[type], values.queryObject)[0]
}

const findOneByEmailMock = (value, fixtures) => {
  const foundObj = Object.values(fixtures.identities).find(
    fixtureObj => fixtureObj.email.toLowerCase() === value.toLowerCase(),
  )

  return Promise.resolve(foundObj)
}

const findByFieldMock = (field, value, type, fixtures) => {
  const foundObjs = Object.values(fixtures[type]).filter(
    fixtureObj => fixtureObj[field] === value,
  )

  return Promise.resolve(foundObjs)
}

const findByMock = (values, type, fixtures) => {
  const foundObjs = filter(Object.values(fixtures[type]), values)
  return Promise.resolve(foundObjs)
}

const findInMock = (field, options, type, fixtures) => {
  const foundObjs = Object.values(fixtures[type]).filter(fixtureObj =>
    options.includes(fixtureObj[field]),
  )

  return Promise.resolve(foundObjs)
}

const findUniqueJournalMock = (email, code, name, journals) => {
  const foundJournal = Object.values(journals).find(
    journal =>
      journal.email.toLowerCase() === email.toLowerCase() ||
      journal.code.toLowerCase() === code.toLowerCase() ||
      journal.name.toLowerCase() === name.toLowerCase(),
  )

  return Promise.resolve(foundJournal)
}

module.exports = { build }
