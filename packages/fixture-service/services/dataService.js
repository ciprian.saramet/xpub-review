const Chance = require('chance')

const chance = new Chance()

module.exports = {
  createUserOnManuscript({ manuscript, fixtures, input = {}, role }) {
    let team = fixtures.getTeamByRoleAndManuscriptId({
      id: manuscript.id,
      role,
    })
    if (!team) {
      team = fixtures.generateTeam({
        role,
        manuscriptId: manuscript.id,
      })
      manuscript.teams.push(team)
    }

    const user = fixtures.generateUser({})

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
      ...input,
    })

    setRelationsToManuscript({ teamMember, user, team, manuscript })

    return teamMember
  },
  createUserOnJournal({ journal, fixtures, input = {}, role }) {
    let team = fixtures.getTeamByRoleAndJournalId({ id: journal.id, role })
    if (!team) {
      team = fixtures.generateTeam({
        role,
        journalId: journal.id,
      })
      journal.teams.push(team)
    }

    const user = fixtures.generateUser({})

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
      ...input,
    })

    setRelationsToJournal({ teamMember, user, journal, team })

    return teamMember
  },
  addUserOnManuscript({ manuscript, fixtures, input = {}, role, user }) {
    let team = fixtures.getTeamByRoleAndManuscriptId({
      id: manuscript.id,
      role,
    })
    if (!team) {
      team = fixtures.generateTeam({
        role,
        manuscriptId: manuscript.id,
      })
      manuscript.teams.push(team)
    }

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
      ...input,
    })

    setRelationsToManuscript({ teamMember, user, team, manuscript })

    return teamMember
  },
  async createReviewOnManuscript({
    manuscript,
    fixtures,
    recommendation,
    teamMember,
  }) {
    const review = fixtures.generateReview({
      manuscriptId: manuscript.id,
      recommendation,
      teamMemberId: teamMember.id,
    })
    review.manuscript = manuscript
    review.member = teamMember
    manuscript.reviews.push(review)

    const comment = fixtures.generateComment({
      reviewId: review.id,
      content: chance.sentence(),
      type: 'public',
    })
    comment.review = review
    review.comments.push(comment)

    return review
  },
  createGlobalUser({ fixtures, input = {}, role }) {
    let team = fixtures.getGlobalTeam()
    if (!team) {
      team = fixtures.generateTeam({
        role,
      })
    }

    const user = fixtures.generateUser({})

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
      ...input,
    })

    createTeamMemberRelations({ teamMember, user, team })
    return teamMember
  },
}

const setRelationsToManuscript = ({ user, team, teamMember, manuscript }) => {
  createTeamMemberRelations({ teamMember, user, team })
  team.manuscript = manuscript
}

const setRelationsToJournal = ({ user, team, teamMember, journal }) => {
  createTeamMemberRelations({ teamMember, user, team })
  team.journal = journal
}

const createTeamMemberRelations = ({ teamMember, user, team }) => {
  teamMember.user = user
  teamMember.team = team
  teamMember.linkUser(user)
  team.members.push(teamMember)
  user.teamMemberships.push(teamMember)
}
