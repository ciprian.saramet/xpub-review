const { HindawiBaseModel } = require('component-model')
const {
  jobs: { connectToJobQueue },
} = require('pubsweet-server/src')
const logger = require('@pubsweet/logger')
const uuidv4 = require('uuid/v4')

class Job extends HindawiBaseModel {
  static get tableName() {
    return 'job'
  }

  static get schema() {
    return {
      properties: {
        teamMemberId: { type: 'string', format: 'uuid' },
        manuscriptId: { type: ['string', 'null'], format: 'uuid' },
        journalId: { type: ['string', 'null'], format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-team-member').model,
        join: {
          from: 'job.teamMemberId',
          to: 'team_member.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'job.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-journal').model,
        join: {
          from: 'job.journalId',
          to: 'journal.id',
        },
      },
    }
  }

  static async schedule({
    params,
    journalId,
    jobHandler,
    teamMemberId,
    executionDate,
    manuscriptId,
  }) {
    const jobQueue = await connectToJobQueue()

    params = JSON.parse(JSON.stringify(params))
    const queueName = uuidv4()

    try {
      const jobId = await jobQueue.publishAfter(
        queueName,
        params,
        {},
        executionDate,
      )

      const job = new Job({ id: jobId, teamMemberId, manuscriptId, journalId })
      await job.save()

      await jobQueue.subscribe(
        queueName,
        { teamSize: 5, teamConcurrency: 5 },
        jobHandler,
      )
      logger.info(`Successfully scheduled job ${jobId} at: ${executionDate}`)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async cancel(id) {
    const jobQueue = await connectToJobQueue()

    try {
      await jobQueue.cancel(id)
      logger.info(`Successfully cancelled job ${id}`)
    } catch (e) {
      throw new Error(e)
    }
  }
}

module.exports = Job
