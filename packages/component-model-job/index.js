const Job = require('./src/entities/job')

module.exports = {
  model: Job,
  modelName: 'Job',
}
