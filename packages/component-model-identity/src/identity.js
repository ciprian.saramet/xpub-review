const { HindawiBaseModel } = require('component-model')
const bcrypt = require('bcrypt')
const config = require('config')

const BCRYPT_COST = config.util.getEnv('NODE_ENV') === 'test' ? 1 : 12

class Identity extends HindawiBaseModel {
  static get tableName() {
    return 'identity'
  }

  static get schema() {
    return {
      properties: {
        type: { type: 'string' },
        isConfirmed: { type: 'boolean' },
        passwordHash: { type: ['string', 'null'] },
        email: { type: ['string', 'null'] },
        aff: { type: ['string', 'null'] },
        country: { type: ['string', 'null'] },
        surname: { type: ['string', 'null'] },
        givenNames: { type: ['string', 'null'] },
        title: {
          type: ['string', 'null'],
          enum: ['mr', 'mrs', 'miss', 'ms', 'dr', 'prof', null],
        },
        identifier: { type: ['string', 'null'] },
        userId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-user').model,
        join: {
          from: 'identity.userId',
          to: 'user.id',
        },
      },
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const { email } = json
    if (email) return { ...json, email: email.toLowerCase() }
    return { ...json }
  }

  async validPassword(password) {
    if (!this.passwordHash) return
    return bcrypt.compare(password, this.passwordHash)
  }

  static async hashPassword(password) {
    return bcrypt.hash(password, BCRYPT_COST)
  }

  toDTO() {
    return {
      email: this.email,
      aff: this.aff,
      isConfirmed: this.isConfirmed,
      country: this.country,
      name: {
        surname: this.surname,
        givenNames: this.givenNames,
        title: this.title,
      },
      identifier: this.identifier,
    }
  }
}

module.exports = Identity
