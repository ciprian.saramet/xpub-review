const uuid = require('uuid')
const { chain, get, map, pick, sortBy, groupBy, last } = require('lodash')
const { HindawiBaseModel } = require('component-model')
const { model: Team } = require('component-model-team')
const { model: TeamMember } = require('component-model-team-member')
const { model: Review } = require('component-model-review')

class Manuscript extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript'
  }

  constructor(properties) {
    super(properties)
    if (!this.id) {
      this.submissionId = uuid.v4()
      this.status = Manuscript.Statuses.draft
      this.version = 1
      this.publicationDates = []
    }
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        submissionId: { type: 'string', format: 'uuid' },
        ended: { type: ['string', 'null'], format: 'date-time' },
        status: { enum: Object.values(Manuscript.Statuses) },
        formState: { type: ['string', 'null'] },
        decision: { type: ['string', 'null'] },
        customId: { type: ['string', 'null'] },
        version: { type: 'number' },
        title: { type: 'string', default: '' },
        articleType: { type: 'string', default: '' },
        articleIds: { type: ['array', 'null'] }, // not used
        abstract: { type: 'string', default: '' },
        subjects: { type: ['array', 'null'] },
        history: { type: ['array', 'null'] },
        publicationDates: { type: ['array', 'null'] },
        notes: { type: ['array', 'null'] },
        technicalCheckToken: { type: ['string', 'null'], format: 'uuid' },
        hasPassedEqa: { type: ['boolean', 'null'] },
        hasPassedEqs: { type: ['boolean', 'null'] },
        journalId: { type: ['string', null], format: 'uuid' },
        conflicts: {
          type: 'object',
          default: {},
          properties: {
            hasConflicts: { type: 'boolean', default: false },
            message: { type: 'string', default: '' },
            hasDataAvailability: { type: 'boolean', default: true },
            dataAvailabilityMessage: { type: 'string', default: '' },
            hasFunding: { type: 'boolean', default: true },
            fundingMessage: { type: 'string', default: '' },
          },
        },
        agreeTc: { type: 'boolean', default: false },
      },
    }
  }

  static get relationMappings() {
    return {
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-file').model,
        join: {
          from: 'manuscript.id',
          to: 'file.manuscriptId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-team').model,
        join: {
          from: 'manuscript.id',
          to: 'team.manuscriptId',
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-review').model,
        join: {
          from: 'manuscript.id',
          to: 'review.manuscriptId',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-journal').model,
        join: {
          from: 'manuscript.journalId',
          to: 'journal.id',
        },
      },
      logs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-audit-log').model,
        join: {
          from: 'manuscript.id',
          to: 'audit_log.manuscriptId',
        },
      },
      reviewerSuggestions: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-reviewer-suggestion').model,
        join: {
          from: 'manuscript.id',
          to: 'reviewer_suggestion.manuscriptId',
        },
      },
    }
  }

  static get Statuses() {
    return {
      draft: 'draft',
      technicalChecks: 'technicalChecks',
      submitted: 'submitted',
      heInvited: 'heInvited',
      heAssigned: 'heAssigned',
      reviewersInvited: 'reviewersInvited',
      underReview: 'underReview',
      reviewCompleted: 'reviewCompleted',
      revisionRequested: 'revisionRequested',
      pendingApproval: 'pendingApproval',
      rejected: 'rejected',
      inQA: 'inQA',
      accepted: 'accepted',
      withdrawalRequested: 'withdrawalRequested',
      withdrawn: 'withdrawn',
      deleted: 'deleted',
      published: 'published',
      olderVersion: 'olderVersion',
    }
  }

  static filterOlderVersions(manuscripts) {
    const submissions = groupBy(manuscripts, 'submissionId')
    const latestVersions = Object.values(submissions).map(versions => {
      if (versions.length === 1) {
        return versions[0]
      }

      const sortedVersions = sortBy(versions, 'version')
      const latestManuscript = last(sortedVersions)

      if (latestManuscript.status === this.Statuses.draft) {
        return sortedVersions[sortedVersions.length - 2]
      }

      return latestManuscript
    })

    return latestVersions
  }

  getSubmittingAuthor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam) {
      throw new Error('Could not find author team')
    }

    if (authorTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return authorTeam.members.find(tm => tm.isSubmitting)
  }

  getAuthors() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam) {
      throw new Error('Could not find author team')
    }

    if (authorTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return authorTeam.members
  }

  getReviewers() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const reviewerTeam = this.teams.find(t => t.role === Team.Role.reviewer)
    if (!reviewerTeam) {
      return []
    }

    if (reviewerTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return reviewerTeam.members
  }

  addReviewers(reviewers) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    let reviewerTeam = this.teams.find(t => t.role === Team.Role.reviewer)
    if (reviewerTeam && !reviewerTeam.members) {
      throw new Error('Members are required.')
    }

    if (!reviewerTeam) {
      reviewerTeam = new Team({
        manuscriptId: this.id,
        role: Team.Role.reviewer,
      })

      this.teams.push(reviewerTeam)
    }

    reviewers.forEach(reviewer => {
      if (!reviewer.user) {
        throw new Error('User is required.')
      }
      reviewerTeam.addMember(reviewer.user, {
        status: TeamMember.Statuses.pending,
        reviewerNumber: reviewer.reviewerNumber,
        alias: reviewer.alias,
      })
    })
  }

  hasHandlingEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    return !!chain(this.teams)
      .find(t => t.role === Team.Role.handlingEditor)
      .get('members')
      .find(member => member.status === 'accepted')
      .value()
  }

  getHandlingEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const handlingEditor = chain(this.teams)
      .find(t => t.role === Team.Role.handlingEditor)
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return handlingEditor
  }

  getHandlingEditorByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const handlingEditor = chain(this.teams)
      .find(t => t.role === Team.Role.handlingEditor)
      .get('members')
      .find(member => member.status === status)
      .value()

    return handlingEditor
  }

  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  assignFile(file) {
    this.files = this.files || []
    this.files.push(file)
  }

  assignReview(review) {
    this.reviews = this.reviews || []
    this.reviews.push(review)
  }

  submitManuscript() {
    this.status = Manuscript.Statuses.technicalChecks
    this.technicalCheckToken = uuid.v4()
    this.publicationDates.push({
      type: Manuscript.Statuses.technicalChecks,
      date: Date.now(),
    })
  }

  updateStatus(status) {
    this.status = status
  }

  setComment() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const responseToRevisionRequest = this.reviews.find(
      review =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )
    if (!responseToRevisionRequest) {
      throw new ValidationError('There has been no request to revision')
    }

    this.comment = responseToRevisionRequest.comments[0].toDTO()
  }

  getLatestEditorReview() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const editorReviews = this.reviews.filter(review =>
      [
        Team.Role.admin,
        Team.Role.editorInChief,
        Team.Role.handlingEditor,
      ].includes(review.member.team.role),
    )

    return last(sortBy(editorReviews, 'updated'))
  }

  getLatestHERecommendation() {
    const editorReviews = this.reviews.filter(
      review => get(review, 'member.team.role') === Team.Role.handlingEditor,
    )
    return last(sortBy(editorReviews, 'updated'))
  }

  getReviewersByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    const reviewerTeam = this.teams.find(t => t.role === Team.Role.reviewer)
    if (!reviewerTeam.members.length === 0) {
      throw new Error('Members are required.')
    }
    return reviewerTeam.members.filter(m => m.status === status)
  }

  toDTO() {
    const membersPerTeam = get(this, 'teams', []).reduce((acc, team) => {
      acc[team.role] = map(sortBy(team.members, 'position'), member =>
        member.toDTO(),
      )
      return acc
    }, {})

    const manuscript = pick(this, [
      'id',
      'submissionId',
      'created',
      'updated',
      'ended',
      'status',
      'formState',
      'decision',
      'version',
      'hasPassedEQS',
      'hasPassedEQA',
      'technicalCheckToken',
      'teams',
      'files',
      'reviews',
      'role',
      'journalId',
      'journal',
      'editorInChief',
      'comment',
    ])

    manuscript.customId =
      manuscript.role === 'author' &&
      ['draft', 'technicalChecks'].includes(manuscript.status)
        ? undefined
        : this.customId

    const meta = pick(this, [
      'title',
      'articleType',
      'articleIds',
      'abstract',
      'subjects',
      'history',
      'publicationDates',
      'notes',
      'agreeTc',
      'conflicts',
    ])

    return {
      ...manuscript,
      meta,
      authors: membersPerTeam.author,
      reviewers: membersPerTeam.reviewer,
      reviews: this.reviews ? this.reviews.map(review => review.toDTO()) : [],
      // get the first HE that didn't decline
      // he either needs to accept the invitation or already accepted
      handlingEditor: chain(membersPerTeam)
        .get('handlingEditor', [])
        .find(
          he =>
            he.status !== TeamMember.Statuses.declined &&
            he.status !== TeamMember.Statuses.removed &&
            he.status !== TeamMember.Statuses.expired,
        )
        .value(),
      files: chain(this.files)
        .map(f => ({ ...f, filename: f.fileName }))
        .sortBy('type', 'position')
        .value(),
      teams: this.teams ? this.teams.map(team => team.toDTO()) : [],
      editorInChief: this.editorInChief
        ? this.editorInChief.toDTO()
        : undefined,
    }
  }

  static async findManuscriptsBySubmissionId({
    submissionId,
    excludedStatus,
    eagerLoadRelations,
    orderByField,
    order,
  }) {
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderBy(orderByField, order)
      .eager(Manuscript._parseEagerRelations(eagerLoadRelations))
  }
}

module.exports = Manuscript
