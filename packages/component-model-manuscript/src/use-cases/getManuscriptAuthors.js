const { sortBy, isEmpty } = require('lodash')

const initialize = ({ Team }) => {
  const filterStrategies = {
    reviewer(authors) {
      authors.forEach(author => {
        delete author.alias.email
      })

      return authors
    },
  }

  function parseByRole(object, role, filterOptions) {
    return filterStrategies[role]
      ? filterStrategies[role](object, filterOptions)
      : object
  }

  return {
    async execute({ manuscript }) {
      let authorsDTOs = manuscript.authors

      if (isEmpty(authorsDTOs)) {
        const team = await Team.findOneBy({
          queryObject: { role: Team.Role.author, manuscriptId: manuscript.id },
          eagerLoadRelations: ['members'],
        })

        if (!team) return

        const teamMembers = sortBy(team.members, 'position')

        authorsDTOs = teamMembers.map(tm => tm.toDTO())
      }

      return parseByRole(authorsDTOs, manuscript.role)
    },
  }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
