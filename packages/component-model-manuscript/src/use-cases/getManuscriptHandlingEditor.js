const initialize = ({ Team, TeamMember, Manuscript }) => {
  const filterStrategies = {
    author(handlingEditor) {
      if (TeamMember.Statuses.declined === handlingEditor.status) {
        return
      }

      return handlingEditor
    },
  }

  function parseByRole(object, role, filterOptions) {
    return filterStrategies[role]
      ? filterStrategies[role](object, filterOptions)
      : object
  }

  return {
    async execute({ manuscript }) {
      let handlingEditorDTO = manuscript.handlingEditor

      if (
        [
          Manuscript.Statuses.draft,
          Manuscript.Statuses.submitted,
          Manuscript.Statuses.technicalChecks,
        ].includes(manuscript.status)
      )
        return

      if (!handlingEditorDTO) {
        const team = await Team.findOneBy({
          queryObject: {
            role: Team.Role.handlingEditor,
            manuscriptId: manuscript.id,
          },
          eagerLoadRelations: ['members'],
        })

        if (!team) return

        const handlingEditorTeamMember = team.members.find(
          m =>
            m.status !== TeamMember.Statuses.removed &&
            m.status !== TeamMember.Statuses.declined &&
            m.status !== TeamMember.Statuses.expired,
        )
        if (!handlingEditorTeamMember) return
        handlingEditorDTO = handlingEditorTeamMember.toDTO()
      }

      return parseByRole(handlingEditorDTO, manuscript.role)
    },
  }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
