const { sortBy, isEmpty } = require('lodash')

const initialize = ({ Team, Manuscript }) => {
  const filterStrategies = {
    author(reviewerTeamMembers, { manuscriptStatus }) {
      if (
        ![
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.pendingApproval,
          Manuscript.Statuses.revisionRequested,
          Manuscript.Statuses.inQA,
          Manuscript.Statuses.olderVersion,
        ].includes(manuscriptStatus)
      ) {
        return []
      }

      reviewerTeamMembers.forEach(reviewer => {
        delete reviewer.alias.name
        delete reviewer.alias.email
      })

      return reviewerTeamMembers
    },
    reviewer(reviewerTeamMembers, { manuscriptStatus, userId }) {
      if (
        [
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.olderVersion,
        ].includes(manuscriptStatus)
      ) {
        return reviewerTeamMembers
      }

      return reviewerTeamMembers.filter(r => r.userId === userId)
    },
  }

  function parseByRole(object, role, filterOptions) {
    return filterStrategies[role]
      ? filterStrategies[role](object, filterOptions)
      : object
  }

  return {
    async execute({ manuscript, userId }) {
      let reviewersDTOs = manuscript.reviewers

      if (
        [
          Manuscript.Statuses.draft,
          Manuscript.Statuses.submitted,
          Manuscript.Statuses.technicalChecks,
          Manuscript.Statuses.heInvited,
          Manuscript.Statuses.heAssigned,
        ].includes(manuscript.status)
      )
        return []

      if (isEmpty(reviewersDTOs)) {
        const team = await Team.findOneBy({
          queryObject: {
            role: Team.Role.reviewer,
            manuscriptId: manuscript.id,
          },
          eagerLoadRelations: ['members'],
        })

        if (!team) return []

        const teamMembers = sortBy(team.members, 'position')

        reviewersDTOs = teamMembers.map(tm => tm.toDTO())
      }

      return parseByRole(reviewersDTOs, manuscript.role, {
        manuscriptStatus: manuscript.status,
        userId,
      })
    },
  }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
