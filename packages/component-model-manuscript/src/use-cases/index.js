const getManuscriptTeamsUseCase = require('./getManuscriptTeams')
const getManuscriptUseCase = require('./getManuscript')
const getManuscriptAuthorsUseCase = require('./getManuscriptAuthors')
const getManuscriptReviewsUseCase = require('./getManuscriptReviews')
const getManuscriptVersionsUseCase = require('./getManuscriptVersions')
const getDraftRevisionUseCase = require('./getDraftRevision')
const getManuscriptHandlingEditorUseCase = require('./getManuscriptHandlingEditor')
const getManuscriptReviewersUseCase = require('./getManuscriptReviewers')
const getManuscriptStatusUseCase = require('./getManuscriptStatus')

module.exports = {
  getManuscriptUseCase,
  getManuscriptAuthorsUseCase,
  getManuscriptReviewsUseCase,
  getManuscriptTeamsUseCase,
  getManuscriptVersionsUseCase,
  getDraftRevisionUseCase,
  getManuscriptHandlingEditorUseCase,
  getManuscriptReviewersUseCase,
  getManuscriptStatusUseCase,
}
