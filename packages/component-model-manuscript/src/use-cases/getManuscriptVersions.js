const config = require('config')
const { get, chain, orderBy } = require('lodash')

const GLOBAL_ROLES = config.get('globalRoles')

const initialize = ({ Journal, Manuscript, User, Team, TeamMember }) => ({
  execute: async ({ submissionId, userId }) => {
    const shouldFilterManuscripts = manuscript => {
      const excludedStatuses = [
        TeamMember.Statuses.declined,
        TeamMember.Statuses.expired,
        TeamMember.Statuses.removed,
      ]
      if (GLOBAL_ROLES.includes(manuscript.role)) return true
      return !excludedStatuses.includes(
        manuscript.teams
          .find(t => t.role === manuscript.role)
          .members.find(member => member.userId === userId).status,
      )
    }
    const shouldRemoveFiles = (requestingUserRole, requestingUserStatus) => {
      const excludedRoles = [Team.Role.handlingEditor, Team.Role.reviewer]
      return (
        excludedRoles.includes(requestingUserRole) &&
        TeamMember.Statuses.pending === requestingUserStatus
      )
    }

    let manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      excludedStatus: Manuscript.Statuses.draft,
      eagerLoadRelations: [
        'files',
        'teams.[members.[team,user.[identities]]]',
        'journal',
      ],
    })
    manuscripts = orderBy(manuscripts, 'version', 'desc')
    const requestingUser = chain(manuscripts)
      .flatMap(manuscript => manuscript.teams)
      .flatMap(team => team.members)
      .find(member => member.userId === userId)
      .value()

    if (
      get(requestingUser, 'team.role') === Team.Role.handlingEditor &&
      get(requestingUser, 'status') === TeamMember.Statuses.removed
    ) {
      throw new AuthorizationError('Operation not permitted!')
    }

    const manuscriptIds = manuscripts.map(m => m.id)

    const user = await User.find(userId, 'teamMemberships.[team]')
    const globalRole = user.getGlobalRole()

    const userRoleOnManuscripts = user.teamMemberships
      .filter(({ team }) => manuscriptIds.includes(team.manuscriptId))
      .reduce(
        (acc, tm) => ({ ...acc, [tm.team.manuscriptId]: tm.team.role }),
        {},
      )

    const journal = await Journal.find(
      manuscripts[0].journalId,
      'teams.members',
    )
    const editorInChief = journal.getEditorInChief()

    manuscripts.forEach(m => {
      m.editorInChief = editorInChief

      m.role = GLOBAL_ROLES.includes(globalRole)
        ? globalRole
        : userRoleOnManuscripts[m.id]
    })

    const lastManuscript = chain(manuscripts)
      .orderBy('version', 'asc')
      .last()
      .value()
    if (shouldRemoveFiles(lastManuscript.role, get(requestingUser, 'status'))) {
      lastManuscript.files = []
    }

    const visibleManuscripts = manuscripts.filter(
      m => m.role && shouldFilterManuscripts(m),
    )

    return visibleManuscripts.map(m => m.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
