const { flatten, map, isEmpty } = require('lodash')

module.exports.initialize = ({ models, useCases }) => {
  const { Team } = models

  const useCasesByRole = {
    [Team.Role.author]: useCases.getManuscriptAuthorsUseCase.initialize(models),
    [Team.Role
      .handlingEditor]: useCases.getManuscriptHandlingEditorUseCase.initialize(
      models,
    ),
    [Team.Role.reviewer]: useCases.getManuscriptReviewersUseCase.initialize(
      models,
    ),
  }

  return {
    async execute({ manuscript, userId }) {
      let teamsDTOs = manuscript.teams

      if (isEmpty(teamsDTOs)) {
        const teams = Team.findByField('manuscriptId', manuscript.id, 'members')

        teamsDTOs = teams.map(t => t.toDTO())
      }

      await Promise.all(
        map(teamsDTOs, async team => {
          if (useCasesByRole[team.role])
            team.members = flatten([
              await useCasesByRole[team.role].execute({ manuscript, userId }),
            ])
        }),
      )

      return teamsDTOs
    },
  }
}
