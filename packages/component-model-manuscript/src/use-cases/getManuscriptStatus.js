const { chain } = require('lodash')

module.exports.initialize = ({ models: { Manuscript, Team, TeamMember } }) => {
  const reviewerStatuses = [
    Manuscript.Statuses.underReview,
    Manuscript.Statuses.reviewCompleted,
    Manuscript.Statuses.reviewersInvited,
  ]
  return {
    async execute({ manuscript, userId }) {
      if (
        reviewerStatuses.includes(manuscript.status) &&
        manuscript.role === Team.Role.reviewer
      ) {
        const reviewerInvitationStatus = chain(manuscript)
          .get('reviewers', [])
          .find(rev => rev.userId === userId)
          .get('status')
          .value()

        switch (reviewerInvitationStatus) {
          case TeamMember.Statuses.pending:
            return Manuscript.Statuses.reviewersInvited
          case TeamMember.Statuses.accepted:
            return Manuscript.Statuses.underReview
          case TeamMember.Statuses.submitted:
            return Manuscript.Statuses.reviewCompleted
          default:
            return manuscript.status
        }
      }
      return manuscript.status
    },
  }
}
