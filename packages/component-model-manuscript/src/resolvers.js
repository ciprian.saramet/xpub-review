const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')

const resolvers = {
  Query: {
    async getManuscript(_, { manuscriptId }, ctx) {
      return useCases.getManuscriptUseCase
        .initialize({ models })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async getManuscriptVersions(_, { submissionId }, ctx) {
      return useCases.getManuscriptVersionsUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
    async getDraftRevision(_, { submissionId }, ctx) {
      return useCases.getDraftRevisionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
  },
  Manuscript: {
    async status(manuscript, query, ctx) {
      return useCases.getManuscriptStatusUseCase
        .initialize({ models })
        .execute({ manuscript, userId: ctx.user })
    },
    async teams(manuscript, query, ctx) {
      return useCases.getManuscriptTeamsUseCase
        .initialize({ models, useCases })
        .execute({ manuscript })
    },
    async authors(manuscript, query, ctx) {
      return useCases.getManuscriptAuthorsUseCase
        .initialize(models)
        .execute({ manuscript })
    },
    async reviews(manuscript, query, ctx) {
      return useCases.getManuscriptReviewsUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
    async reviewers(manuscript, query, ctx) {
      return useCases.getManuscriptReviewersUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
    async handlingEditor(manuscript, query, ctx) {
      return useCases.getManuscriptHandlingEditorUseCase
        .initialize(models)
        .execute({ manuscript })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
