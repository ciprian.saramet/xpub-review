process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const chance = new Chance()

const Manuscript = require('../src/entities/manuscript')

jest.mock('component-model', () => ({
  HindawiBaseModel: jest.fn(),
}))

describe('Manuscript model', () => {
  describe('toDTO', () => {
    it('returns correct handling editor on manuscript when multiple exist', async () => {
      const { Team, TeamMember } = models.build(fixtures)

      const currentHeId = chance.guid()
      const manuscript = new Manuscript({
        submissionId: chance.guid(),
        id: chance.guid(),
        status: Manuscript.Statuses.reviewersInvited,
        version: 1,
      })

      const currentHeStatus = chance.pickone([
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
      ])
      manuscript.teams = [
        {
          id: chance.guid(),
          role: Team.Role.handlingEditor,
          toDTO: function toDTO() {
            return this
          },
          members: [
            {
              id: chance.guid(),
              status: TeamMember.Statuses.expired,
              toDTO: function toDTO() {
                return this
              },
              position: 0,
            },
            {
              id: currentHeId,
              status: currentHeStatus,
              toDTO: function toDTO() {
                return this
              },
              position: 1,
            },
          ],
        },
      ]

      const { handlingEditor } = manuscript.toDTO()

      expect(handlingEditor).toBeDefined()
      expect(currentHeId).toEqual(handlingEditor.id)
      expect(currentHeStatus).toEqual(handlingEditor.status)
    })
  })
})
