process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const chance = new Chance()

const { getManuscriptHandlingEditorUseCase } = require('../src/use-cases')

describe('Get Manuscript Handling Editor Use Case', () => {
  it('return current handling editor when multiple editors exist', async () => {
    const mockedModels = models.build(fixtures)
    const { Team, Manuscript, TeamMember } = mockedModels

    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorInChief,
    })

    const manuscript = fixtures.generateManuscript({
      version: 1,
      submissionId: chance.guid(),
      status: Manuscript.Statuses.reviewersInvited,
      journal: journal.id,
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
        isCorresponding: false,
      },
      role: Team.Role.author,
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: TeamMember.Statuses.expired,
      },
      role: Team.Role.handlingEditor,
    })

    const currentHe = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: chance.pickone([
          TeamMember.Statuses.pending,
          TeamMember.Statuses.accepted,
        ]),
      },
      role: Team.Role.handlingEditor,
    })

    const handlingEditor = await getManuscriptHandlingEditorUseCase
      .initialize(mockedModels)
      .execute({ manuscript })

    expect(handlingEditor).toBeDefined()
    expect(handlingEditor.team.manuscriptId).toEqual(manuscript.id)
    expect(currentHe.id).toEqual(handlingEditor.id)
  })
})
