process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const chance = new Chance()

const { getDraftRevisionUseCase } = require('../src/use-cases')

describe('Get Draft Version Use Case', () => {
  it('returns a draft version if present', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      version: 1,
      submissionId,
      status: 'revisionRequested',
      journal: journal.id,
    })
    const secondVersion = fixtures.generateManuscript({
      version: 2,
      submissionId,
      status: 'draft',
      journal: journal.id,
    })

    const authorMember = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })

    const review = await dataService.createReviewOnManuscript({
      manuscript: secondVersion,
      recommendation: 'responseToRevision',
      fixtures,
      teamMember: authorMember,
    })

    const author = fixtures.users.find(user => user.id === authorMember.userId)

    await dataService.addUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      user: author,
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    const draftRevision = await getDraftRevisionUseCase
      .initialize(mockedModels)
      .execute({ submissionId, userId: authorMember.userId })

    expect(draftRevision.status).toEqual('draft')
    expect(draftRevision.version).toEqual(2)
    expect(draftRevision.submissionId).toEqual(submissionId)
    expect(draftRevision.comment.reviewId).toEqual(review.id)
  })
})
