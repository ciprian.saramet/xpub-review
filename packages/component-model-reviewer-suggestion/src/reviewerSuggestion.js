const { HindawiBaseModel } = require('component-model')

class ReviewerSuggestion extends HindawiBaseModel {
  static get tableName() {
    return 'reviewer_suggestion'
  }

  static get schema() {
    return {
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        givenNames: { type: 'string' },
        surname: { type: 'string' },
        numberOfReviews: { type: 'integer' },
        aff: { type: 'string' },
        email: { type: 'string' },
        profileUrl: { type: 'string' },
        isInvited: { type: 'boolean', default: false },
        type: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'reviewer_suggestion.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const { email } = json
    return { ...json, email: email.toLowerCase() }
  }

  toDTO() {
    return { ...this }
  }
}

module.exports = ReviewerSuggestion
