const Team = require('./src/team')

module.exports = {
  model: Team,
  modelName: 'Team',
}
