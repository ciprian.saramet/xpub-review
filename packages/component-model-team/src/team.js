const { HindawiBaseModel } = require('component-model')
const { model: TeamMember } = require('component-model-team-member')

class Team extends HindawiBaseModel {
  static get tableName() {
    return 'team'
  }

  static get schema() {
    return {
      properties: {
        role: {
          type: 'string',
          enum: Object.values(Team.Role),
          default: null,
        },
        manuscriptId: { type: ['string', 'null'] },
        journalId: { type: ['string', 'null'] },
      },
    }
  }

  /*
    to be moved later on in a repository
  */
  static async createOrFind({ queryObject, eagerLoadRelations, options }) {
    let team = await this.findOneBy({
      queryObject,
      eagerLoadRelations,
    })

    if (!team) {
      team = new Team(options)
    }

    return team
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'team.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-journal').model,
        join: {
          from: 'team.journalId',
          to: 'journal.id',
        },
      },
      members: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-team-member').model,
        join: {
          from: 'team.id',
          to: 'team_member.teamId',
        },
      },
    }
  }

  static get Role() {
    return {
      author: 'author',
      admin: 'admin',
      editorInChief: 'editorInChief',
      reviewer: 'reviewer',
      handlingEditor: 'handlingEditor',
      editorialAssistant: 'editorialAssistant',
    }
  }

  static get GlobalRoles() {
    return [
      Team.Role.admin,
      Team.Role.editorInChief,
      Team.Role.handlingEditor,
      Team.Role.editorialAssistant,
    ]
  }

  addMember(user, invitationOptions) {
    this.members = this.members || []
    const existingMember = this.members.find(
      member => member.userId === user.id,
    )
    if (existingMember) {
      const defaultIdentity = user.getDefaultIdentity()
      if (existingMember.status === 'removed') {
        throw new Error(
          `${this.role} invitation for ${
            defaultIdentity.email
          } was removed and can't be invited again`,
        )
      }
      throw new ValidationError(
        `User ${defaultIdentity.email} is already invited as ${this.role}`,
      )
    }

    const newMember = new TeamMember({
      ...invitationOptions,
      position: this.members.length,
    })
    newMember.linkUser(user)

    this.members.push(newMember)

    return newMember
  }

  toDTO() {
    const object = this.manuscript || this.journal

    return {
      ...this,
      object: object ? object.toDTO() : undefined,
      objectType: this.manuscriptId ? 'manuscript' : 'journal',
      members: this.members ? this.members.map(m => m.toDTO()) : undefined,
    }
  }

  removeMember(memberId) {
    this.members = this.members || []

    const member = this.members.find(member => member.id === memberId)
    if (!member) {
      throw new NotFoundError(
        `The specified user is not invited as a ${this.role}`,
      )
    }
    if (member.isSubmitting) {
      throw new ValidationError(
        `Submitting authors can't be deleted from the team`,
      )
    }

    this.members = this.members.filter(member => member.id !== memberId)
  }
}

module.exports = Team
