const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const { token: tokenService } = require('pubsweet-server/src/authentication')

const useCases = require('./use-cases')

const resolvers = {
  Mutation: {
    async updateUser(_, { input }, ctx) {
      return useCases.updateUserUseCase
        .initialize(models)
        .execute({ userId: ctx.user, input })
    },
    async subscribeToEmails(_, { input }, ctx) {
      return useCases.subscribeToEmailsUseCase
        .initialize(models)
        .execute({ userId: ctx.user })
    },
    async unsubscribeToEmails(_, { input }, ctx) {
      return useCases.unsubscribeToEmailsUseCase
        .initialize(models)
        .execute({ userId: ctx.user, input })
    },
    async changePassword(_, { input }, ctx) {
      return useCases.changePasswordUseCase
        .initialize(tokenService, models)
        .execute({ input, userId: ctx.user })
    },
    async unlinkOrcid(_, { input }, ctx) {
      return useCases.unlinkOrcidUseCase
        .initialize(models)
        .execute({ userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
