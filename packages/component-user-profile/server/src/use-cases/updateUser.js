const initialize = ({ User }) => ({
  execute: async ({ userId, input }) => {
    const user = await User.find(userId, 'identities')
    const localIdentity = user.getDefaultIdentity()
    localIdentity.updateProperties(input)
    await localIdentity.save()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
