const initialize = ({ User }) => ({
  execute: async ({ userId, input }) => {
    const user = await User.find(userId)
    if (!user.isSubscribedToEmails) return
    if (user.unsubscribeToken !== input.token) {
      throw new Error(`Invalid token ${input.token}.`)
    }

    user.updateProperties({ isSubscribedToEmails: false })
    await user.save()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
