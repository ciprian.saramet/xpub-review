const initialize = ({ User }) => ({
  execute: async ({ userId }) => {
    const user = await User.find(userId, 'identities')
    const orcidIdentity = user.identities.find(i => i.type === 'orcid')
    if (!orcidIdentity) {
      throw new Error('There is no Orcid account linked.')
    }
    user.removeIdentity(orcidIdentity.id)
    await user.saveRecursively()
  },
})
const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
