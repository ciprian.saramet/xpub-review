const config = require('config')

const initialize = (tokenService, { User, Identity }) => ({
  execute: async ({ input: { oldPassword, password }, userId }) => {
    if (!config.passwordStrengthRegex.test(password))
      throw new ValidationError(
        'Password is too weak. Please check password requirements.',
      )

    const user = await User.find(userId, 'identities')

    const localIdentity = user.identities.find(i => i.type === 'local')

    if (localIdentity && !(await localIdentity.validPassword(oldPassword))) {
      throw new ValidationError('Wrong username or password.')
    }

    const passwordHash = await Identity.hashPassword(password)

    localIdentity.updateProperties({ passwordHash })
    await localIdentity.save()

    const token = tokenService.create({
      username: localIdentity.email,
      id: user.id,
    })

    return { token }
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
