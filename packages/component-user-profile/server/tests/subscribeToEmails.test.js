process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { models, fixtures } = require('fixture-service')

const { subscribeToEmailsUseCase } = require('../src/use-cases')

describe('Subscribe user to emails', () => {
  it('should subscribe the user to emails', async () => {
    const user = fixtures.generateUser({
      isSubscribedToEmails: false,
    })
    const mockedModels = models.build(fixtures)
    await subscribeToEmailsUseCase
      .initialize(mockedModels)
      .execute({ userId: user.id })

    expect(user.isSubscribedToEmails).toEqual(true)
    expect(user.unsubscribeToken).toBeDefined()
  })
  it('should not return an error when the user is already subscribed to emails', async () => {
    const user = fixtures.generateUser({
      isSubscribedToEmails: true,
    })

    const mockedModels = models.build(fixtures)
    const result = subscribeToEmailsUseCase
      .initialize(mockedModels)
      .execute({ userId: user.id })

    return expect(result).resolves.not.toThrow()
  })
})
