process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { updateUserUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Edit user profile information', () => {
  it('should edit an user', async () => {
    const input = {
      givenNames: chance.first(),
      surname: chance.last(),
    }
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)

    await updateUserUseCase
      .initialize(mockedModels)
      .execute({ userId: user.id, input })

    expect(user.identities[0].surname).toEqual(input.surname)
  })
  it('should return an error when the user id is invalid', async () => {
    const input = {
      surname: chance.last(),
      givenNames: chance.first(),
    }
    const mockedModels = models.build(fixtures)
    const result = updateUserUseCase
      .initialize(mockedModels)
      .execute({ userId: chance.guid(), input })

    return expect(result).rejects.toThrow()
  })
})
