process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const chance = new Chance()
const { unsubscribeToEmailsUseCase } = require('../src/use-cases')

describe('Unsubscribe user to emails', () => {
  it('should unsubscribe the user to emails', async () => {
    const user = fixtures.generateUser({
      isSubscribedToEmails: true,
    })
    const input = {
      token: user.unsubscribeToken,
    }
    const mockedModels = models.build(fixtures)
    await unsubscribeToEmailsUseCase
      .initialize(mockedModels)
      .execute({ userId: user.id, input })

    expect(user.isSubscribedToEmails).toEqual(false)
  })
  it('should not return an error when the user is already unsubscribed to emails', async () => {
    const user = fixtures.generateUser({
      isSubscribedToEmails: false,
    })
    const input = {
      token: user.unsubscribeToken,
    }
    const mockedModels = models.build(fixtures)
    const result = unsubscribeToEmailsUseCase
      .initialize(mockedModels)
      .execute({ userId: user.id, input })

    return expect(result).resolves.not.toThrow()
  })
  it('should return an error when the token is invalid', async () => {
    const user = fixtures.generateUser({
      isSubscribedToEmails: true,
    })
    const input = {
      token: chance.guid(),
    }
    const mockedModels = models.build(fixtures)
    const result = unsubscribeToEmailsUseCase
      .initialize(mockedModels)
      .execute({ userId: user.id, input })

    return expect(result).rejects.toThrow(`Invalid token ${input.token}.`)
  })
})
