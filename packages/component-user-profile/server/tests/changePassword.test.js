process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { models, fixtures, services } = require('fixture-service')

jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const changePasswordUseCase = require('../src/use-cases/changePassword')

describe('Change password use case', () => {
  it('should return success when the old password is correct', async () => {
    const user = fixtures.generateUser({})

    const mockedModels = models.build(fixtures)

    const oldPasswordHash = user.identities[0].passwordHash
    const input = {
      oldPassword: 'password',
      password: 'N3wPassword!',
    }

    const result = await changePasswordUseCase
      .initialize(services.tokenService, mockedModels)
      .execute({ input, userId: user.id })

    expect(result.token).toBeDefined()
    expect(user.identities[0].passwordHash).not.toEqual(oldPasswordHash)
  })

  it('should return an error when the password does not meet minimum requirements', async () => {
    const user = fixtures.generateUser({})

    const mockedModels = models.build(fixtures)

    const input = {
      oldPassword: 'password',
      password: 'simplepass',
    }

    const result = changePasswordUseCase
      .initialize(services.tokenService, mockedModels)
      .execute({ input, userId: user.id })

    return expect(result).rejects.toThrow(
      'Password is too weak. Please check password requirements.',
    )
  })
  it('should return an error when user is not found', async () => {
    const mockedModels = models.build(fixtures)
    const invalidUserId = 'invalid-user-id'
    const input = {
      oldPassword: 'password',
      password: 'N3wPassword!',
    }

    try {
      await changePasswordUseCase
        .initialize(services.tokenService, mockedModels)
        .execute({ input, userId: invalidUserId })
    } catch (e) {
      expect(e.message).toEqual(
        `Object not found: users with 'id' ${invalidUserId}`,
      )
    }
  })
  it('should return an error when the current password is incorrect', async () => {
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)

    const input = {
      oldPassword: 'invalid-password',
      password: 'N3wPassword!',
    }

    const result = changePasswordUseCase
      .initialize(services.tokenService, mockedModels)
      .execute({ input, userId: user.id })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })
})
