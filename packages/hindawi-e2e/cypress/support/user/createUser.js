const createUser = ({ admin, user }) => {
  cy.loginApi(admin.email, admin.password, true)
  cy.visit(`/admin/users`)
    .url()
    .should('include', '/admin/users')
    .get('[data-test-id="add-user"]')
    .click()

    .get('[name="email"]')
    .clear()
    .type(
      `${Cypress.env('email') +
        Math.random()
          .toString(22)
          .substring(8)}@thinslices.com`,
    )

    .get('[role="listbox"]')
    .first()
    .click()
    .get('[role="option"]')
    .eq(user.roleOption)
    .click()

    .get('[name="givenNames"]')
    .type(user.firstName)

    .get('[name="surname"]')
    .type(user.lastName)

    .get('[role="listbox"]')
    .eq(1)
    .click()
    .get('[role="option"]')
    .contains(user.title)
    .click()

    .get('[role="listbox"]')
    .eq(2)
    .click()
    .get('[role="option"]')
    .contains(user.country)
    .click()

    .get('[name="aff"]')
    .type(user.affiliation)

    .get('[type="button"]')
    .contains('SAVE USER')
    .click()
}

Cypress.Commands.add('createUser', createUser)
