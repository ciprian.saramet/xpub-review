const eicMakesDecision = decision => {
  switch (decision) {
    case 'Publish': {
      cy.get('div[label="Your Editorial Decision"]')
        .should('be.visible')
        .click()

      cy.get('div[role="listbox"]')
        .eq(1)
        .click()

      cy.get('[role="option"]')
        .should('contain', 'Return to Handling Editor')
        .should('contain', 'Reject')
        .contains('Publish')
        .click()
        .wait(3000)

      cy.get('[data-test-id="submit-eic-decision"]')
        .should('be.visible')
        .click({ force: true })

      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }
    default: {
      cy.get('div[label="Your Editorial Decision"]')
        .should('be.visible')
        .click()

      cy.get('div[role="listbox"]')
        .eq(1)
        .click()

      cy.get('[role="option"]')
        .contains(decision)
        .click()

      Cypress.env(
        'eicDecisionText',
        'loreimpsum dolor sit ameloreimpsum dolores.',
      )

      cy.get('[data-test-id="eic-decision-message"]')
        .should('be.visible')
        .type(Cypress.env('eicDecisionText'))

      cy.get('[data-test-id="submit-eic-decision"]')
        .should('be.visible')
        .click({ force: true })

      cy.get('[data-test-id="modal-confirm"]')
        .click()
        .wait(2000)
    }
  }
}

Cypress.Commands.add('eicMakesDecision', eicMakesDecision)
