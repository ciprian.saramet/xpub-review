const authorSubmitRevision = ({ updatedFragment }) => {
  cy.get(`[data-test-id="submit-revision-contextual-box"]`)
    .should('be.visible')
    .click()

  cy.get('[data-test-id="submission-title"] input')
    .clear()
    .type(updatedFragment.title)

  cy.get('[data-test-id="submission-type"] button')
    .click()
    .wait(1000)
  cy.get('[data-test-id="submission-type"] [role="option"]')
    .contains(updatedFragment.articleType)
    .click({ force: true })

  cy.get('[data-test-id="submission-abstract"] textarea').type(
    updatedFragment.abstract,
  )

  // Tests for adding files to contextual box

  cy.get('[label="Manuscript Files"] [data-test-id$="-delete"]')
    .first()
    .click()
  cy.wait(3000)

  cy.get('[data-test-id="add-file-manuscript"] input[type="file"]')
    .upload({
      fileName: 'file2.pdf',
    })
    .wait(1500)
  cy.get('[data-test-id="add-file-coverLetter"] input[type="file"]')
    .upload({
      fileName: 'scimakelatex.12834.pdf',
    })
    .wait(1500)
  cy.get('[data-test-id="add-file-supplementary"] input[type="file"]')
    .upload({
      fileName: 'file1.pdf',
    })
    .wait(1500)

  cy.get('[data-test-id="text-area"]')
    .type(Math.random().toString(22))
    .wait(2000)

  cy.get('[type="button"]')
    .contains('Submit revision')
    .click()

  cy.contains('Ready to Submit your Revision?').should('be.visible')
  cy.get('[data-test-id="modal-cancel"]')
    .click()
    .wait(3000)
  cy.contains('Ready to Submit your Revision?').should('not.be.visible')
  cy.get('[type="button"]')
    .contains('Submit revision')
    .click()
    .wait(3000)

  cy.get('[data-test-id="modal-confirm"]')
    .click()
    .wait(3000)

  cy.location().then(loc => {
    Cypress.env(
      'manuscriptId',
      loc.pathname
        .replace('/details', '')
        .split('/')
        .pop(),
    )
  })
}

Cypress.Commands.add('authorSubmitRevision', authorSubmitRevision)
