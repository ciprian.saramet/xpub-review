const createDraft = ({ fragment, author }) => {
  cy.loginApi(author.email, author.password)
  cy.visit('/').wait(3000)

  cy.get('[type="button"]')
    .contains('Submit')
    .click()
    .wait(3000)
  cy.get('[data-test-id="agree-checkbox"] input')
    .should('be.visible')
    .check({ force: true })
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()

  cy.location().then(loc => {
    Cypress.env(
      'manuscriptId',
      loc.pathname
        .replace('/submit', '')
        .split('/')
        .pop(),
    )
  })

  cy.get('[data-test-id="submission-title"] input')
    .clear()
    .type(fragment.title)

  cy.get('[data-test-id="submission-type"] button').click()
  cy.contains(fragment.articleType).click({ force: true })

  cy.get('[data-test-id="submission-abstract"] textarea')
    .clear()
    .type(fragment.abstract)

  fragment.authors.forEach(author => {
    cy.get('[data-test-id="submission-add-author"] button').click()
    cy.get('[data-test-id="email-author"]').type(
      `${Cypress.env('email') +
        Math.random()
          .toString(22)
          .substring(8)}@thinslices.com`,
    )
    cy.get('[data-test-id="givenNames-author"]').type(author.firstName)
    cy.get('[data-test-id="surname-author"]').type(author.lastName)
    cy.get('[data-test-id="affiliation-author"]').type(author.affiliation)

    cy.get('[data-test-id="country-author"]').type(author.country)
    cy.get('[role="option"]')
      .contains(author.country)
      .click({ force: true })

    cy.get('[data-test-id="save-author-0"]').click({ force: true })
  })
  cy.wait(2000)
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()
  cy.wait(2000)

  cy.get('[data-test-id="add-file-manuscript"] input[type="file"]').upload({
    fileName: 'scimakelatex.5292.pdf',
  })
  cy.get('[data-test-id="add-file-coverLetter"] input[type="file"]').upload({
    fileName: 'scimakelatex.12834.pdf',
  })
  cy.get('[data-test-id="add-file-supplementary"] input[type="file"]').upload({
    fileName: 'file1.pdf',
  })
}

Cypress.Commands.add('createDraft', createDraft)
