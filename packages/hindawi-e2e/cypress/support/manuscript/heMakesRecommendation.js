const heMakesRecommendation = recommendation => {
  switch (recommendation) {
    case 'Publish': {
      cy.contains('Reviewer Reports').click()
      cy.contains(recommendation).should('be.visible')
      cy.contains('Your Editorial Recommendation').click()
      cy.contains('Choose in the list').click()
      cy.get('[role="option"]')
        .contains(recommendation)
        .click()
      cy.contains('Submit Recommendation').click()
      cy.get(`[data-test-id="modal-confirm"]`).click()
      break
    }

    case 'Reject': {
      cy.contains(`Reviewer Reports`)
        .should('be.visible')
        .click()
      cy.contains('Your Editorial Recommendation').click()
      cy.contains('Choose in the list').click()
      cy.get('[role="option"]')
        .contains('Reject')
        .click()

      Cypress.env('heRecommendationText-author', 'loreimpsum!')
      Cypress.env('heRecommendationText-eic', 'dolorsitamet!')

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-author"] textarea',
      ).type(Cypress.env('heRecommendationText-author'))

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-eic"] textarea',
      )
        .should('be.visible')
        .type(Cypress.env('heRecommendationText-eic'))

      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy.get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click()
      break
    }

    case 'Minor Revision': {
      cy.contains('Your Editorial Recommendation')
        .should('be.visible')
        .click()

      Cypress.env('heRecommendationText', 'loreimpsum!')

      cy.get(`[role="listbox"]`)
        .find('button')
        .contains('Choose in the list')
        .click()

      cy.get('[role="option"]')
        .contains(recommendation)
        .should('be.visible')
        .click()

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-author"] textarea',
      ).type(Cypress.env('heRecommendationText'))

      cy.contains('Request Revision').click()

      cy.contains('Minor Revision Requested?')

      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }

    case 'Major Revision': {
      cy.contains('Your Editorial Recommendation')
        .should('be.visible')
        .click()

      Cypress.env('heRecommendationText', 'loreimpsum!')

      cy.get(`[role="listbox"]`)
        .find('button')
        .contains('Choose in the list')
        .click()

      cy.get('[role="option"]')
        .contains(recommendation)
        .should('be.visible')
        .click()

      cy.get(
        '[data-test-id="editorial-recommendation-message-for-author"] textarea',
      ).type(Cypress.env('heRecommendationText'))

      cy.contains('Request Revision').click()

      cy.contains('Major Revision Requested?')

      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }

    default: {
      cy.get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .click()
      cy.get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="listbox"]')
        .click()
      cy.get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="option"]')
        .contains(recommendation)
        .click()

      Cypress.env('heRecommendationText', 'loreimpsum!')

      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy.get('[data-test-id="contextual-box-he-recommendation-response"]')
        .should('be.visible')
        .should('contain', 'Required')

      cy.get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .contains('Your Editorial Recommendation')
        .click()

      cy.get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .click()

      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy.get('[data-test-id="contextual-box-he-recommendation-response"]')
        .should('be.visible')
        .should('contain', 'Required')

      cy.get('[data-test-id="editorial-recommendation-message-for-author"]')
        .should('be.visible')
        .type(Cypress.env('heRecommendationText'))
      cy.get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()
      cy.get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click()
    }
  }
}
Cypress.Commands.add('heMakesRecommendation', heMakesRecommendation)
