const eicRevokesHE = () => {
  cy.get('[data-test-id="revoke-icon"]').click({ force: true })
  cy.get('[data-test-id="modal-confirm"]').click()
}

Cypress.Commands.add('eicRevokesHE', eicRevokesHE)
