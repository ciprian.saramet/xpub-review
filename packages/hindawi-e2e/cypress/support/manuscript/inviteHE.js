const inviteHE = ({ eic, he }) => {
  cy.loginApi(eic.email, eic.password)
  cy.visit(`/`)
  cy.wait(3000)

  cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    .should('be.visible')
    .click()
    .wait(2000)
  cy.contains('Assign Handling Editor').click()
  cy.get('[data-test-id="manuscript-assign-he-filter"]')
    .type(Cypress.env('email') + he.email, { force: true })
    .wait(2000)
  cy.get(`[data-test-id^="manuscript-assign-he-invite"]`)
    .contains('INVITE')
    .click()
  cy.get('[data-test-id="modal-confirm"]').click()
}
Cypress.Commands.add('inviteHE', inviteHE)
