const inviteReviewer = ({ reviewer }) => {
  reviewer.forEach(reviewer => {
    cy.wait(2000)
      .get('[data-test-id="reviewer-email"]')
      .should('be.visible')
      .type(Cypress.env('email') + reviewer.email)
    cy.get('[data-test-id="reviewer-givenNames"]').type(reviewer.firstName)
    cy.get('[data-test-id="reviewer-surname"]').type(reviewer.lastName)
    cy.get('[data-test-id="reviewer-aff"]').type(reviewer.affiliation)
    cy.get('[data-test-id="reviewer-country"]').click()
    cy.contains(reviewer.country).click()
    cy.get('[data-type-id="button-invite-reviewer-invite"]').click()
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get('[data-type-id="button-invite-reviewer-invite"]').click()
    cy.get('[data-test-id="modal-confirm"]').click()
    cy.wait(2000)
  })
}
Cypress.Commands.add('inviteReviewer', inviteReviewer)
