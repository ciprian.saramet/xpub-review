const editManuscript = fragment => {
  cy.get('[data-test-id="agree-checkbox"] input')
    .should('be.visible')
    .check({ force: true })
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()

  cy.wait(2000)

  cy.get('[data-test-id="submission-title"] input')
    .clear()
    .type('Fragment 2 - modified')

  cy.get('[data-test-id="submission-type"] button').click()
  cy.contains('Review Article').click({ force: true })

  cy.get('[data-test-id="submission-abstract"] textarea')
    .clear()
    .type('802.11B-modified')

  fragment.authors.forEach(author => {
    cy.get('[data-test-id="submission-add-author"] button').click()
    cy.get('[data-test-id="email-author"]').type(
      `${Cypress.env('email') +
        Math.random()
          .toString(22)
          .substring(8)}@thinslices.com`,
    )
    cy.get('[data-test-id="givenNames-author"]').type(author.firstName)
    cy.get('[data-test-id="surname-author"]').type(author.lastName)
    cy.get('[data-test-id="affiliation-author"]').type(author.affiliation)

    cy.get('[data-test-id="country-author"]').type(author.country)
    cy.get('[role="option"]')
      .contains(author.country)
      .click({ force: true })

    cy.get('[data-test-id="save-author-0"]').click()
  })

  cy.wait(4000)
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()
  cy.wait(4000)

  cy.contains('Main Manuscript')
  cy.log(Cypress.env('manuscriptId'))
  cy.location().then(loc => {
    const manuscriptId = loc.pathname
      .replace('/submit', '')
      .split('/')
      .pop()
    cy.uploadFileViaAPI({
      entityId: manuscriptId,
      fileType: 'supplementary',
      onResponse: () => {},
    })
    cy.uploadFileViaAPI({
      entityId: manuscriptId,
      fileType: 'coverLetter',
      onResponse: () => {},
    })
  })
  cy.wait(3000)
  cy.reload()
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()
}

Cypress.Commands.add('editManuscript', editManuscript)
