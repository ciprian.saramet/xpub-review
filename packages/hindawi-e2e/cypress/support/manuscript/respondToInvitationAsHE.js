const respondToInvitationAsHE = response => {
  cy.get(`[value=${response}]`).click()
  cy.contains('Respond to Invitation').click()
  cy.get(`[data-test-id="modal-confirm"]`)
    .click()
    .wait(3000)
}
Cypress.Commands.add('respondToInvitationAsHE', respondToInvitationAsHE)
