const resendHEInviteApi = ({ he, role }) => {
  cy.visit('/')
  const token = window.localStorage.getItem('token')
  cy.request({
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    url: `/details/${Cypress.env('submissionId')}/${Cypress.env(
      'manuscriptId',
    )}`,
    body: {
      email: Cypress.env('email') + he.username,
      role,
    },
  }).then(response => {
    expect(response.status).to.eq(200) //eslint-disable-line
  })
  cy.visit('dashboard')
}
Cypress.Commands.add('resendHEInviteApi', resendHEInviteApi)
