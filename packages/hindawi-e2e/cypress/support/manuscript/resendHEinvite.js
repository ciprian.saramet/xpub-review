const resendHEInvite = () => {
  cy.get('[data-test-id="resend-icon"]')
    .should('be.visible')
    .click()
  cy.get('[data-test-id="modal-cancel"]').click()

  cy.get('[data-test-id="resend-icon"]')
    .should('be.visible')
    .click()
  cy.get('[data-test-id="modal-confirm"]').click()
}
Cypress.Commands.add('resendHEInvite', resendHEInvite)
