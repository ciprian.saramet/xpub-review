const rejectManuscriptByEQS = ({ admin }) => {
  cy.get('[data-test-id="button-qa-manuscript-technical-checks"]')
    .should('be.visible')
    .click()
  cy.get('[data-test-id="reject-button"]').click()
  cy.get('[data-test-id="modal-confirm"]').click()

  cy.wait(2000)

  cy.contains('Manuscript rejected. Thank you for your technical check!')
}

Cypress.Commands.add('rejectManuscriptByEQS', rejectManuscriptByEQS)
