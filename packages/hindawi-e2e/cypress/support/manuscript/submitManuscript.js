const submitManuscript = ({ fragment, author }) => {
  cy.loginApi(author.email, author.password)
  cy.visit(`/`)

  cy.get('[data-test-id="manuscript-submit"]')
    .should('be.visible')
    .click({ force: true })

  cy.get('[data-test-id="journal-select"] button').click()
  cy.contains(fragment.journalName).click({ force: true })

  cy.get('button')
    .contains('NEXT STEP')
    .click({ force: true })

  cy.get('[type="checkbox"]').click({ force: true })

  cy.get('button')
    .contains('NEXT STEP')
    .click({ force: true })

  cy.location().then(loc => {
    Cypress.env(
      'fragmentIdV1',
      loc.pathname
        // .replace('/submit', '')
        .split('/'),
      // .pop(),
    )
    // Cypress.env(
    //   'collectionId',
    //   loc.pathname.replace('/submit', '').split('/')[2],
    // )
  })

  cy.get('[data-test-id="submission-title"] input')
    .clear({ force: true })
    .type(fragment.title)

  cy.get('[data-test-id="submission-type"] button').click()
  cy.contains(fragment.articleType).click({ force: true })

  cy.get('[data-test-id="submission-abstract"] textarea')
    .clear({ force: true })
    .type(fragment.abstract)

  fragment.authors.forEach(author => {
    cy.get('[data-test-id="submission-add-author"] button').click()
    cy.get('[data-test-id="email-author"]').type(
      `${Cypress.env('email') +
        Math.random()
          .toString(22)
          .substring(8)}@thinslices.com`,
    )
    cy.get('[data-test-id="givenNames-author"]').type(author.firstName)
    cy.get('[data-test-id="surname-author"]').type(author.lastName)
    cy.get('[data-test-id="affiliation-author"]').type(author.affiliation)

    cy.get('[data-test-id="country-author"]').click()
    cy.contains(author.country).click()
    cy.get('.icn_icn_save').click({ force: true })
  })
  cy.wait(2000)
  cy.get('button')
    .contains('NEXT STEP')
    .click()

  cy.get('[data-test-id="add-file-manuscript"] input[type="file"]').upload({
    fileName: 'scimakelatex.5292.pdf',
  })
  cy.get('[data-test-id="add-file-coverLetter"] input[type="file"]').upload({
    fileName: 'scimakelatex.12834.pdf',
  })
  cy.get('[data-test-id="add-file-supplementary"] input[type="file"]').upload({
    fileName: 'file1.pdf',
  })

  cy.wait(3000)
  cy.contains('SUBMIT MANUSCRIPT').click()
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.contains('SUBMIT MANUSCRIPT').click()
  cy.get('[data-test-id="modal-confirm"]').click()
  cy.wait(2000)

  cy.location().then(loc => {
    const manuscriptId = loc.pathname
      .replace('/submit', '')
      .split('/')
      .pop()
    Cypress.env('manuscriptId', manuscriptId)
    cy.get('[data-test-id="go-to-dashboard"]')
      .click()
      .wait(2000)
      .get(`[data-test-id="manuscript-${manuscriptId}"]`)
      .should('be.visible')
  })
}

Cypress.Commands.add('submitManuscript', submitManuscript)
