const submitReview = recommendation => {
  cy.contains('Your Report').click()
  cy.contains('Choose in the list').click()
  cy.contains(recommendation).click()

  // cy.get('input[type="file"]')
  //   .first()
  //   .then(filePicker => {
  //     cy.uploadFile({ fileName: 'file1.pdf', filePicker })
  //   })
  // cy.wait(2000)

  cy.get('[data-test-id="form-report-textarea"]')
    .click()
    .type(Math.random().toString(22))
    .wait(1000)
  cy.get('[data-test-id="submit-reviewer-report"]').click()
  cy.get('[data-test-id="modal-confirm"]').click({ force: true })
  cy.wait(3000)
}

Cypress.Commands.add('submitReview', submitReview)
