const admin = require('../fixtures/users/admin.json')
const author = require('../fixtures/users/author.json')
const manuscriptData = require('../fixtures/models/fragment.json')

const fileTypes = {
  pdf: 'application/pdf',
  doc: 'application/msword',
  docx:
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  txt: 'text/plain',
  excel: 'application/vnd.ms-excel',
  xlm: 'application/vnd.ms-excel',
}

Cypress.Commands.add(
  'loginApi',
  (email, password = Cypress.env('password')) => {
    const localLogin = `mutation{localLogin ( input: { 
    email: "${Cypress.env('email').concat(email)}", 
    password: "${password}"
    }) {
      token
    }
  }`
    window.localStorage.removeItem('token')
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: { query: localLogin },
    }).then(response => {
      const { token } = response.body.data.localLogin
      window.localStorage.setItem('token', token)
      return token
    })
  },
)

Cypress.Commands.add(
  'upload',
  { prevSubject: 'element' },
  (subject, { fileName, fileType = 'pdf' }) =>
    cy.fixture(fileName, 'base64').then(fileContent => {
      cy.window().then(window =>
        Cypress.Blob.base64StringToBlob(fileContent, fileTypes[fileType])
          .then(
            blob =>
              new window.File([blob], fileName, { type: fileTypes[fileType] }),
          )
          .then(testFile => {
            const dataTransfer = new DataTransfer()
            dataTransfer.items.add(testFile)
            const input = subject[0]
            input.files = dataTransfer.files
            cy.wrap(subject).trigger('change', { force: true })
          }),
      )
    }),
)

Cypress.Commands.add(
  'uploadFileViaAPI',
  ({
    entityId,
    fileType = 'manuscript',
    token = window.localStorage.getItem('token'),
    onResponse,
  }) => {
    const file = new File(['some text'], 'filename.pdf', { type: 'text/plain' })
    const fileInput = {
      type: fileType,
      size: file.size,
    }
    const query = `
      mutation uploadFile(
        $entityId: String!
        $fileInput: FileInput
        $file: Upload!
      ) {
        uploadFile(entityId: $entityId, fileInput: $fileInput, file: $file) {
          id
          type
          originalName
          size
          mimeType
        }
      }
    `

    const uploadQuery = {
      operationName: 'uploadFile',
      query,
      variables: {
        entityId,
        fileInput,
        file: null,
      },
    }

    const data = new FormData()
    data.append('operations', JSON.stringify(uploadQuery))
    data.append('map', `{ "0": ["variables.file"] }`)
    data.append('0', file)

    const xhr = new XMLHttpRequest()
    xhr.open('POST', '/graphql')
    xhr.setRequestHeader('Accept', `text/plain`)
    xhr.setRequestHeader('Authorization', `Bearer ${token}`)
    xhr.onreadystatechange = function onReadyStateChange() {
      if (xhr.readyState === 4) {
        const res = JSON.parse(xhr.response)
        onResponse(res.data.uploadFile)
      }
    }
    xhr.send(data)
  },
)

Cypress.Commands.add('useFilters', () => {
  cy.get('[data-test-id="row"]').contains('Filters')
  cy.get('[data-test-id="dashboard-filter-priority"]').click()
  cy.get('[role="option"]')
    .contains('Needs Attention')
    .click()
  cy.get('[data-test-id="fragment-status"]').then($fragment => {
    Cypress._.each($fragment => {
      expect($fragment).contain('Submit Revision')
    })
  })
})

Cypress.Commands.add('searchManuscriptByFragmentId', () => {
  cy.get('[data-test-id="/^fragment-w+/"]')
})

Cypress.Commands.add('createManuscriptViaAPI', noEQS => {
  let manuscriptId = null
  let token = null

  cy.loginApi(admin.email, admin.password, true).then(adminToken => {
    const getJournals = `query{getJournals {
      id
      name
    }}`
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${adminToken}`,
      },
      body: { query: getJournals },
    }).then(response => {
      const journal = response.body.data.getJournals.find(
        j => j.name === 'Bioinorganic Chemistry and Applications',
      )
      Cypress.env('journalId', journal.id)
    })
  })

  cy.loginApi(author.email, author.password).then(userToken => {
    token = userToken
    const createManuscript = `mutation{createDraftManuscript{id, submissionId}}`
    cy.request({
      method: 'POST',
      url: '/graphql',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      body: { query: createManuscript },
    })
      .then(response => {
        const { submissionId } = response.body.data.createDraftManuscript
        manuscriptId = response.body.data.createDraftManuscript.id

        Cypress.env('manuscriptId', manuscriptId)
        Cypress.env('submissionId', submissionId)

        cy.uploadFileViaAPI({
          entityId: manuscriptId,
          token,
          onResponse: () => {},
        })
      })
      .then(() => {
        const updateManuscript = `mutation{updateDraftManuscript(manuscriptId: "${manuscriptId}", autosaveInput:
                { journalId: "${Cypress.env('journalId')}"
                  authors: [
                { aff: "${author.affiliation}"
                  country: "${author.country}"
                  email: "${Cypress.env('email')}${author.email}"
                  givenNames: "${author.firstName}"
                  isCorresponding: true
                  isSubmitting: true
                  surname: "${author.lastName}"
                }]
                  meta: {
                    abstract: "${manuscriptData.abstract}"
                    articleType: "${manuscriptData.articleType}"
                    title: "${manuscriptData.title}"
                    agreeTc: true
                    conflicts: {
                      hasConflicts: ${manuscriptData.conflicts.hasConflicts}
                      message: "${manuscriptData.conflicts.message}"
                      hasDataAvailability: ${
                        manuscriptData.conflicts.hasDataAvailability
                      }
                      dataAvailabilityMessage: "${
                        manuscriptData.conflicts.dataAvailabilityMessage
                      }"
                      hasFunding: ${manuscriptData.conflicts.hasFunding}
                      fundingMessage: "${
                        manuscriptData.conflicts.fundingMessage
                      }"
                  }}}){id}}`
        cy.request({
          method: 'POST',
          url: `/graphql`,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: { query: updateManuscript },
        })
      })
      .then(() => {
        const submitManuscript = `mutation{submitManuscript(manuscriptId:"${manuscriptId}")}`
        cy.request({
          method: 'POST',
          url: `/graphql`,
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: { query: submitManuscript },
        })
      })
  })

  if (noEQS) return

  cy.loginApi(admin.email, admin.password, true).then(adminToken => {
    token = adminToken
    Cypress.env(`token`, token)
    const getManuscript = `query{getManuscript(manuscriptId:"${manuscriptId}"){
        id
        technicalCheckToken
      }}`
    cy.request({
      method: 'POST',
      url: `/graphql`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: { query: getManuscript },
    }).then(response => {
      const { technicalCheckToken } = response.body.data.getManuscript
      const customId = Math.random()
        .toString()
        .slice(-7)
      const approveEQS = `mutation{approveEQS(
            manuscriptId:"${manuscriptId}",
            input:{
              token: "${technicalCheckToken}",
              customId: "${customId}"
            })
          }`
      cy.request({
        method: 'POST',
        url: `/graphql`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${adminToken}`,
        },
        body: { query: approveEQS },
      })
    })
  })
})
