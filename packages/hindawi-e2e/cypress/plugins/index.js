// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const uuid = require('uuid')

const getDBConfig = config => ({
  user: config.env.dbConfig.user,
  host: config.env.dbConfig.host,
  database: config.env.dbConfig.database,
  password: config.env.dbConfig.password,
  port: 5432,
})

const passwordHash = config => config.env.passwordHash

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config

  on('task', {
    async importEntitiesToGQL() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()
      await qaClient.query(`drop table team_member CASCADE;`)
      await qaClient.query(`drop table file;`)
      await qaClient.query(`drop table comment;`)
      await qaClient.query(`drop table review;`)
      await qaClient.query(`drop table identity;`)
      await qaClient.query(`drop table team;`)
      await qaClient.query(`drop table manuscript CASCADE;`)
      await qaClient.query(`drop table "user" CASCADE;`)
      await qaClient.query(`drop table journal CASCADE;`)
      await qaClient.query(
        `DELETE FROM migrations where id NOT LIKE '%entities.sql';`,
      )
      qaClient.end()
      return true
    },

    async clearDatabase() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()
      await qaClient.query('DELETE FROM team_member;')
      await qaClient.query('DELETE FROM file;')
      await qaClient.query('DELETE FROM comment;')
      await qaClient.query('DELETE FROM review;')
      await qaClient.query('DELETE FROM identity;')
      await qaClient.query('DELETE FROM team;')
      await qaClient.query('DELETE FROM audit_log;')
      await qaClient.query('DELETE FROM reviewer_suggestion;')
      await qaClient.query(`DELETE FROM manuscript;`)
      await qaClient.query(`DELETE FROM "user";`)
      await qaClient.query(`DELETE FROM journal;`)
      qaClient.end()
      return true
    },

    async createJournal() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const journalId = uuid.v4()

      await qaClient.query(
        `INSERT INTO journal(id, name, publisher_name, code, email, issn, apc, is_active)
        VALUES ('${journalId}','Bioinorganic Chemistry and Applications','Hindawi LTD', 'BCA', '', '', 1000, true)`,
      )

      qaClient.end()
      return true
    },

    async createGlobalUser({ email, givenName, surname, role }) {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const userId = uuid.v4()
      const teamId = uuid.v4()

      await qaClient.query(
        `INSERT INTO "user"(id, default_identity, is_subscribed_to_emails, unsubscribe_token, agree_tc, is_active)
        VALUES ('${userId}',  'local', true, '${uuid.v4()}', true, true)`,
      )
      await qaClient.query(
        `INSERT INTO identity(user_id, type, is_confirmed, email, given_names, surname, password_hash, title, aff, country)
        VALUES ('${userId}', 'local', true, '${email}', '${givenName}', '${surname}', '${passwordHash(
          config,
        )}', 'mrs', 'Hindawi Research', 'RO')`,
      )
      const journal_id = await qaClient.query(
        `SELECT id FROM journal WHERE name = 'Bioinorganic Chemistry and Applications'`,
      )
      await qaClient.query(
        `INSERT INTO team(id, role, journal_id)
        VALUES('${teamId}', '${role}', '${journal_id.rows[0].id}')`,
      )
      await qaClient.query(
        `INSERT INTO team_member(user_id, team_id, status, alias)
        VALUES('${userId}', '${teamId}', 'accepted' , '{
          "aff": "Hindawi Research",
          "email": "${email}",
          "title": "mrs",
          "country": "RO",
          "surname": "${surname}",
          "givenNames": "${givenName}"
        }')`,
      )
      qaClient.end()
      return true
    },
    async createUser({ email, givenName, surname }) {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      const userId = uuid.v4()

      await qaClient.query(
        `INSERT INTO "user"(id, default_identity, is_subscribed_to_emails, unsubscribe_token, agree_tc, is_active)
        VALUES ('${userId}',  'local', true, '${uuid.v4()}', true, true)`,
      )
      await qaClient.query(
        `INSERT INTO identity(user_id, type, is_confirmed, email, given_names, surname, password_hash, title, aff, country)
        VALUES ('${userId}', 'local', true, '${email}', '${givenName}', '${surname}', '${passwordHash(
          config,
        )}', 'mrs', 'Hindawi Research', 'RO')`,
      )
      qaClient.end()
      return true
    },

    async getJournalId({ journalId }) {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      journalId = await qaClient.query(
        `SELECT id FROM journal WHERE name = 'Bioinorganic Chemistry and Applications'`,
      )

      qaClient.end()
      return true
    },
  })
}
