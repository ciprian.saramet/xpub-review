describe('Change Password', () => {
  beforeEach(() => {
    cy.fixture('users/he').as('he')
    cy.visit('/')
  })

  it('Change user password', function resetPassword() {
    const { he } = this

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get('[data-test-id="admin-menu-button"]').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()
    cy.get('[data-test-id="change-password-btn"]').click()
    cy.get('input[name="currentPassword"]').type('Password1!')
    cy.get('input[name="password"]').type('Password1!')
    cy.get('input[name="confirmPassword"]').type('Password1!')
    cy.get('button')
      .contains('Update password')
      .click()
    cy.get('button')
      .contains('Back to account settings')
      .click()
  })
})
