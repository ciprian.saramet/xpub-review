describe('Edit user profile as admin', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
  })

  it('Edit user profile', function editUserProfileAsAdmin() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')

    cy.get('.icn_icn_caretDown')
      .first()
      .click()
    cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
    cy.contains('Users').click()
    cy.wait(4000)
    cy.get('.icn_icn_edit')
      .eq(1)
      .click()

    cy.get('[data-test-id="first-name-input"]')
      .clear()
      .type('FIRSTNAMEv1')

    cy.get('[data-test-id="last-name-input"]')
      .clear()
      .type('LastNamev1')

    cy.get('[data-test-id="title-dropdown"]').click()
    cy.get('div[role="option"]')
      .first()
      .click()

    cy.get('[data-test-id="country"]')
      .get('div[role="listbox"] input')
      .clear()
      .type('al', { force: true })
      .type('{enter}', { force: true })

    cy.get('[data-test-id="role-dropdown"] button').click()
    cy.get('div[role="option"][aria-selected="true"]').click()
    cy.get('input[name="aff"]').type('v1')
    cy.get('button')
      .contains('EDIT USER')
      .click()
  })
})
