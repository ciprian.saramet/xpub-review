describe('Unsubscribe email notifications', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
  })

  it('Unsubscribe email notifications', function unsubscribe() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('[data-test-id="admin-menu-button"]').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()

    cy.get('button')
      .contains('Unsubscribe')
      .click()
    // cy.get('[data-test-id="modal-root"]').should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get('[data-test-id="modal-root"]').should('not.be.visible')
    cy.get('button')
      .contains('Unsubscribe')
      .click()

    cy.get('[data-test-id="modal-confirm"]').click()
  })
})
