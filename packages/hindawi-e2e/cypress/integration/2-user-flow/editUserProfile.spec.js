describe('Edit User Profile', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
  })

  it('Edit user profile', function editUserProfile() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('.icn_icn_caretDown')
      .eq(0)
      .click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()
    cy.get('.icn_icn_edit').click()
    cy.get('input[name="givenNames"]').type('new')
    cy.get('input[name="surname"]').type('new')
    cy.get('.icn_icn_caretDown')
      .eq(1)
      .click()
    cy.get('div[role="option"]')
      .eq(2)
      .click()
    cy.get('[data-test-id="country-input"]').clear()
    cy.get('div[role="option"]')
      .eq(2)
      .click()
    cy.get('input[name="aff"]').type('new')
    cy.get('.icn_icn_save').click({ force: true })
  })
})
