describe('Create Users', () => {
  beforeEach(() => {
    cy.clearLocalStorage()
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
  })

  it('create new Admin account', function createAdmin() {
    const { admin } = this
    cy.createUser({ admin, user: admin })
  })

  it('create new Author account', function createAuthor() {
    const { author, admin } = this
    cy.createUser({ admin, user: author })
  })

  it('create new EiC account', function createEiC() {
    const { eic, admin } = this
    cy.createUser({ admin, user: eic })
  })

  it('create new HE account', function createHE() {
    const { he, admin } = this
    cy.createUser({ admin, user: he })
  })
})
