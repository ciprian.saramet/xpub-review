describe('Create users into DB', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/reviewer').as('reviewer')
  })
  it('Create admin', function createAdmin() {
    const { admin } = this
    cy.task('createGlobalUser', {
      email: Cypress.env('email') + admin.email,
      givenName: admin.firstName,
      surname: admin.lastName,
      role: 'admin',
    }).should('equal', true)
  })
  it('Create EiC', function createEiC() {
    const { eic } = this
    cy.task('createGlobalUser', {
      email: Cypress.env('email') + eic.email,
      givenName: eic.firstName,
      surname: eic.lastName,
      role: 'editorInChief',
    }).should('equal', true)
  })
  it('Create HE', function createHE() {
    const { he } = this
    cy.task('createGlobalUser', {
      email: Cypress.env('email') + he.email,
      givenName: he.firstName,
      surname: he.lastName,
      role: 'handlingEditor',
    }).should('equal', true)
  })
  it('Create Author', function createAuthor() {
    const { author } = this
    cy.task('createUser', {
      email: Cypress.env('email') + author.email,
      givenName: author.firstName,
      surname: author.lastName,
    }).should('equal', true)
  })
  it('Create reviewers', function createReviewer() {
    const { reviewer } = this
    reviewer.forEach(reviewer => {
      cy.task('createUser', {
        email: Cypress.env('email') + reviewer.email,
        givenName: reviewer.firstName,
        surname: reviewer.lastName,
      }).should('equal', true)
    })
  })
})
