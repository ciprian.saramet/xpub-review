describe('Clear DB', () => {
  it('Clear all database', () => {
    cy.task('clearDatabase').should('equal', true)
  })
})
