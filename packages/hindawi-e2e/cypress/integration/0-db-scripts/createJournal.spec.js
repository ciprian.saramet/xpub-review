describe('Create journal into DB', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/reviewer').as('reviewer')
  })
  it('Create journal', () => {
    cy.task('createJournal', {}).should('equal', true)
  })
})
