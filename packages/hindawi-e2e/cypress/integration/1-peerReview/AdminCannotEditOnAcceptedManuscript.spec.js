describe('Admin cannot edit on Accepted manuscript', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { he } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { reviewer, he } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.inviteReviewer({ reviewer, he })
    cy.wait(3000)
  })

  it('Give a review for publish as Reviewer', function minorRevisionReview() {
    const { reviewer } = this

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsReviewer('yes')
    cy.submitReview('Publish')
    cy.wait(3000)
  })

  it('HE makes recommendation to publish', function heMakesRecommendationToPublish() {
    const { he } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.wait(2000)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.wait(2000)
    cy.heMakesRecommendation('Publish')
  })

  it('EiC makes decision to publish', function eicMakesDecisioToPublish() {
    const { eic } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.eicMakesDecision('Publish')
  })

  it('Manuscript approved by EQA', function approveManuscriptByEQA() {
    const { admin } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.approveManuscriptByEQA({ admin })
  })

  it('Admin tries to edit manuscript in Accepted status', function editManuscriptApi() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit(
      `/submit/${Cypress.env('submissionId')}/${Cypress.env('manuscriptId')}`,
    ).wait(2000)
    cy.location().then(loc => {
      cy.expect(loc.pathname).contain('/404')
    })
  })
})
