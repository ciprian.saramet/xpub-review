describe('Request Manuscript Revision EiC', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('EiC successfully request manuscript revision from author', function requestManuscriptRevision() {
    const { eic, statuses, author } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.submitted.editorInChief)

    cy.eicMakesDecision('Request Revision').then(() => {
      cy.checkStatus(statuses.revisionRequested.editorInChief)

      cy.loginApi(author.email, author.password)
      cy.visit('/')

      cy.checkStatus(statuses.revisionRequested.author)

      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()

      cy.get(`h3`)
        .contains('Editorial Comments')
        .should('be.visible')

      cy.get(`span`)
        .should('be.visible')
        .contains(Cypress.env('eicDecisionText'))

      cy.get(`h3`)
        .contains('Submit Revision')
        .should('be.visible')
        .click()
    })
  })
})
