describe('Delete Draft', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('models/fragment').as('fragment')
  })

  it('Author creates draft', function authorCreatesDraft() {
    const { fragment, author } = this
    cy.createDraft({ fragment, author })
  })

  it('Admin deletes a draft', function adminDeletesDraft() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.get('[type="button"]')
      .contains('DELETE')
      .eq(0)
      .click()
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.get('[type="button"]')
      .contains('DELETE')
      .eq(0)
      .click()
    cy.get('[data-test-id="modal-confirm"]').click()
    cy.visit('/')
  })

  it('Creates draft', function authorCreatesDraft() {
    const { fragment, author } = this
    cy.createDraft({ fragment, author })
  })

  it('Author deletes draft', function authorDeletesDraft() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/').wait(3000)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.get('[type="button"]')
      .contains('DELETE')
      .eq(0)
      .click()
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.get('[type="button"]')
      .contains('DELETE')
      .eq(0)
      .click()
    cy.get('[data-test-id="modal-confirm"]').click()
    cy.visit('/')
  })
})
