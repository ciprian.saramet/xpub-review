describe('Reviewer declines invitation', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)

    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)

    cy.inviteReviewer({ reviewer })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Reviewer should decline invitation', function reviewerDeclinesInvitation() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('no')
    cy.wait(3000)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)

    cy.get('span').contains('DECLINED')
  })
})
