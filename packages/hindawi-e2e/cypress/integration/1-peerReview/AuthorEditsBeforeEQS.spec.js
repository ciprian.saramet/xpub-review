describe('Author edits before EQS', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Author creates draft', function authorCreatesDraft() {
    const { fragment, author } = this
    cy.createDraft({ fragment, author })
  })

  it('Author edits manuscript before EQS', function editManuscript() {
    const { author, fragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/').wait(4000)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.editManuscript(fragment)
    cy.get('[type="button"]')
      .contains('SUBMIT MANUSCRIPT')
      .click()
    cy.visit('/').wait(4000)
    cy.get(
      `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`,
    ).contains('Fragment 2 - modified')
  })
})
