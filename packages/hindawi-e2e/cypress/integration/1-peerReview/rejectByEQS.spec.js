describe('Reject Manuscript by EQS', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI(true)
  })

  it('EA rejects the manuscript', function rejectManuscriptByEQS() {
    const id = Cypress.env('manuscriptId')
    const { admin, author, statuses } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit(`/`)

    cy.wait(5000)

    cy.get(`[data-test-id="manuscript-${id}"]`).click()
    cy.rejectManuscriptByEQS({ admin })
    cy.get(`[data-test-id="journal-logo"]`).click()
    cy.get(`[data-test-id="manuscript-${id}"]`).click()
    cy.checkStatus(statuses.rejected.admin)

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${id}"]`).click()
    cy.checkStatus(statuses.rejected.author)
  })
})
