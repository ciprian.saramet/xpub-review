describe('HE rejects manuscript before reviewer to submit report', () => {
  beforeEach(() => {
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('users/eic').as('eic')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Succesfully submit a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Should Invite HE to manuscript', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Succesfully accept invitation for manuscript as HE', function respondToInvitationAsHE() {
    const { he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)
  })

  it('Succesfully invite reviewer to manuscript as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Accept invitation as reviewer', function acceptInvitationAsreviewer() {
    const { reviewer, statuses } = this

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('yes')
    cy.checkStatus(statuses.underReview.reviewer)
    cy.wait(1000)

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('yes')
    cy.checkStatus(statuses.underReview.reviewer)
  })
})
