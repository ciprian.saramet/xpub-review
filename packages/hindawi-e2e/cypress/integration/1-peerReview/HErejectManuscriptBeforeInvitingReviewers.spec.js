describe('Reject Manuscript as HE before inviting reviewers', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/he').as('he')
    cy.fixture('users/eic').as('eic')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Should invite HE to manuscript', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Successfully accept invitation for manuscript as HE', function respondToInvitationAsHE() {
    const { he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/').wait(2000)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)
  })

  it('Successfully rejects a manuscript as HE', function heMakesRecommendation() {
    const { he, eic, statuses, admin } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/').wait(2000)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heAssigned.handlingEditor)
    cy.heMakesRecommendation('Reject').then(() => {
      cy.checkStatus(statuses.pendingApproval.handlingEditor)
      cy.get(`h3`)
        .contains('Your Editorial Recommendation')
        .should('not.be.visible')

      cy.loginApi(eic.email, eic.password)
      cy.visit('/').wait(2000)

      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.editorInChief).wait(2000)
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText-author'))
        .should('contain', Cypress.env('heRecommendationText-eic'))

      cy.loginApi(admin.email, admin.password, true)
      cy.visit('/').wait(2000)

      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.admin)
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText-author'))
        .should('contain', Cypress.env('heRecommendationText-eic'))
    })
  })
})
