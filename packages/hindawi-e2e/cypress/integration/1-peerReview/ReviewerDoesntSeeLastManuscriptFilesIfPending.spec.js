describe(`HE doesn't see files on latest manuscript if pending`, () => {
  beforeEach(() => {
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('manuscripts/statuses').as('statuses')
    cy.fixture('models/updatedFragment').as('updatedFragment')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Should invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)

    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Should invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)

    cy.inviteReviewer({ reviewer })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Should hide files from pending reviewers and accept', function reviewerDoesntSeeFiles() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.get('.icn_icn_preview').should('not.be.visible')
    cy.get('.icn_icn_downloadZip').should('not.be.visible')
    cy.get('[data-test-id="files-tab"]').should('not.be.visible')

    cy.respondToInvitationAsReviewer('yes')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.wait(3000)

    cy.submitReview('Major Revision')
    cy.wait(3000)
    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE makes recommendation for major revision', function majorRevisionRecommendation() {
    const { he, eic, statuses } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/').wait(2000)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.heMakesRecommendation('Major Revision')

    cy.loginApi(eic.email, eic.password)
    cy.visit('/').wait(2000)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.editorInChief)
  })

  it('Author submit major revision', function majorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.checkStatus(statuses.underReview.author)
  })

  it('Should hide files from pending reviewers only on version 2', function reviewerDoesntSeeFiles() {
    const { reviewer, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.get('.icn_icn_preview').should('not.be.visible')
    cy.get('.icn_icn_downloadZip').should('not.be.visible')
    cy.get('[data-test-id="files-tab"]').should('not.be.visible')

    cy.contains('Version 2').click()
    cy.contains('Version 1').click()

    cy.get('.icn_icn_preview').should('be.visible')
    cy.get('.icn_icn_downloadZip').should('be.visible')
    cy.get('[data-test-id="files-tab"]').should('be.visible')
  })
})
