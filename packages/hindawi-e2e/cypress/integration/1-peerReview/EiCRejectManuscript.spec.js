describe('Reject Manuscript EiC', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Successfully rejects a manuscript as EiC', function rejectManuscript() {
    const { eic, author, statuses } = this
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.submitted.editorInChief)
    cy.eicMakesDecision('Reject').then(() => {
      cy.checkStatus(statuses.rejected.editorInChief)

      cy.loginApi(author.email, author.password)
      cy.visit('/')

      cy.checkStatus(statuses.rejected.author)

      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()

      cy.get(`h3`)
        .contains('Editorial Comments')
        .should('be.visible')

      cy.get(`span`)
        .should('be.visible')
        .contains(Cypress.env('eicDecisionText'))
    })
  })
})
