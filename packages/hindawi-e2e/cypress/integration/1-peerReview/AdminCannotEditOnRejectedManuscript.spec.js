describe('Admin cannot edit Rejected manuscript', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { he } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)
  })

  it('HE makes recommendation to reject', function heMakesRecommendationToReject() {
    const { he } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.wait(2000)
  })

  it('EiC makes decision to reject', function eicMakesDecisioToReject() {
    const { eic } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.eicMakesDecision('Reject')
  })

  it('Admin tries to edit manuscript in Rejected status', function editManuscriptApi() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/').wait(3000)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.get('[data-test-id="button-qa-manuscript-edit"]').should('not.exist')
    cy.visit(
      `/submit/${Cypress.env('submissionId')}/${Cypress.env('manuscriptId')}`,
    )
    cy.url().should('have', '/404')
  })
})
