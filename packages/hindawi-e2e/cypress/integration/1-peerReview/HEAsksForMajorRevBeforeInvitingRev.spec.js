describe('HE asks for major revision before inviting reviewers', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)

    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('HE asks for major revision before inviting reviewers', function heAsksForMajRevBeforeInviteingRev() {
    const { he, statuses, author } = this

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heAssigned.handlingEditor)
    cy.contains('Reviewer Details & Reports').should('be.visible')
    cy.contains('No reviewers invited yet.')
    cy.heMakesRecommendation('Major Revision').then(() => {
      cy.checkStatus(statuses.revisionRequested.handlingEditor)
      cy.contains('Editorial Comments')
      cy.contains('Major Revision Requested')
      cy.should('not.contain', 'Publish')
      cy.should('not.contain', 'Minor Revision Requested')

      cy.loginApi(author.email, author.password)
      cy.visit('/')

      cy.checkStatus(statuses.revisionRequested.author)
      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.contains('Editorial Comments')
      cy.contains('Major Revision Requested')
      cy.contains(Cypress.env('heRecommendationText'))
    })

    cy.contains('Submit Revision')
  })
})
