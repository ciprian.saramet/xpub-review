describe('EiC revokes HE after HE accepted invitation', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('EiC revokes HE after He accepted invitation', function EiCRevokesHe() {
    const { eic, he, statuses } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)

    cy.checkStatus(statuses.heAssigned.handlingEditor)
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.wait(2000)

    cy.checkStatus(statuses.heAssigned.editorInChief)
    cy.eicRevokesHE()
    cy.wait(2000)

    cy.checkStatus(statuses.submitted.editorInChief)
    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.wait(2000)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )
  })
})
