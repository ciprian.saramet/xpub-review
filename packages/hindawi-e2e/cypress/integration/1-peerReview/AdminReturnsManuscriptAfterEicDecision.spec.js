describe('Admin returns manuscript after EiC decision', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Give a review for publish as Reviewer', function submitPublishReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')

    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(he.email, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE makes recommendation to publish', function heMakesRecommendationToPublish() {
    const { he, eic, statuses } = this
    cy.loginApi(he.email, he.password)
    cy.wait(2000)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.wait(2000)
    cy.heMakesRecommendation('Publish')
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.editorInChief)
  })

  it('EiC makes decision to publish', function eicMakesDecisioToPublish() {
    const { eic, statuses } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.editorInChief)
    cy.contains('Publish')
    cy.eicMakesDecision('Publish')
    cy.checkStatus(statuses.inQA.editorInChief)
  })

  it('Manuscript rejected by EQA', function adminRejectManuscript() {
    const { admin, eic } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.rejectManuscriptByEQA()
    cy.get('[data-test-id="journal-logo"]').click()

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.contains('Your Editorial Decision').click()
    cy.contains('Please select').click()
    cy.contains('Publish')
    cy.contains('Return to Handling Editor')
    cy.contains('Reject')

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.contains('Your Editorial Decision').click()
    cy.contains('Please select').click()
    cy.get('[role="option"]').contains('Publish')
    cy.contains('Return to Handling Editor')
    cy.contains('Reject')
  })
})
