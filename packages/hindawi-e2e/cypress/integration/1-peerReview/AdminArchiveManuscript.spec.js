describe('Admin archives manuscript', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Admin deletes manuscript', function deleteManuscript() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(
      `[data-test-id="manuscript-${Cypress.env(
        'manuscriptId',
      )}"] .icn_icn_delete`,
    ).click()
    cy.get('[data-test-id="delete-manuscript-modal"]').should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()

    cy.get(
      `[data-test-id="manuscript-${Cypress.env(
        'manuscriptId',
      )}"] .icn_icn_delete`,
    ).click()

    cy.get('[data-test-id="modal-confirm"]').click()
  })

  it('Everyone sees correct information', function checkAll() {
    const { admin, eic, he, author } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')

    cy.get(
      `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"] .deleteIcon`,
    ).should('not.exist')

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(
      `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"] .deleteIcon`,
    ).should('not.exist')

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(
      `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"] .deleteIcon`,
    ).should('not.exist')

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(
      `[data-test-id="manuscript-${Cypress.env('manuscriptId')}"] .deleteIcon`,
    ).should('not.exist')
  })
})
