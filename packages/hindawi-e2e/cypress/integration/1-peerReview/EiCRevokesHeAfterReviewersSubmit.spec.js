describe('EiC revokes HE after reviewers send reports', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('yes')
    cy.wait(4000)

    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Give a review for publish as Reviewer', function submitMajReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')

    // cy.checkStatus(statuses.reviewCompleted.reviewer) // Bug

    cy.loginApi(he.email, he.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('EiC revokes He after the reviewers submitted reports', function EiCRevokesHeAfterReviewersSubmitedReports() {
    const { eic, he, reviewer, statuses } = this
    cy.loginApi(eic.email, eic.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.editorInChief)

    cy.contains('Reviewer Details & Reports').click()
    cy.get('h4').contains('Recommendation')
    cy.get('h4').contains('Report')

    cy.contains('Your Editorial Decision').click()
    cy.contains('Please select').click()
    cy.contains('Reject')
    cy.contains('Please select').click({ force: true })

    cy.eicRevokesHE()
    cy.checkStatus(statuses.submitted.editorInChief)

    cy.contains('Reviewer Details & Reports').should('not.be.visible')
    cy.contains('Assign Handling Editor')

    cy.get('button')
      .contains('INVITE')
      .click({ force: true })

    cy.loginApi(he.email, he.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit(`/`)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).should(
      'not.be.visible',
    )
  })
})
