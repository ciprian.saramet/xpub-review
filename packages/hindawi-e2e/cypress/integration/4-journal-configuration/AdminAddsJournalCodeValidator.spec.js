describe('Admin adds journal code', () => {
  beforeEach(() => {
    cy.fixture('users/admin')
      .as('admin')
      .then(admin => {
        cy.loginApi(admin.email, admin.password, true).visit('/')
        cy.get('[data-test-id="admin-menu-button"]').click()
        cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
        cy.contains('Journal configuration').click()
        cy.get('[data-test-id="add-journal"]').click()
      })
  })

  it("Doesn't accept input with non-alphanumberic characters", () => {
    cy.get('[data-test-id="code-input"]').click()
    cy.contains('Add Journal').click()

    cy.get('[data-test-id="code-input"]')
      .type('!')
      .as('input')
    cy.get('.erron-wrapper')
      .contains('Invalid')
      .as('error')

    const wrongString = '§=`[];\'\\,./±!@#$%^&*()_+~{}:"|<>?'

    wrongString.split('').forEach(l => {
      cy.get('@input')
        .clear()
        .type(l)
        .get('@error')
        .contains('Invalid')
        .wait(200)
    })
  })

  it("Doesn't accept input with empty string", () => {
    cy.get('[data-test-id="code-input"]').type(' ')
    cy.contains('Add Journal').click()
    cy.get('.erron-wrapper').contains('Required')
  })
})
