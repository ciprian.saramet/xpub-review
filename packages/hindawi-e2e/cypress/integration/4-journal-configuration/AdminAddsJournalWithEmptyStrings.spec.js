describe('Admin adds journal', () => {
  beforeEach(() => {
    cy.fixture('users/admin')
      .as('admin')
      .then(admin => {
        cy.loginApi(admin.email, admin.password, true).visit('/')
        cy.get('[data-test-id="admin-menu-button"]').click()
        cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
        cy.contains('Journal configuration').click()
        cy.get('[data-test-id="add-journal"]').click()
      })
  })

  it('Adds journal with empty fields', () => {
    cy.get('[data-test-id="modal-confirm"]').click()

    cy.get('.erron-wrapper')
      .filter(':contains("Required")')
      .should('have.length', 4)
  })
})
