const Chance = require('chance')

const chance = new Chance()

describe('Admin adds journal', () => {
  const name = `[Auto] Journal ${chance.word()}`
  const code = `${chance.syllable()}${chance.natural({ min: 1, max: 99 })}`
  const issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`
  const apc = `${chance.natural({ min: 100, max: 1999 })}`
  const email = `${Cypress.env('email')}${code}@thinslices.com`

  beforeEach(() => {
    cy.fixture('users/admin')
      .as('admin')
      .then(admin => {
        cy.loginApi(admin.email, admin.password, true).visit('/')
        cy.get('[data-test-id="admin-menu-button"]').click()
        cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
        cy.contains('Journal configuration').click()
        cy.get('[data-test-id="add-journal"]').click()
      })
  })

  it('Adds journal', () => {
    const todayDate = Cypress.moment().format('YYYY-MM-DD')

    cy.get('[data-test-id="journal-name-input"]')
      .type(name)
      .get('[data-test-id="code-input"]')
      .type(code)
      .get('[data-test-id="issn-input"]')
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .type(apc)
      .get('[data-test-id="email-input"]')
      .type(email)
      .get('span')
      .contains('Active for submissions')
      .click()
      .get('[data-test-id="calendar-button"]')
      .contains(todayDate)
      .click()
      .get('.react-calendar')
      .get('.react-calendar__month-view')
      .click()

    cy.get('input[id="downshift-0-input"]')
      .type('{downarrow}')
      .type('{uparrow}')
      .type('{enter}')

    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(0)
      .click()
    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(1)
      .click()
      .get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[data-test-id^="journal-"]') // first journal- is the logo
      .eq(1)
    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .contains(name)
    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .contains(code)
    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .contains(issn)
    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .contains('Editor in Chief')
    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .contains(email)
  })

  it('Adds an active journal', () => {
    const todayDate = Cypress.moment().format('YYYY-MM-DD')

    const name = `[Auto] Journal ${chance.word()}`
    const code = `${chance.syllable()}${chance.natural({ min: 1, max: 99 })}`
    const issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`
    const apc = `${chance.natural({ min: 100, max: 1999 })}`
    const email = `${Cypress.env('email')}${code}@thinslices.com`

    cy.get('[data-test-id="journal-name-input"]')
      .type(name)
      .get('[data-test-id="code-input"]')
      .type(code)
      .get('[data-test-id="issn-input"]')
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .type(apc)
      .get('[data-test-id="email-input"]')
      .type(email)
      .get('span')
      .contains('Active for submissions')
      .click()
      .get('[data-test-id="calendar-button"]')
      .contains(todayDate)

    cy.get('input[id="downshift-0-input"]')
      .type('{downarrow}')
      .type('{uparrow}')
      .type('{enter}')

    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(0)
      .click()
    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(1)
      .click()

    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[data-test-id^="journal-"]')
      .eq(1) // first journal- is the logo
      .contains(name)
      .get('span')
      .contains('ACTIVE')
  })

  it('Returns error if the journal already exists', () => {
    cy.get('[data-test-id="journal-name-input"]')
      .type(name)
      .get('[data-test-id="code-input"]')
      .type(code)
      .get('[data-test-id="issn-input"]')
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .type(apc)
      .get('[data-test-id="email-input"]')
      .type(email)

    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(0)
      .click()
    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(1)
      .click()

    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.contains('Journal already exists.')
  })
})
