const { HindawiBaseModel } = require('component-model')
const { model: Comment } = require('component-model-comment')
const { isEmpty } = require('lodash')

class Review extends HindawiBaseModel {
  static get tableName() {
    return 'review'
  }

  static get schema() {
    return {
      properties: {
        recommendation: { enum: Object.values(Review.Recommendations) },
        open: { type: ['boolean', 'null'] }, // not used in project
        manuscriptId: { type: 'string', format: 'uuid' },
        teamMemberId: { type: 'string', format: 'uuid' },
        submitted: { type: ['string', 'object', 'null'], format: 'date-time' },
      },
    }
  }

  static get relationMappings() {
    return {
      comments: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-comment').model,
        join: {
          from: 'review.id',
          to: 'comment.reviewId',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'review.manuscriptId',
          to: 'manuscript.id',
        },
      },
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-team-member').model,
        join: {
          from: 'review.teamMemberId',
          to: 'team_member.id',
        },
      },
    }
  }

  static get Recommendations() {
    return {
      responseToRevision: 'responseToRevision',
      minor: 'minor',
      major: 'major',
      reject: 'reject',
      publish: 'publish',
      revision: 'revision',
      returnToHE: 'returnToHE',
    }
  }

  addComment({ content, type }) {
    this.comments = this.comments || []

    const commentTypeAlreadyExists = this.comments.some(c => c.type === type)
    if (commentTypeAlreadyExists)
      throw new ValidationError('Cannot add multiple comments of the same type')

    const comment = new Comment({
      content,
      type,
    })

    this.comments.push(comment)

    return comment
  }

  setSubmitted(date) {
    this.submitted = date
  }

  stripEmptyPrivateComments() {
    if (!this.comments) {
      throw new ValidationError('Comments are required.')
    }

    const privateComment = this.comments.find(comm => comm.type === 'private')
    if (privateComment && isEmpty(privateComment.content)) {
      this.comments = this.comments.filter(comm => comm.type !== 'private')
    }
  }

  hasPublicComment() {
    if (!this.comments) {
      throw new ValidationError('Comments are required.')
    }

    const publicComment = this.comments.find(comm => comm.type === 'public')

    if (!publicComment) return false

    if (!publicComment.files) {
      throw new ValidationError('Files are required.')
    }

    return !!publicComment.content || publicComment.files.length > 0
  }

  assignMember(teamMember) {
    this.member = teamMember
  }

  toDTO() {
    return {
      ...this,
      comments: this.comments
        ? this.comments.map(comment => comment.toDTO())
        : [],
      member: this.member ? this.member.toDTO() : undefined,
    }
  }
}

module.exports = Review
