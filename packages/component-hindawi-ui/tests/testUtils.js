import React from 'react'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { render as rtlRender, fireEvent, wait } from 'react-testing-library'

import { theme } from '../'

export const render = ui => {
  const Component = () => (
    <ModalProvider>
      <div id="ps-modal-root" />
      <ThemeProvider theme={theme}>{ui}</ThemeProvider>
    </ModalProvider>
  )

  const utils = rtlRender(<Component />)
  return {
    ...utils,
    selectOption: value => {
      fireEvent.click(utils.container.querySelector(`button[type=button]`))
      wait(() => utils.getByText(value))
      fireEvent.click(utils.getByText(value))
    },
  }
}
