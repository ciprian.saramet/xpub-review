import React from 'react'
import 'jest-dom/extend-expect'
import 'jest-styled-components'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from './testUtils'
import AuthorTagList from '../src/AuthorTagList'

const authors = [
  {
    id: 1,
    alias: {
      aff: 'University of California',
      email: 'john.doe@gmail.com',
      name: {
        surname: 'John',
        givenNames: 'Doe',
      },
    },
    affiliation: 'University of California',
  },
  {
    id: 2,
    alias: {
      aff: 'US Presidency',
      email: 'michael.felps@gmail.com',
      name: {
        surname: 'Michael',
        givenNames: 'Felps',
      },
    },
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    alias: {
      aff: 'US Presidency',
      email: 'barrack.obama@gmail.com',
      name: {
        surname: 'Barrack',
        givenNames: 'Obama',
      },
    },
  },
]

describe('AuthorTagList component', () => {
  afterEach(cleanup)

  it('should render all the authors without affiliations', () => {
    const { getAllByTestId, queryByText } = render(
      <AuthorTagList authors={authors} />,
    )

    const renderedAuthors = getAllByTestId(/author-tag-[0-9]/i).map(
      el => el.firstChild.textContent,
    )

    expect(queryByText(/show affiliations/i)).toBeNull()
    expect(renderedAuthors).toHaveLength(3)
    expect(renderedAuthors).toEqual([
      'Doe John',
      'Felps Michael',
      'Obama Barrack',
    ])
  })

  it('should toggle affiliations', () => {
    const { getByText, getAllByTestId, queryAllByTestId } = render(
      <AuthorTagList authors={authors} withAffiliations />,
    )

    fireEvent.click(getByText(/show affiliations/i))

    const renderedAffiliations = getAllByTestId(/affiliation-[0-9]/i).map(
      el => el.firstChild.textContent,
    )

    expect(renderedAffiliations).toHaveLength(2)
    expect(renderedAffiliations).toEqual([
      'University of California',
      'US Presidency',
    ])

    fireEvent.click(getByText(/hide affiliations/i))
    const hiddenAffiliations = queryAllByTestId(/affiliation-[0-9]/i)

    expect(hiddenAffiliations).toHaveLength(0)
  })
})
