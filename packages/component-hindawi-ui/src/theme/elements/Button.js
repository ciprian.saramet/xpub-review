import { space } from 'styled-system'
import { css } from 'styled-components'
import { darken, lighten, th } from '@pubsweet/ui-toolkit'

const primary = css`
  background-color: ${th('actionPrimaryColor')};
  border: none;
  color: ${th('white')};

  &:hover {
    background-color: ${lighten('actionPrimaryColor', 0.1)};
  }

  &:focus,
  &:active {
    background-color: ${th('actionPrimaryColor')};
    outline: none;
  }

  &[disabled] {
    border: none;
    &,
    &:hover {
      background-color: ${th('furnitureColor')};
    }
  }
`

const secondary = css`
  background: none;
  border: ${th('buttons.borderSize')} solid ${th('actionSecondaryColor')};
  border-radius: ${th('borderRadius')};
  color: ${th('actionSecondaryColor')};

  &:hover {
    background: none;
    border-color: ${darken('actionSecondaryColor', 0.2)};
  }

  &:focus,
  &:active {
    background: none;
    border-color: ${th('actionSecondaryColor')};
    outline: none;
  }

  &[disabled] {
    border-color: ${th('furnitureColor')};
    color: ${th('colorTextPlaceholder')};
    opacity: 0.3;

    &:hover {
      background: none;
    }
  }
`

const buttonWidth = props =>
  props.width
    ? css`
        min-width: calc(${th('gridUnit')} * ${props.width});
      `
    : css`
        width: 100%;
      `

const buttonSize = props => {
  if (props.small) {
    return css`
      height: calc(${th('gridUnit')} * 8);
      font-size: ${th('buttons.small.fontSize')};
      min-width: calc(${th('gridUnit')} * 32);
    `
  } else if (props.xs) {
    return css`
      height: calc(${th('gridUnit')} * 6);
      font-size: ${th('buttons.small.fontSize')};
      line-height: calc(
        ${th('gridUnit')} * 3 - ${th('buttons.borderSize')} * 2
      );
      min-width: calc(${th('gridUnit')} * ${th('button.smallWidth')});
    `
  }
  return css`
    font-size: ${th('buttons.default.fontSize')};
    height: calc(${th('gridUnit')} * ${th('buttons.default.height')});

    ${buttonWidth};
  `
}

export default css`
  align-items: center;
  cursor: pointer;
  display: flex;
  justify-content: center;
  font-family: ${th('defaultFont')};
  font-weight: 700;

  padding: 0 calc(${th('gridUnit')} * 2);

  &:hover,
  &:focus {
    transition: none;
  }

  ${props => (props.primary ? primary : secondary)};
  ${buttonSize};
  ${space};
`
