import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export default {
  Main: css`
    background: ${th('colorBackgroundHue')};
    height: calc(${th('gridUnit')} * 8);
    border-radius: ${th('borderRadius')};
    width: 100%;
  `,
  Value: css`
    border: none;
    font-family: ${th('defaultFont')};

    &:hover {
      color: ${th('colorSecondary')};
    }
  `,
  Placeholder: css`
    color: ${th('colorText')};
    font-style: normal;
    font-family: ${th('defaultFont')};
    font-weight: normal;
    padding: 0 calc(${th('gridUnit')} * 2);
  `,
  Opener: css`
    height: calc(${th('gridUnit')} * 8);
    width: 100%;

    &:hover {
      border-color: ${th('menu.hoverColor')};
    }

    &:active,
    &:focus {
      outline: none;
    }
  `,

  OptionsContainer: css`
    margin-top: calc(${th('gridUnit')} * 2);
  `,
  Options: css`
    background: ${th('colorBackgroundHue')};
    box-shadow: ${th('menu.openerShadow')};
  `,
  Option: css`
    align-items: center;
    border: none;
    color: ${th('colorText')};
    display: flex;
    font-family: ${th('defaultFont')};
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
    height: calc(${th('gridUnit')} * 8);
    padding: calc(${th('gridUnit')} * 4);

    &:hover {
      background-color: ${th('menu.optionBackground')};
    }
  `,
}
