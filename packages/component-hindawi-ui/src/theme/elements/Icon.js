import { css } from 'styled-components'
import { darken, th } from '@pubsweet/ui-toolkit'

const iconColor = ({ color, disabled }) => {
  if (disabled) {
    return css`
      stroke: ${th('colorTextPlaceholder')};
    `
  }

  if (color) {
    return css`
      stroke: color;
    `
  }

  return css`
    stroke: ${th('colorText')};
  `
}

export default css`
  & svg {
    ${iconColor}
    &:hover {
      stroke: ${props => darken(props.color, 0.1)};
    }
    &:active {
      stroke: ${props => darken(props.color, 0.2)};
    }
  }
  display: initial;
`
