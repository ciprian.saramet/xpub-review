A corresponding author.

```js
<Tag>CA</Tag>
```

An editor in chief.

```js
<Tag>EiC</Tag>
```

A tag used for a manuscript status.

```js
<Tag status>Pending Approval</Tag>
```
