import React, { Fragment } from 'react'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2, Button, Spinner } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers } from 'recompose'

import { Text, Row, Icon, withFetching } from '../../'

const FormModal = ({
  title,
  onClose,
  content,
  onSubmit,
  subtitle,
  isFetching,
  cancelText,
  confirmText,
  initialValues,
  fetchingError,
  renderContent,
  validate,
}) => (
  <Root>
    <Icon
      color="#939393"
      fontSize="24px"
      icon="remove1"
      onClick={onClose}
      right={8}
      secondary
      top={8}
    />
    <H2>{title}</H2>
    {subtitle && (
      <Text mb={2} secondary>
        {subtitle}
      </Text>
    )}
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validate}
    >
      {({ handleSubmit, ...formProps }) => (
        <Fragment>
          {renderContent(formProps)}
          {fetchingError && (
            <Text align="center" error>
              {fetchingError}
            </Text>
          )}

          <Row justify={isFetching ? 'center' : 'space-between'} mt={3}>
            {isFetching ? (
              <Spinner size={6} />
            ) : (
              <Fragment>
                <Button
                  data-test-id="modal-cancel"
                  onClick={onClose}
                  width={48}
                >
                  {cancelText}
                </Button>
                <Button
                  data-test-id="modal-confirm"
                  onClick={handleSubmit}
                  primary
                  width={48}
                >
                  {confirmText}
                </Button>
              </Fragment>
            )}
          </Row>
        </Fragment>
      )}
    </Formik>
  </Root>
)

FormModal.propTypes = {
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Subtitle that will be showed on the card. */
  subtitle: PropTypes.string,
  /** The text you want to see on the button when someone submit the report. */
  confirmText: PropTypes.string,
  /** The text you want to see on the button when someone cancel the report. */
  cancelText: PropTypes.string,
  /** Callback function fired when cancel confirmation card. */
  onCancel: PropTypes.func, // eslint-disable-line
  /** Callback function fired when confirm confirmation card. */
  onSubmit: PropTypes.func,
  /** When is true will show a spinner.  */
  onClose: PropTypes.func,
  /** Callback function fired when you want to close the card. */
  isFetching: PropTypes.bool,
  /** The component you want to show on the card. */
  content: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
}

FormModal.defaultProps = {
  title: '',
  subtitle: '',
  confirmText: 'SUBMIT',
  cancelText: 'CANCEL',
  onCancel: () => {},
  onSubmit: () => {},
  onClose: () => {},
  isFetching: false,
  content: () => {},
}

export default compose(
  withFetching,
  withHandlers({
    onSubmit: ({ onSubmit, ...props }) => values => {
      if (typeof onSubmit === 'function') {
        onSubmit(values, props)
      } else {
        props.hideModal()
      }
    },
    onClose: ({ onCancel, ...props }) => () => {
      if (typeof onCancel === 'function') {
        onCancel(props)
      }
      props.hideModal()
    },
    renderContent: ({ content, ...props }) => formProps => {
      if (!content) return null
      if (typeof content === 'object') {
        return content
      } else if (typeof content === 'function') {
        return content({ ...props, formProps })
      }
    },
  }),
  setDisplayName('FormModal'),
)(FormModal)

const Root = styled.div`
  align-items: center;
  background: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 10);
  width: calc(${th('gridUnit')} * 130);

  ${H2} {
    margin: 0;
    text-align: center;
  }
`
