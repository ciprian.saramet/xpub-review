import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import Downshift from 'downshift'
import styled, { css } from 'styled-components'
import { th, override, validationColor } from '@pubsweet/ui-toolkit'

import { useClientRect, Label, Icon } from '../'

const SearchableSelect = ({
  value,
  label,
  options,
  onChange,
  isLoading,
  noOptions,
  noResults,
  filterFunc,
  displayedOptions,
}) => {
  const [inputNode, inputPosition] = useClientRect()

  return (
    <Downshift
      defaultHighlightedIndex={0}
      itemToString={item => (item ? item.value : '')}
      onChange={onChange}
      selectedItem={value}
    >
      {({
        isOpen,
        inputValue,
        toggleMenu,
        selectedItem,
        getItemProps,
        getRootProps,
        getMenuProps,
        getInputProps,
        getLabelProps,
        clearSelection,
        highlightedIndex,
      }) => (
        <Root {...getRootProps()}>
          {label && (
            <Label as="label" {...getLabelProps()}>
              {label}
            </Label>
          )}
          <InputWrapper>
            <Input {...getInputProps()} ref={inputNode} />
            {inputValue ? (
              <StyledIcon
                fontSize="16px"
                icon="remove"
                onClick={clearSelection}
              />
            ) : (
              <StyledIcon fontSize="16px" icon="search" onClick={toggleMenu} />
            )}
          </InputWrapper>
          <Dropdown>
            <ItemList
              {...getMenuProps({
                isOpen,
                inputPosition,
                displayedOptions,
              })}
            >
              {isOpen
                ? renderOptions({
                    options,
                    isLoading,
                    noResults,
                    noOptions,
                    inputValue,
                    filterFunc,
                    selectedItem,
                    getItemProps,
                    highlightedIndex,
                  })
                : null}
            </ItemList>
          </Dropdown>
        </Root>
      )}
    </Downshift>
  )
}

SearchableSelect.propTypes = {
  displayedOptions: PropTypes.number,
  label: PropTypes.node,
  filterFunc: PropTypes.func,
  noOptions: PropTypes.node,
  noResults: PropTypes.node,
  options: PropTypes.arrayOf(PropTypes.object),
  onChange: PropTypes.func,
}

SearchableSelect.defaultProps = {
  displayedOptions: 5,
  label: '',
  filterFunc: (item, inputValue) =>
    !inputValue ||
    item.value.toLowerCase().includes(inputValue.toLowerCase()) ||
    (item.subtext &&
      item.subtext.toLowerCase().includes(inputValue.toLowerCase())),
  noOptions: 'No options available',
  noResults: 'No results found',
  onChange: () => {},
  options: [],
}

const renderOptions = ({
  options,
  isLoading,
  noResults,
  noOptions,
  inputValue,
  filterFunc,
  selectedItem,
  getItemProps,
  highlightedIndex,
}) => {
  if (isLoading) return <NoItems>Loading...</NoItems>
  if (!options.length) return <NoItems>{noOptions}</NoItems>

  const renderedOptions = options.filter(item => filterFunc(item, inputValue))

  return renderedOptions.length ? (
    renderedOptions.map((item, index) => (
      <Item
        {...getItemProps({
          key: item.value,
          index,
          item,
          isHighlighted: highlightedIndex === index,
        })}
      >
        <ItemLabel isSelected={selectedItem === item}>{item.value}</ItemLabel>
        {item.subtext && <ItemSubtext>{item.subtext}</ItemSubtext>}
      </Item>
    ))
  ) : (
    <NoItems>{noResults}</NoItems>
  )
}

const Dropdown = ({ children }) =>
  ReactDOM.createPortal(children, document.body)

export default SearchableSelect

// #region styles
const dropdownPosition = ({ inputPosition }) =>
  css`
    top: ${inputPosition.bottom}px;
    left: ${inputPosition.left}px;
    width: ${inputPosition.width}px;
  `

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  ${override('ui.TextField')};
`

const InputWrapper = styled.div`
  position: relative;
  width: 100%;
`

const Input = styled.input`
  box-sizing: border-box;
  width: 100%;
  height: calc(${th('gridUnit')} * 8);
  padding: 0;
  padding-left: calc(${th('gridUnit')});
  padding-right: calc(${th('gridUnit')} * 8);

  border: ${th('borderWidth')} ${th('borderStyle')} ${validationColor};
  border-radius: ${th('borderRadius')};

  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  color: ${th('textPrimaryColor')};
  ::placeholder {
    color: ${th('textPrimaryColor')};
    opacity: 1;
    font-family: ${th('defaultFont')};
    font-style: italic;
  }
  :focus {
    outline: none;
    border-color: ${th('action.colorActive')};
  }
`

const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
  right: 8px;

  color: ${th('textSecondaryColor')};
`

const ItemList = styled.ul`
  position: absolute;
  ${dropdownPosition}

  max-height: calc(
    ${th('gridUnit')} * 8 * ${({ displayedOptions }) => displayedOptions}
  );
  overflow: auto;
  padding: 0;
  margin: 0;
  margin-top: 2px;

  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-radius: ${th('borderRadius')};
  z-index: ${th('zIndex.select')};
  list-style-type: none;
  visibility: ${({ isOpen }) => (isOpen ? 'visible' : 'hidden')};
  box-shadow: 0 2px 6px -1px rgba(125, 125, 125, 0.5);
`

const Item = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;

  padding: 0 calc(${th('gridUnit')} * 4);
  height: calc(${th('gridUnit')} * 8);

  background: ${({ isHighlighted }) =>
    isHighlighted ? th('highlightColor') : th('white')};
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  cursor: pointer;
`

const ItemLabel = styled.span`
  margin-right: 16px;
  display: inline-block;
  align-items: center;

  color: ${th('textPrimaryColor')};
  font-weight: ${({ isSelected }) => (isSelected ? '700' : 'initial')};

  min-width: 0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const ItemSubtext = styled.span`
  display: flex;
  align-items: center;
  color: ${th('textSecondaryColor')};
`

const NoItems = styled.li`
  display: flex;
  align-items: center;
  padding: 0 calc(${th('gridUnit')} * 4);
  height: calc(${th('gridUnit')} * 8);

  color: ${th('textSecondaryColor')};
  background: ${th('white')};
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBaseMedium')};
  font-style: italic;
  cursor: default;
`
// #endregion
