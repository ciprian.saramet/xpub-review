import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withRouter } from 'react-router-dom'
import { withHandlers, compose } from 'recompose'

import { Icon } from '../'
import { displayHelper } from './styledHelpers'

const Breadcrumbs = ({ renderLink, handleClick, ...rest }) => (
  <Root {...rest} onClick={handleClick}>
    <Icon bold color="colorWarning" fontSize="14px" icon="bredcrumbs2" mr={1} />
    {renderLink(rest)}
  </Root>
)

Breadcrumbs.propTypes = {
  /** Link/URL specifying where to navigate, outside or inside the app.
   * If present the component will behave like a navigation link. */
  path: PropTypes.string,
  /** If true the component will be disabled (can't be interacted with). */
  disabled: PropTypes.bool,
}

Breadcrumbs.defaultProps = {
  path: '',
  disabled: false,
}

export default compose(
  withRouter,
  withHandlers({
    handleClick: ({ path, history }) => () =>
      path ? history.push(path) : history.goBack(),
    renderLink: ({ disabled, children, fontSize }) => () => (
      <Link disabled={disabled} fontSize={fontSize}>
        {children}
      </Link>
    ),
  }),
)(Breadcrumbs)

// #region styles
const Link = styled.a`
  cursor: pointer;
  font-family: ${th('defaultFont')};
  line-height: 1;
  font-size: ${props => (props.fontSize ? props.fontSize : '16px')};
  font-weight: ${props => (props.fontWeight ? 400 : 600)};
`

const Root = styled.div`
  color: ${props => (props.color ? props.color : 'colorText')};
  align-items: ${props => get(props, 'alignItems', 'center')};
  flex: ${props => props.flex || 'none'};
  justify-content: center;
  height: inherit;
  width: max-content;

  &:hover * {
    color: ${th('colorSecondary')};
  }

  ${displayHelper};
  ${space};
`
// #endregion
