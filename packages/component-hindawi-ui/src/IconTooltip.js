import React from 'react'
import PropTypes from 'prop-types'
import 'react-tippy/dist/tippy.css'
import { Tooltip } from 'react-tippy'
import { ThemeProvider, withTheme } from 'styled-components'

import { Icon } from '../'

const IconTooltip = ({
  icon,
  theme,
  primary,
  fontSize,
  interactive,
  content,
  ...rest
}) => (
  <Tooltip
    arrow
    data-test-id="icon-tooltip"
    html={<InfoTooltip content={content} theme={theme} {...rest} />}
    interactive={interactive}
    position="bottom"
    theme="light"
    trigger="click"
  >
    <Icon fontSize={fontSize} icon={icon} primary={primary} {...rest} />
  </Tooltip>
)

const InfoTooltip = ({ theme, content }) => (
  <ThemeProvider theme={theme}>
    {typeof content === 'function' ? content() : content}
  </ThemeProvider>
)
IconTooltip.propTypes = {
  /** What icon to be used. */
  icon: PropTypes.string,
  /** Size of the icon. */
  fontSize: PropTypes.string,
  /** What content to be used in tooltip. */
  content: PropTypes.func,
  /** If true the content can be clicked (can be interacted with). */
  interactive: PropTypes.bool,
}

IconTooltip.defaultProps = {
  icon: 'tooltip',
  fontSize: '14px',
  content: () => {},
  interactive: false,
}

export default withTheme(IconTooltip)
