A generic label.

```js
<Label>First name</Label>
```

A required label.

```js
<Label required>Email</Label>
```
