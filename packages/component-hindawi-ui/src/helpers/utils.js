import { get, chain, find } from 'lodash'

export const handleError = fn => e => {
  fn(get(JSON.parse(e.response), 'error', 'Oops! Something went wrong!'))
}

export const getReportComments = ({ report, type = 'public' }) =>
  chain(report)
    .get('comments', [])
    .find(c => c.type === type)
    .get('content')
    .value()

export const getShortRole = role => {
  switch (role) {
    case 'admin':
    case 'editorInChief':
      return 'EiC'
    case 'handlingEditor':
      return 'HE'
    case 'editorialAssistant':
      return 'EA'
    default:
      return ''
  }
}

export const indexReviewers = (reports = [], invitations = []) => {
  reports.forEach(report => {
    report.reviewerNumber = get(
      find(invitations, ['userId', report.userId]),
      'reviewerNumber',
      0,
    )
  })
  return reports
}

export const trimFormStringValues = values =>
  Object.entries(values).reduce(
    (acc, [key, value]) => ({
      ...acc,
      [key]: typeof value === 'string' ? value.trim() : value,
    }),
    {},
  )

export const parseSearchParams = url => {
  const params = new URLSearchParams(url)
  const parsedObject = {}
  /* eslint-disable */
  for (let [key, value] of params) {
    parsedObject[key] = value
  }
  /* eslint-enable */
  return parsedObject
}
