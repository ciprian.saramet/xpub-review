import React from 'react'
import styled from 'styled-components'
import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withHandlers,
  setDisplayName,
  withStateHandlers,
} from 'recompose'
import { Text, Icon } from '..'

const UserDropDown = ({
  goTo,
  role,
  logout,
  expanded,
  toggleMenu,
  username = '',
}) => (
  <Root>
    <User data-test-id="admin-menu-button" onClick={toggleMenu}>
      <Text>{username}</Text>
      <Icon
        fontSize="12px"
        icon={expanded ? 'caretUp' : 'caretDown'}
        ml={1}
        secondary
      />
    </User>
    {expanded && (
      <Dropdown data-test-id="admin-menu-dropdown">
        {role === 'admin' && (
          <DropdownOption
            data-test-id="admin-dropdown-dashboard"
            onClick={goTo('/admin')}
          >
            Admin Dashboard
          </DropdownOption>
        )}
        <DropdownOption
          data-test-id="admin-dropdown-profile"
          onClick={goTo('/profile')}
        >
          My Profile
        </DropdownOption>
        <DropdownOption data-test-id="admin-dropdown-logout" onClick={logout}>
          Logout
        </DropdownOption>
      </Dropdown>
    )}
    {expanded && <ToggleOverlay onClick={toggleMenu} />}
  </Root>
)

const getNames = obj => {
  const givenName = get(obj, 'identities[0].name.givenNames')
  const surname = get(obj, 'identities[0].name.surname')

  return givenName || surname
}

export default compose(
  withRouter,
  withStateHandlers(
    { expanded: false },
    { toggleMenu: ({ expanded }) => () => ({ expanded: !expanded }) },
  ),
  withProps(({ currentUser }) => ({
    username: getNames(currentUser),
    role: get(currentUser, 'role'),
  })),
  withHandlers({
    goTo: ({ toggleMenu, goTo }) => path => () => {
      toggleMenu()
      goTo(path)
    },
    logout: ({ logout, toggleMenu, goTo }) => () => {
      toggleMenu()
      logout()
    },
  }),
  setDisplayName('UserDropDown'),
)(UserDropDown)

const User = styled.div`
  align-items: center;
  display: flex;
  cursor: pointer;
`

const Root = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  margin-left: calc(${th('gridUnit')} * 2);
  position: relative;
`

const ToggleOverlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  opacity: 0;
`
const DropdownOption = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'dropdown-option',
}))`
  align-items: center;
  color: ${th('colorText')};
  cursor: pointer;
  display: flex;
  justify-content: flex-start;

  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};

  height: calc(${th('gridUnit')} * 8);
  padding: calc(${th('gridUnit')} * 2);

  &:hover {
    background-color: ${th('menu.optionBackground')};
  }
`
export const Dropdown = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'admin-dropdown',
}))`
  background-color: ${th('appBar.colorBackground')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};

  position: absolute;
  top: calc(${th('gridUnit')} * 8);
  width: calc(${th('gridUnit')} * 36);
  z-index: 10;
`
