export { default as ModalRoot } from './src/ModalRoot'
export { default as ModalContext } from './src/ModalContext'
export { default as ModalProvider } from './src/ModalProvider'

export { default as Modal } from './src/Modal' // render props implementation
export { default as useModal } from './src/useModal' // hooks!
export { default as withModal } from './src/withModal' // HOCs
