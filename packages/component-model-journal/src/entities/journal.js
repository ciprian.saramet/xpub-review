const { chain } = require('lodash')
const { HindawiBaseModel } = require('component-model')
const Team = require('component-model-team').model

class Journal extends HindawiBaseModel {
  static get tableName() {
    return 'journal'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        publisherName: { type: ['string', null] },
        issn: { type: ['string', null] },
        code: { type: 'string' },
        email: { type: 'string' },
        apc: { type: ['integer'] },
        isActive: { type: ['boolean', false] },
        activationDate: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'journal.id',
          to: 'manuscript.journalId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-team').model,
        join: {
          from: 'journal.id',
          to: 'team.journalId',
        },
      },
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-journal-article-type').model,
        join: {
          from: 'journal.id',
          to: 'journal_article_type.journalId',
        },
      },
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const { email } = json

    return { ...json, email: email.toLowerCase() }
  }

  getEditorInChief() {
    if (!this.teams) {
      throw new ValidationError('Teams are required')
    }
    const eicTeam = this.teams.find(t => t.role === Team.Role.editorInChief)
    return eicTeam ? eicTeam.members[0] : undefined
  }

  getEditorialAssistant() {
    if (!this.teams) {
      throw new Error('Teams are required')
    }
    const eaTeam = this.teams.find(t => t.role === Team.Role.editorialAssistant)
    return eaTeam ? eaTeam.members[0] : undefined
  }

  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  toDTO() {
    const eicMember = chain(this.teams)
      .find(t => t.role === Team.Role.editorInChief)
      .get('members.0')
      .value()

    return {
      ...this,
      editorInChief: eicMember ? eicMember.toDTO() : undefined,
      articleTypes: this.journalArticleTypes
        ? this.journalArticleTypes
            .map(jat => jat.articleType.toDTO())
            .sort((a, b) => (a.name > b.name ? -1 : 1))
        : [],
    }
  }
}

module.exports = Journal
