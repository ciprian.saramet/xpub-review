process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { activateUserUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Activate user from admin panel', () => {
  it('should return an error when the user id is invalid', async () => {
    const mockedModels = models.build(fixtures)
    const result = activateUserUseCase
      .initialize(mockedModels)
      .execute(chance.guid())

    return expect(result).rejects.toThrow()
  })
  it('should activate an user when the user id is valid', async () => {
    const user = fixtures.generateUser({ isActive: false })
    const mockedModels = models.build(fixtures)
    await activateUserUseCase.initialize(mockedModels).execute(user.id)

    expect(user.isActive).toBe(true)
  })
  it('should not return an error when the user is already active', async () => {
    const user = fixtures.generateUser({ isActive: true })
    const mockedModels = models.build(fixtures)
    const result = activateUserUseCase.initialize(mockedModels).execute(user.id)

    return expect(result).resolves.not.toThrow()
  })
})
