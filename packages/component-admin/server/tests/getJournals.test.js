const { getJournalsUseCase } = require('../src/use-cases')
const { models, fixtures } = require('fixture-service')

const Chance = require('chance')

const chance = new Chance()

describe('getJournals use-case', () => {
  const journals = [
    {
      name: chance.company(),
      code: chance.word().toUpperCase(),
      email: chance.email(),
      apc: chance.natural(),
      issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
        min: 999,
        max: 9999,
      })}`,
      articleTypes: [chance.guid(), chance.guid()],
      created: Date.now(),
      activationDate: Date.now() + 3454,
      isActive: false,
    },
    {
      name: 'Journal 2',
      code: 'HJ2',
      email: 'mihai.2@gmail.com',
      apc: 900,
      articleTypes: [chance.guid(), chance.guid()],
      created: Date.now() + 1,
      isActive: false,
    },
  ]
  const mockedModels = models.build(fixtures)
  journals.forEach(fixtures.generateJournal)

  it('should return an array', async () => {
    const res = await getJournalsUseCase.initialize(mockedModels).execute()
    expect(Array.isArray(res)).toBeTruthy()
  })

  it('should contain objects with journal fields', async () => {
    const res = await getJournalsUseCase.initialize(mockedModels).execute()
    expect(res).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: expect.any(String),
          code: expect.any(String),
          email: expect.any(String),
          issn: expect.any(String),
          articleTypes: expect.any(Array),
          apc: expect.any(Number),
          isActive: expect.any(Boolean),
          activationDate: expect.anything(),
        }),
      ]),
    )
  })

  it('should contain correct number of journals', async () => {
    const res = await getJournalsUseCase.initialize(mockedModels).execute()
    expect(res.length).toEqual(3)
  })

  it('should return journals in the correct order', async () => {
    const res = await getJournalsUseCase.initialize(mockedModels).execute()
    expect(res[0].name).toEqual(journals[1].name)
    expect(res[1].name).toEqual(journals[0].name)
  })
})
