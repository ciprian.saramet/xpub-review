process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const useCases = require('../src/use-cases')

const chance = new Chance()

const notificationService = {
  notifyUserAddedByAdmin: jest.fn(),
}

const generateInput = () => ({
  givenNames: chance.first(),
  surname: chance.first(),
  email: chance.email(),
  country: chance.country(),
  aff: chance.company(),
  title: chance.pickone(['mr', 'mrs', 'miss', 'ms', 'dr', 'prof']),
  role: 'user',
})

describe('Add users from admin panel', () => {
  let input

  beforeEach(async () => {
    input = generateInput()
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
  })

  it('should create a new user', async () => {
    const mockedModels = models.build(fixtures)
    const initialUsersLength = fixtures.users.length

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        notification: notificationService,
        models: mockedModels,
        useCases,
      })
      .execute(input)

    expect(initialUsersLength).toBeLessThan(fixtures.users.length)
  })
  it('should create an editorial assistant', async () => {
    const mockedModels = models.build(fixtures)
    input.role = mockedModels.Team.Role.editorialAssistant
    input.email = chance.email()

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        notification: notificationService,
        models: mockedModels,
        useCases,
      })
      .execute(input)

    const editorialAssistant = fixtures.identities.filter(
      identity => identity.email === input.email,
    )
    expect(editorialAssistant).toBeDefined()
  })

  it('should create a new user and add him in a team', async () => {
    const mockedModels = models.build(fixtures)
    input.role = mockedModels.Team.Role.handlingEditor
    input.email = chance.email()

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        notification: notificationService,
        models: mockedModels,
        useCases,
      })
      .execute(input)
    expect(fixtures.teams.find(team => team.role === input.role)).toBeDefined()
  })

  it('should return an error if the user already exists in the db', async () => {
    const mockedModels = models.build(fixtures)

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        notification: notificationService,
        models: mockedModels,
        useCases,
      })
      .execute(input)

    const initialUsersLength = fixtures.users.length

    try {
      await useCases.addUserFromAdminPanelUseCase
        .initialize({
          notification: notificationService,
          models: mockedModels,
          useCases,
        })
        .execute(input)
    } catch (e) {
      expect(initialUsersLength).toEqual(fixtures.users.length)
      expect(fixtures.users.length).toEqual(initialUsersLength)
    }
  })

  it('should return an error if the user already exists in the db (uppercase email)', async () => {
    const mockedModels = models.build(fixtures)

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        notification: notificationService,
        models: mockedModels,
        useCases,
      })
      .execute(input)

    const initialUsersLength = fixtures.users.length
    input.email = input.email.toUpperCase()

    try {
      await useCases.addUserFromAdminPanelUseCase
        .initialize({
          notification: notificationService,
          models: mockedModels,
          useCases,
        })
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('User already exists in the database.')
      expect(fixtures.users.length).toEqual(initialUsersLength)
    }
  })

  it('should create an eic and add him to a global team', async () => {
    const mockedModels = models.build(fixtures)
    input.role = mockedModels.Team.Role.editorInChief
    input.email = chance.email()

    await useCases.addUserFromAdminPanelUseCase
      .initialize({
        notification: notificationService,
        models: mockedModels,
        useCases,
      })
      .execute(input)
    expect(
      fixtures.teams.find(
        team => team.role === input.role && team.journalId === null,
      ),
    ).toBeDefined()
  })
})
