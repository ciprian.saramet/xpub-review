process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')

const useCases = require('../src/use-cases')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { editUserFromAdminPanelUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Edit users from admin panel', () => {
  it('should edit an user information', async () => {
    const input = {
      surname: chance.last(),
      givenNames: chance.first(),
      role: chance.pickone([
        'admin',
        'handlingEditor',
        'editorInChief',
        'editorialAssistant',
      ]),
    }
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)

    await editUserFromAdminPanelUseCase
      .initialize({ models: mockedModels, useCases })
      .execute({ input, id: user.id })

    expect(user.identities[0].surname).toEqual(input.surname)
  })

  it('should edit the user`s role', async () => {
    const journal = fixtures.journals[0]
    const { user } = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })
    const mockedModels = models.build(fixtures)

    await editUserFromAdminPanelUseCase
      .initialize({ models: mockedModels, useCases })
      .execute({ input: { role: 'admin' }, id: user.id })
    const members = fixtures.findTeamMembers(user.id)

    expect(members[0].team.role).toEqual('admin')
  })
  it('should edit from user role in editorial assistant', async () => {
    const user = fixtures.generateUser({}, { email: chance.email() })
    const mockedModels = models.build(fixtures)
    await editUserFromAdminPanelUseCase
      .initialize({ models: mockedModels, useCases })
      .execute({ input: { role: 'editorialAssistant' }, id: user.id })

    const members = fixtures.findTeamMembers(user.id)
    expect(members[0].team.role).toEqual('editorialAssistant')
  })
  it('should return an error when the user id is invalid', async () => {
    const input = {
      surname: chance.last(),
      givenNames: chance.first(),
      role: chance.pickone(['admin', 'handlingEditor', 'editorInChief']),
    }
    const mockedModels = models.build(fixtures)
    const result = editUserFromAdminPanelUseCase
      .initialize({ models: mockedModels, useCases })
      .execute({ input, id: chance.guid() })

    return expect(result).rejects.toThrow()
  })
})
