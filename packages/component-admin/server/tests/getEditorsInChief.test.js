process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const { getEditorsInChiefUseCase } = require('../src/use-cases')

describe('getEditorsInChief use-case', () => {
  it('should always return an array', async () => {
    const mockedModels = models.build(fixtures)

    const eics = await getEditorsInChiefUseCase
      .initialize(mockedModels)
      .execute()

    expect(Array.isArray(eics)).toEqual(true)
  })
  it('should not return inactive editors in chief', async () => {
    const mockedModels = models.build(fixtures)

    await dataService.createGlobalUser({
      fixtures,
      input: {},
      role: mockedModels.Team.Role.editorInChief,
    })

    const inactiveEIC = await dataService.createGlobalUser({
      fixtures,
      input: {},
      role: mockedModels.Team.Role.editorInChief,
    })

    inactiveEIC.user.isActive = false

    const eics = await getEditorsInChiefUseCase
      .initialize(mockedModels)
      .execute()

    expect(eics.length).toEqual(1)
    expect(eics[0].user.isActive).toEqual(true)
  })
})
