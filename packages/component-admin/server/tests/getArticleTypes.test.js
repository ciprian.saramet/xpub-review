const { getArticleTypesUseCase } = require('../src/use-cases')
const { models, fixtures } = require('fixture-service')

describe('getArticleTypes use-case', () => {
  const mockedModels = models.build(fixtures)

  it('should return an array', async () => {
    const res = await getArticleTypesUseCase.initialize(mockedModels).execute()
    expect(Array.isArray(res)).toBeTruthy()
  })

  it('should contain objects with article type names', async () => {
    const res = await getArticleTypesUseCase.initialize(mockedModels).execute()
    expect(res).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: expect.any(String),
        }),
      ]),
    )
  })

  it('should contain correct number of article types', async () => {
    const res = await getArticleTypesUseCase.initialize(mockedModels).execute()
    expect(res.length).toEqual(3)
  })
})
