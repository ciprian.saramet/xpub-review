process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { addJournalUseCase } = require('../src/use-cases')

const chance = new Chance()
const jobsService = {
  scheduleJournalActivation: jest.fn(),
}

const generateInput = () => ({
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: [chance.guid(), chance.guid()],
  isActive: false,
})

describe('Add journal use case', () => {
  let input

  beforeEach(async () => {
    input = generateInput()
  })

  it('creates a new journal', async () => {
    const mockedModels = models.build(fixtures)
    const { userId: adminId } = dataService.createGlobalUser({
      role: 'admin',
      fixtures,
    })

    const journal = await addJournalUseCase
      .initialize({ models: mockedModels })
      .execute({ input, adminId })

    expect(journal.code).toEqual(input.code)
    expect(journal.articleTypes).toBeDefined()
  })

  it('returns an error if the journal already exists', async () => {
    const mockedModels = models.build(fixtures)
    const { userId: adminId } = dataService.createGlobalUser({
      role: 'admin',
      fixtures,
    })

    await fixtures.generateJournal(input)

    try {
      const journal = await addJournalUseCase
        .initialize({ models: mockedModels })
        .execute({ input, adminId })

      expect(journal.code).toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual('Journal already exists.')
    }
  })
  it('returns an error if the apc is negative', async () => {
    const mockedModels = models.build(fixtures)
    const { userId: adminId } = dataService.createGlobalUser({
      role: 'admin',
      fixtures,
    })
    input.apc *= -1

    try {
      const journal = await addJournalUseCase
        .initialize({ models: mockedModels })
        .execute({ input, adminId })

      expect(journal.code).toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual(
        `Journal APC ${input.apc} cannot be a negative number.`,
      )
    }
  })
  it('creates an active journal if the activation date is set to today', async () => {
    const mockedModels = models.build(fixtures)
    const { userId: adminId } = dataService.createGlobalUser({
      role: 'admin',
      fixtures,
    })

    input.activationDate = new Date().toISOString()
    const journal = await addJournalUseCase
      .initialize({ models: mockedModels, jobsService })
      .execute({ input, userId: adminId })

    expect(jobsService.scheduleJournalActivation).not.toHaveBeenCalled()
    expect(journal.isActive).toBe(true)
  })
  it('creates an inactive journal when the activation date is in the future', async () => {
    const mockedModels = models.build(fixtures)
    const { userId: adminId } = dataService.createGlobalUser({
      role: 'admin',
      fixtures,
    })

    input.activationDate = new Date(chance.date({ year: 2069, string: true }))

    const journal = await addJournalUseCase
      .initialize({ models: mockedModels, jobsService })
      .execute({ input, userId: adminId })

    expect(journal.activationDate).toEqual(input.activationDate)
    expect(jobsService.scheduleJournalActivation).toHaveBeenCalledTimes(1)
    expect(journal.isActive).toBe(false)
  })
  it('returns an error if the eic team-member id is not found', async () => {
    const mockedModels = models.build(fixtures)
    input.eicId = 'invalid-id'
    const { userId: adminId } = dataService.createGlobalUser({
      role: 'admin',
      fixtures,
    })

    try {
      const journal = await addJournalUseCase
        .initialize({ models: mockedModels })
        .execute({ input, adminId })

      expect(journal.name).toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual(
        `Object not found: teamMembers with 'id' ${input.eicId}`,
      )
    }
  })
  it('creates a journal eic team if eic team-member id is provided', async () => {
    const mockedModels = models.build(fixtures)
    const eic = await dataService.createGlobalUser({
      fixtures,
      input: {},
      role: mockedModels.Team.Role.editorInChief,
    })
    const { userId: adminId } = dataService.createGlobalUser({
      role: 'admin',
      fixtures,
    })
    input.eicId = eic.id

    const journal = await addJournalUseCase
      .initialize({ models: mockedModels })
      .execute({ input, adminId })

    const journalEiCTeam = fixtures.teams.find(
      t =>
        t.role === mockedModels.Team.Role.editorInChief &&
        t.manuscriptId === null &&
        t.journalId === journal.id,
    )

    expect(journalEiCTeam).toBeDefined()
    expect(journalEiCTeam.members[0].user.id).toEqual(eic.user.id)
    expect(journal.editorInChief).toBeDefined()
  })
})
