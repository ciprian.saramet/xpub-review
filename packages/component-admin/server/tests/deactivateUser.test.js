process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { deactivateUserUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Deactivate user from admin panel', () => {
  it('should return an error when the user id is invalid', async () => {
    const mockedModels = models.build(fixtures)
    const result = deactivateUserUseCase
      .initialize(mockedModels)
      .execute({ id: chance.guid() })

    return expect(result).rejects.toThrow()
  })
  it('should deactivate an user when the user id is valid', async () => {
    let team
    team = fixtures.getTeamByRole('admin')
    if (!team) team = fixtures.generateTeam({ role: 'admin' })

    const admin = fixtures.generateUser({})
    const teamMember = fixtures.generateTeamMember({
      userId: admin.id,
      teamId: team.id,
    })
    team.members.push(teamMember)

    const user = fixtures.generateUser({ isActive: true })
    const mockedModels = models.build(fixtures)
    await deactivateUserUseCase
      .initialize(mockedModels)
      .execute({ id: user.id, userId: admin.id })

    expect(user.isActive).toBe(false)
  })
  it('should not return an error when the user is already inactive', async () => {
    const user = fixtures.generateUser({ isActive: false })
    const mockedModels = models.build(fixtures)
    const result = deactivateUserUseCase
      .initialize(mockedModels)
      .execute({ id: user.id })

    return expect(result).resolves.not.toThrow()
  })
})
