module.exports.initialize = ({ Job, Journal }) => {
  const jobHandler = async job => {
    try {
      const { journalId } = job.data
      const journal = await Journal.find(journalId)

      journal.updateProperties({ isActive: true })
      await journal.save()

      return job.done()
    } catch (e) {
      throw new Error(e)
    }
  }
  return {
    async scheduleJournalActivation({ journal, teamMemberId }) {
      const params = {
        journalId: journal.id,
      }

      await Job.schedule({
        params,
        executionDate: journal.activationDate,
        jobHandler,
        teamMemberId,
        journalId: journal.id,
      })
    },
  }
}
