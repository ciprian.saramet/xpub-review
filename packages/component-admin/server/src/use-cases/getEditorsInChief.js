const initialize = ({ Team }) => ({
  execute: async () => {
    const globalEICTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.editorInChief,
        manuscriptId: null,
        journalId: null,
      },
      eagerLoadRelations: 'members.user',
    })
    if (!globalEICTeam) return []
    return globalEICTeam.members
      .filter(m => m.user.isActive)
      .map(m => m.toDTO())
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
