const uuid = require('uuid')
const config = require('config')

const initialize = ({
  notification: notificationService,
  models: { User, Identity, Team, Journal },
  useCases,
}) => ({
  execute: async input => {
    const userIdentity = await Identity.findOneByEmail(input.email)
    if (userIdentity) {
      throw new ConflictError('User already exists in the database.')
    }

    const user = new User({
      defaultIdentity: 'local',
      isActive: true,
      isSubscribedToEmails: true,
      confirmationToken: uuid.v4(),
      unsubscribeToken: uuid.v4(),
      agreeTc: true,
    })
    await user.save()

    const identity = new Identity({
      type: 'local',
      isConfirmed: false,
      passwordHash: null,
      givenNames: input.givenNames,
      surname: input.surname,
      email: input.email,
      aff: input.aff,
      title: input.title,
      userId: user.id,
      country: input.country,
    })
    await identity.save()
    await user.assignIdentity(identity)

    const journal = await Journal.findOneBy({
      queryObject: { name: config.get('journal.name') },
      eagerLoadRelations: 'teams.members',
    })

    const editorialAssistant = journal.getEditorialAssistant()

    const useCasesTree = {
      admin: 'addAdminFromAdminPanelUseCase',
      editorInChief: 'addEiCFromAdminPanelUseCase',
      handlingEditor: 'addHEFromAdminPanelUseCase',
      editorialAssistant: 'addEAFromAdminPanelUseCase',
      user: null,
    }
    const useCase = useCasesTree[input.role]
    if (useCase) {
      await useCases[useCase]
        .initialize({ models: { Team } })
        .execute({ user, journal })
    } else await user.save()

    await notificationService.notifyUserAddedByAdmin({
      user,
      identity,
      role: input.role,
      editorialAssistant,
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
