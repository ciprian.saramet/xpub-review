const initialize = ({ User, TeamMember, Team }) => ({
  execute: async () => {
    const users = await User.findAll({ eagerLoadRelations: 'identities' })

    return Promise.all(
      users.map(async user => {
        const teamMember = await TeamMember.findByField('userId', user.id)
        const teams = await Promise.all(
          teamMember.map(async tm => Team.find(tm.teamId)),
        )

        const teamRoles = teams.map(team => team.role)

        const globalRole = teamRoles.find(role =>
          Team.GlobalRoles.includes(role),
        )

        user.role = globalRole || 'user'

        user.identities = [user.getDefaultIdentity()]
        return user.toDTO()
      }),
    )
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
