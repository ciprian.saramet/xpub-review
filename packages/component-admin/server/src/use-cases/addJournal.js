const { Promise } = require('bluebird')

const initialize = ({
  models: { Journal, User, Team, TeamMember, JournalArticleType },
  jobsService,
}) => ({
  execute: async ({ input, userId }) => {
    let journal = {}
    const {
      email,
      name,
      code,
      apc,
      issn,
      articleTypes,
      eicId,
      activationDate,
    } = input
    if (apc < 0) {
      throw new ConflictError(`Journal APC ${apc} cannot be a negative number.`)
    }

    journal = await Journal.findUniqueJournal({ code, name, email })
    if (journal) {
      throw new ConflictError(`Journal already exists.`)
    }

    journal = new Journal({ email, name, code, apc, issn, activationDate })
    await journal.save()
    if (isDateToday(activationDate)) {
      journal.updateProperties({ isActive: true })
      await journal.save()
    } else if (activationDate) {
      const user = await User.find(userId, 'teamMemberships.team')

      const admin = user.teamMemberships.find(
        tm => tm.team.role === Team.Role.admin,
      )
      if (!admin) {
        throw new Error(`User ${user.id} is not part of an admin team.`)
      }
      jobsService.scheduleJournalActivation({ journal, teamMemberId: admin.id })
    }

    if (eicId) {
      const eic = await TeamMember.find(eicId, 'user.identities')

      const journalEiCTeam = new Team({
        role: Team.Role.editorInChief,
        manuscriptId: null,
        journalId: journal.id,
      })
      await journalEiCTeam.save()

      const newEiC = journalEiCTeam.addMember(eic.user, {
        userId: eic.user.id,
        teamId: journalEiCTeam.id,
      })
      journal.assignTeam(journalEiCTeam)

      await newEiC.save()
      await journalEiCTeam.save()
    }

    await Promise.each(articleTypes, async articleTypeId => {
      const journalArticleType = new JournalArticleType({
        articleTypeId,
        journalId: journal.id,
      })
      await journalArticleType.save()
    })

    return journal.toDTO()
  },
})

const isDateToday = inputDate => {
  if (!inputDate) {
    return false
  }

  const date = new Date(inputDate)
  const today = new Date()
  if (
    date.getDay() === today.getDay() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
  ) {
    return true
  }

  return false
}

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
