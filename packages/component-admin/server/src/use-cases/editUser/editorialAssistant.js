const initialize = Team => ({
  execute: async ({ user, journal }) => {
    const eaTeam = await Team.createOrFind({
      queryObject: {
        role: Team.Role.editorialAssistant,
        manuscriptId: null,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
      options: { role: Team.Role.editorialAssistant, journalId: journal.id },
    })

    const newMember = eaTeam.addMember(user, {
      userId: user.id,
      teamId: eaTeam.id,
    })
    await newMember.save()
  },
})

module.exports = {
  initialize,
}
