const initialize = Team => ({
  execute: async ({ user, journal }) => {
    const heTeam = await Team.createOrFind({
      queryObject: {
        role: Team.Role.handlingEditor,
        manuscriptId: null,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
      options: { role: Team.Role.handlingEditor, journalId: journal.id },
    })

    const newMember = heTeam.addMember(user, {
      userId: user.id,
      teamId: heTeam.id,
    })
    await newMember.save()
  },
})

module.exports = {
  initialize,
}
