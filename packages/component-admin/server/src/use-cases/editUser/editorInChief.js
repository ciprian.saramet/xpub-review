const initialize = Team => ({
  execute: async ({ user }) => {
    const eicTeam = await Team.createOrFind({
      queryObject: {
        role: Team.Role.editorInChief,
        manuscriptId: null,
        journalId: null,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
      options: { role: Team.Role.editorInChief },
    })

    const newMember = eicTeam.addMember(user, {
      userId: user.id,
      teamId: eicTeam.id,
    })
    await newMember.save()
  },
})

module.exports = {
  initialize,
}
