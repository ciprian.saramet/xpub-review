const initialize = Team => ({
  execute: async ({ user, journal }) => {
    const adminTeam = await Team.createOrFind({
      queryObject: {
        role: Team.Role.admin,
        manuscriptId: null,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
      options: { role: Team.Role.admin, journalId: journal.id },
    })

    const newMember = adminTeam.addMember(user, {
      userId: user.id,
      teamId: adminTeam.id,
    })
    await newMember.save()
  },
})

module.exports = {
  initialize,
}
