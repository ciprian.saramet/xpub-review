const config = require('config')
const { omit, flatMap } = require('lodash')

const initialize = ({
  models: { User, TeamMember, Team, Journal },
  useCases,
}) => ({
  execute: async ({ input, id, userId }) => {
    const user = await User.find(id, 'identities')

    const localIdentity = user.getDefaultIdentity()
    const identityInput = { ...omit(input, ['role']) }
    localIdentity.updateProperties(identityInput)
    await localIdentity.save()

    const teamMembers = await TeamMember.findBy({ userId: id }, 'team.members')
    const teams = flatMap(teamMembers, tm => tm.team)
    const oldTeam = teams.find(
      team =>
        team.role !== Team.Role.author && team.role !== Team.Role.reviewer,
    )

    if (oldTeam && oldTeam.role !== input.role) {
      if (oldTeam.role === Team.Role.admin && userId === user.id) {
        throw new ConflictError('Admin role cannot be changed.')
      }
      if (
        oldTeam.role === Team.Role.editorialAssistant &&
        oldTeam.members.length === 1
      ) {
        throw new ConflictError(
          'One editorial assistant should remain in the system.',
        )
      } else {
        const teamMember = teamMembers.find(
          teamM => teamM.teamId === oldTeam.id,
        )
        await teamMember.delete()
      }
    }

    if (
      input.role !== 'user' &&
      ((oldTeam && oldTeam.role !== input.role) || !oldTeam)
    ) {
      const useCasesTree = {
        admin: 'editAdminUseCase',
        editorInChief: 'editEiCUseCase',
        handlingEditor: 'editHEUseCase',
        editorialAssistant: 'editEAUseCase',
      }

      const useCase = useCasesTree[input.role]
      const journal = await Journal.findOneBy({
        queryObject: { name: config.get('journal.name') },
      })
      await useCases[useCase].initialize(Team).execute({ user, journal })
    }
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
