const getUsersForAdminPanelUseCase = require('./getUsersForAdminPanel')
const addUserFromAdminPanelUseCase = require('./addUserFromAdminPanel')
const addAdminFromAdminPanelUseCase = require('./addUser/admin')
const addEAFromAdminPanelUseCase = require('./addUser/editorialAssistant')
const addEiCFromAdminPanelUseCase = require('./addUser/editorInChief')
const addHEFromAdminPanelUseCase = require('./addUser/handlingEditor')
const activateUserUseCase = require('./activateUser')
const deactivateUserUseCase = require('./deactivateUser')
const editUserFromAdminPanelUseCase = require('./editUserFromAdminPanel')
const addJournalUseCase = require('./addJournal')
const getJournalsUseCase = require('./getJournals')
const getEditorsInChiefUseCase = require('./getEditorsInChief')
const getArticleTypesUseCase = require('./getArticleTypes')
const editEiCUseCase = require('./editUser/editorInChief')
const editEAUseCase = require('./editUser/editorialAssistant')
const editAdminUseCase = require('./editUser/admin')
const editHEUseCase = require('./editUser/handlingEditor')

module.exports = {
  getUsersForAdminPanelUseCase,
  addUserFromAdminPanelUseCase,
  addAdminFromAdminPanelUseCase,
  addEAFromAdminPanelUseCase,
  addEiCFromAdminPanelUseCase,
  addHEFromAdminPanelUseCase,
  activateUserUseCase,
  deactivateUserUseCase,
  editUserFromAdminPanelUseCase,
  addJournalUseCase,
  getJournalsUseCase,
  getEditorsInChiefUseCase,
  getArticleTypesUseCase,
  editEiCUseCase,
  editEAUseCase,
  editAdminUseCase,
  editHEUseCase,
}
