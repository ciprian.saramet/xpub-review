const initialize = ({ models: { Team } }) => ({
  execute: async ({ user }) => {
    let globalEiCTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.editorInChief,
        manuscriptId: null,
        journalId: null,
      },
      eagerLoadRelations: 'members',
    })

    if (!globalEiCTeam) {
      globalEiCTeam = new Team({
        role: Team.Role.editorInChief,
        manuscriptId: null,
        journalId: null,
      })
      await globalEiCTeam.save()
    }

    const newEiC = globalEiCTeam.addMember(user, {
      userId: user.id,
      teamId: globalEiCTeam.id,
    })
    await newEiC.save()
    await globalEiCTeam.save()
  },
})

module.exports = {
  initialize,
}
