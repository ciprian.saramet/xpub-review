const initialize = ({ models: { Team } }) => ({
  execute: async ({ user, journal }) => {
    let journalAdminTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.admin,
        manuscriptId: null,
        journalId: journal.id,
      },
      eagerLoadRelations: 'members',
    })

    if (!journalAdminTeam) {
      journalAdminTeam = new Team({
        role: Team.Role.admin,
        manuscriptId: null,
        journalId: journal.id,
      })
      await journalAdminTeam.save()
    }

    const newAdmin = journalAdminTeam.addMember(user, {
      userId: user.id,
      teamId: journalAdminTeam.id,
    })
    await newAdmin.save()
    await journalAdminTeam.save()
  },
})

module.exports = {
  initialize,
}
