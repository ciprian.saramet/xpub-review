const initialize = ({ models: { Team } }) => ({
  execute: async ({ user, journal }) => {
    let journalEATeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.editorialAssistant,
        manuscriptId: null,
        journalId: journal.id,
      },
      eagerLoadRelations: 'members',
    })

    if (!journalEATeam) {
      journalEATeam = new Team({
        role: Team.Role.editorialAssistant,
        manuscriptId: null,
        journalId: journal.id,
      })
      await journalEATeam.save()
    }

    const newEA = journalEATeam.addMember(user, {
      userId: user.id,
      teamId: journalEATeam.id,
    })
    await newEA.save()
    await journalEATeam.save()
  },
})

module.exports = {
  initialize,
}
