const initialize = ({ models: { Team } }) => ({
  execute: async ({ user, journal }) => {
    let journalHETeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.handlingEditor,
        manuscriptId: null,
        journalId: journal.id,
      },
      eagerLoadRelations: 'members',
    })

    if (!journalHETeam) {
      journalHETeam = new Team({
        role: Team.Role.handlingEditor,
        manuscriptId: null,
        journalId: journal.id,
      })
      await journalHETeam.save()
    }

    const newHE = journalHETeam.addMember(user, {
      userId: user.id,
      teamId: journalHETeam.id,
    })
    await newHE.save()
    await journalHETeam.save()
  },
})

module.exports = {
  initialize,
}
