const config = require('config')

const journalName = config.get('journal.name')

const getEmailCopyFroUserAddedByAdmin = ({ role }) => {
  let paragraph
  let hasIntro = true
  const hasSignature = true
  switch (role) {
    case 'admin':
      hasIntro = false
      paragraph = `You have been invited to join Hindawi as an Administrator.
    Please confirm your account and set your account details by clicking on the link below.`
      break
    case 'editorInChief':
      paragraph = `You have been invited to join Hindawi as an Editor in Chief.
      Please confirm your account and set your account details by clicking on the link below.`
      break
    case 'editorialAssistant':
      paragraph = `You have been invited to join Hindawi as an Editorial Assistant.
      Please confirm your account and set your account details by clicking on the link below.`
      break
    case 'handlingEditor':
      paragraph = `You have been invited to become an Academic Editor for the journal ${journalName}.
      To begin performing your editorial duties, you will need to create an account on Hindawi’s review system.<br/><br/>
      Please confirm your account details by clicking on the link below.`
      break
    default:
      paragraph = `You have been invited to join Hindawi as a User.
    Please confirm your account and set your account details by clicking on the link below.`
  }

  return { paragraph, hasLink: true, hasIntro, hasSignature }
}

module.exports = {
  getEmailCopyFroUserAddedByAdmin,
}
