const { get } = require('lodash')
const config = require('config')
const { services } = require('helper-service')

const Email = require('@pubsweet/component-email-templating')

const { getEmailCopyFroUserAddedByAdmin } = require('./emailCopy')

const unsubscribeSlug = config.get('unsubscribe.url')

const { name: journalName, staffEmail } = config.get('journal')

module.exports = {
  async notifyUserAddedByAdmin({ user, identity, role, editorialAssistant }) {
    const resetPath = config.get('invite-reset-password.url')
    const baseUrl = config.get('pubsweet-client.baseUrl')
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const { paragraph, ...bodyProps } = getEmailCopyFroUserAddedByAdmin({
      role,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: identity.email,
        name: identity.surname ? `${identity.surname}` : '',
      },
      content: {
        subject: 'Confirm your account',
        ctaLink: services.createUrl(baseUrl, resetPath, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
          givenNames: get(identity, 'givenNames', ''),
          surname: get(identity, 'surname', ''),
          title: get(identity, 'title', ''),
          country: get(identity, 'country', ''),
          aff: get(identity, 'aff', ''),
          email: get(identity, 'email', ''),
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: editorialAssistantName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
}
