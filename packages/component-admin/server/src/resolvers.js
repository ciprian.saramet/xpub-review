const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')
const notification = require('./notifications/notification')
const JobsService = require('./jobs/jobs')

const resolvers = {
  Query: {
    async getUsersForAdminPanel(_, { input }, ctx) {
      return useCases.getUsersForAdminPanelUseCase.initialize(models).execute()
    },
    async getJournals(_, { input }, ctx) {
      return useCases.getJournalsUseCase.initialize(models).execute()
    },
    async getEditorsInChief(_, { input }, ctx) {
      return useCases.getEditorsInChiefUseCase.initialize(models).execute()
    },
    async getArticleTypes() {
      return useCases.getArticleTypesUseCase.initialize(models).execute()
    },
  },
  Mutation: {
    async addUserFromAdminPanel(_, { input }, ctx) {
      return useCases.addUserFromAdminPanelUseCase
        .initialize({ notification, models, useCases })
        .execute(input)
    },
    async activateUser(_, { id }, ctx) {
      return useCases.activateUserUseCase.initialize(models).execute(id)
    },
    async deactivateUser(_, { id }, ctx) {
      return useCases.deactivateUserUseCase
        .initialize(models)
        .execute({ id, userId: ctx.user })
    },
    async editUserFromAdminPanel(_, { id, input }, ctx) {
      return useCases.editUserFromAdminPanelUseCase
        .initialize({ models, useCases })
        .execute({ input, id, userId: ctx.user })
    },
    async addJournal(_, { input }, ctx) {
      const { Job, Journal } = models
      const jobsService = JobsService.initialize({ Job, Journal })

      return useCases.addJournalUseCase
        .initialize({ models, jobsService })
        .execute({ input, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
