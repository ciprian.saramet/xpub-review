import React, { Fragment, useState } from 'react'
import moment from 'moment'
import { Checkbox } from '@pubsweet/ui'
import { Label, DatePicker } from '@hindawi/ui'

const JournalActivation = ({ setFieldValue, value, onChange }) => {
  const [isButtonDisabled, setButtonDisabled] = useState(true)
  return (
    <Fragment>
      <Label pb={2}>
        <Checkbox
          checked={!isButtonDisabled}
          data-test-id="active-inactive-journal"
          label="Active for submissions"
          onChange={() => {
            setButtonDisabled(!isButtonDisabled)
            if (isButtonDisabled) {
              setFieldValue(
                'activationDate',
                new Date(moment(Date.now()).format('YYYY-MM-DD')).toISOString(),
              )
            } else {
              setFieldValue('activationDate', undefined)
            }
          }}
        />
      </Label>
      <DatePicker
        disabled={isButtonDisabled}
        onChange={onChange}
        value={value}
      />
    </Fragment>
  )
}
export default JournalActivation
