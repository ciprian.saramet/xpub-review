A journal with ISSN and APC.

```js
import JournalCard from './JournalCard'
const journal = {
  name: 'Chemistry and Alchemy',
  code: 'ITS_A_SHAM',
  email: 'dr.manivé@spitaluldeurgenteiasi.com',
  apc: 7398657,
  issn: '1337-7357',
  created: Date.now(),
}
;<JournalCard {...journal} />
```

A journal without ISSN.

```js
import JournalCard from './JournalCard'
const journal = {
  name: 'Heart Surgery Journal',
  code: 'HSJ',
  email: 'dr.manivé@spitaluldeurgenteiasi.com',
  apc: 7398657,
  issn: undefined,
  created: Date.now(),
}
;<JournalCard {...journal} />
```

A journal without APC.

```js
import JournalCard from './JournalCard'
const journal = {
  name: 'Social Sciences & Identity Politics',
  code: 'IT_IS_BULLSHIT',
  email: 'dr.manivé@spitaluldeurgenteiasi.com',
  apc: undefined,
  issn: '1337-7357',
  created: Date.now(),
}
;<JournalCard {...journal} />
```

A journal with stupidly long everything.

```js
import JournalCard from './JournalCard'
const journal = {
  name:
    'My Journal Name Is So Long It Will Probably Fuck Up Your Journal Card And Fuck You',
  code: 'MJNISLIWPFUYJCMJNISLIWPFUYJCMJNISLIWPFUYJC',
  email:
    'iamaverysmartprofessorthatgothisphdfromharvard@harvardbusinessschool.com',
  apc: 7398657,
  issn: '1337-7357',
  created: Date.now(),
}
;<JournalCard {...journal} />
```
