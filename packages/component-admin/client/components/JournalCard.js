import React, { Fragment } from 'react'
import moment from 'moment'
import { H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { ShadowedBox, Text, Row, Label, Item } from '@hindawi/ui'

import { get, isNumber } from 'lodash'

const getName = alias =>
  `${get(alias, 'name.givenNames', '')} ${get(alias, 'name.surname', '')}`

const formatAPC = apc =>
  apc
    .toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    })
    .replace(/,/g, ' ')

const JournalCard = ({
  id,
  name,
  code,
  email,
  issn,
  apc,
  ghost,
  articleTypes,
  activationDate,
  editorInChief,
  isActive,
}) => (
  <CustomShadowedBox
    data-test-id={`journal-${id}`}
    ghost={ghost}
    justify="center"
    mb={4}
  >
    <Row alignItems="baseline" justify="left" mb={4}>
      <CustomText ellipsis title={code}>
        {code}
      </CustomText>
      <DotSeparator />
      <WithEllipsis>
        <CustomH3 display="inline-block" ellipsis title={name}>
          {name}
        </CustomH3>
      </WithEllipsis>
    </Row>
    <Row alignItems="baseline" justify="left" mb={4}>
      {isActive ? (
        <Fragment>
          <Text customId fontWeight={700}>
            ACTIVE
          </Text>
          <Text pl={2}>{`since ${moment(activationDate).format(
            'YYYY-MM-DD',
          )}`}</Text>
        </Fragment>
      ) : (
        <Text error fontWeight={700}>
          INACTIVE
        </Text>
      )}
      {activationDate && !isActive && (
        <Text pl={2}>
          {`until ${moment(activationDate).format('YYYY-MM-DD')}`}
        </Text>
      )}
    </Row>

    {articleTypes && articleTypes.length > 0 && (
      <Row justify="left">
        <Item display="inline-block" mb={4} vertical>
          <Label mb={2}>Article Types</Label>
          {articleTypes.map(articleType => (
            <Fragment key={`${id}-${articleType.id}`}>
              <TextWithSeparator mr={2} title={articleType.name}>
                {articleType.name}
              </TextWithSeparator>
            </Fragment>
          ))}
        </Item>
      </Row>
    )}
    <Row justify="space-between">
      <WithEllipsis flex={2} vertical>
        <Label mb={1}>Email</Label>
        <Text ellipsis mr={2} title={email}>
          {email}
        </Text>
      </WithEllipsis>

      <Item alignItems="center" justify="flex-end">
        {editorInChief && (
          <Fragment>
            <Item alignItems="flex-end" flex="auto" vertical>
              <Label mb={2}>Editor in Chief</Label>
              <Text whiteSpace="nowrap">{getName(editorInChief.alias)}</Text>
            </Item>
            <VerticalSeparator />
          </Fragment>
        )}
        {issn && (
          <Item alignItems="flex-end" flex={0} vertical>
            <Label mb={2}>ISSN</Label>
            <Text whiteSpace="nowrap">{issn}</Text>
          </Item>
        )}
        {issn && isNumber(apc) && <VerticalSeparator />}
        {isNumber(apc) && (
          <Item alignItems="flex-end" flex={0} vertical>
            <Label mb={2}>APC</Label>
            <Text whiteSpace="nowrap">{formatAPC(apc)}</Text>
          </Item>
        )}
      </Item>
    </Row>
  </CustomShadowedBox>
)

export default JournalCard

// #region styles
const CustomShadowedBox = styled(ShadowedBox)`
  flex: 1 1 calc(${th('gridUnit')} * 144);
  visibility: ${props => (props.ghost ? 'hidden' : 'visible')};

  @media only screen and (min-width: 1328px) {
    flex: 1 1 49%;

    :nth-child(2n + 1) {
      margin-right: calc(${th('gridUnit')} * 2);
    }

    :nth-child(2n) {
      margin-left: calc(${th('gridUnit')} * 2);
    }
  }
`

const WithEllipsis = styled(Item)`
  display: inline-grid;
`

const CustomText = styled(Text)`
  max-width: 25%;
`

const TextWithSeparator = styled(Text)`
  border-right: 1px solid ${th('colorFurniture')};
  padding: 0 calc(${th('gridUnit')} * 2);

  :first-of-type {
    padding-left: 0;
  }

  :last-of-type {
    border: none;
  }
`

const CustomH3 = styled(H3)`
  display: inline-block;
`

const VerticalSeparator = styled.div`
  border-left: 1px solid ${th('colorFurniture')};
  height: calc(${th('gridUnit')} * 8);
  margin: 0px calc(${th('gridUnit')} * 2);
`

const DotSeparator = styled.div`
  align-self: center;
  background-color: ${th('heading.h2Color')};
  border-radius: 50%;
  height: ${th('gridUnit')};
  margin: 0px calc(${th('gridUnit')} * 2);
  width: ${th('gridUnit')};
`
// #endregion
