import React from 'react'
import { get } from 'lodash'
import { Query } from 'react-apollo'
import { SearchableSelect } from '@hindawi/ui'

import { getEditorsInChief } from '../graphql/queries'

const getName = alias =>
  `${get(alias, 'name.givenNames', '')} ${get(alias, 'name.surname', '')}`

const EICSelect = ({ onChange, value }) => {
  const handleChange = value => onChange(value ? `${value.id}` : '')

  return (
    <Query query={getEditorsInChief}>
      {({ data, loading }) => {
        const eicList = get(data, 'getEditorsInChief', []).map(
          ({ id, alias }) => ({
            id,
            value: getName(alias),
            subtext: `${get(alias, 'email', '')}`,
          }),
        )
        const selected = eicList.find(eic => eic.id === value)

        return (
          <SearchableSelect
            isLoading={loading}
            label="Assign Editor in Chief"
            onChange={handleChange}
            options={eicList}
            value={selected || ''}
          />
        )
      }}
    </Query>
  )
}

export default EICSelect
