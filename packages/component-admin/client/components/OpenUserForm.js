import React from 'react'

import { OpenModal, ActionLink, IconButton } from '@hindawi/ui'

import AdminUserForm from './AdminUserForm'

const OpenUserForm = ({ edit, user, modalKey, onClick }) => (
  <OpenModal
    component={AdminUserForm}
    edit={edit}
    modalKey={modalKey}
    user={user}
  >
    {showModal =>
      edit ? (
        <IconButton fontIcon="editIcon" iconSize={4} onClick={onClick} />
      ) : (
        <ActionLink icon="plus" onClick={onClick}>
          ADD USER
        </ActionLink>
      )
    }
  </OpenModal>
)

export default OpenUserForm
