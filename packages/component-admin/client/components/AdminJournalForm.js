import React, { Fragment } from 'react'
import { get } from 'lodash'
import { TextField } from '@pubsweet/ui'
import { compose, withHandlers } from 'recompose'
import {
  Row,
  Item,
  Label,
  FormModal,
  validators,
  ValidatedFormField,
} from '@hindawi/ui'
import APCField from './APCField'
import EICSelect from './EICSelect'
import ArticleTypes from './ArticleTypes.js'
import JournalActivation from './JournalActivation.js'

const validate = values => {
  const errors = {}

  if (get(values, 'articleTypes', []).length === 0) {
    errors.articleTypes = 'Required'
  }
  return errors
}

const AdminJournalForm = ({
  hideModal,
  isFetching,
  onClose,
  onSubmit,
  onConfirm,
}) => (
  <FormModal
    cancelText="CANCEL"
    confirmText="ADD JOURNAL"
    content={FormFields}
    hideModal={hideModal}
    isFetching={isFetching}
    onCancel={onClose}
    onConfirm={onConfirm}
    onSubmit={onSubmit}
    title="Add Journal"
    validate={validate}
  />
)

const FormFields = ({ formProps }) => (
  <Fragment>
    <Row mt={6}>
      <Item justify="flex-start" vertical>
        <Label pb={2} required>
          Journal Name
        </Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="journal-name-input"
          inline
          name="name"
          validate={[validators.required]}
        />
      </Item>
    </Row>
    <Row>
      <Item mr={4} vertical>
        <Label pb={2} required>
          Journal Code
        </Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="code-input"
          inline
          name="code"
          validate={[validators.required, validators.alphaNumericValidator]}
        />
      </Item>

      <Item maxWidth={24} mr={4} vertical>
        <Label pb={2}>ISSN</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="issn-input"
          inline
          name="issn"
          validate={[validators.issnValidator]}
        />
      </Item>
      <Item maxWidth={24} vertical>
        <Label pb={2} required>
          APC
        </Label>
        <ValidatedFormField
          component={APCField}
          data-test-id="apc-input"
          name="apc"
          validate={[validators.required, validators.apcValidator]}
        />
      </Item>
    </Row>
    <Row>
      <Item mr={8} vertical width={32}>
        <Label required>Email</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="email-input"
          inline
          name="email"
          validate={[validators.required, validators.emailValidator]}
        />
      </Item>
      <Item flex={1} vertical>
        <ValidatedFormField
          component={JournalActivation}
          name="activationDate"
          setFieldValue={formProps.setFieldValue}
        />
      </Item>
    </Row>
    <Row>
      <Item mb={6}>
        <ValidatedFormField component={EICSelect} name="eicId" />
      </Item>
    </Row>

    <Row>
      <Item flex="auto" vertical>
        <Label required>Article Types</Label>
        <ArticleTypes {...formProps} />
      </Item>
      <Item />
    </Row>
  </Fragment>
)

export default compose(
  withHandlers({
    onSubmit: ({ onConfirm, ...props }) => (values, props) => {
      let { apc } = values
      apc = apc && Number(apc.replace(/\s/g, ''))
      onConfirm({ ...values, apc }, props)
    },
    onClose: ({ onCancel }) => props => {
      if (typeof onCancel === 'function') {
        onCancel(props)
      }
      props.hideModal()
    },
  }),
)(AdminJournalForm)
