import React, { Fragment } from 'react'
import { FieldArray } from 'formik'
import { graphql } from 'react-apollo'
import { get, isString } from 'lodash'
import { Checkbox, Spinner } from '@pubsweet/ui'
import { compose, withHandlers } from 'recompose'
import { withFetching, Row, Text } from '@hindawi/ui'

import { getArticleTypes } from '../graphql/queries'

const ArticleTypes = ({
  isFetching,
  data,
  onChange,
  setFieldValue,
  errors,
  submitCount,
}) => (
  <Fragment>
    {isFetching ? (
      <Spinner />
    ) : (
      <FieldArray name="articleTypes">
        {({ form }) => {
          const articleTypesList = get(form, 'values.articleTypes', [])
          const articleTypes = get(data, 'getArticleTypes', [])
          return (
            <Row
              data-test-id="article-types-box"
              justify="space-between"
              mt={2}
            >
              {articleTypes.map(articleType => (
                <Checkbox
                  checked={articleTypesList.find(at => at === articleType.id)}
                  key={articleType.id}
                  label={articleType.name}
                  name={articleType.name}
                  onChange={onChange({
                    articleType,
                    articleTypesList,
                    setFieldValue,
                  })}
                  value={articleTypesList.find(at => at === articleType.id)}
                />
              ))}
            </Row>
          )
        }}
      </FieldArray>
    )}
    {submitCount > 0 &&
      get(errors, 'articleTypes', false) &&
      isString(errors.articleTypes) && (
        <Row justify="flex-start">
          <Text alert small>
            {errors.articleTypes}
          </Text>
        </Row>
      )}
  </Fragment>
)

export default compose(
  withFetching,
  graphql(getArticleTypes),
  withHandlers({
    onChange: () => ({
      articleType,
      articleTypesList,
      setFieldValue,
    }) => () => {
      const isSelected = articleTypesList.find(x => x === articleType.id)
      if (isSelected) {
        articleTypesList = articleTypesList.filter(x => x !== articleType.id)
        setFieldValue('articleTypes', articleTypesList)
      } else {
        articleTypesList.push(articleType.id)
        setFieldValue('articleTypes', articleTypesList)
      }
    },
  }),
)(ArticleTypes)
