import React, { Fragment } from 'react'
import { get } from 'lodash'
import { TextField } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers, withProps } from 'recompose'
import {
  FormModal,
  Item,
  ItemOverrideAlert,
  Label,
  Menu,
  MenuCountry,
  Row,
  RowOverrideAlert,
  ValidatedFormField,
  validators,
  withRoles,
} from '@hindawi/ui'

// #region helpers

const validate = values => {
  const errors = {}

  if (get(values, 'email', '') === '') {
    errors.email = 'Required'
  }

  return errors
}
// #endregion

const FormFields = ({ edit, roles, titles }) => (
  <Fragment>
    {!edit && (
      <Row alignItems="baseline" mb={4} mt={6}>
        <ItemOverrideAlert mr={2} vertical>
          <Label required>Email</Label>
          <ValidatedFormField
            component={TextField}
            data-test-id="email-input"
            inline
            name="email"
            validate={[validators.emailValidator]}
          />
        </ItemOverrideAlert>

        <ItemOverrideAlert data-test-id="role-dropdown" ml={2} vertical>
          <Label required>Role</Label>
          <ValidatedFormField component={Menu} name="role" options={roles} />
        </ItemOverrideAlert>
      </Row>
    )}
    <Row mb={4} mt={edit && 4}>
      <Item mr={2} vertical>
        <Label>First Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="first-name-input"
          inline
          name="givenNames"
        />
      </Item>

      <Item ml={2} vertical>
        <Label>Last Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="last-name-input"
          inline
          name="surname"
        />
      </Item>
    </Row>
    <RowOverrideAlert alignItems="center" mb={4}>
      <ItemOverrideAlert data-test-id="title-dropdown" mr={2} vertical>
        <Label>Title</Label>
        <ValidatedFormField component={Menu} name="title" options={titles} />
      </ItemOverrideAlert>

      <ItemOverrideAlert data-test-id="country" ml={2} vertical>
        <Label>Country</Label>
        <ValidatedFormField
          component={MenuCountry}
          data-test-id="country-dropdwon"
          name="country"
        />
      </ItemOverrideAlert>
    </RowOverrideAlert>
    <Row mb={2}>
      {edit && (
        <ItemOverrideAlert data-test-id="role-dropdown" mr={2} vertical>
          <Label required>Role</Label>
          <ValidatedFormField component={Menu} name="role" options={roles} />
        </ItemOverrideAlert>
      )}
      <Item ml={edit && 2} vertical>
        <Label>Affiliation</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="affiliation-input"
          inline
          name="aff"
        />
      </Item>
    </Row>
  </Fragment>
)

const AdminUserForm = ({
  cancelText = 'Cancel',
  confirmText = 'OK',
  edit,
  hideModal,
  initialValues,
  onClose,
  onSubmit,
  roles,
  title,
  titles,
  user,
}) => (
  <FormModal
    cancelText={cancelText}
    confirmText={confirmText}
    content={FormFields}
    edit={edit}
    hideModal={hideModal}
    initialValues={initialValues}
    onCancel={onClose}
    onSubmit={onSubmit}
    roles={roles}
    subtitle={edit ? get(user, 'email', '') : ''}
    title={title}
    titles={titles}
    validate={validate}
  />
)

export default compose(
  withRoles,
  withProps(({ user, edit }) => ({
    initialValues: {
      ...user,
      title: get(user, 'title') === null ? undefined : get(user, 'title'),
      surname: get(user, 'surname') || '',
      givenNames: get(user, 'givenNames') || '',
      country: get(user, 'country') || '',
      aff: get(user, 'aff') || '',
      role: get(user, 'role') || 'user',
    },
    confirmText: edit ? 'EDIT USER' : 'SAVE USER',
    title: edit ? 'Edit User' : 'Add User',
  })),
  withHandlers({
    onSubmit: ({ onConfirm }) => (values, props) => {
      onConfirm(values, props)
    },
    onClose: ({ onCancel }) => props => {
      if (typeof onCancel === 'function') {
        onCancel(props)
      }
      props.hideModal()
    },
  }),
  setDisplayName('AdminUserForm'),
)(AdminUserForm)
