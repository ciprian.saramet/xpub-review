import gql from 'graphql-tag'
import { userFragment, journalFragment } from './fragments'

export const getUsersForAdminPanel = gql`
  query {
    getUsersForAdminPanel {
      ...userDetails
    }
  }
  ${userFragment}
`
export const currentUser = gql`
  query {
    currentUser {
      id
      role
    }
  }
`

export const getJournals = gql`
  query {
    getJournals {
      ...journalFragment
    }
  }
  ${journalFragment}
`

export const getEditorsInChief = gql`
  query {
    getEditorsInChief {
      id
      alias {
        name {
          givenNames
          surname
        }
        email
      }
    }
  }
`

export const getArticleTypes = gql`
  query {
    getArticleTypes {
      id
      name
    }
  }
`
