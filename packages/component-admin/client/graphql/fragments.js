import gql from 'graphql-tag'

export const userFragment = gql`
  fragment userDetails on User {
    id
    isActive
    role
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        email
        aff
        isConfirmed
        country
      }
    }
  }
`

export const journalFragment = gql`
  fragment journalFragment on Journal {
    id
    name
    code
    issn
    apc
    email
    activationDate
    articleTypes {
      id
      name
    }
    editorInChief {
      id
      alias {
        name {
          givenNames
          surname
        }
      }
    }
    isActive
  }
`
