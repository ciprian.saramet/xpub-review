import gql from 'graphql-tag'

export const addUserFromAdminPanel = gql`
  mutation addUserFromAdminPanel($input: UserInput) {
    addUserFromAdminPanel(input: $input) {
      token
    }
  }
`

export const editUserFromAdminPanel = gql`
  mutation editUserFromAdminPanel($id: ID!, $input: EditUserInput!) {
    editUserFromAdminPanel(id: $id, input: $input)
  }
`

export const activateUser = gql`
  mutation activateUser($id: ID!) {
    activateUser(id: $id)
  }
`
export const deactivateUser = gql`
  mutation deactivate($id: ID!) {
    deactivateUser(id: $id)
  }
`

export const addJournal = gql`
  mutation addJournal($input: AddJournalInput!) {
    addJournal(input: $input) {
      id
    }
  }
`
