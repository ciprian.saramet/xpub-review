import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as queries from './queries'
import * as mutations from './mutations'

export default compose(
  graphql(queries.getArticleTypes),
  graphql(queries.getJournals),
  graphql(mutations.addJournal, {
    name: 'addJournal',
    options: {
      refetchQueries: [{ query: queries.getJournals }],
    },
  }),
)
