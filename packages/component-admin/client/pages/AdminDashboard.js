import React, { Fragment } from 'react'
import { H1 } from '@pubsweet/ui'
import { Row, IconCard } from '@hindawi/ui'

import withUsersGQL from '../graphql'

const AdminDashboard = ({ history }) => (
  <Fragment>
    <H1 justify="flex-start" mt={4} pl={24}>
      Admin dashboard
    </H1>
    <Row justify="flex-start" mt={4} pl={24}>
      <IconCard
        data-test-id="journal-configuration-button"
        icon="edit"
        iconSize={12}
        label="Journal configuration"
        mr={6}
        onClick={() => history.push('/admin/journals')}
      />
      <IconCard
        icon="users"
        iconSize={12}
        label="Users"
        onClick={() => history.push('/admin/users')}
      />
    </Row>
  </Fragment>
)

export default withUsersGQL(AdminDashboard)
