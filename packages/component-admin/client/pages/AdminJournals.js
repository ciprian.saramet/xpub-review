import React from 'react'
import { last } from 'lodash'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers } from 'recompose'
import { Row, Item, Breadcrumbs, withFetching } from '@hindawi/ui'

import { Journals, AdminJournalForm } from '../components/'
import withJournals from '../graphql/withJournalsGQL'

const JournalsPage = ({ addJournal, data: { getJournals }, isFetching }) => (
  <Root>
    <Row alignItems="center" justify="space-between" mb={10}>
      <Item alignItems="center">
        <Breadcrumbs mr={4} path="/admin">
          Admin Dashboard
        </Breadcrumbs>
      </Item>
    </Row>
    <Row mb={6}>
      <Item alignItems="center" justify="flex-end">
        <Modal
          component={AdminJournalForm}
          isFetching={isFetching}
          modalKey="addJournal"
          onConfirm={addJournal}
        >
          {showModal => (
            <Button
              data-test-id="add-journal"
              onClick={showModal}
              primary
              small
              width={36}
            >
              ADD JOURNAL
            </Button>
          )}
        </Modal>
      </Item>
    </Row>
    <Journals journals={getJournals} />
  </Root>
)

const parseError = e => last(e.message.split(':')).trim()
export default compose(
  withFetching,
  withJournals,
  withHandlers({
    addJournal: ({ addJournal }) => (
      input,
      { setFetching, hideModal, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      addJournal({
        variables: {
          input,
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(error => {
          setFetching(false)
          setError(parseError(error))
        })
    },
  }),
)(JournalsPage)

const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 4);
`
