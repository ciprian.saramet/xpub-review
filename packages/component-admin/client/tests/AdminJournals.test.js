import React from 'react'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { fireEvent } from 'react-testing-library'
import { render } from './testUtils'
import Journals from '../pages/AdminJournals'

describe('Admin journal page', () => {
  it('Should open modal after click: "Add journal"', () => {
    const addJournal = jest.fn()
    const { getByText } = render(<Journals addJournal={addJournal} />)
    const addJournalButton = getByText('ADD JOURNAL')
    expect(addJournalButton).toBeInTheDocument()

    fireEvent.click(addJournalButton)
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()
    expect(getByText('CANCEL')).toBeInTheDocument()
  })
})
