import React from 'react'
import 'jest-dom/extend-expect'
import { theme } from '@hindawi/ui'
import { createMemoryHistory } from 'history'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { render as rtlRender } from 'react-testing-library'
import { MockedProvider } from 'react-apollo/test-utils'
import { BrowserRouter as Router } from 'react-router-dom'
import { getArticleTypes, getEditorsInChief } from '../graphql/queries'

import { articleTypes, editorsInChief } from './mockData'

const mocks = [
  {
    request: {
      query: getArticleTypes,
      variables: {},
    },
    result: {
      data: {
        getArticleTypes: articleTypes,
      },
    },
  },
  {
    request: {
      query: getEditorsInChief,
      variables: {},
    },
    result: {
      data: {
        getEditorsInChief: editorsInChief,
      },
    },
  },
]

export const render = (
  ui,
  {
    route = '/',
    history = createMemoryHistory({ initialEntries: [route] }),
  } = {},
) => {
  const Component = () => (
    <Router>
      <MockedProvider addTypename={false} mocks={mocks}>
        <ModalProvider>
          <div id="ps-modal-root" />
          <ThemeProvider theme={theme}>{ui}</ThemeProvider>
        </ModalProvider>
      </MockedProvider>
    </Router>
  )
  const utils = rtlRender(<Component />)
  return {
    ...utils,
    history,
  }
}
