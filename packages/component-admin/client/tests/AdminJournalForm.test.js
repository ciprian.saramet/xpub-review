import React from 'react'
import moment from 'moment'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { cleanup, fireEvent, waitForElement } from 'react-testing-library'
import Chance from 'chance'
import { render } from './testUtils'
import AdminJournalForm from '../components/AdminJournalForm'
import { articleTypes, editorsInChief } from './mockData'

const chance = new Chance()

const journalInput = {
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: articleTypes[2],
  eic: {
    id: editorsInChief[1].id,
    name: `${editorsInChief[1].alias.name.givenNames} ${
      editorsInChief[1].alias.name.surname
    }`,
    email: editorsInChief[1].alias.email,
  },
}

describe('Add journal', () => {
  let onConfirmMock
  let container

  beforeEach(() => {
    cleanup()
    onConfirmMock = jest.fn()
    container = render(<AdminJournalForm onConfirm={onConfirmMock} />)
  })

  it('Should throw error if "Journal Name" input is invalid', async done => {
    const { getByText } = container
    expect(getByText('Journal Name')).toBeInTheDocument()
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()
    fireEvent.click(getByText('ADD JOURNAL'))
    setTimeout(() => {
      expect(getByText('Required')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should throw error if "Journal Code" input is invalid', async done => {
    const { getByText } = render(<AdminJournalForm />)

    expect(getByText('Journal Code')).toBeInTheDocument()
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()
    fireEvent.click(getByText('ADD JOURNAL'))

    setTimeout(() => {
      expect(getByText('Required')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should throw error if "ISSN" input is invalid', async done => {
    const { getByText, getByTestId } = render(<AdminJournalForm />)

    expect(getByText('ISSN')).toBeInTheDocument()
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()
    fireEvent.change(getByTestId('issn-input'), {
      target: { value: chance.email() },
    })
    fireEvent.click(getByText('ADD JOURNAL'))

    setTimeout(() => {
      expect(getByText('Invalid')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should throw "Required" error if "APC" input is null', async done => {
    const { getByText } = render(<AdminJournalForm />)

    expect(getByText('APC')).toBeInTheDocument()
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()
    fireEvent.click(getByText('ADD JOURNAL'))

    setTimeout(() => {
      expect(getByText('Required')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should throw "Invalid number" error if "APC" input is invalid', async done => {
    const { getByText, getByTestId } = render(<AdminJournalForm />)

    expect(getByText('APC')).toBeInTheDocument()
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()
    fireEvent.click(getByText('ADD JOURNAL'))
    fireEvent.change(getByTestId('apc-input'), {
      target: { value: chance.word() },
    })

    setTimeout(() => {
      expect(getByText('Invalid number')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('Should click the active submission and see the current date', async done => {
    const { getByText, getByTestId, getByLabelText } = render(
      <AdminJournalForm />,
    )
    const currentDate = moment().format('YYYY-MM-DD')

    expect(getByText('Active for submissions')).toBeInTheDocument()
    fireEvent.click(getByLabelText('Active for submissions'))

    setTimeout(() => {
      expect(getByTestId('calendar-button')).toBeInTheDocument()
      expect(getByTestId('date-field')).toBeInTheDocument()
      expect(getByText(currentDate)).toBeInTheDocument()
      done()
    })
  })

  it('Should click the calendar button and select a new date', async done => {
    const { getByText, getByTestId, getByLabelText } = render(
      <AdminJournalForm />,
    )
    const dueDay = moment()
      .add(1, 'day')
      .format('DD')
    const newDate = moment()
      .add(1, 'day')
      .format('YYYY-MM-DD')

    expect(getByText('Active for submissions')).toBeInTheDocument()
    const calendarCheckbox = getByLabelText('Active for submissions')
    const calendarButton = getByTestId('calendar-button')
    fireEvent.click(calendarCheckbox)
    fireEvent.click(calendarButton)

    expect(getByText(dueDay)).toBeInTheDocument()
    fireEvent.click(getByText(dueDay))

    setTimeout(() => {
      expect(calendarButton).toBeInTheDocument()
      expect(getByTestId('date-field')).toBeInTheDocument()
      expect(getByText(newDate)).toBeInTheDocument()
      done()
    })
  })

  it('Should throw error if "Email" input is invalid', async done => {
    const { getByText } = render(<AdminJournalForm />)

    expect(getByText('Email')).toBeInTheDocument()
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()
    fireEvent.click(getByText('ADD JOURNAL'))

    setTimeout(() => {
      expect(getByText('Required')).toBeInTheDocument()
      expect(onConfirmMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  // eslint-disable-next-line jest/no-disabled-tests
  it.skip('Should call submit handler if form is valid ', async done => {
    const {
      getByText,
      getByTestId,
      findByLabelText,
      getByLabelText,
      findByText,
    } = container
    expect(getByText('ADD JOURNAL')).toBeInTheDocument()

    fireEvent.change(getByTestId('journal-name-input'), {
      target: { value: journalInput.name },
    })
    fireEvent.change(getByTestId('code-input'), {
      target: { value: journalInput.code },
    })
    fireEvent.change(getByTestId('issn-input'), {
      target: { value: journalInput.issn },
    })
    fireEvent.change(getByTestId('apc-input'), {
      target: { value: journalInput.apc },
    })

    fireEvent.change(getByLabelText('Assign Editor in Chief'), {
      target: { value: journalInput.eic.email },
    })

    const dropdownSelection = await waitForElement(() =>
      findByText(journalInput.eic.name),
    )
    fireEvent.click(dropdownSelection)

    fireEvent.change(getByTestId('email-input'), {
      target: { value: journalInput.email },
    })
    const articleType = await waitForElement(() =>
      findByLabelText(journalInput.articleTypes.name),
    )
    fireEvent.click(articleType)

    fireEvent.click(getByText('ADD JOURNAL'))

    setTimeout(() => {
      expect(onConfirmMock).toHaveBeenCalledWith(
        {
          name: journalInput.name,
          code: journalInput.code,
          issn: journalInput.issn,
          apc: journalInput.apc,
          email: journalInput.email,
          articleTypes: [journalInput.articleTypes.id],
          eicId: journalInput.eic.id,
        },
        expect.anything(),
      )
      done()
    })
  }, 30000)
})
