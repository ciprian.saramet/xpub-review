import React from 'react'
import 'jest-dom/extend-expect'
import { theme } from '@hindawi/ui'
import { ThemeProvider } from 'styled-components'
import { cleanup, fireEvent, render } from 'react-testing-library'

import { WizardButtons } from '../components'

describe('Wizard buttons', () => {
  afterEach(() => {
    cleanup()
  })

  it('should press buttons', () => {
    const prevStepMock = jest.fn()
    const handleSubmitMock = jest.fn()

    const { getByText, queryByText } = render(
      <ThemeProvider theme={theme}>
        <WizardButtons
          handleSubmit={handleSubmitMock}
          prevStep={prevStepMock}
        />
      </ThemeProvider>,
    )
    fireEvent.click(getByText(/back/i))
    expect(prevStepMock).toHaveBeenCalledTimes(1)

    fireEvent.click(getByText(/next/i))
    expect(handleSubmitMock).toHaveBeenCalledTimes(1)

    expect(queryByText(/submit/i)).toBeNull()
  })

  it('should show spinner', () => {
    const { getByText, queryByText } = render(
      <ThemeProvider theme={theme}>
        <WizardButtons isFetching />
      </ThemeProvider>,
    )

    expect(getByText('', { selector: 'svg' })).toBeInTheDocument()
    expect(queryByText(/next|submit|back/i)).toBeNull()
  })
})
