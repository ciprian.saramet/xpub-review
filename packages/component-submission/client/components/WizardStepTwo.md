The first of the submission wizard.

```js
const { Formik } = require('formik')
const { ShadowedBox } = require('component-hindawi-ui')

const journal = {
  submission: {
    links: {
      authorGuidelines: 'https://www.hindawi.com/journals/bca/guidelines/',
      articleProcessing: 'https://www.hindawi.com/journals/bca/apc/',
    },
  },
}

;<Formik>
  {formikProps => (
    <ShadowedBox center pl={4} pr={4} width={90}>
      <WizardStepTwo journal={journal} />
    </ShadowedBox>
  )}
</Formik>
```
