import React, { Fragment } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Checkbox, H2 } from '@pubsweet/ui'
import { Row, Text, Bullet, ActionLink, ValidatedFormField } from '@hindawi/ui'

const validateCheckbox = value => (!value ? 'Required' : undefined)
const WizardStepTwo = ({ journal }) => (
  <Fragment>
    <H2>2. Pre-Submission Checklist</H2>
    <Row mb={8}>
      <Text align="center" mb={2} mt={2} secondary>
        Before continuing please make sure you have reviewed the items on the
        list below:
      </Text>
    </Row>
    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />
      <Text bullet display="inline" ml={4}>
        I am aware that accepted manuscripts are subject to an
        <ActionLink
          display="inline"
          pl={1}
          pr={1}
          to={get(journal, 'submission.links.articleProcessing', '')}
        >
          Article Processing Charge of $1,250.
        </ActionLink>
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />

      <Text bullet display="inline" ml={4}>
        All co-authors have read and agreed on the current version of this
        manuscript.
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />

      <Text bullet display="inline" ml={4}>
        I have the email addresses of all co-authors of the manuscript.
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />

      <Text bullet display="inline" ml={4}>
        I confirm the main manuscript file is in Microsoft Word or Adobe PDF
        format with the tables and figures integrated in the manuscript body.
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />

      <Text bullet display="inline" ml={4}>
        I have all additional electronic files of supplementary materials (e.g.
        datasets, images, audio, video) ready.
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />

      <Text bullet display="inline" ml={4}>
        I am aware that an
        <ActionLink display="inline" pl={1} pr={1} to="https://orcid.org/">
          ORCID
        </ActionLink>
        is required for the corresponding author before the article can be
        published (if accepted). The ORCID should be added via your user
        account.
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />

      <Text bullet display="inline" ml={4}>
        I am aware that if my submission is covered by an
        <ActionLink
          display="inline"
          pl={1}
          pr={1}
          to="https://about.hindawi.com/institutions/"
        >
          institutional membership
        </ActionLink>
        then Hindawi will share details of the manuscript with the administrator
        of the membership.
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mb={2}>
      <Bullet />

      <Text bullet display="inline" ml={4}>
        I have read the journal’s
        <ActionLink
          display="inline"
          pl={1}
          pr={1}
          to="https://www.hindawi.com/journals/bca/guidelines/"
        >
          Author Submission Guidelines.
        </ActionLink>
      </Text>
    </Row>

    <Row alignItems="center" justify="center" mt={12}>
      <div>
        <ValidatedFormField
          component={CustomCheckbox}
          name="meta.agreeTc"
          validate={[validateCheckbox]}
        />
      </div>
    </Row>
  </Fragment>
)

const CustomCheckbox = input => (
  <RootCheckbox data-test-id="agree-checkbox">
    <Checkbox
      checked={input.value}
      {...input}
      label="I have reviewed and understood all of the above."
    />
  </RootCheckbox>
)

export default WizardStepTwo

// #region styled-components
const RootCheckbox = styled.div.attrs(props => ({
  className: 'custom-checkbox',
}))`
  + div[role='alert'] {
    margin-top: 0;
  }
  & label {
    margin-bottom: ${th('gridUnit')};
    & span {
      color: ${th('colorText')};
      font-family: 'Nunito';
      font-size: ${th('fontSizeBase')};
    }
  }
`
// #endregion
