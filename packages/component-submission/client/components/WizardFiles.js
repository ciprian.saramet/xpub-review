import React, { Fragment } from 'react'
import { get } from 'lodash'

import FileSection from './FileSection'

const WizardFiles = ({
  files,
  compact,
  onPreview,
  onUploadFile,
  onChangeList,
  onDeleteFile,
}) => (
  <Fragment>
    <FileSection
      allowedFileExtensions={['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']}
      compact={compact}
      files={get(files, `manuscript`, []).filter(Boolean)}
      isFirst
      listName="manuscript"
      maxFiles={1}
      onChangeList={onChangeList}
      onDeleteFile={onDeleteFile}
      onUploadFile={onUploadFile}
      required
      title="Main Manuscript"
    />
    <FileSection
      allowedFileExtensions={['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']}
      compact={compact}
      files={get(files, `coverLetter`, []).filter(Boolean)}
      listName="coverLetter"
      maxFiles={1}
      onChangeList={onChangeList}
      onDeleteFile={onDeleteFile}
      onUploadFile={onUploadFile}
      title="Cover Letter"
    />
    <FileSection
      compact={compact}
      files={get(files, `supplementary`, []).filter(Boolean)}
      isLast
      listName="supplementary"
      maxFiles={Number.MAX_SAFE_INTEGER}
      onChangeList={onChangeList}
      onDeleteFile={onDeleteFile}
      onUploadFile={onUploadFile}
      title="Supplemental Files"
    />
  </Fragment>
)

export default WizardFiles
