Submission wizard step three.

```js
const { Formik } = require('formik')

const initialValues = {
  files: {
    manuscripts: [
      { id: 'file1', name: 'file1.pdf', size: 123 },
      { id: 'file2', name: 'file2.pdf', size: 45677 },
    ],
    coverLetter: [{ id: 'file3', name: 'file3.pdf', size: 6542 }],
    supplementary: [],
  },
}

;<Formik initialValues={initialValues}>
  {({ values, setFieldValue }) => (
    <WizardStepFour
      onDeleteFile={(file, props) => {
        props.setFetching(true)
        console.log('delete the file', file)
      }}
      setFieldValue={setFieldValue}
      formValues={values}
    />
  )}
</Formik>
```

Submission wizard step three with errors.

```js
const { Formik } = require('formik')

const initialValues = {
  files: {
    manuscripts: [
      { id: 'file1', name: 'file1.pdf', size: 123 },
      { id: 'file2', name: 'file2.pdf', size: 45677 },
    ],
    coverLetter: [{ id: 'file3', name: 'file3.pdf', size: 6542 }],
    supplementary: [],
  },
}

const wizardErrors = {
  fileError: 'Oops! Something wrong happened...',
}

;<Formik initialValues={initialValues}>
  {({ values, setFieldValue }) => (
    <WizardStepFour
      formValues={values}
      onDeleteFile={(file, props) => {
        props.setFetching(true)
        console.log('delete the file', file)
      }}
      wizardErrors={wizardErrors}
      setFieldValue={setFieldValue}
    />
  )}
</Formik>
```
