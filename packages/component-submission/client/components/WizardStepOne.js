import React, { Fragment } from 'react'
import {
  Row,
  Item,
  Menu,
  Label,
  Text,
  ValidatedFormField,
  validators,
} from '@hindawi/ui'
import { H2 } from '@pubsweet/ui'

const WizardStepOne = ({ activeJournals }) => {
  const journals = activeJournals.map(({ id, name }) => ({
    value: id,
    label: name,
  }))

  return (
    <Fragment>
      <H2>1. Select Journal</H2>
      <Row mb={10}>
        <Text align="center" mb={2} mt={2} secondary>
          Please select a Journal
        </Text>
      </Row>
      <Row>
        <Item data-test-id="journal-select" vertical>
          <Label mb={1} required>
            Journal
          </Label>
          <ValidatedFormField
            component={Menu}
            name="journalId"
            options={journals}
            placeholder="Select journal"
            validate={[validators.required]}
          />
        </Item>
      </Row>
    </Fragment>
  )
}

export default WizardStepOne
