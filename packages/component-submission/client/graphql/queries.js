import gql from 'graphql-tag'

import { draftManuscriptDetails } from './fragments'

export const getManuscriptAndActiveJournals = gql`
  query getManuscript($manuscriptId: String!) {
    getManuscript(manuscriptId: $manuscriptId) {
      ...draftManuscriptDetails
    }
    getActiveJournals {
      id
      name
    }
  }
  ${draftManuscriptDetails}
`

export const autosaveState = gql`
  query autosaveState {
    autosave @client {
      error
      updatedAt
      inProgress
    }
  }
`
