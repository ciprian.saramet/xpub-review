import gql from 'graphql-tag'
import { fragments as fileFragments } from 'component-files/client'

export const teamMember = gql`
  fragment teamMember on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
    }
    status
    alias {
      aff
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`

export const draftManuscriptDetails = gql`
  fragment draftManuscriptDetails on Manuscript {
    id
    journalId
    submissionId
    meta {
      abstract
      articleType
      title
      agreeTc
      conflicts {
        hasConflicts
        message
        hasDataAvailability
        dataAvailabilityMessage
        hasFunding
        fundingMessage
      }
    }
    status
    authors {
      ...teamMember
    }
    files {
      ...fileDetails
    }
  }
  ${teamMember}
  ${fileFragments.fileDetails}
`
