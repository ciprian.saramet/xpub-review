import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import { Redirect } from 'react-router'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { withModal } from 'component-modal'
import { Steps, Spinner } from '@pubsweet/ui'
import { compose, withHandlers, withProps } from 'recompose'

import { withGQL as withFilesGQL } from 'component-files/client'

import { MultiAction, withSteps, withFetching } from '@hindawi/ui'

import withSubmissionGQL from '../graphql'
import {
  autosaveForm,
  setInitialValues,
  validateWizard,
  parseError,
} from '../utils'
import { SubmissionStatement, WizardButtons, wizardSteps } from '../components'

const Wizard = ({
  step,
  theme,
  journal,
  history,
  onSubmit,
  prevStep,
  showModal,
  isFetching,
  autosaveForm,
  onEditAuthor,
  onSaveAuthor,
  initialValues,
  onDeleteAuthor,
  permissionError,
  data: { loading },
  editMode,
  ...rest
}) => {
  if (permissionError) {
    return <Redirect to="/404" />
  }
  if (loading) {
    return <Spinner />
  }
  return (
    <Fragment>
      <Steps currentStep={step}>
        {wizardSteps.map(({ stepTitle }) => (
          <Steps.Step key={stepTitle} title={stepTitle} />
        ))}
      </Steps>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validate={validateWizard(step)}
      >
        {({ errors, handleSubmit, setFieldValue, values }) => (
          <Root isFirst={step === 0}>
            {autosaveForm(values)}

            {React.createElement(wizardSteps[step].component, {
              journal,
              setFieldValue,
              formValues: values,
              wizardErrors: errors,
              onEditAuthor: onEditAuthor({ values, setFieldValue }),
              onSaveAuthor: onSaveAuthor({ values, setFieldValue }),
              onDeleteAuthor: onDeleteAuthor({ values, setFieldValue }),
              ...rest,
            })}

            <WizardButtons
              editMode={editMode}
              handleSubmit={handleSubmit}
              history={history}
              isFetching={isFetching}
              isFirst={step === 0}
              isLast={step === wizardSteps.length - 1}
              prevStep={prevStep}
            />
          </Root>
        )}
      </Formik>
    </Fragment>
  )
}
// #region compose
export default compose(
  withSteps,
  withJournal,
  withFetching,
  withSubmissionGQL,
  withFilesGQL(),
  withModal({
    component: MultiAction,
    modalKey: 'submission-wizard',
  }),
  withProps(({ data: { getManuscript } }) => ({
    initialValues: setInitialValues(getManuscript),
    editMode: get(getManuscript, 'status', 'draft') !== 'draft',
    manuscriptStatus: get(getManuscript, 'status'),
  })),
  withProps(({ data, manuscriptStatus }) => ({
    permissionError:
      manuscriptStatus === 'accepted' || manuscriptStatus === 'rejected',
    activeJournals: get(data, 'getActiveJournals', []),
  })),
  withHandlers({
    // autosave
    autosaveForm: ({ match, updateDraft, updateAutosave }) => values => {
      autosaveForm({
        values,
        updateDraft,
        updateAutosave,
      })
    },
    onUploadFile: ({ match, uploadFile }) => (
      file,
      { type, push, setFetching, setError, ...rest },
    ) => {
      const manuscriptId = get(match, 'params.manuscriptId', '')

      const fileInput = {
        type,
        size: file.size,
      }
      if (fileInput.size > 50000000) {
        setError('File must not be bigger than 50MB.')
        return
      }

      setFetching(true)
      uploadFile({ entityId: manuscriptId, fileInput, file })
        .then(uploadedFile => {
          setFetching(false)
          push(uploadedFile)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    onDeleteFile: ({ deleteFile }) => (
      file,
      { index, remove, setError, setFetching },
    ) => {
      setFetching(true)
      deleteFile(file.id)
        .then(() => {
          setFetching(false)
          remove(index)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    onChangeList: ({ updateManuscriptFile }) => ({
      fileId,
      sourceProps,
      toListName: type,
      destinationProps,
    }) => {
      updateManuscriptFile({
        variables: {
          fileId,
          type,
        },
      })
        .then(r => {
          const file = r.data.updateManuscriptFile
          sourceProps.remove(sourceProps.index)
          destinationProps.push(file)
        })
        .catch(e => {
          destinationProps.setError(parseError(e))
        })
    },
    onDeleteAuthor: ({ match, removeAuthor }) => ({
      values,
      setFieldValue,
    }) => (
      { id: authorTeamMemberId },
      { setError, clearError, setEditMode, setFetching, setWizardEditMode },
    ) => {
      const manuscriptId = get(match, 'params.manuscriptId', '')
      clearError()
      setFetching(true)
      removeAuthor({
        variables: { manuscriptId, authorTeamMemberId },
      })
        .then(r => {
          setFetching(false)
          setEditMode(false)
          setFieldValue('authors', r.data.removeAuthorFromManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    onEditAuthor: ({ match, editAuthor }) => ({ values, setFieldValue }) => (
      { id: authorTeamMemberId, ...authorInput },
      { setError, clearError, setEditMode, setFetching, setWizardEditMode },
    ) => {
      const manuscriptId = get(match, 'params.manuscriptId', '')
      clearError()
      setFetching(true)
      editAuthor({
        variables: { manuscriptId, authorInput, authorTeamMemberId },
      })
        .then(r => {
          setFetching(false)
          setWizardEditMode(false)
          setFieldValue('authors', r.data.editAuthorFromManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    onSaveAuthor: ({ addAuthorToManuscript, match }) => ({
      values,
      setFieldValue,
    }) => (
      { id, ...authorInput },
      {
        index,
        formFns,
        setError,
        clearError,
        setEditMode,
        setFetching,
        setWizardEditMode,
      },
    ) => {
      clearError()
      setFetching(true)
      const manuscriptId = get(match, 'params.manuscriptId')
      addAuthorToManuscript({
        variables: { manuscriptId, authorInput },
      })
        .then(r => {
          setFetching(false)
          setWizardEditMode(false)
          setFieldValue('authors', r.data.addAuthorToManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    onSubmit: ({
      step,
      history,
      editMode,
      nextStep,
      showModal,
      setFetching,
      submitManuscript,
    }) => (values, formikBag) => {
      if (step !== wizardSteps.length - 1) {
        if (step === 2 && get(values, 'authors', []).length === 0) {
          formikBag.setFieldError('authors', 'At least one author is required.')
        } else {
          nextStep()
          formikBag.resetForm(values)
        }
      } else if (!editMode) {
        showModal({
          title:
            'By submitting the manuscript, you agree to the following statements:',
          content: SubmissionStatement,
          confirmText: 'AGREE & SUBMIT',
          cancelText: 'BACK TO SUBMISSION',
          onConfirm: ({ hideModal, setFetching, setError }) => {
            setFetching(true)
            submitManuscript({
              variables: { values },
            })
              .then(r => {
                setFetching(false)
                hideModal()
                history.push('/confirmation-page')
              })
              .catch(e => {
                setFetching(false)
                setError(parseError(e))
              })
          },
        })
      } else {
        history.goBack()
      }
    },
  }),
)(Wizard)
// #endregion

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  margin: 0 auto;
  margin-top: calc(${th('gridUnit')} * 10);
  margin-bottom: calc(${th('gridUnit')} * 10);

  padding: calc(${th('gridUnit')} * 10);

  width: calc(${th('gridUnit')} * ${({ isFirst }) => (isFirst ? 148 : 280)});
`
// #endregion
