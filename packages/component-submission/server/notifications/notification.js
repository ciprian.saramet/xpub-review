const config = require('config')
const { services } = require('helper-service')
const Email = require('@pubsweet/component-email-templating')
const { get } = require('lodash')

const { getEmailCopy } = require('./emailCopy')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const resetPath = config.get('invite-reset-password.url')
const { name: journalName, staffEmail } = config.get('journal')

module.exports = {
  async sendToConfirmedAuthors(
    { id, alias, user },
    { manuscript, submittingAuthor, editorialAssistant },
  ) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'confirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const email = new Email({
      type: 'user',
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'LOGIN',
        paragraph,
        signatureName: editorialAssistantName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async sendToUnconfirmedAuthors(
    { id, alias, user },
    { manuscript, submittingAuthor, editorialAssistant },
  ) {
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'unconfirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(baseUrl, resetPath, {
          userId: id,
          confirmationToken: user.confirmationToken,
          ...alias,
        }),
        ctaText: 'CREATE ACCOUNT',
        paragraph,
        signatureName: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async sendSubmittingAuthorInvitation({
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'submitting-author-added-by-admin',
      manuscript,
      submittingAuthor,
      journalName,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const email = new Email({
      type: 'user',
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: {
        email: submittingAuthor.alias.email,
        name: submittingAuthor.alias.surname,
      },
      content: {
        ctaText: 'LOG IN',
        signatureJournal: journalName,
        subject: `Manuscript submitted`,
        ctaLink: services.createUrl(baseUrl, ''),
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    if (!submittingAuthor.isConfirmed) {
      email.content.ctaLink = services.createUrl(baseUrl, resetPath, {
        userId: submittingAuthor.user.id,
        confirmationToken: submittingAuthor.user.confirmationToken,
        ...submittingAuthor.alias,
      })
      email.content.ctaText = 'CREATE ACCOUNT'
    }

    return email.sendEmail()
  },

  async sendSubmittingAuthorConfirmation({
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const emailType = 'submitting-author-manuscript-submitted'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      manuscript,
      journalName,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const email = new Email({
      type: 'user',
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: {
        email: submittingAuthor.alias.email,
        name: `${submittingAuthor.alias.surname}`,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        paragraph,
        signatureName: '',
        signatureJournal: journalName,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async sendEQSEmail({ manuscript }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'eqs-email',
      manuscript,
      journalName,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: staffEmail,
        name: 'Editorial Assistant',
      },
      content: {
        subject: `Manuscript Submitted`,
        paragraph,
        signatureJournal: journalName,
        ctaLink: services.createUrl(baseUrl, config.get('eqs-decision.url'), {
          title: manuscript.title,
          manuscriptId: manuscript.id,
          customId: manuscript.customId,
          token: manuscript.technicalCheckToken,
        }),
        ctaText: 'MAKE DECISION',
        unsubscribeLink: baseUrl,
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
