const uuid = require('uuid')
const { sortBy } = require('lodash')

const initialize = ({
  models: { Manuscript, Team, Identity, User },
  notificationService,
  logEvent,
}) => ({
  execute: async ({
    manuscriptId,
    authorInput: {
      isSubmitting = false,
      isCorresponding = false,
      ...authorIdentity
    },
    userId,
  }) => {
    let user
    const authorTeam = await Team.findOneBy({
      queryObject: { manuscriptId },
      eagerLoadRelations: 'members.[user.[identities]]',
    })

    const identity = await Identity.findOneByEmail(authorIdentity.email)

    if (identity) {
      user = await User.find(identity.userId, 'identities')

      if (!user.isActive) {
        throw new ConflictError('Invited user is inactive.')
      }
    } else {
      user = new User({
        agreeTc: false,
        isActive: true,
        defaultIdentity: 'local',
        confirmationToken: uuid.v4(),
        passwordResetToken: uuid.v4(),
      })

      const newIdentity = new Identity({
        type: 'local',
        isConfirmed: false,
        ...authorIdentity,
      })

      user.assignIdentity(newIdentity)
    }

    if (isCorresponding) {
      authorTeam.members.forEach(a => (a.isCorresponding = false))
    }

    const submittingAuthorExists = authorTeam.members.some(a => a.isSubmitting)
    if (!submittingAuthorExists) {
      isSubmitting = true
    }
    const correspondingAuthorExists = authorTeam.members.some(
      a => a.isCorresponding,
    )
    if (!correspondingAuthorExists) {
      isCorresponding = true
    }

    const author = authorTeam.addMember(user, {
      alias: authorIdentity,
      isSubmitting,
      isCorresponding,
    })
    await authorTeam.saveRecursively()

    const manuscript = await Manuscript.find(manuscriptId)
    if (authorTeam.members.length === 1) {
      author.updateProperties({
        isSubmitting: true,
        isCorresponding: true,
      })
      await author.save()

      notificationService.sendSubmittingAuthorInvitation({
        manuscript,
        submittingAuthor: author,
      })
    }

    if (manuscript.status !== 'draft') {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.author_added,
        objectType: logEvent.objectType.user,
        objectId: user.id,
      })
    }

    return sortBy(authorTeam.members, 'position').map(a => a.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
