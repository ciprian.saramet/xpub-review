const { isEqual, sortBy } = require('lodash')

const initialize = ({ Manuscript }) => ({
  execute: async ({ manuscriptId, autosaveInput }) => {
    const manuscript = await Manuscript.find(manuscriptId, [
      'files',
      'teams.[members.[user.[identities]]]',
    ])

    if (
      manuscript.status === Manuscript.Statuses.accepted ||
      manuscript.status === Manuscript.Statuses.rejected
    ) {
      throw new AuthorizationError('Operation not permitted')
    }

    const existingAuthorsTeam = manuscript.teams.find(
      team => team.role === 'author',
    )

    if (!existingAuthorsTeam) throw new Error('No author team has been found.')

    const existingAuthorsEmails = sortBy(
      existingAuthorsTeam.members,
      'position',
    ).map(member => member.alias.email)

    const inputAuthorsEmails = autosaveInput.authors.map(author => author.email)

    if (!isEqual(existingAuthorsEmails, inputAuthorsEmails)) {
      await Promise.all(
        inputAuthorsEmails.map(async (email, index) => {
          const teamMember = existingAuthorsTeam.members.find(
            member => member.alias.email === email,
          )
          teamMember.updateProperties({ position: index })
          await teamMember.save()
        }),
      )
    }

    if (manuscript.files.length > 1) {
      const existingFilesByType = sortBy(manuscript.files, [
        'type',
        'position',
      ]).map(file => ({ id: file.id, type: file.type }))
      const inputFilesByType = sortBy(autosaveInput.files, [
        'fileType',
        'position',
      ]).map(file => ({ id: file.id, type: file.type }))

      if (!isEqual(existingFilesByType, inputFilesByType)) {
        const parsedFiles = inputFilesByType.reduce(
          (acc, current) => ({
            ...acc,
            [current.type]: [...acc[current.type], { id: current.id }],
          }),
          {
            manuscript: [],
            supplementary: [],
            coverLetter: [],
          },
        )
        await Promise.all(
          Object.values(parsedFiles).map(files =>
            files.map(async (file, index) => {
              const matchingFile = manuscript.files.find(
                mFile => mFile.id === file.id,
              )
              matchingFile.updateProperties({ position: index })
              await matchingFile.save()
            }),
          ),
        )
      }
    }

    manuscript.updateProperties(autosaveInput.meta)
    manuscript.updateProperties({ journalId: autosaveInput.journalId })

    await manuscript.save()

    return manuscript.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
