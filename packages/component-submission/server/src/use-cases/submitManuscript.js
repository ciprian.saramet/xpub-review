const { get } = require('lodash')
const logger = require('@pubsweet/logger')

const initialize = ({
  models: { Journal, Manuscript, Team },
  sendPackage,
  notificationService,
  logEvent,
}) => ({
  execute: async ({ manuscriptId, userId }) => {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[files,teams.members]',
    )
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorialAssistant = journal.getEditorialAssistant()

    await manuscript.submitManuscript()
    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${
        manuscript.technicalCheckToken
      } before saving`,
    )

    if (!manuscript.technicalCheckToken) {
      throw new ConflictError('Something went wrong. Please try again.')
    }

    await manuscript.save()

    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${
        manuscript.technicalCheckToken
      } after saving`,
    )

    await sendPackage({ manuscript })

    const authorTeam = await Team.findOneBy({
      queryObject: {
        manuscriptId,
        role: 'author',
      },
      eagerLoadRelations: 'members.[user.[identities]]',
    })
    const submittingAuthor = authorTeam.members.find(
      author => author.isSubmitting,
    )

    const confirmedCoAuthors = authorTeam.members
      .filter(author => !author.isSubmitting)
      .filter(author => get(author, 'user.identities.0.isConfirmed'))

    const unconfirmedCoAuthors = authorTeam.members
      .filter(author => !author.isSubmitting)
      .filter(author => !get(author, 'user.identities.0.isConfirmed'))

    confirmedCoAuthors.forEach(author =>
      notificationService.sendToConfirmedAuthors(author, {
        manuscript,
        submittingAuthor,
        editorialAssistant,
      }),
    )

    unconfirmedCoAuthors.forEach(author =>
      notificationService.sendToUnconfirmedAuthors(author, {
        manuscript,
        submittingAuthor,
        editorialAssistant,
      }),
    )

    notificationService.sendSubmittingAuthorConfirmation({
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })
    notificationService.sendEQSEmail({ manuscript })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
