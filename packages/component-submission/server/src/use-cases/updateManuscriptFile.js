const { sortBy } = require('lodash')

const initialize = ({ File, Manuscript }) => ({
  execute: async ({ fileId, type }) => {
    const file = await File.find(fileId)

    const manuscript = await Manuscript.find(file.manuscriptId, 'files')

    const filesByCurrentType = manuscript.files.filter(
      mfile => mfile.type === file.type && mfile.id !== file.id,
    )

    sortBy(filesByCurrentType, 'position').forEach((file, index) => {
      file.updateProperties({ position: index })
    })

    await manuscript.saveRecursively()

    const filesByNewTypeLength = manuscript.files.filter(
      file => file.type === type,
    ).length

    file.updateProperties({ type, position: filesByNewTypeLength })
    await file.save()

    return file
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
