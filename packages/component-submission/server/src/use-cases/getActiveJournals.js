const initialize = ({ Journal }) => ({
  execute: async () => {
    const journals = await Journal.findAll({
      queryObject: { isActive: true },
      orderByField: 'name',
      order: 'asc',
    })

    return journals.map(journal => journal.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
