const generateCustomId = () =>
  Date.now()
    .toString()
    .slice(-7)

const initialize = ({ Manuscript, Team, TeamMember, User, Journal }) => ({
  async execute({ journalId }, userId) {
    const author = await User.find(userId, 'identities')
    const teamMemberships = await TeamMember.findBy({ userId }, 'team')
    const userBelongsToAdminOrEditorialAssistantTeam = teamMemberships.some(
      t =>
        t.team.role === Team.Role.admin ||
        t.team.role === Team.Role.editorialAssistant,
    )

    const manuscript = new Manuscript({
      journalId,
      customId: generateCustomId(),
    })

    const authorTeam = new Team({ role: Team.Role.author, journalId })
    manuscript.assignTeam(authorTeam)

    if (!userBelongsToAdminOrEditorialAssistantTeam) {
      authorTeam.addMember(author, {
        isSubmitting: true,
        isCorresponding: true,
      })
    }

    await manuscript.saveRecursively()
    return manuscript.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
