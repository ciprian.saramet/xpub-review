process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const makeMockFn = () => jest.fn(async () => {})
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_submitted: 'manuscript_submitted',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  sendToConfirmedAuthors: makeMockFn(),
  sendToUnconfirmedAuthors: makeMockFn(),
  sendSubmittingAuthorConfirmation: makeMockFn(),
  sendEQSEmail: makeMockFn(),
}
const sendPackage = makeMockFn()

const { submitManuscriptUseCase } = require('../src/use-cases')

describe('Submit Manuscript Use Case', () => {
  it('should update status and publication dates', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const draftManuscript = await fixtures.generateManuscript({
      version: 1,
      status: 'draft',
      publicationDates: [],
      journalId: journal.id,
    })
    await dataService.createUserOnManuscript({
      manuscript: draftManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
    })
    const mockedModels = models.build(fixtures)

    await submitManuscriptUseCase
      .initialize({
        models: mockedModels,
        sendPackage,
        notificationService,
        logEvent,
      })
      .execute({ manuscriptId: draftManuscript.id })

    expect(draftManuscript.status).toBe('technicalChecks')
    expect(draftManuscript.technicalCheckToken).toBeDefined()
    expect(draftManuscript.publicationDates[0].type).toBe('technicalChecks')
    expect(sendPackage).toHaveBeenCalledTimes(1)
  })
})
