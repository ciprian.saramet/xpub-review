process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { models, fixtures } = require('fixture-service')

const { createDraftManuscriptUseCase } = require('../src/use-cases')

describe('Create Draft Manuscript Use Case', () => {
  it('should create a new draft manuscript', async () => {
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)
    const initialManuscriptsLength = fixtures.manuscripts.length

    const manuscript = await createDraftManuscriptUseCase
      .initialize(mockedModels)
      .execute({}, user.id)

    expect(initialManuscriptsLength).toBeLessThan(fixtures.manuscripts.length)
    expect(manuscript.journalId).toBeUndefined()
  })

  it('should add the author of the manuscript to the author team if not admin', async () => {
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)

    await createDraftManuscriptUseCase
      .initialize(mockedModels)
      .execute({}, user.id)

    expect(fixtures.teams[0].role).toEqual('author')
  })

  it('should create manuscript with no authors when called by an admin', async () => {
    const mockedModels = models.build(fixtures)
    const user = fixtures.generateUser({})
    const adminTeam = fixtures.generateTeam({ role: 'admin' })
    const newMember = adminTeam.addMember(user, {})
    await newMember.save()

    const manuscriptDTO = await createDraftManuscriptUseCase
      .initialize(mockedModels)
      .execute({}, user.id)

    expect(manuscriptDTO).toBeDefined()
  })
})
