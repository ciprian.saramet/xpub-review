const { getActiveJournalsUseCase } = require('../src/use-cases')
const { models, fixtures } = require('fixture-service')

const Chance = require('chance')

const chance = new Chance()

describe('Get active journals use case', () => {
  const journals = [
    {
      name: chance.company(),
      activationDate: Date.now() + 3454,
      isActive: false,
    },
    {
      name: chance.company(),
      activationDate: new Date().toISOString(),
      isActive: true,
    },
    {
      name: chance.company(),
      activationDate: new Date().toISOString(),
      isActive: true,
    },
  ]
  const mockedModels = models.build(fixtures)
  journals.forEach(fixtures.generateJournal)

  it('returns an array', async () => {
    const res = await getActiveJournalsUseCase
      .initialize(mockedModels)
      .execute()
    expect(Array.isArray(res)).toBeTruthy()
  })

  it('returns the correct number of active journals', async () => {
    const res = await getActiveJournalsUseCase
      .initialize(mockedModels)
      .execute()
    expect(res.length).toEqual(2)
  })
})
