import { configure } from 'react-testing-library'

configure({
  testIdAttribute: 'data-test-id',
})

Object.assign(global, require('@pubsweet/errors'))
