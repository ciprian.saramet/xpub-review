const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')
const JobsService = require('../src/jobsService/jobsService')

const resolvers = {
  Query: {
    async getManuscripts(_, { input }, ctx) {
      return useCases.getManuscriptsUseCase
        .initialize({ models, useCases })
        .execute({ input, userId: ctx.user })
    },
  },
  Mutation: {
    async deleteManuscript(_, { manuscriptId }, ctx) {
      return useCases.deleteManuscriptUseCase
        .initialize(models)
        .execute({ manuscriptId, userId: ctx.user })
    },
    async archiveManuscript(_, { submissionId }, ctx) {
      const jobsService = JobsService.initialize({ models })

      return useCases.archiveManuscriptUseCase
        .initialize({ models, jobsService })
        .execute(submissionId)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
