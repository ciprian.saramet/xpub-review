const { Promise } = require('bluebird')

module.exports.initialize = ({ models: { Job } }) => ({
  async cancelReviewersJobs({ reviewers }) {
    await Promise.each(reviewers, async reviewer => {
      try {
        if (reviewer.jobs) {
          await Promise.each(reviewer.jobs, async job => {
            await Job.cancel(job.id)
            return job.delete()
          })
        }
      } catch (e) {
        throw new Error(e)
      }
    })
  },
})
