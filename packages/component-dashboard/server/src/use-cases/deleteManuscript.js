const { get } = require('lodash')

const initialize = ({ Manuscript, User, Team }) => ({
  async execute({ manuscriptId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, 'teams.[members]')

    if (!manuscript) {
      throw new Error('Could not find manuscript')
    }

    if (manuscript.status !== Manuscript.Statuses.draft) {
      throw new ValidationError('Only draft manuscripts can be deleted')
    }

    const user = await User.find(userId, 'teamMemberships.[team]')

    if (
      [Team.Role.admin, Team.Role.editorialAssistant].includes(
        user.getGlobalRole(),
      )
    ) {
      await manuscript.delete()
      return
    }

    const authorTeamMembers = get(
      manuscript.teams.find(t => t.role === Team.Role.author),
      'members',
      [],
    )

    if (!authorTeamMembers.length) {
      throw new Error(
        'The author team does not exist or it does not contain any members.',
      )
    }

    const author = manuscript.getSubmittingAuthor()

    if (author.userId === userId) {
      await manuscript.delete()
    }
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
