const GLOBAL_ROLES = ['admin', 'editorInChief', 'editorialAssistant']

const initialize = ({ models, useCases }) => ({
  async execute({ userId, input }) {
    const user = await models.User.find(
      userId,
      'teamMemberships.[team.[manuscript.[teams.[members]]]]',
    )

    const globalRole = user.getGlobalRole()

    const useCase = GLOBAL_ROLES.includes(globalRole)
      ? 'getForGlobalRolesUseCase'
      : 'getForManuscriptRolesUseCase'

    return useCases[useCase]
      .initialize(models)
      .execute({ input, role: globalRole, user })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
