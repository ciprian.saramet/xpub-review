process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { getManuscriptsUseCase } = require('../src/use-cases')

describe('get manusripts', () => {
  let mockedUseCases = {}
  beforeEach(() => {
    mockedUseCases = {
      getForGlobalRolesUseCase: {
        initialize: jest.fn(() => ({ execute: jest.fn() })),
      },
      getForManuscriptRolesUseCase: {
        initialize: jest.fn(() => ({ execute: jest.fn() })),
      },
    }
  })

  it('returns the global role use case if the user is admin ', async () => {
    fixtures.generateManuscript({})
    fixtures.generateManuscript({})

    let team
    team = fixtures.getTeamByRole('admin')
    if (!team) team = fixtures.generateTeam({ role: 'admin' })

    const admin = fixtures.generateUser({})
    const teamMember = fixtures.generateTeamMember({
      userId: admin.id,
      teamId: team.id,
    })
    admin.teamMemberships = admin.teamMemberships || []
    admin.teamMemberships.push(teamMember)

    await teamMember.linkUser(admin)

    teamMember.team = team
    team.members.push(teamMember)

    const mockedModels = models.build(fixtures)

    await getManuscriptsUseCase
      .initialize({ models: mockedModels, useCases: mockedUseCases })
      .execute({ input: { dateOrder: 'desc' }, userId: admin.id })

    expect(
      mockedUseCases.getForGlobalRolesUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
    expect(
      mockedUseCases.getForManuscriptRolesUseCase.initialize,
    ).not.toHaveBeenCalled()
  })

  it('returns the manuscript role use case if the user is author ', async () => {
    const user = fixtures.generateUser({})
    user.teamMemberships = user.teamMemberships || []

    const mockedModels = models.build(fixtures)

    await getManuscriptsUseCase
      .initialize({ models: mockedModels, useCases: mockedUseCases })
      .execute({ input: { dateOrder: 'desc' }, userId: user.id })

    expect(
      mockedUseCases.getForManuscriptRolesUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
    expect(
      mockedUseCases.getForGlobalRolesUseCase.initialize,
    ).not.toHaveBeenCalled()
  })
})
