process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { getForGlobalRolesUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('get manusripts for global roles (admin, eic)', () => {
  it('returns latest manuscript versions if user is admin', async () => {
    const timeObject = new Date()
    const submissionId = chance.guid()

    fixtures.generateManuscript({
      status: 'technicalChecks',
      submissionId,
      version: 1,
      updated: new Date(timeObject.getTime() + 1000 * 10),
    })

    fixtures.generateManuscript({
      status: 'draft',
      submissionId,
      version: 2,
      updated: new Date(timeObject.getTime() + 2000 * 10),
    })

    fixtures.generateManuscript({
      status: 'draft',
      submissionId: chance.guid(),
      updated: new Date(timeObject.getTime() + 3000 * 10),
    })

    fixtures.generateManuscript({
      status: 'submitted',
      submissionId: chance.guid(),
      updated: new Date(timeObject.getTime() + 5000 * 10),
    })

    const revisedSubmissionId = chance.guid()
    fixtures.generateManuscript({
      status: 'olderVersion',
      submissionId: revisedSubmissionId,
      version: 1,
      updated: new Date(timeObject.getTime() + 6000 * 10),
    })

    fixtures.generateManuscript({
      status: 'underReview',
      submissionId: revisedSubmissionId,
      version: 2,
      updated: new Date(timeObject.getTime() + 7000 * 10),
    })

    const mockedModels = models.build(fixtures)

    const manuscripts = await getForGlobalRolesUseCase
      .initialize(mockedModels)
      .execute({ input: {}, role: 'admin' })

    expect(manuscripts).toHaveLength(4)
    expect(manuscripts[0].status).toEqual('underReview')
    expect(manuscripts[manuscripts.length - 1].status).toEqual(
      'technicalChecks',
    )
  })
  it('returns prioritized manuscripts if user is editor in chief', async () => {
    let team
    team = fixtures.getTeamByRole('editorInChief')
    if (!team) team = fixtures.generateTeam({ role: 'editorInChief' })
    const user = fixtures.generateUser({})
    fixtures.generateManuscript({})

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
    })

    user.teamMemberships = [teamMember]
    teamMember.linkUser(user)
    teamMember.team = team
    team.members.push(teamMember)

    const mockedModels = models.build(fixtures)

    const manuscripts = await getForGlobalRolesUseCase
      .initialize(mockedModels)
      .execute({
        input: { priorityFilter: 'inProgress' },
        role: 'editorInChief',
      })
    expect(manuscripts.length).toBeLessThan(fixtures.manuscripts.length)
  })
  it('returns non-draft manuscripts if user is editor in chief', async () => {
    let team
    team = fixtures.getTeamByRole('editorInChief')
    if (!team) team = fixtures.generateTeam({ role: 'editorInChief' })
    const user = fixtures.generateUser({})
    fixtures.generateManuscript({})
    fixtures.generateManuscript({ status: 'submitted' })
    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
    })

    user.teamMemberships = [teamMember]
    teamMember.linkUser(user)
    teamMember.team = team
    team.members.push(teamMember)

    const mockedModels = models.build(fixtures)

    const manuscripts = await getForGlobalRolesUseCase
      .initialize(mockedModels)
      .execute({
        input: {},
        role: 'editorInChief',
      })

    expect(manuscripts.length).toBeLessThan(fixtures.manuscripts.length)

    const draftManuscripts = manuscripts.filter(m => m.status === 'draft')
    expect(draftManuscripts).toHaveLength(0)
  })
})
