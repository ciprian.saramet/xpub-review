process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { deleteManuscriptUseCase } = require('../src/use-cases')

describe('Delete manuscript', () => {
  it('should successfuly delete a draft manuscript as admin', async () => {
    const journal = fixtures.journals[0]
    const adminMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      role: 'admin',
    })

    const manuscript = fixtures.generateManuscript({
      status: 'draft',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
      input: {},
    })
    const mockedModels = models.build(fixtures)
    const initialManuscriptLength = fixtures.manuscripts.length
    await deleteManuscriptUseCase
      .initialize(mockedModels)
      .execute({ manuscriptId: manuscript.id, userId: adminMember.userId })

    expect(fixtures.manuscripts.length).toBeLessThan(initialManuscriptLength)
  })

  it('should successfuly delete a draft manuscript (without an author) as admin', async () => {
    const journal = fixtures.journals[0]
    const adminMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      role: 'admin',
    })

    const manuscript = fixtures.generateManuscript({
      status: 'draft',
    })
    const mockedModels = models.build(fixtures)
    const initialManuscriptLength = fixtures.manuscripts.length
    await deleteManuscriptUseCase
      .initialize(mockedModels)
      .execute({ manuscriptId: manuscript.id, userId: adminMember.userId })

    expect(fixtures.manuscripts.length).toBeLessThan(initialManuscriptLength)
  })

  it('should successfuly delete a draft manuscript as an author', async () => {
    const manuscript = fixtures.generateManuscript({
      status: 'draft',
    })
    const authorMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
      input: { isSubmitting: true },
    })
    const mockedModels = models.build(fixtures)
    const initialManuscriptLength = fixtures.manuscripts.length
    await deleteManuscriptUseCase
      .initialize(mockedModels)
      .execute({ manuscriptId: manuscript.id, userId: authorMember.userId })

    expect(fixtures.manuscripts.length).toBeLessThan(initialManuscriptLength)
  })

  it('should not delete the draft manuscript if user is reviewer', async () => {
    const manuscript = fixtures.generateManuscript({
      status: 'draft',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true },
      role: 'author',
    })
    const reviewerMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {},
      role: 'reviewer',
    })
    const mockedModels = models.build(fixtures)
    const initialManuscriptLength = fixtures.manuscripts.length
    await deleteManuscriptUseCase
      .initialize(mockedModels)
      .execute({ manuscriptId: manuscript.id, userId: reviewerMember.userId })

    expect(fixtures.manuscripts.length).toEqual(initialManuscriptLength)
  })

  it('should return an error when tring to delete a non-draft manuscript', async () => {
    const journal = fixtures.journals[0]
    const adminMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      role: 'admin',
    })

    const manuscript = fixtures.generateManuscript({
      status: 'submitted',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
      input: {},
    })
    const mockedModels = models.build(fixtures)
    const result = deleteManuscriptUseCase
      .initialize(mockedModels)
      .execute({ manuscriptId: manuscript.id, userId: adminMember.userId })
    return expect(result).rejects.toThrow(
      'Only draft manuscripts can be deleted',
    )
  })
})
