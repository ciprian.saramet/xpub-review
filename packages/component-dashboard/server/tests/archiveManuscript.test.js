process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { archiveManuscriptUseCase } = require('../src/use-cases')

const jobsService = { cancelReviewersJobs: jest.fn() }

describe('Archive manuscript', () => {
  it('should successfuly archive a manuscript as admin', async () => {
    const manuscript = fixtures.generateManuscript({ status: 'assignHe' })
    const mockedModels = models.build(fixtures)
    await archiveManuscriptUseCase
      .initialize({ models: mockedModels, jobsService })
      .execute(manuscript.submissionId)

    expect(manuscript.status).toEqual('deleted')
  })

  it('should return an error when deleting a draft manuscript', async () => {
    const manuscript = fixtures.generateManuscript({})
    const mockedModels = models.build(fixtures)
    const result = archiveManuscriptUseCase
      .initialize({ models: mockedModels, jobsService })
      .execute(manuscript.submissionId)

    return expect(result).rejects.toThrow(
      'Draft manuscripts cannot be deleted.',
    )
  })

  it('should return an error when deleting a manuscript already deleted', async () => {
    const manuscript = fixtures.generateManuscript({ status: 'deleted' })
    const mockedModels = models.build(fixtures)
    const result = archiveManuscriptUseCase
      .initialize({ models: mockedModels, jobsService })
      .execute(manuscript.submissionId)

    return expect(result).rejects.toThrow('Manuscript already deleted.')
  })
})
