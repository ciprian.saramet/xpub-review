import gql from 'graphql-tag'

export const manuscriptMetaDashboard = gql`
  fragment manuscriptMetaDashboard on ManuscriptMeta {
    articleType
    title
  }
`

export const handlingEditorDashboard = gql`
  fragment handlingEditorDashboard on TeamMember {
    id
    status
    alias {
      name {
        surname
        givenNames
      }
    }
  }
`

export const authorDashboard = gql`
  fragment authorDashboard on TeamMember {
    id
    isSubmitting
    isCorresponding
    alias {
      aff
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`

export const dashboardManuscriptDetails = gql`
  fragment dashboardManuscriptDetails on Manuscript {
    id
    submissionId
    role
    status
    customId
    created
    meta {
      ...manuscriptMetaDashboard
    }
    handlingEditor {
      ...handlingEditorDashboard
    }
    authors {
      ...authorDashboard
    }
    reviewers {
      id
      status
    }
    version
  }
  ${manuscriptMetaDashboard}
  ${handlingEditorDashboard}
  ${authorDashboard}
`
