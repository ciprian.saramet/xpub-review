import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withRouter } from 'react-router'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { H3, H4, DateParser } from '@pubsweet/ui'
import {
  Row,
  Text,
  Icon,
  Item,
  StatusTag,
  ActionLink,
  TextTooltip,
  MultiAction,
  AuthorTagList,
  ReviewerBreakdown,
} from '@hindawi/ui'
import { Modal } from 'component-modal'
import { compose, setDisplayName, withProps, withHandlers } from 'recompose'

import withHEStatus from './withHEStatus'

const ManuscriptCard = ({
  onClick,
  journalName,
  visibleStatus,
  manuscriptType = {},
  manuscript: {
    meta,
    role,
    status,
    created,
    version,
    customId,
    authors = [],
    id: manuscriptId,
    reviewers = [],
  },
  manuscript,
  isLast,
  onDeleteManuscript,
  canSeeReviewerReports,
  handlingEditorVisibleStatus,
  statusColor,
  ...rest
}) => {
  const { articleType = '' } = meta
  const title = meta.title || 'No title'
  return (
    <Root
      data-test-id={`manuscript-${manuscriptId}`}
      isLast={isLast}
      onClick={onClick}
    >
      <MainContainer>
        <Row alignItems="center" justify="space-between">
          <TextTooltip title={title}>
            <H3>{title}</H3>
          </TextTooltip>
          <StatusTag statusColor={statusColor} version={version}>
            {visibleStatus}
          </StatusTag>
        </Row>
        {authors && authors.length > 0 && (
          <Row alignItems="center" justify="flex-start" mb={2}>
            <AuthorTagList authors={authors} withTooltip />
          </Row>
        )}
        <Row alignItems="center" justify="flex-start">
          {customId && !!articleType && (
            <Text customId mr={2}>{`ID ${customId}`}</Text>
          )}
          {created && status !== 'draft' && (
            <DateParser humanizeThreshold={0} timestamp={created}>
              {(timestamp, timeAgo) => (
                <Text
                  mr={6}
                >{`Submitted on ${timestamp} (${timeAgo} ago)`}</Text>
              )}
            </DateParser>
          )}
          <Text primary>{manuscriptType.label || articleType}</Text>
          {!!articleType && (
            <Text journal ml={2}>
              {journalName}
            </Text>
          )}
        </Row>
        <Row alignItems="center" height={8} justify="flex-start">
          <H4>Handling editor</H4>
          <Text display="flex" ml={2} mr={6} whiteSpace="nowrap">
            {handlingEditorVisibleStatus}
          </Text>

          {canSeeReviewerReports && (
            <Fragment>
              <H4 mr={1}>Reviewer Reports</H4>
              <ReviewerBreakdown reviewers={reviewers} />
            </Fragment>
          )}
          {status === 'draft' ||
          (status !== 'draft' &&
            (role === 'admin' || role === 'editorialAssistant')) ? (
            <Item
              data-test-id="delete-manuscript-modal"
              justify="flex-end"
              onClick={e => e.stopPropagation()}
            >
              <Modal
                cancelText={`${
                  manuscript.status === 'draft' ? 'CANCEL' : 'NO'
                }`}
                component={MultiAction}
                confirmText={`${
                  manuscript.status === 'draft' ? 'DELETE' : 'OK'
                }`}
                modalKey={`deleteManuscript-${manuscriptId}`}
                onConfirm={onDeleteManuscript}
                title={`Are you sure you want to ${
                  manuscript.status === 'draft'
                    ? 'delete this submission?'
                    : 'remove this manuscript?'
                }`}
              >
                {showModal => (
                  <ActionLink
                    fontSize="12px"
                    fontWeight={700}
                    onClick={showModal}
                  >
                    <Icon fontSize="10px" icon="delete" mr={1} />
                    DELETE
                  </ActionLink>
                )}
              </Modal>
            </Item>
          ) : null}
        </Row>
      </MainContainer>
      <SideNavigation>
        <Icon color="colorSecondary" icon="caretRight" />
      </SideNavigation>
    </Root>
  )
}
export default compose(
  withRouter,
  withJournal,
  withHEStatus,
  withProps(
    ({ manuscript: { meta, role, reviewers, status }, journal = {} }) => ({
      statusColor: get(journal, `statuses.${status}.color`),
      visibleStatus: get(journal, `statuses.${status}.${role}.label`),
      manuscriptType: get(journal, 'manuscriptTypes', []).find(
        t => t.value === get(meta, 'articleType', ''),
      ),

      journalName: get(journal, 'metadata.nameText', 'The journal name here'),
      canSeeReviewerReports:
        reviewers &&
        reviewers.length > 0 &&
        !['author', 'reviewer'].includes(role),
    }),
  ),

  withHandlers({
    onClick: ({ manuscript, history, ...rest }) => () => {
      if (manuscript.status === 'draft') {
        history.push(`/submit/${manuscript.submissionId}/${manuscript.id}`)
      } else {
        history.push(`/details/${manuscript.submissionId}/${manuscript.id}`)
      }
    },
    onDeleteManuscript: ({ manuscript, deleteManuscript }) => modalProps => {
      deleteManuscript(manuscript, modalProps)
    },
  }),
  setDisplayName('ManuscriptCard'),
)(ManuscriptCard)
// #region styles

const teamMember = PropTypes.shape({
  id: PropTypes.string,
  isSubmitting: PropTypes.bool,
  isCorresponding: PropTypes.bool,
  alias: PropTypes.shape({
    aff: PropTypes.string,
    email: PropTypes.string,
    country: PropTypes.string,
    name: PropTypes.shape({
      surname: PropTypes.string,
      givenNames: PropTypes.string,
      title: PropTypes.string,
    }),
  }),
})

ManuscriptCard.propTypes = {
  manuscript: PropTypes.shape({
    id: PropTypes.string,
    role: PropTypes.string,
    created: PropTypes.string,
    customId: PropTypes.string,
    visibleStatus: PropTypes.string,
    meta: PropTypes.shape({
      agreeTc: PropTypes.bool,
      title: PropTypes.string,
      abstract: PropTypes.string,
      articleType: PropTypes.string,
    }),
    handlingEditor: teamMember,
    authors: PropTypes.arrayOf(teamMember),
  }),
}

ManuscriptCard.defaultProps = {
  manuscript: {},
}

// #region styles
const MainContainer = styled.div`
  justify-content: flex-start;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 4);
  padding-bottom: ${th('gridUnit')};
  width: calc(100% - (${th('gridUnit')} * 5));
  overflow: hidden;
  ${Row} {
    [data-tooltipped] {
      overflow: hidden;
    }
    ${H4} {
      white-space: nowrap;
    }
    ${H3} {
      display: block;
      margin-bottom: 0;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      padding-right: calc(${th('gridUnit')} * 2);
    }
  }
`
const SideNavigation = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue2')
    ? th('colorBackgroundHue2')
    : th('colorBackgroundHue')};
  border-top-right-radius: ${th('borderRadius')};
  border-bottom-right-radius: ${th('borderRadius')};
  display: flex;
  justify-content: center;
  width: calc(${th('gridUnit')} * 6);
`

const Root = styled.div`
  background-color: #fff;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  cursor: pointer;
  display: flex;
  margin: calc(${th('gridUnit')} / 2) calc(${th('gridUnit')} / 2)
    calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} / 2);

  ${H3} {
    margin: 0;
    margin-bottom: calc(${th('gridUnit')} * 2);
  }

  &:hover {
    box-shadow: ${th('dashboardCard.hoverShadow')};
  }

  margin-bottom: calc(${props => (props.isLast ? 6 : 2)} * ${th('gridUnit')});
`
// #endregion
