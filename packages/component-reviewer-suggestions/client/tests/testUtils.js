import React from 'react'
import { theme } from '@hindawi/ui'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { MockedProvider } from 'react-apollo/test-utils'
import { render as rtlRender } from 'react-testing-library'

import { loadReviewerSuggestions } from '../graphql/queries'

const mocks = [
  {
    request: {
      query: loadReviewerSuggestions,
      variables: {
        manuscriptId: '558131fe-b308-548b-8e6c-750c59380466',
      },
    },
    result: {
      data: {
        loadReviewerSuggestions: [
          {
            id: '9484df8e-e5c0-58d8-bc4e-0642409fc349',
            givenNames: 'Lora',
            surname: 'Brent',
            email: 'jolosa@nizza.tc',
            profileUrl: 'http://denu.sh/otpomul',
            aff: 'Alaska Air Group, Inc.',
            numberOfReviews: 34,
            type: 'publons',
            isInvited: false,
          },
          {
            id: '9484df8e-e5c0-58d8-bc4e-0642409fc149',
            givenNames: 'Charles',
            surname: 'Lottie',
            email: 'vepeji@me.ee',
            profileUrl: 'http://okuoziz.sm/mameol',
            aff: 'Corning Inc.',
            numberOfReviews: 55,
            type: 'publons',
            isInvited: false,
          },
          {
            id: '9484df8e-e5c0-58d8-bc4e-0642409fc134',
            givenNames: 'Charles',
            surname: 'Lottie',
            email: 'vepeji@me.ee',
            profileUrl: 'http://okuoziz.sm/mameol',
            aff: 'Corning Inc.',
            numberOfReviews: 55,
            type: 'publons',
            isInvited: false,
          },
        ],
      },
    },
  },
  {
    request: {
      query: loadReviewerSuggestions,
      variables: {
        manuscriptId: '6638-2324',
      },
    },
    result: {
      data: {
        loadReviewerSuggestions: [],
      },
    },
  },
]

export const render = ui => {
  const Component = () => (
    <MockedProvider addTypename={false} mocks={mocks}>
      <ModalProvider>
        <div id="ps-modal-root" />
        <ThemeProvider theme={theme}>{ui}</ThemeProvider>
      </ModalProvider>
    </MockedProvider>
  )

  return rtlRender(<Component />)
}
