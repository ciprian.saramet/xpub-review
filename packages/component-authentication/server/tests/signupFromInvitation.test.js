process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { fixtures, models, services } = require('fixture-service')

const { signUpFromInvitationUseCase } = require('../src/use-cases')

const chance = new Chance()

const generateInput = () => ({
  givenNames: chance.first(),
  surname: chance.first(),
  title: chance.pickone(['mr', 'mrs', 'miss', 'ms', 'dr', 'prof']),
  country: chance.country(),
  aff: chance.company(),
  password: 'Password1!',
  email: chance.email(),
  agreeTc: true,
  confirmationToken: chance.guid(),
})

describe('signupFromInvitation use case', () => {
  let input

  beforeEach(() => {
    input = generateInput()
  })

  it('should return success', async () => {
    const user = fixtures.generateUser(
      {
        isActive: true,
      },
      { isConfirmed: false },
    )
    input.email = user.identities[0].email
    input.confirmationToken = user.confirmationToken
    const mockedModels = models.build(fixtures)

    const result = await signUpFromInvitationUseCase
      .initialize(services.tokenService, mockedModels)
      .execute(input)
    expect(result.token).toBeDefined()
  })

  it('should return an error when confirmation token is not valid', async () => {
    const user = fixtures.generateUser(
      {
        isActive: true,
      },
      { isConfirmed: false },
    )
    input.email = user.identities[0].email
    const mockedModels = models.build(fixtures)

    try {
      await signUpFromInvitationUseCase
        .initialize(services.tokenService, mockedModels)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('Invalid confirmation token.')
    }
  })

  it('should return error for password too weak', async () => {
    const user = fixtures.generateUser({
      isActive: true,
    })
    input.email = user.identities[0].email
    const mockedModels = models.build(fixtures)

    input.password = 'weak-very-weak'
    try {
      await signUpFromInvitationUseCase
        .initialize(services.tokenService, mockedModels)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual(
        'Password is too weak. Please check password requirements.',
      )
    }
  })

  it('should return an error if the user is deactivated', async () => {
    const user = fixtures.generateUser({
      isActive: false,
    })
    input.email = user.identities[0].email
    input.confirmationToken = user.confirmationToken
    const mockedModels = models.build(fixtures)

    const result = signUpFromInvitationUseCase
      .initialize(services.tokenService, mockedModels)
      .execute(input)

    return expect(result).rejects.toThrow('Something went wrong.')
  })

  it('should return error when identity does not exist', async () => {
    const mockedModels = models.build(fixtures)
    input.email = 'someone@thinslices.com'
    try {
      await signUpFromInvitationUseCase
        .initialize(services.tokenService, mockedModels)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('Something went wrong.')
    }
  })
})
