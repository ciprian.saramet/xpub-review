process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models, fixtures, services } = require('fixture-service')

const chance = new Chance()
const localLoginCase = require('../src/use-cases/localLogin')

describe('local login use case', () => {
  it('should return success when the username and password are correct', async () => {
    const user = fixtures.generateUser({
      isActive: true,
    })
    const mockedModels = models.build(fixtures)

    const result = await localLoginCase
      .initialize(services.tokenService, mockedModels)
      .execute({
        email: user.identities[0].email,
        password: 'password',
      })

    expect(result.token).toBeDefined()
  })

  it('should return an error when the email is wrong', async () => {
    const mockedModels = models.build(fixtures)

    const result = localLoginCase
      .initialize(services.tokenService, mockedModels)
      .execute({
        email: chance.email(),
        password: 'password',
      })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })

  it('should return an error when the password is wrong', async () => {
    const user = fixtures.generateUser({
      isActive: true,
    })

    const mockedModels = models.build(fixtures)

    const result = localLoginCase
      .initialize(services.tokenService, mockedModels)
      .execute({
        email: user.identities[0].email,
        password: 'wrong-password',
      })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })

  it('should return an error if the user is deactivated', async () => {
    const user = fixtures.generateUser({
      isActive: false,
    })

    const mockedModels = models.build(fixtures)

    const result = localLoginCase
      .initialize(services.tokenService, mockedModels)
      .execute({
        email: user.identities[0].email,
        password: 'password',
      })

    return expect(result).rejects.toThrow('Wrong username or password.')
  })
})
