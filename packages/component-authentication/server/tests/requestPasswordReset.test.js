process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { fixtures, models } = require('fixture-service')
const requestPassword = require('../src/use-cases/requestPasswordReset')

const notificationService = {
  sendForgotPasswordEmail: jest.fn(),
}

describe('request password reset use case', () => {
  it('should return an error when the user has already requested a password reset', async () => {
    const user = fixtures.generateUser({
      passwordResetTimestamp: Date.now(),
    })

    const mockedModels = models.build(fixtures)

    try {
      await requestPassword
        .initialize(notificationService, mockedModels)
        .execute(user.identities[0].email)
    } catch (e) {
      expect(e.message).toEqual('A password reset has already been requested.')
    }
  })

  it('should return success when the user is inactive', async () => {
    const user = fixtures.generateUser({
      isActive: false,
    })

    const mockedModels = models.build(fixtures)

    const result = requestPassword
      .initialize(notificationService, mockedModels)
      .execute(user.identities[0].email)
    expect(result).resolves.not.toThrow()
  })

  it('sets the password reset properties when the body is correct', async () => {
    const user = fixtures.generateUser({ isActive: true })
    const mockedModels = models.build(fixtures)
    await requestPassword
      .initialize(notificationService, mockedModels)
      .execute(user.identities[0].email)

    expect(user.passwordResetToken).not.toBeNull()
    expect(user.passwordResetTimestamp).not.toBeNull()
  })

  it('should return success if the email is non-existent', async () => {
    const email = 'anca.ioana@gmail.com'

    const result = requestPassword
      .initialize(notificationService, models.build(fixtures))
      .execute(email)

    return expect(result).resolves.not.toThrow()
  })
})
