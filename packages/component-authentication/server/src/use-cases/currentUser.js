const initialize = ({ loggedUser, models }) => ({
  execute: async ({ userId }) => {
    const currentUser = await loggedUser.getCurrentUser({ models, userId })
    currentUser.identities = [currentUser.getDefaultIdentity()]
    return currentUser.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
