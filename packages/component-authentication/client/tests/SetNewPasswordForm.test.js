import React from 'react'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { theme } from '@hindawi/ui'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from './testUtils'

import SetNewPasswordForm from '../components/SetNewPasswordForm'

describe('Set New Password Form', () => {
  afterEach(cleanup)

  it('should not submit if validation fails', async done => {
    const setPasswordMock = jest.fn()

    const { getByText } = render(
      <SetNewPasswordForm setPassword={setPasswordMock} />,
    )

    fireEvent.click(getByText(/confirm/i))

    setTimeout(() => {
      expect(getByText(/required/i)).toBeInTheDocument()
      expect(setPasswordMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should check that passwords are the same', async done => {
    const setPasswordMock = jest.fn()

    const { getByTestId, getByText } = render(
      <SetNewPasswordForm setPassword={setPasswordMock} />,
    )

    fireEvent.change(getByTestId('password'), {
      target: { value: 'apassword' },
    })

    fireEvent.change(getByTestId('confirm-password'), {
      target: { value: 'anotherpassowrd' },
    })

    fireEvent.click(getByText(/confirm/i))

    setTimeout(() => {
      expect(getByText(`Passwords don't match.`)).toBeInTheDocument()
      expect(getByText(`Password criteria not met`)).toBeInTheDocument()
      expect(setPasswordMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should validate password', async done => {
    const setPasswordMock = jest.fn()

    const { getByTestId, getByText } = render(
      <SetNewPasswordForm setPassword={setPasswordMock} />,
    )

    fireEvent.blur(getByTestId('password'), {
      target: { value: '' },
    })

    setTimeout(() => {
      expect(getByText('at least 6 characters')).toHaveStyleRule(
        'color',
        theme.colorError,
      )

      expect(getByText('at least 1 uppercase character (A-Z)')).toHaveStyleRule(
        'color',
        theme.colorError,
      )

      expect(getByText('at least 1 lowercase character (a-z)')).toHaveStyleRule(
        'color',
        theme.colorError,
      )

      expect(getByText('at least 1 digit (0-9)')).toHaveStyleRule(
        'color',
        theme.colorError,
      )

      expect(
        getByText('at least 1 special character (punctuation)'),
      ).toHaveStyleRule('color', theme.colorError)

      done()
    })
  })

  it('should call set password if everything is valid', async done => {
    const setPasswordMock = jest.fn()

    const { getByTestId, getByText } = render(
      <SetNewPasswordForm setPassword={setPasswordMock} />,
    )

    fireEvent.change(getByTestId('password'), {
      target: { value: '1Password!' },
    })

    fireEvent.change(getByTestId('confirm-password'), {
      target: { value: '1Password!' },
    })

    fireEvent.click(getByText(/confirm/i))

    setTimeout(() => {
      expect(getByText('at least 6 characters')).toHaveStyleRule(
        'color',
        theme.actionPrimaryColor,
      )

      expect(getByText('at least 1 uppercase character (A-Z)')).toHaveStyleRule(
        'color',
        theme.actionPrimaryColor,
      )

      expect(getByText('at least 1 lowercase character (a-z)')).toHaveStyleRule(
        'color',
        theme.actionPrimaryColor,
      )

      expect(getByText('at least 1 digit (0-9)')).toHaveStyleRule(
        'color',
        theme.actionPrimaryColor,
      )

      expect(
        getByText('at least 1 special character (punctuation)'),
      ).toHaveStyleRule('color', theme.actionPrimaryColor)

      expect(setPasswordMock).toHaveBeenCalledTimes(1)
      expect(setPasswordMock).toHaveBeenCalledWith(
        {
          confirmPassword: '1Password!',
          password: '1Password!',
        },
        expect.anything(),
      )

      done()
    })
  })
})
