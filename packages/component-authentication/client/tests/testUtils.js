import React from 'react'
import { theme } from '@hindawi/ui'
import { MemoryRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { fireEvent, wait, render as rtlRender } from 'react-testing-library'

const ENTER = 13

export const render = (ui, cfg = {}) => {
  const Component = () => (
    <ThemeProvider theme={theme}>
      <MemoryRouter initialEntries={cfg.initialEntries}>{ui}</MemoryRouter>
    </ThemeProvider>
  )

  const utils = rtlRender(<Component />)

  return {
    ...utils,
    clickCheckbox: () => {
      fireEvent.click(utils.container.querySelector(`input[type=checkbox]`))
    },
    selectOption: value => {
      fireEvent.click(utils.container.querySelector(`button[type=button]`))
      wait(() => utils.getByText(value))
      fireEvent.click(utils.getByText(value))
    },
    selectCountry: value => {
      fireEvent.change(utils.getByTestId('country-menu'), {
        target: {
          value,
        },
      })
      fireEvent.keyUp(utils.getByTestId('country-menu'), {
        target: {
          which: ENTER,
        },
      })
    },
  }
}
