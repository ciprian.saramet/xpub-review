import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row } from '@hindawi/ui'

export const RightContainer = styled.div`
  align-items: center;
  display: flex;
  height: ${th('appBar.height')};
  margin-right: calc(${th('gridUnit')} * 2);

  position: absolute;
  right: 0;
  top: 0;
`

export const LogoContainer = styled.div`
  align-items: center;
  display: flex;
  height: ${th('appBar.height')};
  margin-left: calc(${th('gridUnit')} * 2);

  position: absolute;
  top: 0;
  left: 0;
  z-index: 1;

  cursor: pointer;
`

export const Root = styled.div`
  align-items: center;
  background-color: ${th('appBar.colorBackground')};
  box-shadow: ${th('appBar.boxShadow')};
  height: calc(${th('gridUnit')} * 18);
  display: flex;
  justify-content: center;
  padding: 0 calc(${th('gridUnit')} * 4);
  z-index: ${th('zIndex.appBar')};
`

export const RibbonRow = styled(Row)`
  background-color: ${th('colorInfo')};
  position: sticky;
  z-index: 1;
  top: ${th('appBar.height')};
`
