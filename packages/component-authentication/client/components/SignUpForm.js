import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import { withProps } from 'recompose'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, Checkbox, H2, Spinner, TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Text,
  Label,
  Menu,
  ActionLink,
  MenuCountry,
  ShadowedBox,
  ValidatedFormField,
  PasswordValidation,
  validators,
  passwordValidator,
} from '@hindawi/ui'

import { BottomRow } from './'

const containerPaddings = {
  pt: 0,
  pr: 0,
  pb: 0,
  pl: 0,
}
const validateCheckbox = value => (!value ? 'Required' : undefined)
const SignUpForm = ({
  step,
  onSubmit,
  prevStep,
  isInvited,
  buttonText,
  isFetching,
  fetchingError,
  initialValues,
  journal: { titles = [] },
}) => (
  <Formik
    initialValues={initialValues}
    onSubmit={onSubmit}
    validate={step === 1 ? passwordValidator : null}
  >
    {({ handleSubmit, errors, touched, values }) => (
      <Root {...containerPaddings}>
        <H2 mt={10}>{isInvited ? 'Add new account details' : 'Sign up'}</H2>
        {isInvited && <Text align="center">{get(values, 'email', '')}</Text>}

        {step === 0 && (
          <Fragment>
            <Row height={12} mt={6} pl={8} pr={8}>
              <Item mr={2} vertical>
                <Label required>First Name</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="first-name"
                  inline
                  name="givenNames"
                  validate={[validators.required]}
                />
              </Item>

              <Item ml={2} vertical>
                <Label required>Last Name</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="last-name"
                  inline
                  name="surname"
                  validate={[validators.required]}
                />
              </Item>
            </Row>

            <Row height={12} mt={6} pl={8} pr={8}>
              <Item mr={2} vertical>
                <Label required>Title</Label>
                <ValidatedFormField
                  component={Menu}
                  data-test-id="title-menu"
                  name="title"
                  options={titles}
                  validate={[validators.required]}
                />
              </Item>

              <Item ml={2} vertical>
                <Label required>Country</Label>
                <ValidatedFormField
                  component={MenuCountry}
                  data-test-id="country-menu"
                  name="country"
                  validate={[validators.required]}
                />
              </Item>
            </Row>

            <Row height={12} mt={6} pl={8} pr={8}>
              <Item vertical>
                <Label required>Affiliation</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="affiliation"
                  inline
                  name="aff"
                  validate={[validators.required]}
                />
              </Item>
            </Row>

            <CheckboxRow mt={4}>
              <ValidatedFormField
                component={CustomCheckbox}
                data-test-id="checkbox-agree"
                name="agreeTc"
                validate={[validateCheckbox]}
              />
            </CheckboxRow>

            <Row height={12} mt={10} pl={8} pr={8}>
              <Text display="inline" mb={8} secondary small>
                This account information will be processed by us in accordance
                with our Privacy Policy for the purpose of registering your
                account and allowing you to use the services available via the
                platform. Please read our
                <ActionLink
                  display="inline"
                  ml={2}
                  mr={2}
                  to="https://www.hindawi.com/privacy/"
                >
                  Privacy Policy
                </ActionLink>
                for further information.
              </Text>
            </Row>
          </Fragment>
        )}

        {step === 1 && (
          <Fragment>
            {!isInvited && (
              <Row height={12} mt={6} pl={8} pr={8}>
                <Item vertical>
                  <Label required>Email</Label>
                  <ValidatedFormField
                    component={TextField}
                    data-test-id="sign-up-email"
                    inline
                    name="email"
                    validate={[validators.required, validators.emailValidator]}
                  />
                </Item>
              </Row>
            )}

            <PasswordValidation
              errors={errors}
              formValues={values}
              pl={8}
              pr={8}
              touched={touched}
            />
          </Fragment>
        )}

        <Row height={10} mt={6} pl={8} pr={8}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Fragment>
              {step === 1 && (
                <Button fullWidth mr={4} onClick={prevStep} secondary>
                  BACK
                </Button>
              )}

              <Button
                data-test-id="form-submit"
                fullWidth
                ml={step === 1 ? 4 : 0}
                onClick={handleSubmit}
                primary
                type="submit"
              >
                {buttonText}
              </Button>
            </Fragment>
          )}
        </Row>

        {fetchingError && (
          <Row justify="center" mt={4} pl={8} pr={8}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <BottomRow alignItems="center" height={16} mt={6}>
          <Text display="flex" secondary>
            {`Already have an account? `}
            <ActionLink internal pl={1} to="/login">
              Sign in
            </ActionLink>
          </Text>
        </BottomRow>
      </Root>
    )}
  </Formik>
)

export default withProps(({ isInvited, step }) => {
  let buttonText = ''

  if (step === 0) {
    buttonText = isInvited
      ? 'PROCEED TO SET PASSWORD'
      : 'PROCEED TO SET EMAIL AND PASSWORD'
  } else {
    buttonText = isInvited ? 'CONFIRM' : 'CREATE ACCOUNT'
  }

  return { buttonText }
})(SignUpForm)

const CustomCheckbox = input => (
  <RootCheckbox>
    <Checkbox checked={input.value} data-test-id="agree-checkbox" {...input} />
    <Text display="inline-flex">
      I agree with the
      <ActionLink display="inline" ml={1} to="https://www.hindawi.com/terms/">
        Terms of Service
      </ActionLink>
    </Text>
  </RootCheckbox>
)

// #region styled-components
const Root = styled(ShadowedBox)`
  margin: 0 auto;
  margin-top: calc(${th('gridUnit')} * 10);
`

const RootCheckbox = styled.div.attrs(props => ({
  className: 'custom-checkbox',
}))`
  display: flex;

  + div[role='alert'] {
    margin-top: 0;
  }

  & label {
    margin-bottom: ${th('gridUnit')};
    & span {
      color: ${th('colorText')};
      font-family: ${th('fontReading')};
      font-size: ${th('fontSizeBase')};
    }
  }
`

const CheckboxRow = styled.div`
  padding-left: calc(${th('gridUnit')} * 8);
  width: 100%;

  ${space};
`
// #endregion
