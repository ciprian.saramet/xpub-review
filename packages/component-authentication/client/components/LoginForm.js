import React from 'react'
import { Formik } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H2, Spinner, TextField } from '@pubsweet/ui'
import {
  Row,
  Text,
  Item,
  Label,
  ActionLink,
  PasswordField,
  ValidatedFormField,
} from '@hindawi/ui'
import { withHandlers, compose } from 'recompose'

import { BottomRow } from './'
import { loginValidate } from '../utils'

const LoginForm = ({ loginUser, fetchingError, isFetching, onEnter }) => (
  <Formik onSubmit={loginUser} validate={loginValidate}>
    {({ handleSubmit }) => (
      <Root>
        <H2>Login</H2>

        <Row mt={6} pl={10} pr={10}>
          <Item vertical>
            <Label required>Email</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="login-email"
              inline
              name="email"
              onKeyPress={onEnter(handleSubmit)}
            />
          </Item>
        </Row>

        <Row mt={4} pl={10} pr={10}>
          <Item vertical>
            <Label required>Password</Label>
            <ValidatedFormField
              component={PasswordField}
              data-test-id="login-password"
              inline
              name="password"
              onKeyPress={onEnter(handleSubmit)}
            />
          </Item>
        </Row>

        <Row height={2} justify="flex-end" pl={10} pr={10}>
          <ActionLink alignItems="center" internal to="/password-reset">
            Forgot your password?
          </ActionLink>
        </Row>

        <Row mt={6}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Button
              data-test-id="login-button"
              ml={10}
              mr={10}
              onClick={handleSubmit}
              primary
              width={72}
            >
              LOG IN
            </Button>
          )}
        </Row>

        {fetchingError && (
          <Row height={4} justify="flex-start" mt={2} pl={10} pr={10}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <BottomRow alignItems="center" bgColor="#f6f6f6" height={16} mt={6}>
          <Text display="flex" secondary>
            {`Don't have an account?`}
            <ActionLink alignItems="center" internal pl={1} to="/signup">
              Sign up
            </ActionLink>
          </Text>
        </BottomRow>
      </Root>
    )}
  </Formik>
)

export default compose(
  withHandlers({
    onEnter: () => handleSubmit => event => {
      if (event.which === 13) {
        event.preventDefault()
        handleSubmit()
      }
    },
  }),
)(LoginForm)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border-radius: ${th('gridUnit')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  margin-top: calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 10);
  width: calc(${th('gridUnit')} * 92);
`
// #endregion
