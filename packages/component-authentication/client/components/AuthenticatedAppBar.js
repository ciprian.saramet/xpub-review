import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Route } from 'react-router-dom'
import { Logo, Text, UserDropDown } from '@hindawi/ui'

import {
  Root,
  RibbonRow,
  LogoContainer,
  RightContainer,
} from './sharedStyledComponents'

const autosaveIndicatorPaths = [
  '/submit/:submissionId/:manuscriptId',
  '/details/:submissionId/:manuscriptId',
]

const AuthenticatedAppBar = ({
  goTo,
  logo,
  logout,
  currentUser,
  isConfirmed,
  goToDashboard,
  submitButton: SubmitButton,
  autosaveIndicator: AutosaveIndicator,
}) => (
  <Fragment>
    <Root>
      <LogoContainer>
        <Logo goTo={goToDashboard} height={54} src={logo} title="Hindawi" />
      </LogoContainer>
      <RightContainer>
        {autosaveIndicatorPaths.map(path => (
          <Route component={AutosaveIndicator} exact key={path} path={path} />
        ))}
        <Route
          component={() => <SubmitButton currentUser={currentUser} />}
          exact
          path="/"
        />
        <UserDropDown currentUser={currentUser} goTo={goTo} logout={logout} />
      </RightContainer>
    </Root>
    {!get(currentUser, 'identities.0.isConfirmed', true) && (
      <RibbonRow>
        <Text pb={1} pt={2}>
          Your account is not confirmed. Please check your email.
        </Text>
      </RibbonRow>
    )}
  </Fragment>
)

export default AuthenticatedAppBar
