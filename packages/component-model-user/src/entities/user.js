const uuid = require('uuid')

const { HindawiBaseModel } = require('component-model')

class User extends HindawiBaseModel {
  static get tableName() {
    return 'user'
  }

  constructor(properties) {
    super(properties)
    if (!this.id) {
      this.isSubscribedToEmails = true
      this.unsubscribeToken = uuid.v4()
    }
  }

  static get schema() {
    return {
      properties: {
        defaultIdentity: { type: 'string' },
        isActive: { type: 'boolean' },
        isSubscribedToEmails: { type: 'boolean' },
        confirmationToken: { type: ['string', 'null'], format: 'uuid' },
        invitationToken: { type: ['string', 'null'], format: 'uuid' },
        passwordResetToken: { type: ['string', 'null'], format: 'uuid' },
        unsubscribeToken: { type: ['string', 'null'], format: 'uuid' },
        agreeTc: { type: 'boolean' },
        passwordResetTimestamp: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      identities: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-identity').model,
        join: {
          from: 'user.id',
          to: 'identity.userId',
        },
      },
      teamMemberships: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-team-member').model,
        join: {
          from: 'user.id',
          to: 'team_member.userId',
        },
      },
    }
  }

  getDefaultIdentity() {
    if (!this.identities) {
      throw new Error('Identities are required.')
    }

    return this.identities.find(
      identity => identity.type === this.defaultIdentity,
    )
  }

  assignIdentity(identity) {
    this.identities = this.identities || []
    this.identities.push(identity)
  }

  removeIdentity(identityId) {
    if (!this.identities) {
      throw new Error('Identities are required.')
    }

    const identity = this.identities.find(
      identity => identity.id === identityId,
    )
    if (!identity) {
      throw new Error(`Identity does not exist.`)
    }

    if (identity.type === 'local') {
      throw new ValidationError(`The default identity cannot be removed.`)
    }
    this.identities = this.identities.filter(
      identity => identity.id !== identityId,
    )
  }

  toDTO() {
    return {
      ...this,
      identities:
        this.identities && this.identities.map(identity => identity.toDTO()),
    }
  }

  getGlobalRole() {
    if (!this.teamMemberships) {
      throw new Error('TeamMemberships are required.')
    }

    const globalMember = this.teamMemberships.find(
      member => !member.team.manuscriptId,
    )

    return globalMember ? globalMember.team.role : undefined
  }
}

module.exports = User
