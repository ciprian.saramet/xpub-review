const initialize = ({ User, TeamMember, Team, Identity }) => ({
  execute: async ({ userId }) => {
    const currentUser = await User.find(userId)
    const teamMember = await TeamMember.findByField('userId', currentUser.id)
    const teams = await Promise.all(
      teamMember.map(async tm => Team.find(tm.teamId)),
    )

    const teamRoles = teams.map(team => team.role)
    const globalRole = teamRoles.find(role => Team.GlobalRoles.includes(role))
    currentUser.role = globalRole || 'user'
    const currentUserIdentities = await Identity.findByField(
      'userId',
      currentUser.id,
    )
    currentUser.identities = currentUserIdentities
    return currentUser.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
