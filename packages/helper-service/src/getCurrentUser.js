module.exports = {
  async getCurrentUser({ models, userId }) {
    const currentUser = await models.User.find(userId, 'identities')
    const teamMember = await models.TeamMember.findByField(
      'userId',
      currentUser.id,
    )
    const teams = await Promise.all(
      teamMember.map(async tm => models.Team.find(tm.teamId)),
    )

    const teamRoles = teams.map(team => team.role)
    const globalRole = teamRoles.find(role =>
      models.Team.GlobalRoles.includes(role),
    )
    currentUser.role = globalRole || 'user'
    return currentUser
  },
}
