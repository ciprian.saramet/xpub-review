process.env.BABEL_ENV = 'development'
process.env.NODE_ENV = 'development'

const path = require('path')

module.exports = dir => {
  const include = [
    path.join(dir, 'src'),
    /component-modal/,
    /@pubsweet\/[^/]+\/src/,
    /component-hindawi-ui\/src/,
    /component-hidawi-ui\/src\/peerReviewComponents/,
    /component-[A-z-]*\/client/,
  ]
  return {
    devtool: 'cheap-module-source-map',
    devServer: {
      compress: true,
      disableHostCheck: true,
      public: 'styleguide.review.hindawi.com',
    },
    module: {
      rules: [
        {
          oneOf: [
            {
              test: /\.jsx?$/,
              include,
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env', '@babel/preset-react'],
                plugins: ['@babel/plugin-proposal-class-properties'],
                cacheDirectory: true,
              },
            },
            {
              test: /\.css$/,
              use: ['style-loader', 'css-loader'],
            },
            {
              exclude: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/],
              loader: 'file-loader',
              options: {
                name: 'static/media/[name].[hash:8].[ext]',
              },
            },
          ],
        },
      ],
    },
    resolve: {
      alias: {
        'styled-components': path.resolve(
          '../../',
          'node_modules',
          'styled-components',
        ),
      },
    },
    watch: true,
  }
}
