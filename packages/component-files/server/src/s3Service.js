const { promisify } = require('util')
const { S3 } = require('aws-sdk')
const config = require('config')

const { region, accessKeyId, secretAccessKey, bucket } = config.get(
  'pubsweet-component-aws-s3',
)

const s3 = new S3({
  region,
  accessKeyId,
  secretAccessKey,
  params: {
    Bucket: bucket,
  },
  useAccelerateEndpoint: true,
})

const uploadAsync = promisify(s3.upload.bind(s3))
const deleteObject = promisify(s3.deleteObject.bind(s3))
const getSignedUrl = promisify(s3.getSignedUrl.bind(s3))
const copyObject = promisify(s3.copyObject.bind(s3))
const getObjectAsync = promisify(s3.getObject.bind(s3))
const listObjectsAsync = promisify(s3.listObjectsV2.bind(s3))

module.exports.listObjects = prefix =>
  listObjectsAsync({
    Prefix: prefix,
  })

module.exports.getObject = key => getObjectAsync({ Key: key })

module.exports.getObjectStream = key =>
  s3.getObject({ Key: key }).createReadStream()

module.exports.upload = async ({ key, mimetype, stream, metadata = {} }) => {
  const uploadParams = {
    Key: key,
    Body: stream,
    ContentType: mimetype,
    Metadata: metadata,
  }

  return uploadAsync(uploadParams)
}

module.exports.delete = async key => deleteObject({ Key: key })

module.exports.getSignedUrl = async key =>
  getSignedUrl('getObject', { Key: key })

module.exports.copyObject = async ({ prevKey, newKey }) => {
  const copyParams = {
    CopySource: `${bucket}/${prevKey}`,
    Key: newKey,
  }

  return copyObject(copyParams)
}
