const uuid = require('uuid')

const initialize = ({ models: { File, Comment }, s3Service }) => ({
  async execute({ entityId, fileInput, fileData }) {
    const { stream, filename, mimetype } = await fileData
    const key = `${entityId}/${uuid.v4()}`
    const parsedName = filename

    await s3Service.upload({
      key,
      stream,
      mimetype,
      metadata: {
        filename,
        type: fileInput.type,
      },
    })

    const file = new File({
      manuscriptId: null,
      commentId: entityId,
      size: fileInput.size,
      fileName: parsedName,
      providerKey: key,
      mimeType: mimetype,
      originalName: filename,
      type: File.Types[fileInput.type],
    })
    await file.save()

    const comment = await Comment.find(entityId, 'files')
    comment.assignFile(file)
    await comment.save()

    return {
      ...file,
      filename: file.fileName,
    }
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
