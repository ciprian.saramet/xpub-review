const initialize = ({ logEvent, s3Service, models, useCases }) => {
  const { File } = models
  const manuscriptTypes = [
    File.Types.manuscript,
    File.Types.supplementary,
    File.Types.coverLetter,
  ]
  const commentTypes = [
    File.Types.reviewComment,
    File.Types.responseToReviewers,
  ]
  return {
    execute: async ({
      params: { entityId, fileInput, file: fileData },
      userId,
    }) => {
      if (fileInput.size > 50000000) {
        return new ValidationError('File must not be bigger than 50MB.')
      }

      const isCommentFile = commentTypes.includes(fileInput.type)
      const isManuscriptFile = manuscriptTypes.includes(fileInput.type)

      let useCase
      if (isManuscriptFile) useCase = 'uploadManuscriptFile'
      else if (isCommentFile) useCase = 'uploadCommentFile'
      else throw new Error('File must have a valid type.')

      return useCases[useCase]
        .initialize({ models, s3Service, logEvent })
        .execute({
          entityId,
          fileInput,
          fileData,
          userId,
        })
    },
  }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
