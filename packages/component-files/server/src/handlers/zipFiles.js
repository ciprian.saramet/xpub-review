const { chain, get } = require('lodash')
const nodeArchiver = require('archiver')

const zipFiles = ({
  s3,
  models: { File, Team, User },
  archiver = nodeArchiver,
}) => async (req, res) => {
  const { manuscriptId } = req.params
  const manuscriptFileTypes = [
    File.Types.manuscript,
    File.Types.coverLetter,
    File.Types.supplementary,
  ]

  const user = await User.find(req.user, 'teamMemberships.team')
  const userRole = chain(user)
    .get('teamMemberships', [])
    .find(tm => tm.team.manuscriptId === manuscriptId)
    .get('team.role')
    .value()

  const manuscriptFiles = await File.findBy({ manuscriptId })

  const filteredFiles = manuscriptFiles
    .filter(file => manuscriptFileTypes.includes(file.type))
    .filter(
      f =>
        !(f.type === File.Types.coverLetter && userRole === Team.Role.reviewer),
    )
  const archive = archiver('zip')
  archive.pipe(res)

  filteredFiles
    .map(file => ({ stream: s3.getObjectStream(file.providerKey), file }))
    .forEach(({ stream, file }) => {
      if (get(file, 'type') === 'supplementary') file.type = 'supplemental'
      archive.append(stream, {
        name: `${get(file, 'type', 'supplementary')}/${get(file, 'fileName')}`,
      })
    })

  archive.finalize()
}

module.exports = zipFiles
