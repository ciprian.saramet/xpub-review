import { withHandlers } from 'recompose'

export default withHandlers({
  previewFile: ({ getSignedUrl }) => file => {
    if (!getSignedUrl) return

    const windowReference = window.open()
    getSignedUrl(file.id).then(({ signedUrl }) => {
      windowReference.location = signedUrl
    })
  },
})
