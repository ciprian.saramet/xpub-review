const { HindawiBaseModel } = require('component-model')

class ArticleType extends HindawiBaseModel {
  static get tableName() {
    return 'article_type'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-journal-article-type').model,
        join: {
          from: 'article_type.id',
          to: 'journal_article_type.articleTypeId',
        },
      },
    }
  }
  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = ArticleType
