const ArticleType = require('./src/entities/articleType')

module.exports = {
  model: ArticleType,
  modelName: 'ArticleType',
}
