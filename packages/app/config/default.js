require('dotenv').config()
const path = require('path')
const { get } = require('lodash')
const logger = require('./loggerCustom')
const components = require('./components.json')
const journalConfig = require('../app/config/journal')

Object.assign(global, require('@pubsweet/errors'))

const getDbConfig = () => {
  if (process.env.DATABASE) {
    return {
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DATABASE,
      host: process.env.DB_HOST,
      port: 5432,
      ssl: true,
      idleTimeoutMillis: 30000,
      connectionTimeoutMillis: 20000,
    }
  }
  return {}
}

module.exports = {
  authsome: {
    mode: path.resolve(__dirname, 'authsome-mode.js'),
    teams: {
      handlingEditor: {
        name: 'Handling Editors',
      },
      reviewer: {
        name: 'Reviewer',
      },
    },
  },
  pubsweet: {
    components,
  },
  dbManager: {
    migrationsPath: path.join(process.cwd(), 'migrations'),
  },
  'pubsweet-server': {
    db: getDbConfig(),
    pool: { min: 0, max: 10 },
    ignoreTerminatedConnectionError: true,
    port: 3000,
    logger,
    uploads: 'uploads',
    secret: 'SECRET',
    enableExperimentalGraphql: true,
    graphiql: true,
    morganLogFormat:
      ':remote-addr [:date[clf]] :method :url :graphql[operation] :status :response-time ms',
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    baseUrl: process.env.CLIENT_BASE_URL || 'http://localhost:3000',
    'login-redirect': '/',
    'redux-log': process.env.NODE_ENV !== 'production',
    theme: process.env.PUBSWEET_THEME,
  },
  orcid: {
    clientID: process.env.ORCID_CLIENT_ID,
    clientSecret: process.env.ORCID_CLIENT_SECRET,
    callbackPath: '/api/users/orcid/callback',
    successPath: '/profile',
  },
  'mail-transport': {
    sendmail: true,
  },
  'password-reset': {
    url:
      process.env.PUBSWEET_PASSWORD_RESET_URL ||
      'http://localhost:3000/password-reset',
    sender: process.env.PUBSWEET_PASSWORD_RESET_SENDER || 'dev@example.com',
    hoursUntilNextRequest: 24,
  },
  'pubsweet-component-aws-s3': {
    secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    region: process.env.AWS_S3_REGION,
    bucket: process.env.AWS_S3_BUCKET,
  },
  'mts-service': {
    ftp: {
      user: process.env.FTP_USERNAME,
      password: process.env.FTP_PASSWORD,
      host: process.env.FTP_HOST,
      port: 21,
      localRoot: `./`,
      remoteRoot: '/BCA/',
      exclude: ['*.js'],
    },
    journal: get(journalConfig, 'metadata.mts'),
    xml: {
      compact: true,
      ignoreComment: true,
      spaces: 2,
      fullTagEmptyElement: true,
    },
  },
  'invite-reset-password': {
    url: process.env.PUBSWEET_INVITE_PASSWORD_RESET_URL || '/invite',
  },
  'forgot-password': {
    url: process.env.PUBSWEET_FORGOT_PASSWORD_URL || '/forgot-password',
  },
  'accept-handling-editor': {
    url: process.env.PUBSWEET_ACCEPT_HE || '/emails/accept-he',
  },
  'decline-handling-editor': {
    url: process.env.PUBSWEET_DECLINE_HE || '/emails/decline-he',
  },
  'invite-reviewer': {
    url: process.env.PUBSWEET_INVITE_REVIEWER_URL || '/invite-reviewer',
  },
  'accept-review-new-user': {
    url:
      process.env.PUBSWEET_ACCEPT_NEW_REVIEWER ||
      '/emails/accept-review-new-user',
  },
  'accept-review': {
    url: process.env.PUBSWEET_ACCEPT_REVIEWER || '/emails/accept-review',
  },
  'decline-review': {
    url: process.env.PUBSWEET_DECLINE_REVIEWER || '/emails/decline-review',
  },
  'confirm-signup': {
    url: process.env.PUBSWEET_CONFIRM_SIGNUP_URL || '/confirm-signup',
  },
  'eqs-decision': {
    url: process.env.PUBSWEET_EQS_DECISION || '/eqs-decision',
  },
  'eqa-decision': {
    url: process.env.PUBSWEET_EQA_DECISION || '/eqa-decision',
  },
  unsubscribe: {
    url: process.env.PUBSWEET_UNSUBSCRIBE_URL || '/unsubscribe',
  },
  roles: get(journalConfig, 'rolesAccess'),
  mailer: {
    path: `${__dirname}/${
      JSON.parse(get(process, 'env.EMAIL_SENDING', true))
        ? 'mailer'
        : 'mailer-mock'
    }`,
  },
  SES: {
    accessKey: process.env.AWS_SES_ACCESS_KEY,
    secretKey: process.env.AWS_SES_SECRET_KEY,
    region: process.env.AWS_SES_REGION,
  },
  publicKeys: ['pubsweet-client', 'authsome'],
  statuses: get(journalConfig, 'statuses'),
  publons: {
    key: process.env.PUBLONS_KEY || '',
    reviewersUrl: 'https://api.clarivate.com/reviewer-connect/api/',
  },
  journalConfig,
  journal: {
    name: get(journalConfig, 'metadata.nameText'),
    staffEmail: get(journalConfig, 'metadata.email'),
    ...get(journalConfig, 'metadata.emailData'),
  },
  features: {
    mts: JSON.parse(get(process, 'env.FEATURE_MTS', true)),
  },
  recommendations: {
    minor: 'minor',
    major: 'major',
    reject: 'reject',
    publish: 'publish',
    type: {
      review: 'review',
      editor: 'editorRecommendation',
    },
  },
  mininumNumberOfInvitedReviewers: 5,
  mininumNumberOfSubmittedReports: 2,
  passwordStrengthRegex: new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&,.?;\'*><)([}{}":`~+=_-\\|/])(?=.{6,128})',
  ),
  activityLogEvents: get(journalConfig, 'activityLogEvents', {}),
  activityLogObjectTypes: get(journalConfig, 'activityLogObjectTypes', {}),
  globalRoles: ['admin', 'editorInChief', 'editorialAssistant'],
  reminders: {
    businessDays: JSON.parse(get(process, 'env.BUSINESS_DAYS', true)),
    handlingEditor: {
      inviteReviewers: {
        days: {
          first: process.env.REMINDER_REVIEWER_INVITATION_FIRST || 5,
          second: process.env.REMINDER_REVIEWER_INVITATION_SECOND || 8,
          third: process.env.REMINDER_REVIEWER_INVITATION_THIRD || 11,
        },
        timeUnit: process.env.REMINDER_REVIEWER_INVITATION_TIME_UNIT || 'days',
      },
      acceptInvitation: {
        days: {
          first: process.env.REMINDER_HE_FIRST || 3,
          second: process.env.REMINDER_HE_SECOND || 6,
        },
        remove: process.env.REMINDER_REMOVE_HE || 8,
        timeUnit: process.env.REMINDER_HE_TIME_UNIT || 'days',
      },
    },
    reviewer: {
      acceptInvitation: {
        days: {
          first: process.env.REMINDER_REVIEWER_FIRST || 4,
          second: process.env.REMINDER_REVIEWER_SECOND || 7,
          third: process.env.REMINDER_REVIEWER_THIRD || 14,
        },
        remove: process.env.REMINDER_REMOVE_REVIEWER || 21,
        timeUnit: process.env.REMINDER_REVIEWER_TIME_UNIT || 'days',
      },
      submitReport: {
        days: {
          first: process.env.REMINDER_ACCEPTED_REVIEWER_FIRST || 7,
          second: process.env.REMINDER_ACCEPTED_REVIEWER_SECOND || 13,
          third: process.env.REMINDER_ACCEPTED_REVIEWER_THIRD || 18,
        },
        remove: process.env.REMINDER_REMOVE_ACCEPTED_REVIEWER || 24,
        timeUnit: process.env.REMINDER_ACCEPTED_REVIEWER_TIME_UNIT || 'days',
      },
    },
  },
}
