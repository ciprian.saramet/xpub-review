import React from 'react'
import gql from 'graphql-tag'
import ReactDOM from 'react-dom'
import { Root } from 'pubsweet-client'
import { theme } from '@hindawi/ui'
import { createBrowserHistory } from 'history'
import { JournalProvider } from 'xpub-journal'
import { ModalProvider } from 'component-modal'
import { JournalProvider as HindawiJournalProvider } from 'component-journal-info'

import Routes from './routes'
import * as journal from './config/journal'

// wait for PS to stop supporting redux
const store = {
  subscribe: () => {},
  dispatch: () => {},
  getState: () => {},
}

const history = createBrowserHistory()

const typeDefs = gql`
  extend type Mutation {
    updateAutosave: Boolean
  }
`

const makeApolloConfig = ({ cache, link, ...config }) => {
  const clientState = {
    typeDefs,
    resolvers: {
      Mutation: {
        updateAutosave: (_, { params }, { cache }) => {
          const data = {
            autosave: {
              __typename: 'AutosaveState',
              ...params,
            },
          }
          cache.writeData({ data })
          return null
        },
      },
    },
    defaults: {
      autosave: {
        __typename: 'AutosaveState',
        error: null,
        updatedAt: null,
        inProgress: false,
      },
    },
  }
  return {
    cache,
    link,
    ...config,
    ...clientState,
  }
}

const render = () => {
  ReactDOM.render(
    <React.Fragment>
      <ModalProvider>
        <HindawiJournalProvider journal={journal}>
          <JournalProvider journal={journal}>
            <Root
              connectToWebSocket={false}
              history={history}
              makeApolloConfig={makeApolloConfig}
              routes={<Routes />}
              store={store}
              theme={theme}
            />
          </JournalProvider>
        </HindawiJournalProvider>
      </ModalProvider>
      <div id="ps-modal-root" style={{ height: 0 }} />
      <div id="portal-root" style={{ height: 0 }} />
    </React.Fragment>,
    document.getElementById('root'),
  )
}

render()
