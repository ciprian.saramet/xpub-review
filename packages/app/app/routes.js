import React, { Fragment } from 'react'
import { Route, Switch } from 'react-router'
import { Dashboard } from 'component-dashboard/client'
import {
  Login,
  SignUp,
  InfoPage,
  ResetPassword,
  ConfirmAccout,
  SetNewPassword,
  AuthenticatedComponent,
  SignUpFromInvitation,
} from 'component-authentication/client'
import { EQSDecision, EQADecision } from 'component-screening/client'
import { EmailResponse, ManuscriptDetails } from 'component-chief-minus/client'
import { SubmissionConfirmation, Wizard } from 'component-submission/client'
import {
  AdminDashboard,
  AdminUsers,
  AdminRoute,
  AdminJournals,
} from 'component-admin/client'
import { UserProfilePage, ChangePassword } from 'component-user-profile/client'

import FourOFour from './FourOFour'
import HindawiApp from './HindawiApp'
import ReviewerRedirect from './ReviewerRedirect'
import ManuscriptRedirect from './ManuscriptRedirect'

const Routes = () => (
  <HindawiApp>
    <Switch>
      <Route component={Login} exact path="/login" />
      <Route component={SignUp} exact path="/signup" />
      <Route component={ConfirmAccout} exact path="/confirm-signup" />
      <Route component={SignUpFromInvitation} exact path="/invite" />
      <Route component={ResetPassword} exact path="/password-reset" />
      <Route component={SetNewPassword} exact path="/forgot-password" />
      <Route component={InfoPage} exact path="/info-page" />
      <Route component={EQSDecision} exact path="/eqs-decision" />
      <Route component={EQADecision} exact path="/eqa-decision" />

      <AdminRoute component={AdminUsers} exact path="/admin/users" />
      <AdminRoute component={AdminJournals} exact path="/admin/journals" />
      <AdminRoute component={AdminDashboard} exact path="/admin" />

      <Route
        component={SubmissionConfirmation}
        exact
        path="/confirmation-page"
      />

      <Route component={EmailResponse} exact path="/emails/:action" />

      <Route
        component={ManuscriptRedirect}
        exact
        path="/projects/:projectId/versions/:versionId/details"
      />
      <Route component={ReviewerRedirect} exact path="/invite-reviewer" />
      <Route component={FourOFour} exact path="/404" />

      <AuthenticatedComponent>
        {({ currentUser }) => (
          <Fragment>
            <Route
              component={routeProps => (
                <Dashboard currentUser={currentUser} {...routeProps} />
              )}
              exact
              path="/"
            />
            <Route
              component={routeProps => (
                <ManuscriptDetails currentUser={currentUser} {...routeProps} />
              )}
              currentUser={currentUser}
              exact
              path="/details/:submissionId/:manuscriptId"
            />

            <Route
              component={ChangePassword}
              exact
              path="/profile/change-password"
            />
            <Route
              component={Wizard}
              exact
              path="/submit/:submissionId/:manuscriptId"
            />
            <Route component={UserProfilePage} exact path="/profile" />
          </Fragment>
        )}
      </AuthenticatedComponent>
    </Switch>
  </HindawiApp>
)

export default Routes
