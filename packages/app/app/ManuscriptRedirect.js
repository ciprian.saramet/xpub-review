import React, { useEffect } from 'react'
import { get } from 'lodash'
import { Loader, parseSearchParams } from '@hindawi/ui'

const ManuscriptRedirect = ({ location, history, match }) => {
  useEffect(() => {
    const { agree, invitationId } = parseSearchParams(location.search)
    const submissionId = get(match, 'params.projectId')
    const manuscriptId = get(match, 'params.versionId')

    if (invitationId && agree === 'true') {
      history.push({
        pathname: `/emails/accept-review`,
        search: `?invitationId=${invitationId}&manuscriptId=${manuscriptId}&submissionId=${submissionId}`,
      })
    } else {
      history.replace({
        pathname: `/details/${submissionId}/${manuscriptId}`,
      })
    }
  }, [history, location.search, match])

  return <Loader />
}

export default ManuscriptRedirect
