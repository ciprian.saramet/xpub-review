import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { Button, H1, H4 } from '@pubsweet/ui'
import { ActionLink, NotFound, SVGLogo } from '@hindawi/ui'

function FourOFour({ history, journal, ...rest }) {
  return (
    <Root>
      <SVGLogo />
      <H1 mt={4}>Page not found</H1>
      <H4 mt={4}>We couldn’t find the page you are looking for.</H4>
      <Button mt={6} onClick={() => history.push('/')} primary small>
        GO TO DASHBOARD
      </Button>

      <NotFound mt={6} />

      <H4 mt={4}>
        In case of any urgent situation contact
        <ActionLink ml={1} to="mailto:technology@hindawi.com">
          technology@hindawi.com
        </ActionLink>
        .
      </H4>

      <H4 mt={4}>
        You can visit our journals on
        <ActionLink display="inline" ml={1} to="https://www.hindawi.com">
          www.hindawi.com
        </ActionLink>
      </H4>
    </Root>
  )
}

export default withJournal(FourOFour)

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding-top: calc(${th('gridUnit')} * 18);
`
// #endregion
