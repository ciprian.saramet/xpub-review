module.exports = {
  questionsForAuthors: [
    {
      formKey: 'conflicts.hasConflicts',
      label: 'Do any authors have conflict of interest to declare?',
    },
    {
      formKey: 'conflicts.dataAvailability',
      label:
        'Have you included a data availabity statement in your manuscript?',
    },
    {
      formKey: 'conflicts.funding',
      label: 'Have you provided a funding statement in your manuscript?',
    },
  ],
}
