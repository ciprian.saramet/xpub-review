module.exports = {
  author: 'Author',
  admin: 'Admin',
  handlingEditor: 'Handling Editor',
  editorInChief: 'Editor in Chief',
  editorialAssistant: 'Editorial Assistant',
}
