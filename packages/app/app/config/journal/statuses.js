module.exports = {
  draft: {
    importance: 1,
    color: 'statusApproved',
    author: {
      label: 'Complete Submission',
      needsAttention: true,
      archived: false,
    },
    admin: {
      label: 'Complete Submission',
      needsAttention: false,
      inProgress: true,
    },
    editorialAssistant: {
      label: 'Complete Submission',
      needsAttention: false,
      inProgress: true,
    },
  },
  technicalChecks: {
    importance: 2,
    color: 'statusApproved',
    author: {
      label: 'Submitted',
      needsAttention: false,
      inProgress: true,
    },
    editorInChief: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Approve QA',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Approve QA',
      needsAttention: true,
    },
  },
  submitted: {
    importance: 3,
    color: 'statusApproved',
    author: {
      label: 'Submitted',
      needsAttention: false,
      inProgress: true,
    },
    editorInChief: {
      label: 'Assign HE',
      needsAttention: true,
    },
    admin: {
      label: 'Assign HE',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Assign HE',
      needsAttention: true,
    },
  },
  heInvited: {
    importance: 4,
    color: 'statusApproved',
    author: {
      label: 'HE Invited',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    editorInChief: {
      label: 'HE Invited',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
  },
  heAssigned: {
    importance: 5,
    color: 'statusApproved',
    author: {
      label: 'HE Assigned',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
    editorInChief: {
      label: 'HE Assigned',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
  },
  reviewersInvited: {
    importance: 6,
    color: 'statusApproved',
    author: {
      label: 'Reviewers Invited',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'Check Review Process',
      needsAttention: true,
    },
    editorInChief: {
      label: 'Reviewers Invited',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    admin: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
  },
  underReview: {
    importance: 7,
    color: 'statusApproved',
    author: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'Check Review Process',
      needsAttention: true,
    },
    editorInChief: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Complete Review',
      needsAttention: true,
    },
    admin: {
      label: 'Complete Review',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Complete Review',
      needsAttention: true,
    },
  },
  reviewCompleted: {
    importance: 8,
    color: 'statusApproved',
    author: {
      label: 'Review Completed',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
    editorInChief: {
      label: 'Review Completed',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Review Completed',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
  },
  revisionRequested: {
    importance: 9,
    color: 'statusApproved',
    author: {
      label: 'Submit Revision',
      needsAttention: true,
    },
    handlingEditor: {
      label: 'Revision Requested',
      needsAttention: false,
      inProgress: true,
    },
    editorInChief: {
      label: 'Revision Requested',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Revision Requested',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Submit Revision',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Submit Revision',
      needsAttention: true,
    },
  },
  pendingApproval: {
    importance: 10,
    color: 'statusApproved',
    author: {
      label: 'Pending Approval',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'Pending Approval',
      needsAttention: false,
      inProgress: true,
    },
    editorInChief: {
      label: 'Make Decision',
      needsAttention: true,
    },
    reviewer: {
      label: 'Pending Approval',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Make Decision',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Make Decision',
      needsAttention: true,
    },
  },
  rejected: {
    importance: 11,
    color: 'statusApproved',
    author: {
      label: 'Rejected',
      needsAttention: false,
      archived: true,
    },
    handlingEditor: {
      label: 'Rejected',
      needsAttention: false,
      archived: true,
    },
    editorInChief: {
      label: 'Rejected',
      needsAttention: false,
      archived: true,
    },
    reviewer: {
      label: 'Rejected',
      needsAttention: false,
      archived: true,
    },
    admin: {
      label: 'Rejected',
      needsAttention: false,
      archived: true,
    },
    editorialAssistant: {
      label: 'Rejected',
      needsAttention: false,
      archived: true,
    },
  },
  inQA: {
    importance: 12,
    color: 'statusApproved',
    author: {
      label: 'Pending approval',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    editorInChief: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Approve QA',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Approve QA',
      needsAttention: true,
    },
  },
  accepted: {
    importance: 13,
    color: 'statusApproved',
    author: {
      label: 'Accepted',
      needsAttention: false,
      archived: true,
    },
    handlingEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: true,
    },
    editorInChief: {
      label: 'Accepted',
      needsAttention: false,
      archived: true,
    },
    reviewer: {
      label: 'Accepted',
      needsAttention: false,
      archived: true,
    },
    admin: {
      label: 'Accepted',
      needsAttention: false,
      archived: true,
    },
    editorialAssistant: {
      label: 'Accepted',
      needsAttention: false,
      archived: true,
    },
  },
  withdrawalRequested: {
    importance: 14,
    color: 'statusApproved',
    author: {
      label: 'Withdrawal Requested',
      needsAttention: false,
      inProgress: true,
    },
    handlingEditor: {
      label: 'Withdrawal Requested',
      needsAttention: false,
      inProgress: true,
    },
    editorInChief: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
    reviewer: {
      label: 'Withdrawal Requested',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
  },
  withdrawn: {
    importance: 15,
    color: 'statusApproved',
    author: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    handlingEditor: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    editorInChief: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    reviewer: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    admin: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    editorialAssistant: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
  },
  deleted: {
    importance: 16,
    color: 'statusApproved',
    author: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    handlingEditor: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    editorInChief: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    reviewer: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    admin: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    editorialAssistant: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
  },
  olderVersion: {
    importance: 8,
    color: 'statusApproved',
    reviewer: {
      label: 'Review Completed',
      needsAttention: false,
      archived: true,
    },
  },
}
