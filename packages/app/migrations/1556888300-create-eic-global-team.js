const { Team, TeamMember } = require('@pubsweet/models')
const { chain, get } = require('lodash')
const { Promise } = require('bluebird')

module.exports = {
  up: async () => {
    const journalTeams = await Team.findBy(
      {
        role: Team.Role.editorInChief,
        manuscriptId: null,
      },
      'members.user.identities',
    )

    const users = chain(journalTeams)
      .flatMap(t => t.members)
      .map(tm => tm.user)
      .uniqBy('id')
      .value()

    let globalTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.editorInChief,
        manuscriptId: null,
        journalId: null,
      },
    })
    if (!globalTeam) {
      globalTeam = new Team({
        role: Team.Role.editorInChief,
        manuscriptId: null,
        journalId: null,
      })
      await globalTeam.save()
    }

    await Promise.each(users, async user => {
      const globalTeamMembers = get(globalTeam, 'members', [])
      const newTeamMember = new TeamMember({
        userId: user.id,
        teamId: globalTeam.id,
        position: globalTeamMembers.length,
      })
      newTeamMember.linkUser(user)
      await newTeamMember.save()

      globalTeamMembers.push(newTeamMember)
      await globalTeam.save()
    })
  },
}
