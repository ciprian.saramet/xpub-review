const { Job, TeamMember } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const now = new Date()

    const pgJobs = await Job.query()
      .withSchema('pgboss')
      .select('*')
      .from('job')
      .where('startafter', '>=', now)

    logger.info(`Found ${pgJobs.length} jobs set to execute in the future.`)
    const re = new RegExp(
      '({){0,1}[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}(}){0,1}',
    )

    await Promise.each(pgJobs, async pgJob => {
      const match = pgJob.name.match(re)
      if (!match) {
        return
      }

      try {
        await TeamMember.find(match[0])

        let job = await Job.findOneBy({
          queryObject: { teamMemberId: match[0] },
        })

        if (job) {
          return
        }

        job = new Job({
          id: pgJob.id,
          teamMemberId: match[0],
          manuscriptId: pgJob.data.manuscriptId,
        })

        await job.save()
      } catch (e) {
        logger.error(e)
      }
    })
  },
}
