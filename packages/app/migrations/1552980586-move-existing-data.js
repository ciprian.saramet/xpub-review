const mimetype = require('mimetype')
const BaseModel = require('@pubsweet/base-model')
const logger = require('@pubsweet/logger')
const Promise = require('bluebird')
const { v4 } = require('uuid')
const { HindawiBaseModel } = require('component-model')
const { flatMap, forOwn, flatten, concat } = require('lodash')

BaseModel.prototype.$beforeInsert = function $beforeInsert() {
  this.id = this.id || v4()
  this.created = this.created || new Date().toISOString()
  this.updated = this.created
}

HindawiBaseModel.prototype.saveRecursively = async function saveRecursively() {
  return this.constructor
    .query()
    .upsertGraph(this, { insertMissing: true, relate: true, noDelete: true })
}

const {
  Team,
  User,
  File,
  Review,
  Journal,
  Comment,
  Identity,
  Manuscript,
  TeamMember,
} = require('@pubsweet/models')

const GLOBAL_ROLES = {
  admin: 'admin',
  eic: 'editorInChief',
  he: 'handlingEditor',
}

class Entity extends BaseModel {
  static get tableName() {
    return 'entities'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'string', format: 'uuid' },
        data: { type: 'string', format: 'jsonb' },
      },
    }
  }
}

module.exports = {
  up: async () => {
    const entities = await Entity.all()
    const journals = await Journal.all()
    const journalId = journals[0].id

    const { globalHeTeam, eicTeam, adminTeam } = await findOrCreateGlobalTeams(
      journalId,
    )

    await createUsers({
      users: entities.filter(entity => entity.data.type === 'user'),
      globalTeams: { globalHeTeam, eicTeam, adminTeam },
    })

    await createManuscripts({
      entities,
      journalId,
      globalTeams: { globalHeTeam, eicTeam, adminTeam },
    })
  },
}

const createManuscripts = async ({ entities, journalId, globalTeams }) => {
  const collections = entities.filter(
    entity => entity.data.type === 'collection',
  )

  if (collections.length === 0) {
    logger.warn(`No collections have been found in the entities table`)
    return
  }

  return Promise.each(collections, async (collection, index, length) => {
    logger.info(`Processing submission ${index + 1} out of ${length}...`)
    const fragments = entities.filter(
      entity =>
        entity.data.type === 'fragment' &&
        entity.data.collectionId === collection.id,
    )

    if (fragments.length === 0) {
      logger.warn(
        `No fragments have been found for collection with UUID ${
          collection.id
        }`,
      )
      return
    }

    return Promise.each(fragments, async fragment => {
      if (!fragment.data.authors || fragment.data.authors.length === 0) {
        logger.error(`Fragment ${fragment.id} does not have any authors`)
        return
      }

      const manuscript = new Manuscript({
        id: fragment.id,
        journalId,
        customId: collection.data.customId,
        submissionId: collection.id,
        version: fragment.data.version,
        title: fragment.data.metadata.title,
        articleType: fragment.data.metadata.type,
        abstract: fragment.data.metadata.abstract,
        agreeTc: getAgreeToTerms(fragment.data),
        publicationDates: setPublicationDates({
          fragment: fragment.data,
          status: collection.data.status,
        }),
        technicalCheckToken: getTechnicalCheckToken(collection.data),
        hasPassedEqa: hasPassedEQA(collection.data),
        hasPassedEqs: hasPassedEQS(collection.data),
        conflicts: getConflicts(fragment.data),
      })

      await manuscript.save()
      manuscript.updateProperties({
        status: getStatus({
          collection: collection.data,
          manuscriptVersion: manuscript.version,
        }),
        created: new Date(fragment.data.created),
      })
      await manuscript.save()

      if (fragment.data.files) {
        await createFiles({ files: fragment.data.files, manuscript })
      }

      const globalMembers = await getGlobalTeamMembers(globalTeams)

      if (fragment.data.invitations && fragment.data.invitations.length > 0) {
        await createReviewers({
          invitations: fragment.data.invitations,
          manuscript,
        })
      }

      if (
        collection.data.invitations &&
        collection.data.invitations.length > 0
      ) {
        await addHandlingEditors({
          invitations: collection.data.invitations,
          manuscript,
        })
      }

      if (fragment.data.revision) {
        await createManuscriptFromRevision({
          collection: collection.data,
          journalId,
          fragment: fragment.data,
          collectionId: collection.id,
        })
      }

      if (
        fragment.data.recommendations &&
        fragment.data.recommendations.length > 0
      ) {
        await createRecommendations({
          recommendations: fragment.data.recommendations,
          manuscript,
          globalMembers,
        })
      }

      await addAuthors({
        authors: fragment.data.authors,
        manuscript,
      })

      if (fragment.data.responseToReviewers) {
        await createResponseToReviewers({
          responseToReviewers: fragment.data.responseToReviewers,
          manuscript,
        })
      }
    })
  })
}

const createUsers = async ({ users, globalTeams }) =>
  Promise.each(users, async (entityUser, index, length) => {
    logger.info(`Processing user ${index + 1} out of ${length}...`)
    if (await Identity.findOneByEmail(entityUser.data.email)) {
      logger.warn(`${entityUser.data.email} already exists.`)
      return
    }

    const user = new User({
      id: entityUser.id,
      defaultIdentity: 'local',
      isSubscribedToEmails: entityUser.data.notifications.email.user,
      unsubscribeToken: v4(),
      agreeTc: true,
      isActive: entityUser.data.isActive,
    })

    const identity = new Identity({
      type: 'local',
      isConfirmed: entityUser.data.isConfirmed,
      email: entityUser.data.email,
      givenNames: entityUser.data.firstName,
      surname: entityUser.data.lastName,
      title: setTitle(entityUser.data.title),
      aff: entityUser.data.affiliation,
      country: entityUser.data.country,
      passwordHash: entityUser.data.passwordHash,
    })

    await user.assignIdentity(identity)
    await user.saveRecursively()

    if (hasGlobalRole(entityUser.data)) {
      addUserToGlobalTeam({ globalTeams, user, entityUser: entityUser.data })
    }
  })

const findOrCreateGlobalTeams = async journalId => {
  let globalHeTeam = await Team.findOneBy({
    queryObject: {
      role: GLOBAL_ROLES.he,
      manuscriptId: null,
    },
  })

  if (!globalHeTeam) {
    globalHeTeam = await createTeam({
      Team,
      role: GLOBAL_ROLES.he,
      journalId,
    })
  }

  let eicTeam = await Team.findOneBy({
    queryObject: {
      role: GLOBAL_ROLES.eic,
      manuscriptId: null,
    },
  })

  if (!eicTeam) {
    eicTeam = await createTeam({
      Team,
      role: GLOBAL_ROLES.eic,
      journalId,
    })
  }

  let adminTeam = await Team.findOneBy({
    queryObject: {
      role: GLOBAL_ROLES.admin,
      manuscriptId: null,
    },
  })

  if (!adminTeam) {
    adminTeam = await createTeam({
      Team,
      role: GLOBAL_ROLES.admin,
      journalId,
    })
  }

  return { globalHeTeam, eicTeam, adminTeam }
}

const createTeam = async ({ Team, role, journalId }) => {
  const team = new Team({
    role,
    journalId,
  })

  await team.save()
  return team
}

const hasGlobalRole = user =>
  user.editorInChief || user.admin || user.handlingEditor

const addUserToGlobalTeam = async ({ globalTeams, user, entityUser }) => {
  let team = {}
  if (entityUser.admin) {
    team = globalTeams.adminTeam
  } else if (entityUser.editorInChief) {
    team = globalTeams.eicTeam
  } else if (entityUser.handlingEditor) {
    team = globalTeams.globalHeTeam
  }
  await team.addMember(user)
  await team.saveRecursively()
}

const getTechnicalCheckToken = collection => {
  if (collection.technicalChecks) {
    return collection.technicalChecks.token
  }

  return null
}

const hasPassedEQS = collection => {
  if (
    collection.technicalChecks &&
    !collection.technicalChecks.token &&
    !collection.technicalChecks.eqa
  ) {
    return true
  }

  return false
}

const hasPassedEQA = collection => {
  if (collection.technicalChecks) {
    return collection.technicalChecks.eqa
  }

  return false
}

const setPublicationDates = ({ fragment, status }) => {
  const pubDates = []
  let publicationType = ''
  if (fragment.submitted) {
    publicationType = Manuscript.Statuses.technicalChecks
    pubDates.push({
      type: publicationType,
      date: fragment.submitted,
    })
  }

  if (status === Manuscript.Statuses.inQA) {
    publicationType = Manuscript.Statuses.inQA
    pubDates.push({
      type: publicationType,
      date: fragment.submitted,
    })
  }

  return pubDates
}

const createFiles = async ({ files, manuscript }) => {
  await Promise.each(Object.keys(files), async key => {
    await Promise.each(files[key], async (f, index) => {
      const file = new File({
        manuscriptId: manuscript.id,
        commentId: undefined,
        type: key === 'manuscripts' ? 'manuscript' : key,
        fileName: f.id,
        size: f.size,
        originalName: f.originalName,
        position: index,
        mimeType: mimetype.lookup(f.originalName),
      })

      manuscript.assignFile(file)
      await file.save()
    })
  })
}

const getAgreeToTerms = fragment => {
  if (fragment.declarations) {
    return fragment.declarations.agree
  }

  return false
}

const getConflicts = fragment => {
  if (fragment.conflicts) {
    fragment.conflicts.hasDataAvailability =
      fragment.conflicts.hasDataAvailability === 'yes'
    fragment.conflicts.hasConflicts = fragment.conflicts.hasConflicts === 'yes'
    fragment.conflicts.hasFunding = fragment.conflicts.hasFunding === 'yes'
  } else {
    fragment.conflicts = {}
  }

  return {
    type: 'object',
    default: {},
    properties: {
      hasConflicts: fragment.conflicts.hasConflicts,
      message: fragment.conflicts.message,
      hasDataAvailability: fragment.conflicts.hasDataAvailability,
      dataAvailabilityMessage: fragment.conflicts.dataAvailabilityMessage,
      hasFunding: fragment.conflicts.hasFunding,
      fundingMessage: fragment.conflicts.fundingMessage,
    },
  }
}

const setTitle = title => {
  const allowedTitles = ['mr', 'mrs', 'miss', 'ms', 'dr', 'prof', null]
  if (allowedTitles.includes(title)) {
    return title
  }

  return null
}

const createReviewers = async ({ invitations, manuscript }) => {
  const reviewerTeam = new Team({
    journalId: manuscript.journalId,
    role: Team.Role.reviewer,
    manuscriptId: manuscript.id,
  })
  manuscript.assignTeam(reviewerTeam)

  await reviewerTeam.save()

  await Promise.each(invitations, async invitation => {
    let user
    try {
      user = await User.find(invitation.userId, 'identities')
    } catch (e) {
      logger.warn(`Fragment ${manuscript.id} might have corrupt invitations.`)
      return
    }

    const options = {
      reviewerNumber: invitation.reviewerNumber,
      responded: invitation.respondedOn
        ? new Date(invitation.respondedOn)
        : null,
      userId: user.id,
      teamId: reviewerTeam.id,
    }

    const reviewer = reviewerTeam.addMember(user, options)
    await reviewer.save()

    await reviewer.updateProperties({
      created: new Date(invitation.invitedOn),
      status: getInvitationStatus(invitation),
    })
    await reviewer.save()
  })
}

const getInvitationStatus = invitation => {
  if (invitation.hasAnswer) {
    if (invitation.isAccepted) {
      return TeamMember.Statuses.accepted
    }
    return TeamMember.Statuses.declined
  }
  return TeamMember.Statuses.pending
}

const createRecommendations = async ({
  recommendations,
  manuscript,
  globalMembers,
}) => {
  await Promise.each(recommendations, async rec => {
    const allTeamMembers = concat(
      flatMap(manuscript.teams, t => t.members),
      globalMembers,
    ).filter(Boolean)

    const teamMember = allTeamMembers.find(
      member => member.userId === rec.userId,
    )
    if (!teamMember) {
      logger.warn(
        `Fragment ${manuscript.id} might have corrupt recommendations`,
      )
      return
    }
    const review = createNewReview({
      rec,
      teamMemberId: teamMember.id,
      manuscriptId: manuscript.id,
    })

    review.assignMember(teamMember)
    await review.save()

    const team = await Team.find(teamMember.teamId)
    if (team.role === Team.Role.reviewer) {
      teamMember.updateProperties({ status: TeamMember.Statuses.submitted })
      await teamMember.save()
    }

    if (!rec.comments) {
      return
    }

    await Promise.each(rec.comments, async comm => {
      const comment = review.addComment({
        type: comm.public ? Comment.Types.public : Comment.Types.private,
        content: comm.content,
      })
      await comment.updateProperties({ reviewId: review.id })
      await comment.save()

      if (!comm.files) {
        return
      }

      await Promise.each(comm.files, async (file, index) => {
        if (file === null) {
          return
        }
        const commentFile = createCommentFile({
          file,
          index,
          commentId: comment.id,
        })
        comment.assignFile(commentFile)
        await commentFile.save()
      })
    })
  })
}

const getRecommendation = recommendation => {
  if (recommendation === 'return-to-handling-editor')
    return Review.Recommendations.returnToHE
  return recommendation
}

const addHandlingEditors = async ({ invitations, manuscript }) => {
  const heTeam = new Team({
    journalId: manuscript.journalId,
    role: Team.Role.handlingEditor,
    manuscriptId: manuscript.id,
  })
  manuscript.assignTeam(heTeam)
  await heTeam.save()

  await Promise.each(invitations, async invitation => {
    let user
    try {
      user = await User.find(invitation.userId, 'identities')
    } catch (e) {
      logger.warn(
        `Collection ${manuscript.submissionId} might have corrupt invitations.`,
      )
      return
    }

    const options = {
      responded: invitation.respondedOn
        ? new Date(invitation.respondedOn)
        : null,
      teamId: heTeam.id,
      userId: user.id,
    }

    const handlingEditor = heTeam.addMember(user, options)
    await handlingEditor.save()

    await handlingEditor.updateProperties({
      created: new Date(invitation.invitedOn),
      status: getInvitationStatus(invitation),
    })

    await handlingEditor.save()
  })
}

const createManuscriptFromRevision = async ({
  collection,
  journalId,
  fragment,
  collectionId,
}) => {
  const { revision } = fragment
  if (!revision.authors || revision.authors.length === 0) {
    logger.error(
      `Revision from collection ${collectionId} does not have any authors`,
    )
    return
  }
  const manuscript = new Manuscript({
    journalId,
    customId: collection.customId,
    title: revision.metadata.title,
    articleType: revision.metadata.type,
    abstract: revision.metadata.abstract,
    agreeTc: true,
    technicalCheckToken: getTechnicalCheckToken(collection),
    hasPassedEqa: hasPassedEQA(collection),
    hasPassedEqs: hasPassedEQS(collection),
    conflicts: getConflicts(fragment),
  })

  await manuscript.save()
  manuscript.updateProperties({
    version: fragment.version + 1,
    submissionId: collectionId,
    status: Manuscript.Statuses.draft,
    created: new Date(fragment.created),
  })
  await manuscript.save()

  if (collection.invitations && collection.invitations.length > 0) {
    await addHandlingEditors({
      invitations: collection.invitations,
      manuscript,
    })
  }

  if (revision.files) {
    await createFiles({ files: revision.files, manuscript })
  }

  await addAuthors({ authors: revision.authors, manuscript })

  await createResponseToReviewers({
    responseToReviewers: revision.responseToReviewers,
    manuscript,
  })
}

const createResponseToReviewers = async ({
  responseToReviewers = { content: '' },
  manuscript,
}) => {
  const authors = flatMap(
    manuscript.teams.filter(t => t.role === Team.Role.author),
    t => t.members,
  )

  const submittingAuthor = authors.find(member => member.isSubmitting)

  if (!submittingAuthor) {
    logger.error(
      `Manuscript ${manuscript.id} does not have a submitting author!`,
    )
    return
  }

  const review = new Review({
    recommendation: Review.Recommendations.responseToRevision,
    teamMemberId: submittingAuthor.id,
    manuscriptId: manuscript.id,
  })

  await review.save()

  const comment = review.addComment({
    type: Comment.Types.public,
    content: responseToReviewers.content,
  })
  await comment.updateProperties({ reviewId: review.id })
  await comment.save()

  if (!responseToReviewers.file) {
    return
  }

  const file = createCommentFile({
    file: responseToReviewers.file,
    index: 1,
    commentId: comment.id,
  })
  await file.save()
}

const createNewReview = ({ rec, teamMemberId, manuscriptId }) => {
  const newReview = new Review({
    teamMemberId,
    manuscriptId,
    created: new Date(rec.createdOn),
    submitted: rec.submittedOn ? new Date(rec.submittedOn) : null,
    recommendation: getRecommendation(rec.recommendation),
  })
  return newReview
}

const createCommentFile = ({ file, index, commentId }) => {
  const commentFile = new File({
    manuscriptId: undefined,
    commentId,
    fileName: file.id,
    size: file.size,
    originalName: file.originalName,
    type: File.Types.reviewComment,
    mimeType: mimetype.lookup(file.originalName),
    position: index,
  })
  return commentFile
}

const addAuthors = async ({ authors, manuscript }) => {
  const authorTeam = new Team({
    journalId: manuscript.journalId,
    role: Team.Role.author,
    manuscriptId: manuscript.id,
  })
  manuscript.assignTeam(authorTeam)

  await authorTeam.save()

  await Promise.each(authors, async author => {
    let user
    try {
      user = await User.find(author.id, 'identities')
    } catch (e) {
      logger.warn(`Fragment ${manuscript.id} might have corrupt authors.`)
      return
    }

    const options = {
      status: TeamMember.Statuses.accepted,
      teamId: authorTeam.id,
      userId: user.id,
      isSubmitting: author.isSubmitting,
      isCorresponding: author.isCorresponding,
    }

    const authorMember = authorTeam.addMember(user, options)
    await authorMember.save()
  })
}

const getGlobalTeamMembers = async globalTeams => {
  const globalTeamMembers = []
  forOwn(globalTeams, team => {
    globalTeamMembers.push(team.members)
  })
  return flatten(globalTeamMembers)
}

const getStatus = ({ collection, manuscriptVersion }) => {
  if (collection.status === Manuscript.Statuses.deleted) {
    return Manuscript.Statuses.deleted
  }
  if (collection.fragments.length === manuscriptVersion) {
    return collection.status || Manuscript.Statuses.draft
  }

  return Manuscript.Statuses.olderVersion
}
