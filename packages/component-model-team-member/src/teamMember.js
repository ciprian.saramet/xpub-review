const { HindawiBaseModel } = require('component-model')
const { pick, get } = require('lodash')

class TeamMember extends HindawiBaseModel {
  static get tableName() {
    return 'team_member'
  }

  constructor(properties) {
    super(properties)
    if (!this.id) {
      this.status = TeamMember.Statuses.pending
    }
  }

  static get schema() {
    return {
      properties: {
        userId: { type: 'string', format: 'uuid' },
        teamId: { type: 'string', format: 'uuid' },
        position: { type: 'integer' },
        isSubmitting: { type: ['boolean', null] },
        isCorresponding: { type: ['boolean', null] },
        status: { enum: Object.values(TeamMember.Statuses) },
        reviewerNumber: { type: ['integer', null] },
        responded: { type: ['string', 'object', 'null'], format: 'date-time' },
        alias: {
          type: 'object',
          properties: {
            surname: { type: ['string', 'null'] },
            givenNames: { type: ['string', 'null'] },
            email: { type: 'string' },
            aff: { type: ['string', 'null'] },
            country: { type: ['string', 'null'] },
            title: { type: ['string', 'null'] },
          },
        },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-user').model,
        join: {
          from: 'team_member.userId',
          to: 'user.id',
        },
      },
      team: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-team').model,
        join: {
          from: 'team_member.teamId',
          to: 'team.id',
        },
      },
      jobs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-job').model,
        join: {
          from: 'team_member.id',
          to: 'job.teamMemberId',
        },
      },
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const alias = JSON.parse(json.alias)
    const email = get(alias, 'email', '')
    return { ...json, alias: { ...alias, email: email.toLowerCase() } }
  }

  static get Statuses() {
    return {
      pending: 'pending',
      accepted: 'accepted',
      declined: 'declined',
      submitted: 'submitted',
      expired: 'expired',
      removed: 'removed',
    }
  }

  linkUser(user) {
    this.user = user
    if (!this.alias) {
      const defaultIdentity = user.getDefaultIdentity()
      this.alias = pick(defaultIdentity, [
        'surname',
        'title',
        'givenNames',
        'email',
        'aff',
        'country',
      ])
    }
  }

  getName() {
    return `${get(this, 'alias.givenNames', '')} ${get(
      this,
      'alias.surname',
      '',
    )}`
  }

  getLastName() {
    return `${get(this, 'alias.surname', '')}`
  }

  toDTO() {
    return {
      ...this,
      invited: this.created,
      user: this.user ? this.user.toDTO() : undefined,
      alias: {
        ...this.alias,
        name: {
          surname: this.alias.surname,
          givenNames: this.alias.givenNames,
          title: this.alias.title,
        },
      },
      role: this.team ? this.team.role : undefined,
    }
  }
}

module.exports = TeamMember
