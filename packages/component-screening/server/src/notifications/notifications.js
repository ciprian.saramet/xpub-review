const config = require('config')
const { get } = require('lodash')
const { services } = require('helper-service')

const Email = require('@pubsweet/component-email-templating')

const baseUrl = config.get('pubsweet-client.baseUrl')

const { name: journalName, staffEmail } = config.get('journal')
const unsubscribeSlug = config.get('unsubscribe.url')

const { getEmailCopy } = require('./emailCopy')

module.exports = {
  async notifyEiCWhenEQAReturnsManuscript({
    manuscript: { customId, submissionId, id, title },
    reason,
    editorInChief,
    submittingAuthor,
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      comments: reason,
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'eic-manuscript-returned-by-eqa',
    })

    const email = new Email({
      type: 'system',
      toUser: {
        email: editorInChief.alias.email,
      },
      fromEmail: `${journalName} <${staffEmail}>`,
      content: {
        subject: `${customId}: Manuscript returned with comments`,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: this.baseUrl,
        ctaLink: services.createUrl(
          this.baseUrl,
          `/details/${submissionId}/${id}`,
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyHEWhenEQAApprovesManuscript({
    submittingAuthor,
    editorInChief,
    handlingEditor,
    manuscript: { id, customId, title, submissionId },
    editorialAssistant,
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const eicName = editorInChief.getName()
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : eicName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'he-manuscript-approved-by-eqa',
      targetUserName: eicName,
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: get(handlingEditor, 'alias.email', ''),
        name: get(handlingEditor, 'alias.surname', ''),
      },
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      content: {
        subject: `${customId}: Editorial decision confirmed`,
        paragraph,
        signatureName: editorialAssistantName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(baseUrl, `/details/${submissionId}/${id}`),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(handlingEditor, 'user.id', ''),
          token: get(handlingEditor, 'user.unsubscribeToken', ''),
        }),
        signatureJournal: journalName,
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyAuthorsWhenEQAApprovesManuscript({
    authors,
    manuscript: { id, customId, title, submissionId },
    editorialAssistant,
    editorInChief,
  }) {
    const eicName = editorInChief.getName()
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : eicName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: title,
      emailType: 'author-manuscript-approved-by-eqa',
    })

    authors.forEach(async author => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: get(author, 'alias.email', ''),
          name: get(author, 'alias.surname', ''),
        },
        fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
        content: {
          subject: `${customId}: Manuscript accepted`,
          paragraph,
          signatureName: editorialAssistantName,
          signatureJournal: journalName,
          ctaText: 'MANUSCRIPT DETAILS',
          ctaLink: services.createUrl(
            baseUrl,
            `/details/${submissionId}/${id}`,
          ),
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: get(author, 'user.id', ''),
            token: get(author, 'user.unsubscribeToken', ''),
          }),
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },

  async notifyReviewersWhenEQAApprovesManuscript({
    reviewers,
    submittingAuthor,
    editorInChief,
    manuscript: { id, customId, title, submissionId },
    editorialAssistant,
  }) {
    const eicName = editorInChief.getName()
    const submittingAuthorName = submittingAuthor.getName()
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : eicName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'reviewer-manuscript-approved-by-eqa',
    })

    reviewers.forEach(async reviewer => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: get(reviewer, 'alias.email', ''),
          name: get(reviewer, 'alias.surname', ''),
        },
        fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
        content: {
          subject: `${customId}: A manuscript you reviewed has been accepted`,
          paragraph,
          signatureName: editorialAssistantName,
          signatureJournal: journalName,
          ctaText: 'MANUSCRIPT DETAILS',
          ctaLink: services.createUrl(
            baseUrl,
            `/details/${submissionId}/${id}`,
          ),
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: get(reviewer, 'user.id', ''),
            token: get(reviewer, 'user.unsubscribeToken', ''),
          }),
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },
}
