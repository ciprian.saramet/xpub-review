const initialize = ({
  notification,
  models: { Manuscript, Team, TeamMember },
}) => ({
  async execute({ manuscriptId, input }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, journal.teams.members]',
    )

    if (manuscript.status !== Manuscript.Statuses.inQA) {
      throw new ValidationError(`Cannot approve EQA in the current status.`)
    }

    const { token } = input
    if (manuscript.hasPassedEqa !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    manuscript.updateProperties({
      technicalCheckToken: null,
      hasPassedEqa: true,
      status: Manuscript.Statuses.accepted,
    })
    await manuscript.save()

    const { journal } = manuscript
    const editorInChief = journal.getEditorInChief()
    const handlingEditor = manuscript.getHandlingEditor()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const authors = manuscript.getAuthors()
    const reviewerTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )

    notification.notifyHEWhenEQAApprovesManuscript({
      submittingAuthor,
      editorInChief,
      handlingEditor,
      manuscript,
    })
    notification.notifyAuthorsWhenEQAApprovesManuscript({
      authors,
      editorInChief,
      manuscript,
    })
    if (reviewerTeam) {
      const reviewers = manuscript.getReviewersByStatus(
        TeamMember.Statuses.submitted,
      )
      notification.notifyReviewersWhenEQAApprovesManuscript({
        reviewers,
        submittingAuthor,
        editorInChief,
        manuscript,
      })
    }
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
