const initialize = ({ models: { Manuscript }, logEvent }) => ({
  async execute({ manuscriptId, input }) {
    const { customId, token } = input
    const manuscript = await Manuscript.find(manuscriptId)
    const duplicateId = await Manuscript.findOneBy({
      queryObject: { customId },
    })

    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    if (duplicateId) {
      throw new ValidationError('The provided ID already exists.')
    }

    manuscript.updateProperties({
      customId,
      hasPassedEqs: true,
      technicalCheckToken: null,
      status: 'submitted',
    })
    await manuscript.save()

    logEvent({
      userId: null,
      manuscriptId,
      action: logEvent.actions.eqs_approved,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['adminOrEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
