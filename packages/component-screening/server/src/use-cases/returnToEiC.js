const initialize = ({ notification, models: { Manuscript } }) => ({
  async execute({ manuscriptId, input }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, journal.teams.members]',
    )
    const { token } = input

    if (manuscript.status !== Manuscript.Statuses.inQA) {
      throw new ValidationError(
        `Cannot return manuscript in the current status.`,
      )
    }

    if (manuscript.hasPassedEqa !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    manuscript.updateProperties({
      technicalCheckToken: null,
      hasPassedEqa: true,
      status: Manuscript.Statuses.pendingApproval,
    })
    await manuscript.save()

    const { journal } = manuscript
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    notification.notifyEiCWhenEQAReturnsManuscript({
      manuscript,
      reason: input.reason,
      editorInChief,
      submittingAuthor,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
