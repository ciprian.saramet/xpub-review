const approveEQSUseCase = require('./approveEQS')
const declineEQSUseCase = require('./declineEQS')
const approveEQAUseCase = require('./approveEQA')
const returnToEiCUseCase = require('./returnToEiC')
const triggerMTSpackageUseCase = require('./triggerMTSpackage')

module.exports = {
  approveEQSUseCase,
  declineEQSUseCase,
  approveEQAUseCase,
  returnToEiCUseCase,
  triggerMTSpackageUseCase,
}
