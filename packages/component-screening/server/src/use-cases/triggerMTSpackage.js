const initialize = ({ models: { Manuscript }, sendPackage }) => ({
  async execute({ submissionId }) {
    const manuscriptVersions = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      excludedStatus: Manuscript.Statuses.deleted,
      eagerLoadRelations:
        '[files,teams.members,reviews.[comments.files,member.team]]',
    })
    await Promise.all(
      manuscriptVersions.map(async manuscript =>
        sendPackage({ manuscript, isEQA: true }),
      ),
    )
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
