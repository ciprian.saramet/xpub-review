process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { returnToEiCUseCase } = require('../src/use-cases')

const notification = {
  notifyEiCWhenEQAReturnsManuscript: jest.fn(),
}
const chance = new Chance()

describe('Return manuscript to EIC use case', () => {
  let journal
  let manuscript
  let token
  let reason
  let mockedModels

  beforeEach(async () => {
    // eslint-disable-next-line prefer-destructuring
    journal = fixtures.journals[0]
    token = chance.guid()
    reason = chance.paragraph()
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'admin',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    manuscript = fixtures.generateManuscript({
      journal,
      technicalCheckToken: token,
      status: 'inQA',
      hasPassedEqa: null,
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'submitted' },
      role: 'reviewer',
    })
    mockedModels = models.build(fixtures)
  })

  it('return manuscript to eic', async () => {
    await returnToEiCUseCase
      .initialize({ notification, models: mockedModels })
      .execute({
        manuscriptId: manuscript.id,
        input: { token, reason },
      })

    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.hasPassedEqa).toBe(true)
    expect(manuscript.status).toBe(
      mockedModels.Manuscript.Statuses.pendingApproval,
    )
    expect(
      notification.notifyEiCWhenEQAReturnsManuscript,
    ).toHaveBeenCalledTimes(1)
  })
  it('should return error when token is invalid', async () => {
    const anotherToken = chance.guid()
    const response = returnToEiCUseCase
      .initialize({ notification, models: mockedModels })
      .execute({
        manuscriptId: manuscript.id,
        input: { token: anotherToken, reason },
      })
    return expect(response).rejects.toThrow('Invalid token.')
  })
  it('should throw error if the manuscript already passed EQA', async () => {
    manuscript.hasPassedEqa = true
    const result = returnToEiCUseCase
      .initialize({ notification, models: mockedModels })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
      })
    return expect(result).rejects.toThrow('Manuscript already handled.')
  })
})
