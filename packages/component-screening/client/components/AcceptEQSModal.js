import React from 'react'

import { TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Label,
  FormModal,
  ValidatedFormField,
  validators,
} from '@hindawi/ui'

const lengthValidator = validators.exactLength(7)

const AcceptEQSModal = ({ handleApproveEQS, hideModal }) => (
  <FormModal
    cancelText="CANCEL"
    confirmText="OK"
    content={() => (
      <Row mt={6}>
        <Item vertical>
          <Label required>Manuscript ID</Label>
          <ValidatedFormField
            component={TextField}
            data-test-id="customId"
            inline
            name="customId"
            validate={[
              validators.required,
              validators.digitValidator,
              lengthValidator,
            ]}
          />
        </Item>
      </Row>
    )}
    hideModal={hideModal}
    onSubmit={handleApproveEQS}
    title="Accept manuscript"
  />
)

export default AcceptEQSModal
