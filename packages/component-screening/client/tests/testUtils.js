import React from 'react'
import { theme } from '@hindawi/ui'
import { ThemeProvider } from 'styled-components'
import { render as rtlRender } from 'react-testing-library'

export const render = ui => {
  const Component = () => <ThemeProvider theme={theme}>{ui}</ThemeProvider>

  return rtlRender(<Component />)
}
