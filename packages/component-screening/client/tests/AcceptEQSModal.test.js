import React from 'react'
import 'jest-dom/extend-expect'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from './testUtils'
import AcceptEQSModal from '../components/AcceptEQSModal'

describe('a default client test', () => {
  afterEach(cleanup)

  it('should validate on blur', done => {
    const onSubmitMock = jest.fn()
    const { getByTestId, getByText } = render(
      <AcceptEQSModal handleApproveEQS={onSubmitMock} />,
    )

    fireEvent.blur(getByTestId('customId'))
    fireEvent.click(getByText('OK'))

    setTimeout(() => {
      expect(getByText('Required')).toBeInTheDocument()
      expect(onSubmitMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should not allow letters', done => {
    const onSubmitMock = jest.fn()
    const { getByTestId, getByText } = render(
      <AcceptEQSModal handleApproveEQS={onSubmitMock} />,
    )

    fireEvent.change(getByTestId('customId'), {
      target: {
        value: '1234asd',
      },
    })
    fireEvent.click(getByText('OK'))

    setTimeout(() => {
      expect(getByText('Invalid number')).toBeInTheDocument()
      expect(onSubmitMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should not allow less than 7 digits', done => {
    const onSubmitMock = jest.fn()
    const { getByTestId, getByText } = render(
      <AcceptEQSModal handleApproveEQS={onSubmitMock} />,
    )

    fireEvent.change(getByTestId('customId'), {
      target: {
        value: '123',
      },
    })
    fireEvent.click(getByText('OK'))

    setTimeout(() => {
      expect(
        getByText('The input should have exactly 7 characters!'),
      ).toBeInTheDocument()
      expect(onSubmitMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should not allow more than 7 digits', done => {
    const onSubmitMock = jest.fn()
    const { getByTestId, getByText } = render(
      <AcceptEQSModal handleApproveEQS={onSubmitMock} />,
    )

    fireEvent.change(getByTestId('customId'), {
      target: {
        value: '1234567890',
      },
    })
    fireEvent.click(getByText('OK'))

    setTimeout(() => {
      expect(
        getByText('The input should have exactly 7 characters!'),
      ).toBeInTheDocument()
      expect(onSubmitMock).toHaveBeenCalledTimes(0)
      done()
    })
  })

  it('should allow submit when provided a 7 digit input', done => {
    const onSubmitMock = jest.fn()
    const { getByTestId, getByText } = render(
      <AcceptEQSModal handleApproveEQS={onSubmitMock} />,
    )

    fireEvent.change(getByTestId('customId'), {
      target: {
        value: '1234567',
      },
    })
    fireEvent.click(getByText('OK'))

    setTimeout(() => {
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })
})
