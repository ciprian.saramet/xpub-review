import React from 'react'
import { Modal } from 'component-modal'
import { H2, Button } from '@pubsweet/ui'
import { compose, withHandlers, withProps } from 'recompose'
import { Text, Row, ShadowedBox } from '@hindawi/ui'

import withGQL from '../graphql'
import { parseSearchParams, parseError } from '../utils'
import { DeclineEQSModal, AcceptEQSModal } from '../components'

const EQSDecision = ({ handleDeclineEQS, handleApproveEQS, title }) => (
  <ShadowedBox center mt={6} width={130}>
    <H2 mt={6}>Editorial decision</H2>

    <Row justify="center" mt={2}>
      <Text secondary>
        Did manuscript titled{' '}
        <Text display="inline" fontWeight="bold" secondary>
          {title}
        </Text>{' '}
        pass EQS checks?
      </Text>
    </Row>

    <Row justify="space-around" mb={6} mt={6}>
      <Modal
        component={DeclineEQSModal}
        handleDeclineEQS={handleDeclineEQS}
        modalKey="rejectEQS"
      >
        {showModal => (
          <Button
            data-test-id="reject-button"
            mr={2}
            onClick={showModal}
            secondary
            width={48}
          >
            NO
          </Button>
        )}
      </Modal>
      <Modal
        component={AcceptEQSModal}
        handleApproveEQS={handleApproveEQS}
        modalKey="acceptEQS"
      >
        {showModal => (
          <Button
            data-test-id="accept-button"
            onClick={showModal}
            primary
            width={48}
          >
            YES
          </Button>
        )}
      </Modal>
    </Row>
  </ShadowedBox>
)

export default compose(
  withGQL,
  withProps(({ history }) => {
    const { title, token, manuscriptId } = parseSearchParams(
      history.location.search,
    )

    return { title, token, manuscriptId }
  }),
  withHandlers({
    handleDeclineEQS: ({ declineEQS, token, manuscriptId, history }) => ({
      setFetching,
      setError,
      hideModal,
    }) => {
      setFetching(true)

      declineEQS({
        variables: {
          manuscriptId,
          input: {
            token,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript rejected. Thank you for your technical check!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
    handleApproveEQS: ({ approveEQS, token, manuscriptId, history }) => (
      { customId },
      { setFetching, setError, hideModal },
    ) => {
      setFetching(true)

      approveEQS({
        variables: {
          manuscriptId,
          input: {
            token,
            customId,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript accepted. Thank you for your technical check!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e))
        })
    },
  }),
)(EQSDecision)
