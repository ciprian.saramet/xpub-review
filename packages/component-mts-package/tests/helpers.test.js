const { get } = require('lodash')
const Chance = require('chance')
const {
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { getJsonTemplate } = require('../src/getJsonTemplate')
const { defaultConfig, defaultParseXmlOptions } = require('../config/default')

const { convertToXML, composeJson } = require('../src/helpers')

const chance = new Chance()
describe('MTS helpers', () => {
  describe('composeJson', () => {
    it('should include the manuscript title, abstract', async () => {
      const manuscript = fixtures.generateManuscript({
        title: chance.word(),
        abstract: chance.paragraph(),
      })
      await dataService.createUserOnManuscript({
        manuscript,
        fixtures,
        role: 'author',
      })

      const composedJson = composeJson({
        manuscript,
        isEQA: false,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(
        get(
          composedJson,
          'article.front.article-meta.title-group.article-title._text',
        ),
      ).toBe(manuscript.title)
      expect(
        get(composedJson, 'article.front.article-meta.abstract._text'),
      ).toBe(manuscript.abstract)
    })
    // eslint-disable-next-line
    it.skip('should return a json with a rev-group when isEQA is true', async () => {
      const { fragment } = fixtures.fragments
      const fragmentUsers = [
        {
          isReviewer: true,
          assignmentDate: chance.timestamp(),
          email: chance.email(),
          title: 'Dr',
          recommendation: {
            id: chance.guid(),
            userId: chance.guid(),
            comments: [
              {
                files: [],
                public: true,
                content: chance.sentence(),
              },
              {
                files: [],
                public: false,
                content: chance.sentence(),
              },
            ],
            createdOn: chance.timestamp(),
            updatedOn: chance.timestamp(),
            submittedOn: chance.timestamp(),
            recommendation: 'publish',
            recommendationType: 'review',
          },
          country: chance.country(),
          lastName: chance.first(),
          firstName: chance.last(),
          affiliation: chance.company(),
          submissionDate: chance.timestamp(),
        },
        {
          isReviewer: false,
          assignmentDate: chance.timestamp(),
          email: chance.email(),
          title: 'Dr',
          recommendation: {
            id: chance.guid(),
            userId: chance.guid(),
            comments: [
              {
                files: [],
                public: true,
                content: chance.sentence(),
              },
              {
                files: [],
                public: false,
                content: chance.sentence(),
              },
            ],
            createdOn: chance.timestamp(),
            updatedOn: chance.timestamp(),
            submittedOn: chance.timestamp(),
            recommendation: 'publish',
            recommendationType: 'editorRecommendation',
          },
          country: chance.country(),
          lastName: chance.first(),
          firstName: chance.last(),
          affiliation: chance.company(),
          submissionDate: chance.timestamp(),
        },
      ]
      const composedJson = composeJson({
        fragment,
        isEQA: true,
        fragmentUsers,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(composedJson.article.front['rev-group']).toHaveProperty('rev')
      expect(composedJson.article.front['rev-group'].rev).toHaveLength(
        fragmentUsers.length,
      )
    })
    // eslint-disable-next-line
    it.skip('should return a json with correct article-meta data', async () => {
      const { fragment } = fixtures.fragments
      const composedJson = composeJson({
        fragment,
        isEQA: false,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(
        composedJson.article.front['article-meta']['contrib-group'],
      ).toHaveProperty('contrib')
      expect(
        composedJson.article.front['article-meta']['contrib-group'].contrib,
      ).toHaveLength(fragment.authors.length)
      expect(
        composedJson.article.front['article-meta']['title-group'][
          'article-title'
        ],
      ).toEqual(fragment.metadata.title)
    })
  })
  describe('convertToXML', () => {
    it('should return a properly formatted object', () => {
      const jsonTemplate = getJsonTemplate(defaultConfig)
      const xmlFile = convertToXML({
        prefix: defaultConfig.prefix,
        options: defaultParseXmlOptions,
        json: jsonTemplate,
      })

      expect(xmlFile).toHaveProperty('name')
      expect(xmlFile).toHaveProperty('content')
      expect(xmlFile.content).toContain(
        jsonTemplate.article.front['article-meta']['title-group'][
          'article-title'
        ]._text,
      )
    })
  })
})
