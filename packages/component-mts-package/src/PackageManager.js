const fs = require('fs')
const AWS = require('aws-sdk')
const { get, isEmpty } = require('lodash')
const { promisify } = require('util')
const FtpDeploy = require('ftp-deploy')
const nodeArchiver = require('archiver')
const logger = require('@pubsweet/logger')

const createFilesPackage = (s3Config, archiver = nodeArchiver) => {
  const s3 = new AWS.S3({
    secretAccessKey: s3Config.secretAccessKey,
    accessKeyId: s3Config.accessKeyId,
    region: s3Config.region,
    params: {
      Bucket: s3Config.bucket,
    },
    useAccelerateEndpoint: true,
  })
  const asyncGetObject = promisify(s3.getObject.bind(s3))

  return async ({ manuscript, fileTypes, xmlFile, isEQA = false }) => {
    const { files = [] } = manuscript

    if (isEmpty(files)) throw new Error('Failed to create a package')

    let packageName = get(xmlFile, 'name', '').replace('.xml', '')
    if (isEQA) {
      packageName = `ACCEPTED_${packageName}.${manuscript.version}`
    }

    const s3Files = await Promise.all(
      files.map(file =>
        asyncGetObject({
          Key: file.providerKey,
        }).then(s3File => ({
          ...s3File,
          fileName: file.fileName,
        })),
      ),
    )

    return new Promise((resolve, reject) => {
      const packageOutput = fs.createWriteStream(`${packageName}.zip`)
      const archive = archiver('zip')

      archive.pipe(packageOutput)
      archive.append(xmlFile.content, { name: xmlFile.name })

      s3Files.forEach(f => {
        const name = get(f, 'fileName')
        archive.append(f.Body, {
          name,
        })
      })

      archive.on('error', reject)

      archive.on('end', err => {
        if (err) reject(err)
        logger.debug(
          `MTS archive created ${packageName} - archive stream ended`,
        )
      })

      packageOutput.on('close', () => {
        logger.debug(
          `MTS archive created ${packageName} - ${archive.pointer()} bytes written to zip file`,
        )
        resolve()
      })

      archive.finalize()
    })
  }
}

const readFile = promisify(fs.readFile)

const fileError = filename => err => {
  deleteFile(filename)
  throw err
}

const uploadFiles = async ({ filename, s3Config, config }) => {
  const data = await readFile(filename)
  const s3 = new AWS.S3({
    secretAccessKey: s3Config.secretAccessKey,
    accessKeyId: s3Config.accessKeyId,
    region: s3Config.region,
    params: {
      Bucket: s3Config.bucket,
    },
    useAccelerateEndpoint: true,
  })
  const asyncUploadS3 = promisify(s3.upload.bind(s3))

  const params = {
    Body: data,
    Key: `mts/${filename}`,
  }

  return asyncUploadS3(params)
    .then(() => {
      logger.debug(`Successfully uploaded ${filename} to S3`)
      return uploadFTP({ filename, config })
    })
    .then(() => {
      logger.debug(`Successfully uploaded ${filename} to FTP`)
      deleteFile(filename)
    })
    .catch(fileError(filename))
}

const deleteFilesS3 = async ({ fileKeys, s3Config }) => {
  AWS.config.update({
    secretAccessKey: s3Config.secretAccessKey,
    accessKeyId: s3Config.accessKeyId,
    region: s3Config.region,
  })
  const s3 = new AWS.S3()

  const params = {
    Bucket: s3Config.bucket,
    Delete: {
      Objects: fileKeys.map(file => ({ Key: file })),
    },
  }

  const deleteObjectsS3 = promisify(s3.deleteObjects.bind(s3))

  return deleteObjectsS3(params)
}

const deleteFile = filename => {
  fs.access(filename, fs.constants.F_OK, err => {
    if (!err) {
      fs.unlink(filename, err => {
        logger.info(`Deleted ${filename}`)
        if (err) throw err
      })
    }
  })
}

const uploadFTP = ({ filename, config }) => {
  const ftpDeploy = new FtpDeploy()
  const configs = {
    ...config,
    include: [filename],
  }
  return ftpDeploy.deploy(configs)
}

module.exports = {
  createFilesPackage,
  uploadFiles,
  deleteFilesS3,
}
