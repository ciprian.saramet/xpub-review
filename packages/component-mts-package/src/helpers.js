const convert = require('xml-js')
const { get, find, pick } = require('lodash')

const { getJsonTemplate } = require('./getJsonTemplate')

const {
  setFiles,
  setHistory,
  setMetadata,
  setQuestions,
  setReviewers,
  createFileName,
  setContributors,
  setEmptyReviewers,
} = require('./templateSetters')

module.exports = {
  convertToXML: ({ json = {}, options, prefix }) => {
    const content = convert.json2xml(json, options)
    const customId = get(
      json,
      'article.front.article-meta.article-id[0]._text',
      createFileName({ prefix }),
    )
    const name = `${customId}.xml`
    return {
      name,
      content,
    }
  },
  composeJson: ({ config, options, isEQA = false, manuscript = {} }) => {
    const {
      teams = [],
      files = [],
      publicationDates = [],
      reviews = [],
    } = manuscript

    const meta = pick(manuscript, ['title', 'abstract', 'articleType'])

    const submitted = get(
      find(publicationDates, { type: 'technicalChecks' }),
      'date',
      Date.now(),
    )

    const authorTeam = teams.find(t => t.role === 'author')

    const jsonTemplate = getJsonTemplate(config)
    const fileName = createFileName({
      id: manuscript.customId,
      prefix: config.prefix,
    })

    let composedJson = {
      ...jsonTemplate,
      ...setMetadata({
        options,
        meta,
        fileName,
        jsonTemplate,
      }),
      ...setContributors(authorTeam.members, jsonTemplate),
      ...setHistory(submitted, jsonTemplate),
      ...setFiles(files, jsonTemplate),
      ...setQuestions(manuscript.conflicts, jsonTemplate),
      ...setEmptyReviewers(jsonTemplate),
    }

    if (isEQA) {
      const mtsReviews = reviews.filter(
        rev =>
          [
            'reviewer',
            'handlingEditor',
            'editorInChief',
            'admin',
            'editorialAssistant',
          ].includes(rev.member.team.role) && rev.submitted,
      )
      composedJson = setReviewers(mtsReviews, jsonTemplate)
    }

    return composedJson
  },
}
