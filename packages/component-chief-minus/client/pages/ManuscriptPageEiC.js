import React, { Fragment } from 'react'
import { get } from 'lodash'
import { compose, withHandlers, withProps } from 'recompose'

import useVisibleStatuses from '../useVisibleStatuses'
import { withEditorInChiefGQL } from '../graphql/withGQL'
import {
  EICDecision,
  CommentWithFile,
  EditorialComments,
  ReviewerDetailsEIC,
  EditorInChiefAssignHE,
} from '../components'

const ManuscriptPageEiC = ({
  status,
  decisions,
  reviewers,
  manuscript,
  toggleAssignHE,
  recommendations,
  reviewerReports,
  editorialReviews,
  assignHEExpanded,
  authorResponseProps,
  inviteHandlingEditor,
  handleEditorialDecision,
  editorialCommentsDecisions,
  ...rest
}) => {
  const {
    EICDecisionBox,
    CommentWithFileBox,
    EditorialCommentsBox,
    ReviewerDetailsEICBox,
    EditorInChiefAssignHEBox,
  } = useVisibleStatuses({
    manuscript,
    reviewerReports,
    editorialReviews,
  })

  return (
    <Fragment>
      <EditorialComments
        decisions={editorialCommentsDecisions}
        editorialReviews={editorialReviews}
        isVisible={EditorialCommentsBox.isVisible}
        mt={4}
        startExpanded={EditorialCommentsBox.startExpanded}
      />
      <CommentWithFile
        {...authorResponseProps}
        commentLabel="Author Reply"
        isVisible={CommentWithFileBox.isVisible}
        mt={4}
        startExpanded={CommentWithFileBox.startExpanded}
      />
      <EditorInChiefAssignHE
        expanded={assignHEExpanded}
        inviteHandlingEditor={inviteHandlingEditor}
        isVisible={EditorInChiefAssignHEBox.isVisible}
        manuscript={manuscript}
        toggle={toggleAssignHE}
      />
      <ReviewerDetailsEIC
        isVisible={ReviewerDetailsEICBox.isVisible}
        manuscript={manuscript}
        mt={4}
        options={recommendations}
        reviewerReports={reviewerReports}
        reviewers={reviewers}
        startExpanded={ReviewerDetailsEICBox.startExpanded}
      />
      <EICDecision
        highlight={EICDecisionBox.isHighlighted}
        isVisible={EICDecisionBox.isVisible}
        mt={4}
        onSubmit={handleEditorialDecision}
        options={decisions}
        status={status}
      />
    </Fragment>
  )
}

export default compose(
  withEditorInChiefGQL,

  withProps(({ manuscript }) => ({
    status: get(manuscript, 'status'),
  })),

  withHandlers({
    inviteHandlingEditor: ({ manuscript, inviteHandlingEditor }) => (
      he,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      inviteHandlingEditor({
        variables: {
          submissionId: manuscript.submissionId,
          userId: he.user.id,
        },
      })
        .then(() => {
          modalProps.setFetching(true)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    publishAsEiC: ({ manuscript, makeDecisionToPublishAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      return makeDecisionToPublishAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    rejectAsEiC: ({ manuscript, makeDecisionToRejectAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      return makeDecisionToRejectAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    returnToHEAsEiC: ({ manuscript, makeDecisionToReturnAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      return makeDecisionToReturnAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    requestRevisionAsEiC: ({
      manuscript,
      requestRevisionAsEiC,
      collapseEicDecision,
    }) => (content, modalProps, formikBag) => {
      modalProps.setFetching(true)

      return requestRevisionAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          formikBag.resetForm({ decision: '', message: '' })
          modalProps.setFetching(false)
          modalProps.hideModal()
          collapseEicDecision()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
  }),
  withHandlers({
    handleEditorialDecision: ({
      rejectAsEiC,
      publishAsEiC,
      returnToHEAsEiC,
      requestRevisionAsEiC,
    }) => ({ decision, message }, modalProps, formikBag) => {
      switch (decision) {
        case 'publish':
          return publishAsEiC(message, modalProps, formikBag)
        case 'return-to-handling-editor':
          return returnToHEAsEiC(message, modalProps, formikBag)
        case 'revision':
          return requestRevisionAsEiC(message, modalProps, formikBag)
        case 'reject':
          return rejectAsEiC(message, modalProps, formikBag)
        default:
          break
      }
    },
  }),
)(ManuscriptPageEiC)
