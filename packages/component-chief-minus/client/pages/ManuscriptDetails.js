import React from 'react'
import moment from 'moment'
import { chain, get } from 'lodash'
import { Spinner } from '@pubsweet/ui'
import { graphql } from 'react-apollo'
import styled from 'styled-components'
import { Redirect } from 'react-router'
import { th } from '@pubsweet/ui-toolkit'
import { RemoteOpener } from '@hindawi/ui'
import { compose, withHandlers, withProps } from 'recompose'

import decisions from '../decisions'
import recommendations from '../recommendations'
import { withGQL, queries, withEditorInChiefGQL } from '../graphql'
import editorialCommentsDecisions from '../editorialCommentsDecisions'
import {
  ManuscriptHeader,
  ManuscriptMetadata,
  ManuscriptDetailsTop,
  manuscriptDetailsPages,
} from '../'

const ManuscriptDetails = ({
  data,
  role,
  match,
  history,
  loading,
  versions,
  removeHE,
  manuscript,
  currentUser,
  permissionError,
  reviewers,
  isLatestVersion,
  reviewerReports,
  editorialReviews,
  cancelHEInvitation,
  authorResponseProps,
  editorialCommentsReviews,
}) => {
  if (permissionError && permissionError.includes('Operation not permitted')) {
    return <Redirect to="/404" />
  }
  if (loading || !manuscript) return <Spinner />
  return (
    <RemoteOpener>
      {({ expanded, toggle }) => (
        <Root>
          <ManuscriptDetailsTop
            history={history}
            isAdmin={role === 'admin' || role === 'editorialAssistant'}
            isLatestVersion={isLatestVersion}
            manuscript={manuscript}
            match={match}
            versions={versions}
          />

          <ManuscriptHeader
            isLatestVersion={isLatestVersion}
            manuscript={manuscript}
            onCancelHEInvitation={cancelHEInvitation}
            removeHE={removeHE}
            toggleAssignHE={toggle}
          />

          <ManuscriptMetadata manuscript={manuscript} />

          {React.createElement(manuscriptDetailsPages[role], {
            match,
            data,
            history,
            reviewers,
            decisions,
            manuscript,
            currentUser,
            recommendations,
            editorialReviews,
            reviewerReports,
            isLatestVersion,
            authorResponseProps,
            toggleAssignHE: toggle,
            editorialCommentsDecisions,
            assignHEExpanded: expanded,
          })}
        </Root>
      )}
    </RemoteOpener>
  )
}

export default compose(
  withGQL,
  withEditorInChiefGQL,
  graphql(queries.getManuscriptVersions, {
    options: ({ match }) => ({
      variables: {
        submissionId: get(match, 'params.submissionId', ''),
      },
    }),
  }),
  withProps(({ data, match }) => ({
    permissionError: get(data, 'error.message'),
    loading: data.loading,
    versions: chain(data)
      .get('getManuscriptVersions', [])
      .map(m => ({
        label: `Version ${m.version}`,
        value: m.id,
      }))
      .value(),
    manuscript: chain(data)
      .get('getManuscriptVersions', [])
      .find(m => m.id === get(match, 'params.manuscriptId'))
      .value(),
  })),
  withProps(({ data, versions, match, manuscript }) => ({
    role: get(manuscript, 'role'),
    reviewers: get(manuscript, 'reviewers') || [],
    reviews: get(manuscript, 'reviews') || [],
    status: get(manuscript, 'status'),
    isLatestVersion:
      chain(data)
        .get('getManuscriptVersions', [])
        .maxBy('version')
        .get('id')
        .value() === get(match, 'params.manuscriptId'),
    authorResponse: get(manuscript, 'reviews', []).find(
      r => get(r, 'recommendation') === 'responseToRevision',
    ),
    submittingAuthor: get(manuscript, 'authors', []).find(a =>
      get(a, 'isSubmitting'),
    ),
  })),
  withProps(({ authorResponse, submittingAuthor, reviewers, reviews }) => ({
    authorResponseProps: {
      date: get(authorResponse, 'submitted'),
      message: get(authorResponse, 'comments.0.content'),
      file: get(authorResponse, 'comments.0.files.0'),
      boxLabel: 'Response to Revision Request',
      authorName: `${get(submittingAuthor, 'alias.name.givenNames', '')} ${get(
        submittingAuthor,
        'alias.name.surname',
        '',
      )}`,
    },
    reviewers:
      reviewers &&
      reviewers
        .filter(reviewer => get(reviewer, 'status') !== 'expired')
        .map(reviewer => ({
          ...reviewer,
          review: reviews.find(
            review => review.member.user.id === reviewer.user.id,
          ),
        })),
    editorialReviews: reviews
      .filter(r =>
        [
          'admin',
          'editorInChief',
          'handlingEditor',
          'editorialAssistant',
        ].includes(get(r, 'member.role')),
      )
      .map(r => ({
        ...r,
        submittedTimestamp: moment(get(r, 'submitted', '')).valueOf(),
      }))
      .sort((a, b) => b.submittedTimestamp - a.submittedTimestamp),
    reviewerReports: reviews.filter(
      review => review.member.role === 'reviewer',
    ),
  })),
  withHandlers({
    cancelHEInvitation: ({ cancelHandlingEditorInvitation }) => (
      invitation,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      cancelHandlingEditorInvitation({
        variables: {
          teamMemberId: invitation.id,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    removeHE: ({ removeHandlingEditor }) => (invitation, modalProps) => {
      modalProps.setFetching(true)
      removeHandlingEditor({
        variables: {
          teamMemberId: invitation.id,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
  }),
)(ManuscriptDetails)

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 4);
  /* TODO: remove the padding-bottom when the dropdown Menu will be able to flip when not fitting into the viewport*/
  padding-bottom: calc(${th('gridUnit')} * 30);
`
// #endregion
