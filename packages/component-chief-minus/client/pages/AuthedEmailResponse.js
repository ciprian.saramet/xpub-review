import { withFetching } from '@hindawi/ui'
import { compose, lifecycle, withHandlers } from 'recompose'

import { parseError } from '../utils'
import { EmailResponseLayout } from '../components'
import { withReviewerGQL, withHandlingEditorGQL } from '../graphql/withGQL'

export default compose(
  withFetching,
  withReviewerGQL,
  withHandlingEditorGQL,
  withHandlers({
    acceptReviewerInvitation: ({
      history,
      setError,
      setFetching,
      acceptReviewerInvitation,
      params: { invitationId, manuscriptId, submissionId },
    }) => () => {
      setFetching(true)
      acceptReviewerInvitation({
        variables: {
          teamMemberId: invitationId,
        },
      })
        .then(() => {
          setFetching(false)
          history.replace(`/details/${submissionId}/${manuscriptId}`)
        })
        .catch(e => {
          setError(parseError(e))
          setFetching(false)
        })
    },
    acceptHandlingEditorInvitation: ({
      history,
      setError,
      setFetching,
      acceptHandlingEditorInvitation,
      params: { teamMemberId, manuscriptId, submissionId },
    }) => () => {
      setFetching(true)
      acceptHandlingEditorInvitation({
        variables: {
          teamMemberId,
        },
      })
        .then(() => {
          setFetching(false)
          history.replace(`/details/${submissionId}/${manuscriptId}`)
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e.message))
        })
    },
    declineHandlingEditorInvitation: ({
      history,
      setError,
      setFetching,
      params: { teamMemberId },
      declineHandlingEditorInvitation,
    }) => () => {
      setFetching(true)
      declineHandlingEditorInvitation({
        variables: {
          teamMemberId,
          reason: '',
        },
      })
        .then(() => {
          setFetching(false)
          history.replace('/info-page', {
            path: '/',
            title: 'Thank you',
            buttonText: 'GO TO DASHBOARD',
            content: 'Your decision has been submitted.',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(parseError(e.message))
        })
    },
  }),
  lifecycle({
    componentDidMount() {
      const {
        action,
        acceptReviewerInvitation,
        acceptHandlingEditorInvitation,
        declineHandlingEditorInvitation,
      } = this.props
      switch (action) {
        case 'accept-review':
          acceptReviewerInvitation()
          break
        case 'accept-he':
          acceptHandlingEditorInvitation()
          break
        case 'decline-he':
          declineHandlingEditorInvitation()
          break
        default:
          break
      }
    },
  }),
)(EmailResponseLayout)
