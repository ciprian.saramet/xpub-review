import ManuscriptPageAdmin from './ManuscriptPageAdmin'
import ManuscriptPageEiC from './ManuscriptPageEiC'
import ManuscriptPageHE from './ManuscriptPageHE'
import ManuscriptPageAuthor from './ManuscriptPageAuthor'
import ManuscriptPageReviewer from './ManuscriptPageReviewer'

export const manuscriptDetailsPages = {
  admin: ManuscriptPageAdmin,
  editorialAssistant: ManuscriptPageAdmin,
  editorInChief: ManuscriptPageEiC,
  author: ManuscriptPageAuthor,
  reviewer: ManuscriptPageReviewer,
  handlingEditor: ManuscriptPageHE,
}

export { default as EmailResponse } from './EmailResponse'
export { default as NewReviewerEmail } from './NewReviewerEmail'
export { default as AuthedEmailResponse } from './AuthedEmailResponse'
export { default as UnauthedEmailResponse } from './UnauthedEmailResponse'
