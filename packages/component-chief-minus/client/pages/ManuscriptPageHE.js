import React, { Fragment } from 'react'
import { get } from 'lodash'
import { withRouter } from 'react-router'
import { compose, withHandlers, withProps } from 'recompose'

import useVisibleStatuses from '../useVisibleStatuses'
import { withHandlingEditorGQL } from '../graphql/withGQL'
import {
  CommentWithFile,
  HERecommendation,
  EditorialComments,
  ReviewerDetailsHE,
  RespondToEditorialInvitation,
} from '../components'

const ManuscriptPageHE = ({
  status,
  reviewers,
  manuscript,
  inviteReviewer,
  reviewerReports,
  recommendations,
  editorialReviews,
  respondToInvitation,
  authorResponseProps,
  cancelReviewerInvitation,
  editorialCommentsDecisions,
  handleEditorialRecommendation,
}) => {
  const {
    CommentWithFileBox,
    HERecommendationBox,
    EditorialCommentsBox,
    ReviewerDetailsHEBox,
    RespondToEditorialInvitationBox,
  } = useVisibleStatuses({
    manuscript,
    reviewerReports,
    editorialReviews,
  })

  return (
    <Fragment>
      <EditorialComments
        decisions={editorialCommentsDecisions}
        editorialReviews={editorialReviews}
        isVisible={EditorialCommentsBox.isVisible}
        mt={4}
        startExpanded={EditorialCommentsBox.startExpanded}
      />
      <CommentWithFile
        {...authorResponseProps}
        commentLabel="Author Reply"
        isVisible={CommentWithFileBox.isVisible}
        mt={4}
        startExpanded={CommentWithFileBox.startExpanded}
      />
      <RespondToEditorialInvitation
        highlight={RespondToEditorialInvitationBox.isHighlighted}
        isVisible={RespondToEditorialInvitationBox.isVisible}
        mt={4}
        onSubmit={respondToInvitation}
        startExpanded={RespondToEditorialInvitationBox.startExpanded}
      />
      <ReviewerDetailsHE
        cancelReviewerInvitation={cancelReviewerInvitation}
        canInvitePublons={ReviewerDetailsHEBox.canInvitePublons}
        canInviteReviewers={ReviewerDetailsHEBox.canInviteReviewers}
        highlight={ReviewerDetailsHEBox.isHighlighted}
        inviteReviewer={inviteReviewer}
        isVisible={ReviewerDetailsHEBox.isVisible}
        manuscript={manuscript}
        mt={4}
        options={recommendations}
        reviewerReports={reviewerReports}
        reviewers={reviewers}
        startExpanded={ReviewerDetailsHEBox.startExpanded}
      />
      <HERecommendation
        highlight={HERecommendationBox.highlight}
        isVisible={HERecommendationBox.isVisible}
        onSubmit={handleEditorialRecommendation}
        options={recommendations}
        status={status}
      />
    </Fragment>
  )
}

const parseError = e => e.message.split(':')[1].trim()
export default compose(
  withRouter,
  withHandlingEditorGQL,

  withProps(({ manuscript }) => ({
    status: get(manuscript, 'status'),
    manuscriptId: get(manuscript, 'id'),
    handlingEditor: get(manuscript, 'handlingEditor'),
  })),

  withHandlers({
    respondToInvitation: ({
      history,
      handlingEditor,
      acceptHandlingEditorInvitation,
      declineHandlingEditorInvitation,
    }) => ({ heDecision, reason }, modalProps) => {
      const teamMemberId = get(handlingEditor, 'id')
      modalProps.setFetching(true)
      if (heDecision === 'yes') {
        acceptHandlingEditorInvitation({
          variables: {
            teamMemberId,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            modalProps.hideModal()
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(parseError(e))
          })
      } else {
        declineHandlingEditorInvitation({
          variables: {
            teamMemberId,
            reason,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            modalProps.hideModal()
            history.replace('/')
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(e.message)
          })
      }
    },
    handleEditorialRecommendation: ({
      manuscriptId,
      recommendToRejectAsHE,
      requestRevisionAsHE,
      recommendToPublishAsHE,
    }) => (values, modalProps) => {
      modalProps.setFetching(true)
      let handleRecommendationFn
      switch (values.recommendation) {
        case 'reject':
          handleRecommendationFn = recommendToRejectAsHE({
            variables: {
              manuscriptId,
              input: {
                forAuthor: values.public,
                forEiC: values.private,
              },
            },
          })
          break
        case 'publish':
          handleRecommendationFn = recommendToPublishAsHE({
            variables: {
              manuscriptId,
              input: {
                forAuthor: values.public,
                forEiC: values.private,
              },
            },
          })
          break
        case 'minor':
        case 'major':
          handleRecommendationFn = requestRevisionAsHE({
            variables: {
              manuscriptId,
              content: values.public,
              type: values.recommendation,
            },
          })
          break
        default:
          return null
      }
      handleRecommendationFn
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    inviteReviewer: ({ inviteReviewer, manuscript }) => (input, modalProps) => {
      modalProps.setFetching(true)
      inviteReviewer({
        variables: {
          manuscriptId: manuscript.id,
          input,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
          if (modalProps.formProps) {
            modalProps.formProps.resetForm({})
          }
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(parseError(e))
        })
    },
    cancelReviewerInvitation: ({ cancelReviewerInvitation, manuscript }) => (
      reviewer,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      cancelReviewerInvitation({
        variables: {
          manuscriptId: manuscript.id,
          teamMemberId: reviewer.id,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(parseError(e))
        })
    },
  }),
)(ManuscriptPageHE)
