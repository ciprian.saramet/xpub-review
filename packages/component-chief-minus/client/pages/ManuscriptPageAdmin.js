import React, { Fragment } from 'react'
import { get } from 'lodash'
import { compose, withHandlers, withProps } from 'recompose'
import { ActivityLogGQL } from 'component-activity-log/client'

import useVisibleStatuses from '../useVisibleStatuses'
import { withEditorInChiefGQL, withHandlingEditorGQL } from '../graphql/withGQL'
import {
  EICDecision,
  CommentWithFile,
  EditorialComments,
  ReviewerDetailsHE,
  EditorInChiefAssignHE,
} from '../components'

const ManuscriptPageAdmin = ({
  match,
  status,
  reviewers,
  decisions,
  manuscript,
  toggleAssignHE,
  recommendations,
  inviteReviewer,
  reviewerReports,
  assignHEExpanded,
  editorialReviews,
  authorResponseProps,
  inviteHandlingEditor,
  handleEditorialDecision,
  cancelReviewerInvitation,
  editorialCommentsDecisions,
}) => {
  const {
    EICDecisionBox,
    CommentWithFileBox,
    EditorialCommentsBox,
    ReviewerDetailsHEBox,
    EditorInChiefAssignHEBox,
  } = useVisibleStatuses({
    manuscript,
    reviewerReports,
    editorialReviews,
  })
  return (
    <Fragment>
      <EditorialComments
        decisions={editorialCommentsDecisions}
        editorialReviews={editorialReviews}
        isVisible={EditorialCommentsBox.isVisible}
        manuscript={manuscript}
        mt={4}
        startExpanded={EditorialCommentsBox.startExpanded}
      />
      <CommentWithFile
        {...authorResponseProps}
        commentLabel="Author Reply"
        isVisible={CommentWithFileBox.isVisible}
        mt={4}
        startExpanded={CommentWithFileBox.startExpanded}
      />
      <EditorInChiefAssignHE
        expanded={assignHEExpanded}
        inviteHandlingEditor={inviteHandlingEditor}
        isVisible={EditorInChiefAssignHEBox.isVisible}
        manuscript={manuscript}
        toggle={toggleAssignHE}
      />
      <ReviewerDetailsHE
        cancelReviewerInvitation={cancelReviewerInvitation}
        canInvitePublons={ReviewerDetailsHEBox.canInvitePublons}
        canInviteReviewers={ReviewerDetailsHEBox.canInviteReviewers}
        highlight={ReviewerDetailsHEBox.isHighlighted}
        inviteReviewer={inviteReviewer}
        isVisible={ReviewerDetailsHEBox.isVisible}
        manuscript={manuscript}
        mt={4}
        options={recommendations}
        reviewerReports={reviewerReports}
        reviewers={reviewers}
        startExpanded={ReviewerDetailsHEBox.startExpanded}
      />
      <EICDecision
        highlight={EICDecisionBox.isHighlighted}
        isVisible={EICDecisionBox.isVisible}
        mt={4}
        onSubmit={handleEditorialDecision}
        options={decisions}
        status={status}
      />
      <ActivityLogGQL match={match} />
    </Fragment>
  )
}
const parseError = e => e.message.split(':')[1].trim()
export default compose(
  withEditorInChiefGQL,
  withHandlingEditorGQL,

  withProps(({ manuscript }) => ({
    status: get(manuscript, 'status'),
  })),

  withHandlers({
    inviteHandlingEditor: ({ manuscript, inviteHandlingEditor }) => (
      he,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      inviteHandlingEditor({
        variables: {
          submissionId: manuscript.submissionId,
          userId: he.user.id,
        },
      })
        .then(() => {
          modalProps.setFetching(true)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    publishAsEiC: ({ manuscript, makeDecisionToPublishAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      makeDecisionToPublishAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    rejectAsEiC: ({ manuscript, makeDecisionToRejectAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      makeDecisionToRejectAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(true)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    returnToHEAsEiC: ({ manuscript, makeDecisionToReturnAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      makeDecisionToReturnAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    requestRevisionAsEiC: ({
      manuscript,
      requestRevisionAsEiC,
      collapseEicDecision,
    }) => (content, modalProps, formikBag) => {
      modalProps.setFetching(true)
      requestRevisionAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          formikBag.resetForm({ decision: '', message: '' })
          modalProps.setFetching(true)
          modalProps.hideModal()
          collapseEicDecision()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
  }),

  withHandlers({
    handleEditorialDecision: ({
      rejectAsEiC,
      publishAsEiC,
      returnToHEAsEiC,
      requestRevisionAsEiC,
    }) => ({ decision, message }, modalProps, formikBag) => {
      switch (decision) {
        case 'publish':
          publishAsEiC(message, modalProps, formikBag)
          break
        case 'return-to-handling-editor':
          returnToHEAsEiC(message, modalProps, formikBag)
          break
        case 'revision':
          requestRevisionAsEiC(message, modalProps, formikBag)
          break
        case 'reject':
          rejectAsEiC(message, modalProps, formikBag)
          break
        default:
          break
      }
    },
    cancelReviewerInvitation: ({ cancelReviewerInvitation, manuscript }) => (
      reviewer,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      cancelReviewerInvitation({
        variables: {
          manuscriptId: manuscript.id,
          teamMemberId: reviewer.id,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(parseError(e))
        })
    },
    inviteReviewer: ({ inviteReviewer, manuscript }) => (input, modalProps) => {
      modalProps.setFetching(true)
      inviteReviewer({
        variables: {
          manuscriptId: manuscript.id,
          input,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
          if (modalProps.formProps) {
            modalProps.formProps.resetForm({})
          }
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(parseError(e))
        })
    },
  }),
)(ManuscriptPageAdmin)
