import React, { Fragment } from 'react'
import { get } from 'lodash'
import { withJournal } from 'xpub-journal'
import { compose, withHandlers, withProps } from 'recompose'

import { withAuthorGQL } from '../graphql'
import useVisibleStatuses from '../useVisibleStatuses'
import {
  CommentWithFile,
  EditorialComments,
  SubmitRevision,
  AuthorReviews,
} from '../components'

const ManuscriptPageAuthor = ({
  match,
  journal,
  decisions,
  editAuthor,
  manuscript,
  currentUser,
  removeAuthor,
  revisionDraft,
  updateAutosave,
  isLatestVersion,
  reviewerReports,
  editorialReviews,
  getFileSignedUrl,
  authorResponseProps,
  updateDraftRevision,
  addFileToManuscript,
  updateManuscriptFile,
  addAuthorToManuscript,
  submitRevisionAsAuthor,
  removeFileFromManuscript,
  invitationsWithReviewers,
  editorialCommentsDecisions,
}) => {
  const {
    AuthorReviewsBox,
    SubmitRevisionBox,
    CommentWithFileBox,
    EditorialCommentsBox,
  } = useVisibleStatuses({
    manuscript,
    revisionDraft,
    reviewerReports,
    isLatestVersion,
    editorialReviews,
  })
  return (
    <Fragment>
      <EditorialComments
        decisions={editorialCommentsDecisions}
        editorialReviews={editorialReviews}
        isVisible={EditorialCommentsBox.isVisible}
        mt={4}
        startExpanded={EditorialCommentsBox.startExpanded}
      />
      <CommentWithFile
        {...authorResponseProps}
        commentLabel="Your Reply"
        isVisible={CommentWithFileBox.isVisible}
        mt={4}
        startExpanded={CommentWithFileBox.startExpanded}
      />
      <AuthorReviews
        currentUser={currentUser}
        invitations={invitationsWithReviewers}
        isVisible={AuthorReviewsBox.isVisible}
        journal={journal}
        reviewerReports={reviewerReports}
      />
      <SubmitRevision
        addAuthorToManuscript={addAuthorToManuscript}
        addFileToManuscript={addFileToManuscript}
        editAuthor={editAuthor}
        getFileSignedUrl={getFileSignedUrl}
        highlight={SubmitRevisionBox.isHighlighted}
        isVisible={SubmitRevisionBox.isVisible}
        match={match}
        mt={4}
        onSubmit={submitRevisionAsAuthor}
        removeAuthor={removeAuthor}
        removeFileFromManuscript={removeFileFromManuscript}
        revisionDraft={revisionDraft}
        updateAutosave={updateAutosave}
        updateDraftRevision={updateDraftRevision}
        updateManuscriptFile={updateManuscriptFile}
      />
    </Fragment>
  )
}

export default compose(
  withAuthorGQL,
  withJournal,
  withProps(({ data }) => ({
    revisionDraft: get(data, 'getDraftRevision'),
  })),

  withHandlers({
    submitRevisionAsAuthor: ({
      match,
      history,
      submitRevision,
      revisionDraft,
    }) => (values, modalProps) => {
      modalProps.setFetching(true)

      submitRevision({
        variables: {
          submissionId: get(match, 'params.submissionId', ''),
        },
      })
        .then(() => {
          const path = `/details/${get(match, 'params.submissionId')}/${get(
            revisionDraft,
            'id',
          )}`

          modalProps.setFetching(false)
          modalProps.hideModal()
          history.push(path)
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
  }),
)(ManuscriptPageAuthor)
