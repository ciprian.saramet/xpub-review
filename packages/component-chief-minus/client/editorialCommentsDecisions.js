export default [
  {
    label: 'Publish',
    value: 'publish',
    message: 'Publish',
  },
  {
    label: 'Request revision',
    value: 'revision',
    message: 'Revision requested',
  },
  {
    label: 'Reject',
    value: 'reject',
    message: 'Reject',
  },
  {
    label: 'Request minor revision',
    value: 'minor',
    message: 'Minor Revision Requested',
  },
  {
    label: 'Request major revision',
    value: 'major',
    message: 'Major Revision Requested',
  },
  {
    label: 'Return to Handling Editor',
    value: 'returnToHE',
    message: 'Returned to Handling Editor',
  },
]
