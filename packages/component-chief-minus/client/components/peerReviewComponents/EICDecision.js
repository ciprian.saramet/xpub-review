import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withHandlers, withProps, fromRenderProps } from 'recompose'

import {
  Row,
  Item,
  Menu,
  Label,
  Textarea,
  validators,
  MultiAction,
  RemoteOpener,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

const MessageForm = ({ message }) => (
  <Row>
    <Item vertical>
      <Label mb={2} required>
        {message}
      </Label>
      <ValidatedFormField
        component={Textarea}
        data-test-id="eic-decision-message"
        name="message"
        validate={[validators.required]}
      />
    </Item>
  </Row>
)

const SubmitButton = ({ handleSubmit, values }) => (
  <Row justify="flex-end" mb={4}>
    <StyledButton
      data-test-id="submit-eic-decision"
      height={8}
      onClick={handleSubmit}
      primary
      width={34}
    >
      Submit decision
    </StyledButton>
  </Row>
)

const EICDecision = ({
  initialValues,
  eicExpanded,
  isVisible = true,
  decisions,
  toggleEIC,
  onSubmit,
  validate,
  options,
  ...rest
}) =>
  isVisible && (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validate={validate}
    >
      {({ handleSubmit, values }) => (
        <ContextualBox
          expanded={eicExpanded}
          label="Your Editorial Decision"
          toggle={toggleEIC}
          {...rest}
        >
          <Root>
            <Row width={62}>
              <Item mb={2} vertical>
                <Label mb={2} required>
                  Decision
                </Label>
                <ValidatedFormField
                  component={Menu}
                  data-test-id="eic-decision-menu"
                  inline
                  name="decision"
                  options={decisions}
                  placeholder="Please select"
                  validate={[validators.required]}
                />
              </Item>
            </Row>
            <Fragment>
              {(values.decision === 'revision' ||
                values.decision === 'reject') && (
                <MessageForm message="Comments for Author" />
              )}

              {values.decision === 'return-to-handling-editor' && (
                <MessageForm message="Comments for Handling Editor" />
              )}

              {values.decision && (
                <SubmitButton handleSubmit={handleSubmit} values={values} />
              )}
            </Fragment>
          </Root>
        </ContextualBox>
      )}
    </Formik>
  )

const filteredDecisions = (status, options) => {
  switch (status) {
    case 'submitted': {
      return options.filter(
        ({ value }) => value === 'revision' || value === 'reject',
      )
    }
    case 'pendingApproval': {
      return options.filter(({ value }) =>
        ['publish', 'return-to-handling-editor', 'reject'].includes(value),
      )
    }
    default:
      return options.filter(({ value }) => value === 'reject')
  }
}

export default compose(
  withProps(({ options, status }) => ({
    initialValues: { decision: '', message: '' },
    decisions: filteredDecisions(status, options),
  })),
  withModal({
    component: MultiAction,
    modalKey: 'eicDecision',
  }),
  fromRenderProps(RemoteOpener, ({ toggle, expanded }) => ({
    toggleEIC: toggle,
    eicExpanded: expanded,
  })),
  withHandlers({
    onSubmit: ({ onSubmit, showModal, options, initialValues, toggleEIC }) => (
      values,
      formikBag,
    ) => {
      const { title, subtitle, confirmButton } = options.find(
        ({ value }) => value === get(values, 'decision'),
      )
      showModal({
        cancelText: 'Cancel',
        confirmText: confirmButton,
        onConfirm: modalProps => {
          onSubmit(values, modalProps, formikBag).then(() => {
            formikBag.resetForm({})
            modalProps.hideModal()
            toggleEIC()
          })
        },
        subtitle,
        title,
      })
    },
  }),
)(EICDecision)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')} * 4);
  padding-bottom: 0;
`
const StyledButton = styled(Button)`
  height: calc(${th('gridUnit')} * 8);
  line-height: normal;
`

EICDecision.propTypes = {
  /** Visibility state of contextual box. */
  isVisible: PropTypes.bool,
}

EICDecision.defaultProps = {
  isVisible: true,
}
