import React from 'react'
import styled from 'styled-components'
import { DateParser } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { File } from 'component-files/client'
import { Box, Row, Text, Item, Label, ContextualBox } from '@hindawi/ui'

const CommentWithFile = ({
  mt,
  file,
  date,
  message,
  boxLabel,
  isVisible,
  commentLabel,
  authorName,
  startExpanded,
}) =>
  isVisible ? (
    <ContextualBox label={boxLabel} mt={mt} startExpanded={startExpanded}>
      <Root>
        <Box pb={4} pl={4} pr={4} pt={4}>
          <Row alignItems="flex-start" justify="space-between" mb={1}>
            <Item>
              <Label>{commentLabel}</Label>
            </Item>

            <Item alignItems="baseline" justify="flex-end">
              <Text mr={2}>{authorName}</Text>
              {date && (
                <DateParser timestamp={date}>
                  {date => <Text secondary>{date}</Text>}
                </DateParser>
              )}
            </Item>
          </Row>
          <Row justify="flex-start" mb={file ? 4 : 0}>
            <Text>{message}</Text>
          </Row>
          {file && (
            <Row justify="flex-start">
              <Item vertical>
                <Label mb={1}>File</Label>
                <File item={file} />
              </Item>
            </Row>
          )}
        </Box>
      </Root>
    </ContextualBox>
  ) : null

export default CommentWithFile

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')} * 2);
`
