import React, { Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DateParser } from '@pubsweet/ui'
import { get, isEqual, orderBy } from 'lodash'
import { compose, shouldUpdate, withProps } from 'recompose'
import { Label, Text, Row, Item } from '@hindawi/ui'

import PersonInvitation from '../PersonInvitation'

const ReviewersTable = ({
  reviewers = [],
  onCancelReviewerInvitation,
  canCancelReviewerInvitation = true,
}) =>
  reviewers.length > 0 ? (
    <Table>
      <thead>
        <tr>
          <th colSpan={5}>
            <Label>Full Name</Label>
          </th>
          <th>
            <Label>Invited on</Label>
          </th>
          <th>
            <Label>Responded on</Label>
          </th>
          <th>
            <Label>Submitted on</Label>
          </th>
          {canCancelReviewerInvitation && <th>&nbsp;</th>}
        </tr>
      </thead>
      <tbody>
        {reviewers.map((reviewer, index) => (
          <TableRow data-test-id={`reviewer-${reviewer.id}`} key={reviewer.id}>
            <NameTd colSpan={5}>
              {`${get(reviewer, 'alias.name.givenNames', '')} ${get(
                reviewer,
                'alias.name.surname',
              )}`}
              {reviewer.reviewerNumber && (
                <Text customId ml={2}>{`Reviewer ${
                  reviewer.reviewerNumber
                }`}</Text>
              )}
            </NameTd>
            <td>
              <DateParser timestamp={reviewer.invited}>
                {timestamp => timestamp}
              </DateParser>
            </td>
            <td>
              {reviewer.status !== 'pending' ? (
                <Fragment>
                  {reviewer.responded && (
                    <DateParser timestamp={reviewer.responded}>
                      {timestamp => timestamp}
                    </DateParser>
                  )}
                  <Text
                    display="inline-flex"
                    ml={reviewer.status !== 'pending' ? 2 : 0}
                    secondary
                  >
                    {reviewer.status.toUpperCase()}
                  </Text>
                </Fragment>
              ) : (
                <Text
                  display="inline-flex"
                  ml={reviewer.status !== 'pending' ? 2 : 0}
                  secondary
                >
                  {reviewer.status.toUpperCase()}
                </Text>
              )}
            </td>
            <td>
              {get(reviewer, 'review') &&
                get(reviewer, 'review.submitted') !== null && (
                  <DateParser timestamp={get(reviewer, 'review.submitted')}>
                    {timestamp => (
                      <Text display="inline-flex">{timestamp}</Text>
                    )}
                  </DateParser>
                )}
            </td>
            {canCancelReviewerInvitation && (
              <HiddenCell>
                {reviewer.status === 'pending' && (
                  <PersonInvitation
                    invitation={reviewer}
                    onRevoke={onCancelReviewerInvitation}
                    withName={false}
                  />
                )}
              </HiddenCell>
            )}
          </TableRow>
        ))}
      </tbody>
    </Table>
  ) : (
    <Row mb={4} ml={4} mt={4}>
      <Item>
        <Text data-test-id="error-empty-state" emptyState>
          No reviewers invited yet.
        </Text>
      </Item>
    </Row>
  )

const orderInvitations = i => {
  switch (i.status) {
    case 'pending':
      return -1
    case 'accepted':
      return 0
    default:
      return 1
  }
}

export default compose(
  shouldUpdate(
    ({ reviewers }, { reviewers: nextReviewers }) =>
      !isEqual(reviewers, nextReviewers),
  ),
  withProps(({ reviewers = [] }) => ({
    reviewers: orderBy(reviewers, orderInvitations),
  })),
)(ReviewersTable)

// #region styles
const Table = styled.table`
  border-collapse: collapse;
  width: 100%;

  & thead {
    border-bottom: 1px solid ${th('colorBorder')};
    background-color: ${th('colorBackgroundHue2')};
  }

  & th {
    height: calc(${th('gridUnit')} * 8);
  }

  & th,
  & td {
    border: none;
    font-family: ${th('defaultFont')};
    min-width: calc(${th('gridUnit')} * 24);
    padding-left: calc(${th('gridUnit')} * 4);
    text-align: start;
    vertical-align: middle;
  }
`

const HiddenCell = styled.td`
  opacity: 0;
  padding-top: calc(${th('gridUnit')} / 2);
`

const NameTd = styled.td`
  color: ${th('colorSecondary')};
  font-family: ${th('defaultFont')};
  text-decoration: underline;

  position: relative;
`

const TableRow = styled.tr`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom: 1px solid ${th('colorBorder')};
  height: calc(${th('gridUnit')} * 8);

  & td:first-child {
    min-width: calc(${th('gridUnit')} * 60);
  }

  &:hover {
    background-color: ${th('colorBackgroundHue3')};

    ${HiddenCell} {
      opacity: 1;
    }
  }
`
// #endregion
