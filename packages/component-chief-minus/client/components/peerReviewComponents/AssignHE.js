import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { Button, TextField } from '@pubsweet/ui'
import { th, lighten } from '@pubsweet/ui-toolkit'
import {
  compose,
  withProps,
  withHandlers,
  setDisplayName,
  withStateHandlers,
} from 'recompose'
import {
  Row,
  Item,
  Text,
  Icon,
  Label,
  MultiAction,
  ContextualBox,
} from '@hindawi/ui'

const AssignHE = ({
  onChange,
  onInvite,
  clearSearch,
  searchValue,
  handlingEditors,
  ...rest
}) => (
  <ContextualBox height={8} highlight label="Assign Handling Editor" {...rest}>
    <Root>
      <TextContainer>
        <TextField
          data-test-id="manuscript-assign-he-filter"
          inline
          onChange={e => {
            onChange(e.target.value)
          }}
          placeholder="Filter by name or email"
          value={searchValue}
        />
        {searchValue !== '' && (
          <Icon
            data-test-id="clear-filter"
            icon="remove"
            onClick={clearSearch}
            right={10}
            top={10}
          />
        )}
      </TextContainer>
      {handlingEditors.length > 0 ? (
        <Fragment>
          <Row height={6} pl={4}>
            <Item alignItems="center" flex={1}>
              <Label>Name</Label>
            </Item>
            <Item alignItems="center" flex={2}>
              <Label>Email</Label>
            </Item>
            <Item alignItems="center" flex={1} />
          </Row>
          {handlingEditors.map((he, index) => (
            <CustomRow
              data-test-id={`manuscript-assign-he-invite-${he.id}`}
              height={8}
              isFirst={index === 0}
              isLast={index === handlingEditors.length - 1}
              key={he.id}
              pl={4}
            >
              <WithEllipsis>
                <Text ellipsis secondary title={he.name}>
                  {he.name}
                </Text>
              </WithEllipsis>
              <Item flex={2}>
                <Text secondary>{he.email}</Text>
              </Item>
              <Item flex={1} justify="flex-end">
                <Modal
                  component={MultiAction}
                  confirmText="Invite"
                  modalKey={`${he.id}-inviteHE`}
                  onConfirm={onInvite(he)}
                  subtitle={he.name}
                  title="Confirm Invitation"
                >
                  {showModal => (
                    <CustomButton
                      mr={6}
                      onClick={showModal}
                      secondary
                      width={24}
                    >
                      INVITE
                    </CustomButton>
                  )}
                </Modal>
              </Item>
            </CustomRow>
          ))}
        </Fragment>
      ) : (
        <Row alignItems="baseline" justify="flex-start" pl={2}>
          <Icon color="colorError" icon="warning" pr={1} />
          <Text secondary>No handling editors have been found.</Text>
        </Row>
      )}
    </Root>
  </ContextualBox>
)

AssignHE.propTypes = {
  /** The list of available handling editors. */
  handlingEditors: PropTypes.arrayOf(PropTypes.object).isRequired,
  /** Invites the selected handling editor. */
  onInvite: PropTypes.func.isRequired,
}

export default compose(
  withProps(({ handlingEditors = [] }) => ({
    handlingEditors: handlingEditors.map(he => {
      const givenNames = get(he, 'alias.name.givenNames', '')
      const surname = get(he, 'alias.name.surname', '')
      const email = get(he, 'alias.email', '')
      return {
        ...he,
        email,
        name: `${givenNames} ${surname}`,
        searchIndex: `${givenNames} ${surname} ${email}`,
      }
    }),
  })),
  withStateHandlers(
    { searchValue: '' },
    {
      onChange: ({ searchValue }) => value => ({
        searchValue: value.toLowerCase(),
      }),
      clearSearch: () => () => ({
        searchValue: '',
      }),
    },
  ),
  withProps(({ searchValue, handlingEditors = [] }) => ({
    handlingEditors: handlingEditors.filter(he =>
      he.searchIndex.toLowerCase().includes(searchValue),
    ),
  })),
  withHandlers({
    onInvite: ({ onInvite }) => ({
      name,
      email,
      searchIndex,
      ...handlingEditor
    }) => modalProps => onInvite(handlingEditor, modalProps),
  }),
  setDisplayName('AssignHandlingEditor'),
)(AssignHE)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;

  ${space};
`
const WithEllipsis = styled(Item)`
  display: inline-grid;
`
const TextContainer = styled.div`
  margin: calc(${th('gridUnit')} * 2);
  margin-bottom: calc(${th('gridUnit')} * 4);
  position: relative;
  width: calc(${th('gridUnit')} * 80);
`

const CustomButton = styled(Button)`
  height: calc(${th('gridUnit')} * 6);
  opacity: 0;
  line-height: calc(${th('gridUnit')} * 4);

  &:hover {
    background-color: ${lighten('actionSecondaryColor', 0.2)};
    border-color: ${lighten('actionSecondaryColor', 0.2)};
    color: ${th('backgroundColor')};
  }
`

const CustomRow = styled(Row)`
  border-top: ${props => props.isFirst && th('box.border')};
  border-bottom: ${props => !props.isLast && th('box.border')};

  &:hover {
    background-color: #eee;
    border-bottom-left-radius: ${props => props.isLast && '3px'};
    border-bottom-right-radius: ${props => props.isLast && '3px'};

    ${CustomButton} {
      opacity: 1;
    }
  }
`
// #endregion
