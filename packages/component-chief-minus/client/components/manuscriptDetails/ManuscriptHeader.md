Manuscript header without a HE assigned.

```js
const authors = [
    {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ecbafc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ecsdfc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ec56fc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-144cbafc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ec33fc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
]

const collection = {
  customId: '55113358',
  visibleStatus: 'Pending Approval',
  handlingEditor: {
    id: 'he-1',
    name: 'Handlington Ignashevici',
  },
  invitations: [],
}

const fragment = {
  authors,
  created: Date.now(),
  submitted: Date.now(),
  metadata: {
    journal: 'Awesomeness',
    title: 'A very ok title with many authors',
    type: 'research',
  },
}

const currentUser = {
  permissions: {
    canAssignHE: true,
  },
}

const journal = {
  manuscriptTypes: [
    {
      label: 'Research',
      value: 'research',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
    {
      label: 'Review',
      value: 'review',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
  ],
}
;<ManuscriptHeader
  collection={collection}
  fragment={fragment}
  journal={journal}
  currentUser={currentUser}
  handlingEditors={[]}
  inviteHE={() => console.log('go to invite HE ctx box')}
  revokeInvitation={id => console.log('revoke invitation', id)}
/>
```

Manuscript header with a pending HE invitation.

```js
const authors = [
   {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ecbafc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ecsdfc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ec56fc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-144cbafc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ec33fc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
]

const collection = {
  customId: '55113358',
  visibleStatus: 'Pending Approval',
  handlingEditor: {
    id: 'he-1',
    name: 'Handlington Ignashevici',
  },
  invitations: [
    {
      id: 'b4305ab6-84e6-48a3-9eb9-fbe0ec80c694',
      role: 'handlingEditor',
      type: 'invitation',
      reason: 'because',
      userId: 'he-1',
      hasAnswer: false,
      invitedOn: 1533713919119,
      isAccepted: false,
    },
  ],
}

const handlingEditors = [
  {
    id: 'he-1',
    firstName: 'Handlington',
    lastName: 'Ignashevici',
    name: 'Handlington Ignashevici',
  },
]

const fragment = {
  authors,
  created: Date.now(),
  submitted: Date.now(),
  metadata: {
    journal: 'Awesomeness',
    title: 'A very ok title with many authors',
    type: 'research',
  },
}

const currentUser = {
  permissions: {
    canAssignHE: true,
  },
}

const journal = {
  manuscriptTypes: [
    {
      label: 'Research',
      value: 'research',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
    {
      label: 'Review',
      value: 'review',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
  ],
}
;<ManuscriptHeader
  collection={collection}
  fragment={fragment}
  journal={journal}
  currentUser={currentUser}
  handlingEditors={handlingEditors}
  inviteHE={() => console.log('go to invite HE ctx box')}
  revokeInvitation={id => console.log('revoke invitation', id)}
/>
```

Manuscript header with a pending HE invitation.

```js
const authors = [
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ecbafc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ecsdfc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ec56fc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-144cbafc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
  {
    affiliation: 'TSD',
    affiliationNumber: 1,
    country: 'AX',
    id: '5001955e-cc18-42d4-b0ca-15ec33fc48fe',
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
    isCorresponding: true,
    title: 'mr',
  },
]

const collection = {
  customId: '55113358',
  visibleStatus: 'Pending Approval',
  handlingEditor: {
    id: 'he-1',
    name: 'Handlington Ignashevici',
  },
  invitations: [
    {
      id: 'b4305ab6-84e6-48a3-9eb9-fbe0ec80c694',
      role: 'handlingEditor',
      type: 'invitation',
      reason: 'because',
      userId: 'cb7e3e26-6a09-4b79-a6ff-4d1235ee2381',
      hasAnswer: true,
      invitedOn: 1533713919119,
      isAccepted: true,
      respondedOn: 1533714034932,
    },
  ],
}

const fragment = {
  authors,
  created: Date.now(),
  submitted: Date.now(),
  metadata: {
    journal: 'Awesomeness',
    title: 'A very ok title with many authors',
    type: 'research',
  },
}

const currentUser = {
  permissions: {
    canAssignHE: false,
  },
}

const journal = {
  manuscriptTypes: [
    {
      label: 'Research',
      value: 'research',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
    {
      label: 'Review',
      value: 'review',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
  ],
}
;<ManuscriptHeader
  collection={collection}
  fragment={fragment}
  journal={journal}
  currentUser={currentUser}
  inviteHE={() => console.log('go to invite HE ctx box')}
  revokeInvitation={id => console.log('revoke invitation', id)}
/>
```
