import React, { Fragment } from 'react'
import { H4 } from '@pubsweet/ui'
import { isEmpty, get } from 'lodash'
import { withProps, compose } from 'recompose'
import { Row, Text, ContextualBox } from '@hindawi/ui'

import { ManuscriptFileList } from './'

const ManuscriptMetadata = ({
  files,
  abstract,
  filesLabel,
  conflicts = {},
}) => (
  <Fragment>
    {!!abstract && (
      <ContextualBox
        data-test-id="abstract-tab"
        label="Abstract"
        mt={2}
        startExpanded
        transparent
      >
        <Text lineHeight="18px" whiteSpace="pre-wrap">
          {abstract}
        </Text>
      </ContextualBox>
    )}
    {conflicts.hasConflicts && (
      <ContextualBox
        data-test-id="conflict-of-interest-tab"
        label="Conflict of Interest"
        mt={2}
        transparent
      >
        <Row alignItems="center" justify="flex-start">
          <H4 mb={2} mt={2}>
            Conflicts of interest:
          </H4>
          <Text ml={2}>{get(conflicts, 'message', '')}</Text>
        </Row>

        {get(conflicts, 'dataAvailabilityMessage', '') && (
          <Row alignItems="center" justify="flex-start">
            <H4 mb={2} mt={2}>
              Data availability statement:
            </H4>
            <Text ml={2}>{get(conflicts, 'dataAvailabilityMessage', '')}</Text>
          </Row>
        )}
        {get(conflicts, 'fundingMessage', '') && (
          <Row alignItems="center" justify="flex-start">
            <H4 mb={2} mt={2}>
              Funding statement:
            </H4>
            <Text ml={2}>{get(conflicts, 'fundingMessage', '')}</Text>
          </Row>
        )}
      </ContextualBox>
    )}
    {!isEmpty(files) && (
      <ContextualBox
        data-test-id="files-tab"
        label={filesLabel}
        mt={2}
        transparent
      >
        <ManuscriptFileList files={files} />
      </ContextualBox>
    )}
  </Fragment>
)

export default compose(
  withProps(({ manuscript }) => ({
    files: get(manuscript, 'files', ''),
    conflicts: get(manuscript, 'meta.conflicts'),
    abstract: get(manuscript, 'meta.abstract', 'abc'),
    filesLabel: `Files (${get(manuscript, 'files', []).length})`,
  })),
)(ManuscriptMetadata)
