import React, { Fragment } from 'react'
import { get } from 'lodash'
import { withJournal } from 'xpub-journal'
import { H2, H4, Button, DateParser } from '@pubsweet/ui'
import { compose, withProps, setDisplayName } from 'recompose'
import { Row, Text, Label, StatusTag, AuthorTagList } from '@hindawi/ui'

import PersonInvitation from '../PersonInvitation'
import withHEStatus from './decorators/withHEStatus'
import withLabelVisibility from './decorators/withLabelVisibility'

const HEAssignator = ({
  removeHE,
  toggleAssignHE,
  handlingEditor,
  onCancelHEInvitation,
  isLatestVersion,
  canSeeInvite,
  canSeeRevoke,
  canSeeRemove,
}) =>
  (canSeeInvite || canSeeRevoke || canSeeRemove) && (
    <Fragment>
      <PersonInvitation
        invitation={handlingEditor}
        label="Handling Editor"
        onRemove={removeHE}
        onRevoke={onCancelHEInvitation}
      />
      {!get(handlingEditor, 'id', null) && isLatestVersion && (
        <Button ml={2} onClick={toggleAssignHE} primary width={12} xs>
          INVITE
        </Button>
      )}
    </Fragment>
  )

const HELabel = ({
  handlingEditorVisibleStatus,
  canSeeLabelAuthorReviewer,
  canSeeLabelOldVersionsAdminOrEiC,
  canSeeLabelUnassigned,
}) =>
  (canSeeLabelAuthorReviewer ||
    canSeeLabelOldVersionsAdminOrEiC ||
    canSeeLabelUnassigned) && (
    <Fragment>
      <Label mr={2}>Handling Editor</Label>
      <Text>{handlingEditorVisibleStatus}</Text>
    </Fragment>
  )

const ManuscriptHeader = ({
  removeHE,
  manuscript,
  journalTitle,
  editorInChief,
  visibleStatus,
  handlingEditor,
  toggleAssignHE,
  isLatestVersion,
  onCancelHEInvitation,
  manuscriptType = {},
  handlingEditorVisibleStatus,
  canSeeLabelAuthorReviewer,
  canSeeLabelOldVersionsAdminOrEiC,
  canSeeInvite,
  canSeeRevoke,
  canSeeRemove,
  canSeeLabelUnassigned,
  statusColor,
}) => (
  <Fragment>
    <Row
      alignItems="center"
      data-test-id="manuscript-title"
      height={10}
      justify="space-between"
    >
      <H2>{manuscript.meta.title || 'No title'}</H2>
      <StatusTag
        old={!isLatestVersion}
        statusColor={statusColor}
        version={manuscript.version}
      >
        {isLatestVersion ? `${visibleStatus}` : 'Viewing An Older Version'}
      </StatusTag>
    </Row>

    {manuscript.authors.length > 0 && (
      <Row
        alignItems="center"
        data-test-id="authors-row"
        justify="flex-start"
        mt={2}
      >
        <AuthorTagList
          authors={manuscript.authors}
          withAffiliations
          withTooltip
        />
      </Row>
    )}
    <Row alignItems="center" justify="flex-start" mt={2}>
      {manuscript.customId && (
        <Text customId data-test-id="manuscript-id" mr={2}>{`ID ${
          manuscript.customId
        }`}</Text>
      )}
      {manuscript.created && (
        <DateParser durationThreshold={0} timestamp={manuscript.created}>
          {timestamp => <Text mr={6}>Submitted on {timestamp}</Text>}
        </DateParser>
      )}
      <Text>{manuscriptType.label}</Text>
      <Text journal ml={2}>
        {journalTitle}
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mt={2}>
      <H4>Editor in Chief</H4>
      <Text ml={2} mr={6}>
        {editorInChief}
      </Text>

      <HELabel
        canSeeLabelAuthorReviewer={canSeeLabelAuthorReviewer}
        canSeeLabelOldVersionsAdminOrEiC={canSeeLabelOldVersionsAdminOrEiC}
        canSeeLabelUnassigned={canSeeLabelUnassigned}
        handlingEditorVisibleStatus={handlingEditorVisibleStatus}
      />

      <HEAssignator
        canSeeInvite={canSeeInvite}
        canSeeRemove={canSeeRemove}
        canSeeRevoke={canSeeRevoke}
        handlingEditor={handlingEditor}
        isLatestVersion={isLatestVersion}
        onCancelHEInvitation={onCancelHEInvitation}
        removeHE={removeHE}
        toggleAssignHE={toggleAssignHE}
      />
    </Row>
  </Fragment>
)

export default compose(
  withJournal,
  withProps(({ journal = {}, manuscript }) => ({
    statusColor: get(journal, `statuses.${manuscript.status}.color`),
    manuscriptType: get(journal, 'manuscriptTypes', []).find(
      t => t.value === get(manuscript, 'meta.articleType', ''),
    ),
    role: get(manuscript, 'role'),
    journalTitle: get(journal, 'metadata.nameText', ''),
    manuscriptStatus: get(manuscript, 'status', ''),
    visibleStatus: get(
      journal,
      `statuses.${manuscript.status}.${manuscript.role}.label`,
    ),
    handlingEditor: get(manuscript, 'handlingEditor', {}),
    heStatus: get(manuscript, 'handlingEditor.status', ''),
    editorInChief: `${get(
      manuscript,
      'editorInChief.alias.name.givenNames',
      '',
    )} ${get(manuscript, 'editorInChief.alias.name.surname', 'Unassigned')}`,
  })),
  withLabelVisibility,
  withHEStatus,
  setDisplayName('ManuscriptHeader'),
)(ManuscriptHeader)
