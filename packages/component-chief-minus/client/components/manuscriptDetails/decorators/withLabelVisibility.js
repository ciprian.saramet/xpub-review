import { withProps } from 'recompose'

export default withProps(
  ({ role, heStatus, isLatestVersion, manuscriptStatus }) => ({
    canSeeLabelAuthorReviewer:
      ['pending', 'accepted', ''].includes(heStatus) &&
      !['admin', 'editorInChief', 'editorialAssistant'].includes(role),

    canSeeLabelOldVersionsAdminOrEiC:
      ['accepted', '', 'pending'].includes(heStatus) &&
      !isLatestVersion &&
      ['admin', 'editorInChief', 'editorialAssistant'].includes(role),

    canSeeLabelUnassigned:
      heStatus === '' &&
      ['admin', 'editorInChief'].includes(role) &&
      ['technicalChecks', 'revisionRequested'].includes(manuscriptStatus),

    canSeeInvite:
      isLatestVersion &&
      heStatus === '' &&
      ['admin', 'editorInChief', 'editorialAssistant'].includes(role) &&
      !['technicalChecks', 'revisionRequested', 'rejected'].includes(
        manuscriptStatus,
      ),

    canSeeRevoke:
      isLatestVersion &&
      heStatus === 'accepted' &&
      ['admin', 'editorInChief', 'editorialAssistant'].includes(role) &&
      !['technicalChecks', 'rejected', 'accepted'].includes(manuscriptStatus),

    canSeeRemove:
      heStatus === 'pending' &&
      ['admin', 'editorInChief', 'editorialAssistant'].includes(role) &&
      isLatestVersion,
  }),
)
