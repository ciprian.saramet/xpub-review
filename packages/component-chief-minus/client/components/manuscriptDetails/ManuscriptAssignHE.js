import React from 'react'
import PropTypes from 'prop-types'
import { ContextualBox } from '@hindawi/ui'

import { AssignHE } from '../peerReviewComponents'

const ManuscriptAssignHE = ({
  toggle,
  assignHE,
  expanded,
  isFetching,
  inviteHandlingEditor,
  handlingEditors = [],
  currentUser: {
    permissions: { canAssignHE = false },
  },
}) =>
  canAssignHE ? (
    <ContextualBox
      data-test-id="assign-handling-editor"
      expanded={expanded}
      label="Assign Handling Editor"
      scrollIntoView
      toggle={toggle}
    >
      <AssignHE
        handlingEditors={handlingEditors}
        inviteHandlingEditor={assignHE}
        isFetching={isFetching}
      />
    </ContextualBox>
  ) : null

ManuscriptAssignHE.propTypes = {
  /** Handling editors you want to be displayed. */
  handlingEditors: PropTypes.arrayOf(PropTypes.object),
  /** Callback function fired when the handling editor is invited. */
  inviteHandlingEditor: PropTypes.func,
}

ManuscriptAssignHE.defaultProps = {
  handlingEditors: [],
  inviteHandlingEditor: () => {},
}
export default ManuscriptAssignHE
