import React, { Fragment } from 'react'
import { compose, withProps } from 'recompose'

import { ManuscriptFileSection } from '../'

const ManuscriptFileList = ({
  files = [],
  coverLetter,
  manuscripts,
  supplementary,
}) => (
  <Fragment>
    <ManuscriptFileSection label="MAIN MANUSCRIPT" list={manuscripts} />

    <ManuscriptFileSection label="COVER LETTER" list={coverLetter} />

    <ManuscriptFileSection label="SUPPLEMENTAL FILES" list={supplementary} />
  </Fragment>
)

export default compose(
  withProps(({ files }) => ({
    manuscripts: files.filter(f => f.type === 'manuscript'),
    coverLetter: files.filter(f => f.type === 'coverLetter'),
    supplementary: files.filter(f => f.type === 'supplementary'),
  })),
)(ManuscriptFileList)
