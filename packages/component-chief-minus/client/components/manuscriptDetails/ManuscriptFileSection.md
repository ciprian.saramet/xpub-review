Manuscript file section that appears in ManuscriptFilesList

```js
const files = {
  supplementary: [
    {
      id:
        '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/02db6c5e-2938-45ac-a5ee-67ae63919bb2',
      name: 'Supplementary File 1.jpg',
      size: 59621,
      originalName: 'Supplementary File 1.jpg',
    },
    {
      id:
        '8dca903a-05b9-45ab-89b9-9cb99a9a29c6/5e69e3d9-7f9d-4e8d-b649-6e6a45658d75',
      name: 'Supplementary File 2.docx',
      size: 476862,
      originalName: 'Supplementary File 2.docx',
    },
  ]
}
;<ManuscriptFileSection
  list={files.supplementary}
  label="Supplementary files"
  onDownload={() => alert('downloading')}
  onPreview={() => alert('No preview')}
/>
```
