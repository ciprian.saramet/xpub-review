import React, { Fragment } from 'react'
import { chain, get, isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import { DateParser } from '@pubsweet/ui'
import { DownloadZip, PreviewFile } from 'component-files/client'
import { compose, withHandlers, withProps } from 'recompose'
import { Row, Item, Icon, Text, ActionLink, Breadcrumbs } from '@hindawi/ui'

import { ManuscriptVersion } from './'

const ManuscriptDetailsTop = ({
  match,
  history,
  isAdmin,
  goToEdit,
  versions,
  manuscript,
  manuscriptFile,
  goToTechnicalCheck,
  canOverrideTechChecks,
  canEditManuscript,
}) => (
  <Row alignItems="center" height={4} mb={4}>
    <Breadcrumbs path="/">Dashboard</Breadcrumbs>

    <Item alignItems="center" justify="flex-end">
      {canOverrideTechChecks && isAdmin && (
        <ActionLink
          alignItems="center"
          data-test-id="button-qa-manuscript-technical-checks"
          fontSize="14px"
          fontWeight={600}
          mr={4}
          onClick={goToTechnicalCheck}
        >
          <Icon fontSize="14px" icon="checks" mr={2} />
          Technical Checks
        </ActionLink>
      )}
      {canEditManuscript && (
        <ActionLink
          alignItems="center"
          data-test-id="button-qa-manuscript-edit"
          fontSize="14px"
          fontWeight={600}
          mr={4}
          onClick={goToEdit}
        >
          <Icon fontSize="14px" icon="edit" mr={2} />
          Edit
        </ActionLink>
      )}

      {!isEmpty(manuscript.files) && (
        <Fragment>
          <PreviewFile file={manuscriptFile} />
          <DownloadZip manuscript={manuscript} />
        </Fragment>
      )}

      <DateParser
        durationThreshold={0}
        timestamp={get(manuscript, 'created', '')}
      >
        {timestamp => (
          <Text mr={2} secondary>
            Updated on {timestamp}
          </Text>
        )}
      </DateParser>

      <ManuscriptVersion history={history} match={match} versions={versions} />
    </Item>
  </Row>
)

export default compose(
  withProps(({ manuscript }) => ({
    status: get(manuscript, 'status', 'draft'),
    version: get(manuscript, 'version', ''),
    customId: get(manuscript, 'customId', ''),
    technicalCheckToken: get(manuscript, 'technicalCheckToken', ''),
    manuscriptFile: chain(manuscript)
      .get('files', [])
      .find(file => file.type === 'manuscript')
      .value(),
    role: get(manuscript, 'role', ''),
  })),
  withProps(({ status, role, isAdmin, isLatestVersion }) => ({
    canOverrideTechChecks:
      (status === 'technicalChecks' || status === 'inQA') &&
      (role === 'admin' || role === 'editorialAssistant'),
    canEditManuscript:
      isAdmin && isLatestVersion && !['accepted', 'rejected'].includes(status),
  })),
  withHandlers({
    goToEdit: ({ history, manuscript }) => () => {
      history.push(`/submit/${manuscript.submissionId}/${manuscript.id}`)
    },
    goToTechnicalCheck: ({
      history,
      status,
      customId,
      manuscript,
      technicalCheckToken,
    }) => () => {
      const stage = status === 'technicalChecks' ? 'eqs' : 'eqa'
      const title = get(manuscript, 'meta.title', '')
      const id = get(manuscript, 'id', '')
      history.push({
        pathname: `/${stage}-decision`,
        search: `?manuscriptId=${id}&customId=${customId}&token=${technicalCheckToken}&title=${title}`,
      })
    },
  }),
)(ManuscriptDetailsTop)

ManuscriptDetailsTop.propTypes = {
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
  /** Object with versions of manuscript. */
  versions: PropTypes.array, //eslint-disable-line
  /** An async call that takes you to edit. */
  goToEdit: PropTypes.func,
  /** An async call that takes you to thchnical check. */
  goToTechnicalCheck: PropTypes.func,
  /** Object containing token for current user. */
  currentUser: PropTypes.object, //eslint-disable-line
}

ManuscriptDetailsTop.defaultProps = {
  fragment: {},
  collection: {},
  versions: [],
  goToEdit: () => {},
  goToTechnicalCheck: () => {},
  currentUser: {},
}
