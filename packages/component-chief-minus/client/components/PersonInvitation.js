import React, { Fragment } from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'
import { Label, Icon, MultiAction, Text } from '@hindawi/ui'

const PersonInvitation = ({
  label,
  invitation,
  removeHEditor,
  withUnassigned,
  invitationName,
  invitationStatus,
  revokeInvitation,
  withName = true,
}) => (
  <Root>
    {label && <Label>{label}</Label>}
    {withUnassigned && !get(invitation, 'id') && <Text>Unassigned</Text>}
    {withName && <Text ml={label ? 1 : 0}>{invitationName}</Text>}

    {invitationStatus === 'pending' ? (
      <Fragment>
        <Modal
          cancelText="BACK"
          component={MultiAction}
          confirmText="REVOKE"
          modalKey={`${invitation.id}-revoke`}
          onConfirm={revokeInvitation}
          subtitle="Are you sure you want to revoke the invitation?"
          title={invitationName}
        >
          {showModal => (
            <Icon
              data-test-id="revoke-icon"
              fontSize="14px"
              icon="remove"
              ml={2}
              onClick={showModal}
            />
          )}
        </Modal>
      </Fragment>
    ) : (
      invitationStatus === 'accepted' && (
        <Fragment>
          <Modal
            cancelText="BACK"
            component={MultiAction}
            confirmText="REVOKE"
            modalKey={`${invitation.id}-remove`}
            onConfirm={removeHEditor}
            subtitle="Deleting the handling editor at this moment will also remove all his work."
            title="Revoke invitation?"
          >
            {showModal => (
              <Icon
                data-test-id="revoke-icon"
                fontSize="14px"
                icon="remove"
                ml={2}
                onClick={showModal}
              />
            )}
          </Modal>
        </Fragment>
      )
    )}
  </Root>
)

export default compose(
  withProps(({ invitation }) => ({
    invitationName: `${get(invitation, 'alias.name.givenNames', '')} ${get(
      invitation,
      'alias.name.surname',
      '',
    )}`,
    invitationStatus: get(invitation, 'status'),
  })),
  withHandlers({
    revokeInvitation: ({ onRevoke, invitation }) => modalProps => {
      if (typeof onRevoke === 'function') {
        onRevoke(invitation, modalProps)
      }
    },
    removeHEditor: ({ onRemove, invitation }) => modalProps => {
      if (typeof onRemove === 'function') {
        onRemove(invitation, modalProps)
      }
    },
  }),
)(PersonInvitation)

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;

  ${space};
`
// #endregion
