import React from 'react'

import {
  ReviewersTable,
  ReviewerReports,
  ReviewerDetails,
} from './peerReviewComponents'

const ReviewerDetailsEIC = ({
  mt,
  options,
  isVisible,
  reviewers,
  manuscript,
  startExpanded,
  reviewerReports,
  canInviteReviewers,
  cancelReviewerInvitation,
}) => (
  <ReviewerDetails
    isVisible={isVisible}
    mt={mt}
    reviewers={reviewers}
    startExpanded={startExpanded}
    tabButtons={['Reviewer Details', 'Reviewer Reports']}
  >
    <ReviewersTable
      canCancelReviewerInvitation={false}
      onCancelReviewerInvitation={cancelReviewerInvitation}
      reviewers={reviewers}
    />

    <ReviewerReports options={options} reviewerReports={reviewerReports} />
  </ReviewerDetails>
)

export default ReviewerDetailsEIC
