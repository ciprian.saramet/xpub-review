import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { EditorialReportCard, ContextualBox } from '@hindawi/ui'

const EditorialComments = ({
  reviews,
  decisions,
  isVisible,
  editorialReviews,
  ...rest
}) =>
  isVisible ? (
    <ContextualBox
      data-test-id="contextual-box-editorial-comments"
      label="Editorial Comments"
      {...rest}
    >
      <Root>
        {editorialReviews.map(r => (
          <EditorialReportCard
            decisions={decisions}
            key={r.id}
            mb={2}
            privateLabel="Message For Editorial Team"
            publicLabel="Message For Author"
            report={r}
          />
        ))}
      </Root>
    </ContextualBox>
  ) : null

export default EditorialComments

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};

  padding: calc(${th('gridUnit')} * 2);
  padding-bottom: 0;
`
