import React from 'react'
import { Query } from 'react-apollo'

import { AssignHE } from './peerReviewComponents'
import { getHandlingEditors } from '../graphql/queries'

const EditorInChiefAssignHE = ({
  toggle,
  expanded,
  isVisible,
  manuscript,
  inviteHandlingEditor,
}) =>
  isVisible ? (
    <Query
      query={getHandlingEditors}
      variables={{ manuscriptId: manuscript.id }}
    >
      {({ data: { getHandlingEditors: handlingEditors } }) => (
        <AssignHE
          expanded={expanded}
          handlingEditors={handlingEditors}
          mt={4}
          onInvite={inviteHandlingEditor}
          toggle={toggle}
        />
      )}
    </Query>
  ) : null

export default EditorInChiefAssignHE
