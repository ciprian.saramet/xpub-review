import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withProps, compose } from 'recompose'
import { Row, Text, ContextualBox } from '@hindawi/ui'

import { ReviewerReportAuthor } from './'

const SubmittedReportsNumberForAuthorReviews = ({ reports }) => (
  <Row fitContent justify="flex-end">
    <Text customId mr={2}>
      {reports}
    </Text>
    <Text mr={2} pr={2} secondary>
      submitted
    </Text>
  </Row>
)

const AuthorReviews = ({
  token,
  toggle,
  journal,
  isVisible,
  submittedReports,
}) =>
  submittedReports.length > 0 &&
  isVisible && (
    <ContextualBox
      label="Reviewer Reports"
      mb={4}
      mt={4}
      rightChildren={() => (
        <SubmittedReportsNumberForAuthorReviews
          reports={submittedReports.length}
        />
      )}
      toggle={toggle}
    >
      <Root>
        {submittedReports.map(r => (
          <ReviewerReportAuthor
            journal={journal}
            key={r.id}
            report={r}
            token={token}
          />
        ))}
      </Root>
    </ContextualBox>
  )

AuthorReviews.propTypes = {
  /** The list of available reports. */
  reports: PropTypes.arrayOf(PropTypes.object),
  /** Object containing the list of recommendations. */
  journal: PropTypes.object, //eslint-disable-line
  /** Contains the token of the currently logged user. */
  token: PropTypes.string,
}

AuthorReviews.defaultProps = {
  reports: [],
  journal: {},
  token: '',
}

export default compose(
  withProps(({ currentUser, reviewerReports }) => ({
    token: get(currentUser, 'token', ''),
    submittedReports: reviewerReports.filter(review => review.submitted),
  })),
)(AuthorReviews)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
`
