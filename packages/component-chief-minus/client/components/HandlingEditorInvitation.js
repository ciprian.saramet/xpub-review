import React from 'react'
import { Button } from '@pubsweet/ui'
import { Row } from '@hindawi/ui'

import PersonInvitation from './PersonInvitation'

const HandlingEditorInvitation = ({
  toggle,
  handlingEditor,
  cancelInvitation,
}) => (
  <Row justify="flex-start">
    <PersonInvitation
      invitation={handlingEditor}
      label="Handling Editor"
      onRevoke={cancelInvitation}
    />
    {!handlingEditor.id && (
      <Button ml={2} onClick={toggle} primary size="small">
        INVITE
      </Button>
    )}
  </Row>
)

export default HandlingEditorInvitation
