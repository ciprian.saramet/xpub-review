import React from 'react'
import { PublonsTable } from 'component-reviewer-suggestions/client'

import { InviteReviewers } from '.'
import { ReviewerDetails, ReviewerReports } from './peerReviewComponents'

const ReviewerDetailsHE = ({
  mt,
  options,
  isVisible,
  manuscript,
  highlight,
  startExpanded,
  reviewers = [],
  inviteReviewer,
  reviewerReports,
  canInvitePublons,
  canInviteReviewers,
  cancelReviewerInvitation,
}) => (
  <ReviewerDetails
    highlight={highlight}
    isVisible={isVisible}
    mt={mt}
    reviewers={reviewers}
    startExpanded={startExpanded}
    tabButtons={[
      'Reviewer Details',
      'Reviewer Suggestions',
      'Reviewer Reports',
    ]}
  >
    <InviteReviewers
      cancelReviewerInvitation={cancelReviewerInvitation}
      canInviteReviewers={canInviteReviewers}
      onInvite={inviteReviewer}
      reviewers={reviewers}
    />
    <PublonsTable
      canInvitePublons={canInvitePublons}
      manuscriptId={manuscript.id}
      onInvite={inviteReviewer}
    />
    <ReviewerReports options={options} reviewerReports={reviewerReports} />
  </ReviewerDetails>
)

export default ReviewerDetailsHE
