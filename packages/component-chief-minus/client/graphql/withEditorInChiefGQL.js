import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import {
  refetchGetAuditLogEvents,
  refetchGetManuscriptVersions,
} from './refetchQueries'

const withEditorInChiefGQL = compose(
  graphql(mutations.inviteHandlingEditor, {
    name: 'inviteHandlingEditor',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.cancelHandlingEditorInvitation, {
    name: 'cancelHandlingEditorInvitation',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.removeHandlingEditor, {
    name: 'removeHandlingEditor',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.makeDecisionToPublishAsEiC, {
    name: 'makeDecisionToPublishAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.makeDecisionToRejectAsEiC, {
    name: 'makeDecisionToRejectAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.makeDecisionToReturnAsEiC, {
    name: 'makeDecisionToReturnAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.requestRevisionAsEiC, {
    name: 'requestRevisionAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
)

export default withEditorInChiefGQL
