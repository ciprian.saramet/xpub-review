import gql from 'graphql-tag'

export const teamMember = gql`
  fragment teamMember on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
    }
    status
    alias {
      aff
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`

export const fileFragment = gql`
  fragment fileDetails on File {
    id
    type
    size
    filename
    originalName
    mimeType
  }
`

export const reviewDetails = gql`
  fragment reviewDetails on Review {
    id
    member {
      role
      user {
        id
      }
      alias {
        name {
          surname
          givenNames
        }
      }
      reviewerNumber
    }
    created
    comments {
      id
      type
      content
      files {
        ...fileDetails
      }
    }
    recommendation
    open
    submitted
  }
  ${fileFragment}
`

export const manuscriptFragment = gql`
  fragment manuscriptDetails on Manuscript {
    id
    status
    submissionId
    journalId
    submissionId
    customId
    created
    authors {
      id
      isSubmitting
      isCorresponding
      alias {
        aff
        name {
          surname
          givenNames
        }
        email
        country
      }
    }
    meta {
      title
      abstract
      articleType
      conflicts {
        hasConflicts
        message
        dataAvailabilityMessage
        fundingMessage
      }
    }
    version
    technicalCheckToken
    role
    customId
    files {
      ...fileDetails
    }
    reviews {
      ...reviewDetails
    }
    handlingEditor {
      ...teamMember
    }
    editorInChief {
      ...teamMember
    }
    reviewers {
      ...teamMember
    }
  }
  ${teamMember}
  ${fileFragment}
  ${reviewDetails}
`

export const userFragment = gql`
  fragment userDetails on User {
    id
    isActive
    role
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        email
        aff
        isConfirmed
        country
      }
    }
  }
`
