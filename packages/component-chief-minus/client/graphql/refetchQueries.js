import { queries as activityLogQueries } from 'component-activity-log/client'
import * as queries from './queries'

export const refetchGetManuscriptVersions = ({
  params: { submissionId = '' },
}) => ({
  query: queries.getManuscriptVersions,
  variables: { submissionId },
})

export const refetchGetAuditLogEvents = ({
  params: { manuscriptId = '' },
}) => ({
  query: activityLogQueries.getAuditLogEvents,
  variables: { manuscriptId },
})

export const refetchLoadReviewerSuggestions = ({
  params: { manuscriptId = '' },
}) => ({
  query: queries.loadReviewerSuggestions,
  variables: { manuscriptId },
})
