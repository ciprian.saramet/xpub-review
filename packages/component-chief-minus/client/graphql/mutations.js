import gql from 'graphql-tag'
import { fragments as submissionFragments } from 'component-submission/client'
import { reviewDetails } from './fragments'

export const requestRevisionAsHE = gql`
  mutation requestRevisionAsHE(
    $manuscriptId: String!
    $content: String!
    $type: AllowedRevisionType!
  ) {
    requestRevisionAsHE(
      manuscriptId: $manuscriptId
      content: $content
      type: $type
    )
  }
`

export const recommendToRejectAsHE = gql`
  mutation recommendToRejectAsHE(
    $manuscriptId: String!
    $input: RecommendationToRejectAsHEInput
  ) {
    recommendToRejectAsHE(manuscriptId: $manuscriptId, input: $input)
  }
`
export const recommendToPublishAsHE = gql`
  mutation recommendToPublishAsHE(
    $manuscriptId: String!
    $input: RecommendationToPublishAsHEInput
  ) {
    recommendToPublishAsHE(manuscriptId: $manuscriptId, input: $input)
  }
`

export const getFileSignedUrl = gql`
  mutation getFileSignedUrl($fileId: String!) {
    getFileSignedUrl(fileId: $fileId)
  }
`

export const inviteHandlingEditor = gql`
  mutation inviteHandlingEditor($submissionId: String!, $userId: String!) {
    inviteHandlingEditor(submissionId: $submissionId, userId: $userId)
  }
`

export const removeHandlingEditor = gql`
  mutation removeHandlingEditor($teamMemberId: String!) {
    removeHandlingEditor(teamMemberId: $teamMemberId)
  }
`

export const cancelHandlingEditorInvitation = gql`
  mutation cancelHandlingEditorInvitation($teamMemberId: String!) {
    cancelHandlingEditorInvitation(teamMemberId: $teamMemberId)
  }
`

export const acceptHandlingEditorInvitation = gql`
  mutation acceptHandlingEditorInvitation($teamMemberId: String!) {
    acceptHandlingEditorInvitation(teamMemberId: $teamMemberId)
  }
`

export const declineHandlingEditorInvitation = gql`
  mutation declineHandlingEditorInvitation(
    $teamMemberId: String!
    $reason: String
  ) {
    declineHandlingEditorInvitation(
      teamMemberId: $teamMemberId
      reason: $reason
    )
  }
`

export const inviteReviewer = gql`
  mutation inviteReviewer($manuscriptId: String!, $input: ReviewerInput!) {
    inviteReviewer(manuscriptId: $manuscriptId, input: $input)
  }
`

export const cancelReviewerInvitation = gql`
  mutation cancelReviewerInvitation(
    $manuscriptId: String!
    $teamMemberId: String!
  ) {
    cancelReviewerInvitation(
      manuscriptId: $manuscriptId
      teamMemberId: $teamMemberId
    )
  }
`
export const makeDecisionToPublishAsEiC = gql`
  mutation makeDecisionToPublishAsEiC($manuscriptId: String!) {
    makeDecisionToPublishAsEiC(manuscriptId: $manuscriptId)
  }
`

export const makeDecisionToRejectAsEiC = gql`
  mutation makeDecisionToRejectAsEiC(
    $manuscriptId: String!
    $content: String!
  ) {
    makeDecisionToRejectAsEiC(manuscriptId: $manuscriptId, content: $content)
  }
`

export const requestRevisionAsEiC = gql`
  mutation requestRevisionAsEiC($manuscriptId: String!, $content: String!) {
    requestRevisionAsEiC(manuscriptId: $manuscriptId, content: $content)
  }
`

export const makeDecisionToReturnAsEiC = gql`
  mutation makeDecisionToReturnAsEiC(
    $manuscriptId: String!
    $content: String!
  ) {
    makeDecisionToReturnAsEiC(manuscriptId: $manuscriptId, content: $content)
  }
`

// #region Reviewer mutations
export const acceptReviewerInvitation = gql`
  mutation acceptReviewerInvitation($teamMemberId: String!) {
    acceptReviewerInvitation(teamMemberId: $teamMemberId)
  }
`

export const declineReviewerInvitation = gql`
  mutation declineReviewerInvitation($teamMemberId: String!) {
    declineReviewerInvitation(teamMemberId: $teamMemberId)
  }
`

export const updateAutosave = gql`
  mutation updateAutosave($params: AutosaveInput) {
    updateAutosave(params: $params) @client
  }
`

export const updateDraftRevision = gql`
  mutation updateDraftRevision(
    $manuscriptId: String!
    $autosaveInput: RevisionAutosaveInput
  ) {
    updateDraftRevision(
      manuscriptId: $manuscriptId
      autosaveInput: $autosaveInput
    ) {
      ...draftManuscriptDetails
      comment {
        id
        type
        content
        files {
          ...fileDetails
        }
      }
    }
  }
  ${submissionFragments.draftManuscriptDetails}
`

export const submitRevision = gql`
  mutation submitRevision($submissionId: String!) {
    submitRevision(submissionId: $submissionId)
  }
`

export const updateDraftReview = gql`
  mutation updateDraftReview(
    $reviewId: String!
    $input: UpdateDraftReviewInput!
  ) {
    updateDraftReview(reviewId: $reviewId, input: $input) {
      ...reviewDetails
    }
  }
  ${reviewDetails}
`
export const updateAutosaveReview = gql`
  mutation updateAutosave($params: AutosaveInput) {
    updateAutosave(params: $params) @client
  }
`
export const submitReview = gql`
  mutation submitReview($reviewId: String!) {
    submitReview(reviewId: $reviewId)
  }
`
// #endregion
