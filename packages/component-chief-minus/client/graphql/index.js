import * as queries from './queries'
import * as fragments from './fragments'
import * as mutations from './mutations'

export { queries, fragments, mutations }

export {
  withAuthorGQL,
  withReviewerGQL,
  default as withGQL,
  withEditorInChiefGQL,
  withHandlingEditorGQL,
} from './withGQL'
