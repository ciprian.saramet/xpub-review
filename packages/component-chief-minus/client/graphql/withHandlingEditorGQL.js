import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import {
  refetchGetManuscriptVersions,
  refetchLoadReviewerSuggestions,
} from './refetchQueries'

const withHandlingEditorGQL = compose(
  graphql(mutations.acceptHandlingEditorInvitation, {
    name: 'acceptHandlingEditorInvitation',
    options: ({ match }) => ({
      refetchQueries: match.params.submissionId
        ? [refetchGetManuscriptVersions(match)]
        : [],
    }),
  }),
  graphql(mutations.declineHandlingEditorInvitation, {
    name: 'declineHandlingEditorInvitation',
    options: {
      refetchQueries: ['getManuscripts'],
    },
  }),
  graphql(mutations.requestRevisionAsHE, {
    name: 'requestRevisionAsHE',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
  graphql(mutations.recommendToRejectAsHE, {
    name: 'recommendToRejectAsHE',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
  graphql(mutations.recommendToPublishAsHE, {
    name: 'recommendToPublishAsHE',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
  graphql(mutations.inviteReviewer, {
    name: 'inviteReviewer',
    options: ({ match }) => {
      const refetchQueries = [
        refetchGetManuscriptVersions(match),
        refetchLoadReviewerSuggestions(match),
      ]
      return { refetchQueries }
    },
  }),

  graphql(mutations.cancelReviewerInvitation, {
    name: 'cancelReviewerInvitation',
    options: ({ match }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      return { refetchQueries }
    },
  }),
)

export default withHandlingEditorGQL
