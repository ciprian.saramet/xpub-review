import React from 'react'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from './testUtils'
import PersonInvitation from '../components/PersonInvitation'

const invitation = {
  id: '6dfd6286-3366-41fb-83e4-aed80955e579',
  isSubmitting: null,
  isCorresponding: null,
  status: 'pending',
  alias: {
    aff: 'Boko Haram',
    email: 'alexandru.munteanu+r1@thinslices.com',
    country: 'RO',
    name: {
      surname: 'MuntRev1',
      givenNames: 'AlexRev1',
    },
  },
}

describe('Person Invitation', () => {
  afterEach(cleanup)

  it('should show unassigned', () => {
    const { getByText, queryByTestId } = render(
      <PersonInvitation
        invitation={{
          id: null,
          isSubmitting: null,
          isCorresponding: null,
          status: null,
          alias: {
            aff: 'Boko Haram',
            email: 'alexandru.munteanu+r1@thinslices.com',
            country: 'RO',
            name: {
              surname: 'MuntRev1',
              givenNames: 'AlexRev1',
            },
          },
        }}
        label="Handling Editor"
        withUnassigned
      />,
    )

    expect(getByText('Unassigned')).toBeInTheDocument()
    expect(queryByTestId(/revoke-icon/i)).toBeNull()
  })

  it('should revoke the invitation', () => {
    const onRevokeMock = jest.fn()

    const { getByText, getByTestId } = render(
      <PersonInvitation
        invitation={invitation}
        label="Reviewer"
        onRevoke={onRevokeMock}
      />,
    )

    fireEvent.click(getByTestId(/revoke-icon/i))

    expect(getByText('AlexRev1 MuntRev1')).toBeInTheDocument()

    fireEvent.click(getByTestId(/modal-confirm/i))

    expect(onRevokeMock).toHaveBeenCalledTimes(1)
    expect(onRevokeMock).toHaveBeenCalledWith(invitation, expect.anything())
  })
})
