import React from 'react'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from '../testUtils'
import { InviteReviewersForm } from '../..'

const reviewer = {
  email: 'pedro.ruiz@universidaddecolombia.co',
  givenNames: 'Pedro Rúiz',
  surname: 'Gonzales',
}

describe('Invite Reviewers Form', () => {
  afterEach(cleanup)

  it('should validate', done => {
    const { getByText, getAllByText } = render(<InviteReviewersForm />)

    fireEvent.click(getByText(/send/i))

    setTimeout(() => {
      const formErrors = getAllByText(/required/i)
      expect(formErrors).toHaveLength(3)

      done()
    })
  })

  it('should submit if form is valid', done => {
    const onInviteMock = jest.fn()

    const { getByText, getByTestId } = render(
      <InviteReviewersForm onInvite={onInviteMock} />,
    )

    fireEvent.change(getByTestId('reviewer-email'), {
      target: {
        value: reviewer.email,
      },
    })

    fireEvent.change(getByTestId('reviewer-givenNames'), {
      target: {
        value: reviewer.givenNames,
      },
    })

    fireEvent.change(getByTestId('reviewer-surname'), {
      target: {
        value: reviewer.surname,
      },
    })

    fireEvent.click(getByText(/send/i))

    setTimeout(() => {
      expect(onInviteMock).toHaveBeenCalledTimes(1)
      expect(onInviteMock).toHaveBeenCalledWith(reviewer, expect.anything())

      done()
    })
  })
})
