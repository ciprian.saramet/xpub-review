import React from 'react'
import { cleanup, fireEvent } from 'react-testing-library'

import { EICDecision } from '../../'
import { render } from '../testUtils'

const mockDecisions = [
  {
    label: 'Publish',
    value: 'publish',
    message: 'Published',
    title: 'Publish Manuscript?',
    subtitle: 'A publish decision is final',
    confirmButton: 'Publish manuscript',
  },
  {
    label: 'Return to Handling Editor',
    value: 'return-to-handling-editor',
    message: 'Return Manuscript',
    title: 'Return Manuscript?',
    subtitle: 'A returning manuscript to Handling Editor decision is final',
    confirmButton: 'Return Manuscript',
  },
  {
    label: 'Request Revision',
    value: 'revision',
    message: 'Revision Requested',
    title: 'Request revision?',
    subtitle: null,
    confirmButton: 'Request Revision',
  },
  {
    label: 'Reject',
    value: 'reject',
    message: 'Rejected',
    title: 'Reject manuscript?',
    subtitle: 'A rejection decision is final',
    confirmButton: 'Reject manuscript',
  },
]

describe('EiC Decision', () => {
  afterEach(cleanup)

  it('should not call onSubmit when the message field is empty', () => {
    const onSubmitMock = jest.fn()

    const { selectOption, getByText } = render(
      <EICDecision
        isVisible
        onSubmit={onSubmitMock}
        options={mockDecisions}
        startExpanded
        status="submitted"
      />,
    )
    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Reject')
    expect(onSubmitMock).toHaveBeenCalledTimes(0)
  })

  it('should call onSubmit with the correct reject decision & message', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByText, getByTestId, selectOption } = render(
      <EICDecision
        isVisible
        onSubmit={onSubmitMock}
        options={mockDecisions}
        startExpanded
        status="submitted"
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Reject')
    fireEvent.change(getByTestId('eic-decision-message'), {
      target: { value: 'bine pa' },
    })
    fireEvent.click(getByText('Submit decision'))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('should call onSubmit with the correct revision decision & message', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByTestId, selectOption, getByText } = render(
      <EICDecision
        isVisible
        onSubmit={onSubmitMock}
        options={mockDecisions}
        startExpanded
        status="submitted"
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Request Revision')
    fireEvent.change(getByTestId('eic-decision-message'), {
      target: { value: 'you can do better' },
    })

    fireEvent.click(getByTestId(/submit-eic-decision/i))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })

  it('should call onSubmit with publish decision', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByTestId, selectOption, getByText } = render(
      <EICDecision
        isVisible
        onSubmit={onSubmitMock}
        options={mockDecisions}
        startExpanded
        status="pendingApproval"
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Publish')
    fireEvent.click(getByTestId(/submit-eic-decision/i))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })
  it('should call onSubmit with with the correct return to he decision & message', done => {
    const onSubmitMock = jest.fn(() => Promise.resolve())
    const { getByTestId, selectOption, getByText } = render(
      <EICDecision
        isVisible
        onSubmit={onSubmitMock}
        options={mockDecisions}
        startExpanded
        status="pendingApproval"
      />,
    )

    fireEvent.click(getByText('Your Editorial Decision'))
    selectOption('Return to Handling Editor')
    fireEvent.change(getByTestId('eic-decision-message'), {
      target: { value: 'you can do better' },
    })

    fireEvent.click(getByTestId(/submit-eic-decision/i))

    setTimeout(() => {
      fireEvent.click(getByTestId(/modal-confirm/i))
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      done()
    })
  })
})
