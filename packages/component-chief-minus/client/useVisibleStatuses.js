import { get, chain, isEmpty } from 'lodash'
import { useJournal } from 'component-journal-info'

const useVisibleStatuses = ({
  currentUser,
  manuscript = {},
  revisionDraft = false,
  editorialReviews = [],
  isLatestVersion,
  reviewerReports = [],
}) => {
  const { parsedStatuses: statuses } = useJournal()
  const status = get(manuscript, 'status')
  const role = get(manuscript, 'role')
  const reviewers = get(manuscript, 'reviewers') || []
  const reviews = get(manuscript, 'reviews') || []
  const handlingEditor = get(manuscript, 'handlingEditor')
  const currentUserId = get(currentUser, 'id')
  const version = get(manuscript, 'version')
  const authorResponse = get(manuscript, 'reviews', []).find(
    r => get(r, 'recommendation') === 'responseToRevision',
  )
  const currentReviewer = chain(reviewers)
    .find(reviewer => reviewer.user.id === currentUserId)
    .value()

  const AuthorReviews = {
    author: {
      isVisible: status === statuses.revisionRequested || !isLatestVersion,
    },
  }
  const CommentWithFile = {
    author: {
      isVisible: !isEmpty(authorResponse),
      startExpanded:
        !isEmpty(authorResponse) && status !== statuses.revisionRequested,
    },
    admin: {
      isVisible:
        !isEmpty(authorResponse) && status !== statuses.revisionRequested,
      startExpanded: !isEmpty(authorResponse) && status === statuses.submitted,
    },
    editorialAssistant: {
      isVisible:
        !isEmpty(authorResponse) && status !== statuses.revisionRequested,
      startExpanded: !isEmpty(authorResponse) && status === statuses.submitted,
    },
    editorInChief: {
      isVisible:
        !isEmpty(authorResponse) && status !== statuses.revisionRequested,
      startExpanded: !isEmpty(authorResponse) && status === statuses.submitted,
    },
    handlingEditor: {
      isVisible:
        !isEmpty(authorResponse) &&
        version > 1 &&
        get(handlingEditor, 'status') === 'accepted',
      startExpanded:
        [
          statuses.heAssigned,
          statuses.reviewersInvited,
          statuses.underReview,
        ].includes(status) && !isEmpty(authorResponse),
    },
    reviewer: {
      isVisible:
        authorResponse &&
        ['accepted', 'submitted'].includes(get(currentReviewer, 'status')),
      startExpanded:
        status === statuses.underReview && !isEmpty(authorResponse),
    },
  }
  const EditorialComments = {
    author: {
      isVisible:
        !!editorialReviews.length &&
        ![statuses.pendingApproval, statuses.inQA].includes(status),
      startExpanded: true,
    },
    admin: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    editorialAssistant: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    editorInChief: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    handlingEditor: {
      isVisible:
        !!editorialReviews.length &&
        get(handlingEditor, 'status') === 'accepted',
      startExpanded: true,
    },
    reviewer: {
      isVisible:
        !!editorialReviews.length &&
        ['accepted', 'submitted'].includes(get(currentReviewer, 'status')),
      startExpanded: true,
    },
  }

  const EditorInChiefAssignHE = {
    admin: {
      isVisible: status === statuses.submitted,
    },
    editorialAssistant: {
      isVisible: status === statuses.submitted,
    },
    editorInChief: {
      isVisible: status === statuses.submitted,
    },
  }

  const EICDecision = {
    admin: {
      isVisible: ![
        statuses.technicalChecks,
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),
      isHighlighted: [statuses.pendingApproval].includes(status),
    },
    editorialAssistant: {
      isVisible: ![
        statuses.technicalChecks,
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),
      isHighlighted: [statuses.pendingApproval].includes(status),
    },
    editorInChief: {
      isVisible: ![
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),
      isHighlighted: [statuses.pendingApproval].includes(status),
    },
  }

  const HERecommendation = {
    handlingEditor: {
      isVisible: [statuses.heAssigned, statuses.reviewCompleted].includes(
        status,
      ),
      highlight: [statuses.reviewCompleted].includes(status),
    },
  }

  const RespondToEditorialInvitation = {
    handlingEditor: {
      isVisible: status === statuses.heInvited,
      isHighlighted: true,
      startExpanded: true,
    },
  }

  const RespondToReviewerInvitation = {
    reviewer: {
      isVisible: get(currentReviewer, 'status') === 'pending',
      isHighlighted: true,
      startExpanded: true,
    },
  }

  const ReviewerDetailsEIC = {
    admin: {
      isVisible: get(handlingEditor, 'status') === 'accepted',
      startExpanded: false,
    },
    editorialAssistant: {
      isVisible: get(handlingEditor, 'status') === 'accepted',
      startExpanded: false,
    },
    editorInChief: {
      isVisible: get(handlingEditor, 'status') === 'accepted',
      startExpanded: false,
    },
  }
  const heAdmin = {
    isVisible:
      ![statuses.heInvited, statuses.rejected].includes(status) &&
      get(handlingEditor, 'status') === 'accepted',

    startExpanded:
      ([
        statuses.heAssigned,
        statuses.reviewersInvited,
        statuses.underReview,
      ].includes(status) &&
        isEmpty(authorResponse)) ||
      status === statuses.reviewCompleted,

    isHighlighted: [
      statuses.heAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
    ].includes(status),

    canInviteReviewers: [
      statuses.heAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
    ].includes(status),

    canInvitePublons: [
      statuses.heAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
    ].includes(status),
  }
  const ReviewerDetailsHE = {
    handlingEditor: heAdmin,
    admin: heAdmin,
    editorialAssistant: heAdmin,
  }

  const reviewerCanSeeHisReport = !!chain(reviews)
    .find(review => get(review, 'member.user.id') === currentUserId)
    .get('submitted')
    .value()
  const reviewerCanSubmitReport =
    get(currentReviewer, 'status') === 'accepted' &&
    ![statuses.revisionRequested, statuses.olderVersion].includes(status)
  const ReviewerReportBox = {
    reviewer: {
      isVisible: reviewerCanSeeHisReport,
      startExpanded: true,
    },
  }

  const ReviewerSubmitReport = {
    reviewer: {
      isVisible: reviewerCanSubmitReport && !reviewerCanSeeHisReport,
      isHighlighted: true,
    },
  }

  const SubmitRevision = {
    author: {
      isVisible: revisionDraft && status === statuses.revisionRequested,
      isHighlighted: true,
    },
  }

  return {
    AuthorReviewsBox: AuthorReviews[role],
    CommentWithFileBox: CommentWithFile[role],
    EditorialCommentsBox: EditorialComments[role],
    EditorInChiefAssignHEBox: EditorInChiefAssignHE[role],
    EICDecisionBox: EICDecision[role],
    HERecommendationBox: HERecommendation[role],
    RespondToEditorialInvitationBox: RespondToEditorialInvitation[role],
    RespondToReviewerInvitationBox: RespondToReviewerInvitation[role],
    ReviewerDetailsEICBox: ReviewerDetailsEIC[role],
    ReviewerDetailsHEBox: ReviewerDetailsHE[role],
    ReviewerReportContextualBox: ReviewerReportBox[role],
    ReviewerSubmitReportBox: ReviewerSubmitReport[role],
    SubmitRevisionBox: SubmitRevision[role],
  }
}

export default useVisibleStatuses
