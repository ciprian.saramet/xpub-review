export default [
  {
    label: 'Publish',
    value: 'publish',
    message: 'Publish',
    title: 'Publish Manuscript?',
    subtitle: 'A publish decision is final',
    confirmButton: 'Publish manuscript',
  },
  {
    label: 'Return to Handling Editor',
    value: 'return-to-handling-editor',
    message: 'Return Manuscript',
    title: 'Return Manuscript?',
    subtitle: 'A returning manuscript to Handling Editor decision is final',
    confirmButton: 'Return Manuscript',
  },
  {
    label: 'Request Revision',
    value: 'revision',
    message: 'Revision Requested',
    title: 'Request revision?',
    subtitle: null,
    confirmButton: 'Request Revision',
  },
  {
    label: 'Reject',
    value: 'reject',
    message: 'Reject',
    title: 'Reject manuscript?',
    subtitle: 'A rejection decision is final',
    confirmButton: 'Reject manuscript',
  },
]
