process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const Chance = require('chance')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  review_submitted: 'submitted a report',
}
logEvent.objectType = { manuscript: 'manuscript' }

const { submitReviewUseCase } = require('../../src/use-cases/submitReview')

const notificationService = {
  notifyHEWhenReviewerSubmitsReview: jest.fn(),
}

const chance = new Chance()

describe('Submit Review', () => {
  it('should successfully submit review', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })

    await dataService.createUserOnManuscript({
      fixtures,
      input: { isSubmitting: true, isCorresponding: true },
      manuscript,
      role: 'author',
    })

    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {},
      role: 'reviewer',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'admin',
    })

    const review = fixtures.generateReview({
      teamMemberId: teamMember.id,
      recommendation: chance.pickone(['minor', 'major', 'reject', 'publish']),
    })
    const comment = fixtures.generateComment({
      reviewId: review.id,
      type: 'public',
      content: chance.sentence(),
      files: [],
    })

    review.manuscript = manuscript
    review.member = teamMember
    review.comments = [comment]

    const mockedModels = models.build(fixtures)

    await submitReviewUseCase
      .initialize({ notificationService, models: mockedModels, logEvent })
      .execute({
        reviewId: review.id,
        userId: teamMember.user.id,
      })
    expect(['minor', 'major', 'reject', 'publish']).toContain(
      review.recommendation,
    )
    expect(
      notificationService.notifyHEWhenReviewerSubmitsReview,
    ).toHaveBeenCalledTimes(1)
    expect(review.comments[0].type).toEqual('public')
    expect(review.comments[0].content).not.toBeNull()
    expect(manuscript.status).toEqual('reviewCompleted')
  })

  it('should throw error if user cannot update review', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {},
      role: 'reviewer',
    })

    const review = fixtures.generateReview({
      teamMemberId: teamMember.id,
      recommendation: chance.pickone(['minor', 'major', 'reject', 'publish']),
    })
    const comment = fixtures.generateComment({
      reviewId: review.id,
      type: 'public',
      content: chance.sentence(),
    })

    review.manuscript = manuscript
    review.member = teamMember
    review.comments = [comment]

    const mockedModels = models.build(fixtures)

    try {
      const result = await submitReviewUseCase
        .initialize({
          models: mockedModels,
          logEvent,
        })
        .execute({
          reviewId: review.id,
          userId: chance.guid(),
        })
      expect(result).toBeDefined()
    } catch (e) {
      expect(e.message).toEqual('User can not update selected review')
    }
  })

  it('should throw error if user submits empty review', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {},
      role: 'reviewer',
    })

    const review = fixtures.generateReview({
      teamMemberId: teamMember.id,
      recommendation: chance.pickone(['minor', 'major', 'reject', 'publish']),
    })

    review.manuscript = manuscript
    review.member = teamMember
    review.comments = []

    const mockedModels = models.build(fixtures)

    try {
      await submitReviewUseCase
        .initialize({
          models: mockedModels,
          logEvent,
        })
        .execute({
          reviewId: review.id,
          userId: teamMember.user.id,
        })
      expect(review).toBeDefined()
    } catch (e) {
      expect(e.message).toEqual('Cannot submit an empty review')
    }
  })

  it('should strip private and empty comments', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })

    await dataService.createUserOnManuscript({
      fixtures,
      input: { isSubmitting: true, isCorresponding: true },
      manuscript,
      role: 'author',
    })

    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {},
      role: 'reviewer',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'admin',
    })
    const review = fixtures.generateReview({
      teamMemberId: teamMember.id,
      recommendation: chance.pickone(['minor', 'major', 'reject', 'publish']),
    })

    const publicComment = fixtures.generateComment({
      reviewId: review.id,
      type: 'public',
      content: chance.sentence(),
      files: [],
    })

    const privateComment = fixtures.generateComment({
      reviewId: review.id,
      type: 'private',
    })

    review.manuscript = manuscript
    review.member = teamMember
    review.comments = [publicComment, privateComment]

    const mockedModels = models.build(fixtures)
    await submitReviewUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
      })
      .execute({
        reviewId: review.id,
        userId: teamMember.user.id,
      })
    expect(
      fixtures.comments.filter(comm => comm.type === 'private'),
    ).toHaveLength(0)
    expect(review.comments[0].type).not.toEqual('private')
  })
})
