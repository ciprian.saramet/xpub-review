process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  reviewer_declined: 'declined invitation to review',
}
logEvent.objectType = { manuscript: 'manuscript' }

const { declineReviewerInvitationUseCase } = require('../../src/use-cases')

const notificationService = {
  notifyHEAboutReviewerInvitationDecision: jest.fn(),
}

describe('Decline invitation as a reviewer', () => {
  it('decline the invitation', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'reviewer',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const handlingEditorMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)
    const teamMemberId = teamMember.id
    await declineReviewerInvitationUseCase
      .initialize({ notificationService, models: mockedModels, logEvent })
      .execute({ teamMemberId, userId: handlingEditorMember.userId })

    expect(teamMember.status).toEqual('declined')
    expect(
      notificationService.notifyHEAboutReviewerInvitationDecision,
    ).toHaveBeenCalledTimes(1)
  })
})
