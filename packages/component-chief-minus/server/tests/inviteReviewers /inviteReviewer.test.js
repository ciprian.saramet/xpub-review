process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  reviewer_invited: 'sent reviewer invitation to',
}
logEvent.objectType = { user: 'user' }

const { inviteReviewerUseCase } = require('../../src/use-cases')

const emailJobsService = {
  scheduleEmailsWhenReviewerIsInvited: jest.fn(),
}

const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}

const invitationsService = {
  sendInvitationToReviewer: jest.fn(),
}

describe('Invite reviewer', () => {
  it('Invite user in the reviewer team', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })
    const reviewer = fixtures.generateUser({})
    const identity = fixtures.generateIdentity({ userId: reviewer.id })
    reviewer.assignIdentity(identity)
    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const manuscriptTeam = fixtures.generateTeam({
      role: 'reviewer',
      manuscriptId: manuscript.id,
    })
    manuscriptTeam.members = [reviewer]
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'admin',
    })

    const handlingEditorMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)

    await inviteReviewerUseCase
      .initialize({
        invitationsService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          email: reviewer.identities[0].email,
          givenNames: reviewer.identities[0].givenNames,
          surname: reviewer.identities[0].surname,
        },
        userId: handlingEditorMember.userId,
      })

    expect(manuscriptTeam.members).toHaveLength(1)
    expect(manuscriptTeam.members.map(m => m.identities[0].userId)).toContain(
      reviewer.id,
    )
    expect(invitationsService.sendInvitationToReviewer).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.scheduleEmailsWhenReviewerIsInvited,
    ).toHaveBeenCalledTimes(1)
    expect(removalJobsService.scheduleRemovalJob).toHaveBeenCalledTimes(1)
  })
})
