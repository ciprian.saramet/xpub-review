process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const Chance = require('chance')

const chance = new Chance()

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  reviewer_agreed: 'accepted invitation to review',
}
logEvent.objectType = { manuscript: 'manuscript' }
const { acceptReviewerInvitationUseCase } = require('../../src/use-cases')

const emailJobsService = {
  scheduleEmailsWhenReviewerAcceptsInvitation: jest.fn(),
}

const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}

const notificationService = {
  notifyReviewerOnCancelOrAcceptInvitation: jest.fn(),
  notifyHEAboutReviewerInvitationDecision: jest.fn(),
}

describe('Accept invitation as a reviewer', () => {
  it('accept the invitation', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    manuscript.journal = journal

    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'reviewer',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const handlingEditorMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)
    const teamMemberId = teamMember.id

    await acceptReviewerInvitationUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
      })
      .execute({ teamMemberId, userId: handlingEditorMember.userId })
    expect(teamMember.status).toEqual('accepted')
    expect(teamMember.reviewerNumber).toEqual(1)
    expect(
      emailJobsService.scheduleEmailsWhenReviewerAcceptsInvitation,
    ).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyReviewerOnCancelOrAcceptInvitation,
    ).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyHEAboutReviewerInvitationDecision,
    ).toHaveBeenCalledTimes(1)
  })

  it('receive the old reviewer number if reviewer has accepted on an older version of the manuscript after major revision ', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 1,
    })
    firstVersion.journal = journal

    const secondVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 2,
    })
    secondVersion.journal = journal
    await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })
    let firstReviewer
    let secondReviewer
    firstReviewer = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: 'accepted', reviewerNumber: 1 },
      role: 'reviewer',
    })
    secondReviewer = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: 'submitted', reviewerNumber: 2 },
      role: 'reviewer',
    })
    secondReviewer = await dataService.addUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: {
        status: 'accepted',
        reviewerNumber: 2,
      },
      role: 'reviewer',
      user: fixtures.users.find(u => u.id === secondReviewer.userId),
    })
    firstReviewer = await dataService.addUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: {
        status: 'pending',
      },
      role: 'reviewer',
      user: fixtures.users.find(u => u.id === firstReviewer.userId),
    })
    const mockedModels = models.build(fixtures)
    const teamMemberId = firstReviewer.id
    await acceptReviewerInvitationUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
      })
      .execute({ teamMemberId, userId: firstReviewer.userId })
    expect(firstReviewer.status).toEqual('accepted')
    expect(firstReviewer.reviewerNumber).toEqual(1)
  })
  it('receives the next available reviewer number if has not accepted to review one of the older manuscript versions after major revision ', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 1,
    })
    firstVersion.journal = journal

    const secondVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 2,
    })
    secondVersion.journal = journal
    await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })
    let firstReviewer
    let secondReviewer
    let thirdReviewer
    firstReviewer = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: {
        status: 'pending',
      },
      role: 'reviewer',
    })
    secondReviewer = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: {
        status: 'submitted',
        reviewerNumber: 1,
      },
      role: 'reviewer',
    })
    thirdReviewer = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: {
        status: 'submitted',
        reviewerNumber: 2,
      },
      role: 'reviewer',
    })
    secondReviewer = await dataService.addUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: {
        status: 'submitted',
        reviewerNumber: 1,
      },
      role: 'reviewer',
      user: fixtures.users.find(u => u.id === secondReviewer.userId),
    })
    thirdReviewer = await dataService.addUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: {
        status: 'submitted',
        reviewerNumber: 2,
      },
      role: 'reviewer',
      user: fixtures.users.find(u => u.id === thirdReviewer.userId),
    })
    firstReviewer = await dataService.addUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: {
        status: 'pending',
      },
      role: 'reviewer',
      user: fixtures.users.find(u => u.id === firstReviewer.userId),
    })
    const mockedModels = models.build(fixtures)
    const teamMemberId = firstReviewer.id
    await acceptReviewerInvitationUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
      })
      .execute({ teamMemberId })
    expect(firstReviewer.status).toEqual('accepted')
    expect(firstReviewer.reviewerNumber).toEqual(3)
  })
})
