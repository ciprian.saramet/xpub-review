process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  updateDraftRevisionUseCase,
} = require('../../src/use-cases/submitRevision')

const chance = new Chance()

describe('Update Draft Version Use Case', () => {
  it('changes the comment info', async () => {
    const mockedModels = models.build(fixtures)
    const manuscript = fixtures.generateManuscript({})

    const submittingAuthor = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
      input: { isSubmitting: true },
    })

    const responseToRevision = chance.sentence()
    const input = {
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
      authors: [],
      content: responseToRevision,
    }
    const review = await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'responseToRevision',
      fixtures,
      teamMember: submittingAuthor,
    })

    await updateDraftRevisionUseCase.initialize(mockedModels).execute({
      manuscriptId: manuscript.id,
      autosaveInput: input,
    })

    expect(review.comments[0].content).toEqual(responseToRevision)
  })
})
