process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  makeDecisionToReturnAsEiCUseCase,
} = require('../../src/use-cases/makeDecisionAsEiC')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_returned: 'returned manuscript with comments to',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  notifyHEWhenEICReturnsManuscript: jest.fn(),
}
const chance = new Chance()
describe('Make decision to return to HE as EiC', () => {
  let journal
  let manuscript
  let editorInChiefMember
  let mockedModels

  beforeEach(async () => {
    // eslint-disable-next-line prefer-destructuring
    journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    manuscript = fixtures.generateManuscript({
      journalId: journal.id,
      status: 'draft',
    })

    editorInChiefMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    mockedModels = models.build(fixtures)
  })

  it('creates a new review and comment', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.pendingApproval

    await makeDecisionToReturnAsEiCUseCase
      .initialize({ notificationService, models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(
      mockedModels.Manuscript.Statuses.reviewCompleted,
    )
  })
  it('should send email to handling editor', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.pendingApproval
    await makeDecisionToReturnAsEiCUseCase
      .initialize({ notificationService, models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(
      notificationService.notifyHEWhenEICReturnsManuscript,
    ).toHaveBeenCalled()
  })
  it('should throw error if the manuscript is in a wrong status', async () => {
    const statuses = mockedModels.Manuscript.Statuses
    const wrongStatuses = [
      statuses.draft,
      statuses.technicalChecks,
      statuses.submitted,
      statuses.heInvited,
      statuses.heAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
      statuses.revisionRequested,
      statuses.accepted,
      statuses.rejected,
      statuses.inQA,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
    ]
    await Promise.each(wrongStatuses, async wrongStatus => {
      const manuscript = await fixtures.generateManuscript({
        journalId: journal.id,
        status: wrongStatus,
      })
      dataService.createUserOnManuscript({
        manuscript,
        fixtures,
        input: { isSubmitting: true, isCorresponding: false },
        role: 'author',
      })
      try {
        await makeDecisionToReturnAsEiCUseCase
          .initialize({ notificationService, models: mockedModels })
          .execute({
            manuscriptId: manuscript.id,
            userId: editorInChiefMember.userId,
            content: chance.sentence(),
          })
        throw new Error(`Use-case should fail for status ${wrongStatus}`)
      } catch (err) {
        expect(err.message).toEqual(
          `Cannot return a manuscript in the current status.`,
        )
      }
    })
  })
})
