process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_rejected: 'decision is to Reject manuscript',
}
logEvent.objectType = { manuscript: 'manuscript' }

const {
  makeDecisionToRejectAsEiCUseCase,
} = require('../../src/use-cases/makeDecisionAsEiC')

const jobsService = { cancelReviewersJobs: jest.fn() }

const notificationService = {
  notifyReviewersWhenEICRejectsManuscript: jest.fn(),
  notifyAuthorsWhenEiCMakesDecisionToRejectBeforePeerReview: jest.fn(),
  notifyAuthorsWhenEiCMakesDecisionToRejectAfterPeerReview: jest.fn(),
  notifyHEWhenEICRejectsManuscript: jest.fn(),
}
const chance = new Chance()
describe('Make decision to reject as EiC', () => {
  let journal
  let manuscript
  let editorInChiefMember
  let mockedModels

  beforeEach(async () => {
    // eslint-disable-next-line prefer-destructuring
    journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    editorInChiefMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    manuscript = fixtures.generateManuscript({
      journalId: journal.id,
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    mockedModels = models.build(fixtures)
  })
  it('creates a new review and comment', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.submitted
    await makeDecisionToRejectAsEiCUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        jobsService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(mockedModels.Manuscript.Statuses.rejected)
  })
  it('should send email to reviewers', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.reviewCompleted
    dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: 'submitted',
      },
      role: 'reviewer',
    })
    await makeDecisionToRejectAsEiCUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        jobsService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(
      notificationService.notifyReviewersWhenEICRejectsManuscript,
    ).toHaveBeenCalled()
  })
  it('should send email to authors before peer review', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.submitted
    await makeDecisionToRejectAsEiCUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        jobsService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(
      notificationService.notifyAuthorsWhenEiCMakesDecisionToRejectBeforePeerReview,
    ).toHaveBeenCalled()
  })
  it('should send email to authors after peer review', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.reviewCompleted
    dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: 'submitted',
      },
      role: 'reviewer',
    })
    await makeDecisionToRejectAsEiCUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        jobsService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(
      notificationService.notifyAuthorsWhenEiCMakesDecisionToRejectAfterPeerReview,
    ).toHaveBeenCalled()
  })
  it('should send email to handling editor', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.reviewCompleted
    const handlingEditor = dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'handlingEditor',
    })
    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'reject',
      fixtures,
      teamMember: handlingEditor,
    })
    await makeDecisionToRejectAsEiCUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        jobsService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(
      notificationService.notifyHEWhenEICRejectsManuscript,
    ).toHaveBeenCalled()
  })
  it('should throw error if the manuscript is in a wrong status', async () => {
    const statuses = mockedModels.Manuscript.Statuses
    const wrongStatuses = [
      statuses.draft,
      statuses.rejected,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.inQA,
      statuses.accepted,
      statuses.published,
      statuses.olderVersion,
    ]
    await Promise.each(wrongStatuses, async wrongStatus => {
      const manuscript = fixtures.generateManuscript({
        journalId: journal.id,
        status: wrongStatus,
      })
      dataService.createUserOnManuscript({
        manuscript,
        fixtures,
        input: { isSubmitting: true, isCorresponding: false },
        role: 'author',
      })
      try {
        await makeDecisionToRejectAsEiCUseCase
          .initialize({
            notificationService,
            models: mockedModels,
            jobsService,
          })
          .execute({
            manuscriptId: manuscript.id,
            userId: editorInChiefMember.userId,
            content: chance.sentence(),
          })
        throw new Error(`Use-case should fail for status ${wrongStatus}`)
      } catch (err) {
        expect(err.message).toEqual(
          `Cannot reject a manuscript in the current status.`,
        )
      }
    })
  })
})
