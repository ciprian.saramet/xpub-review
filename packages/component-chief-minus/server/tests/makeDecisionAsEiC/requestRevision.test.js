process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  revision_requested: 'requested a revision',
}
logEvent.objectType = { manuscript: 'manuscript' }

const {
  requestRevisionAsEiCUseCase,
} = require('../../src/use-cases/makeDecisionAsEiC')

const notificationService = {
  notifySAWhenEiCRequestsRevision: jest.fn(),
}

const chance = new Chance()
describe('Request revision as EiC', () => {
  it('changes the status of a manuscript', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChiefMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const submissionId = chance.guid()
    const manuscript = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 1,
    })
    dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    const mockedModels = models.build(fixtures)

    await requestRevisionAsEiCUseCase
      .initialize({ notificationService, models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual('revisionRequested')

    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )
    expect(manuscripts).toHaveLength(2)
    expect(
      manuscripts.find(m => m.version === 2 && m.status === 'draft'),
    ).toBeTruthy()
  })
  it('throws an error when the manuscript has a handling editor', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChiefMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'handlingEditor',
      input: {
        status: 'accepted',
      },
    })
    const mockedModels = models.build(fixtures)

    try {
      const result = await requestRevisionAsEiCUseCase
        .initialize({ notificationService, models: mockedModels, logEvent })
        .execute({
          manuscriptId: manuscript.id,
          userId: editorInChiefMember.userId,
          content: chance.sentence(),
        })
      // if the test is ok, this should be reached
      expect(result).toBeDefined()
    } catch (e) {
      expect(e.message).toEqual(
        'Cannot make a request to revision after a Handling Editor has been assigned.',
      )
    }
  })
})
