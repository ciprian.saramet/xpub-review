process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { cancelHandlingEditorInvitationUseCase } = require('../../src/use-cases')

const notificationService = {
  notifyHEWhenInvitationDeclined: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_revoked: 'invitation_revoked',
}
logEvent.objectType = { user: 'user' }

describe('Cancel invitation', () => {
  it('cancels an invitation when the input is correct', async () => {
    const mockedModels = models.build(fixtures)
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({
      journalId: journal.id,
      status: mockedModels.Manuscript.Statuses.heInvited,
    })

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'handlingEditor',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    await cancelHandlingEditorInvitationUseCase
      .initialize({ notificationService, models: mockedModels, logEvent })
      .execute({ teamMemberId: handlingEditor.id })

    expect(
      notificationService.notifyHEWhenInvitationDeclined,
    ).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(
      mockedModels.Manuscript.Statuses.submitted,
    )
  })
})
