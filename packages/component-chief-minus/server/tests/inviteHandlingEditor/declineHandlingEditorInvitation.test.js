process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  declineHandlingEditorInvitationUseCase,
} = require('../../src/use-cases')

const notificationService = {
  notifyEiCAboutHEInvitationDecisionEmail: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_declined: 'invitation_declined',
}
logEvent.objectType = { manuscript: 'manuscript' }

describe('Decline invitation', () => {
  it('declines an invitation when the input is correct', async () => {
    const mockedModels = models.build(fixtures)

    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const handlingEditor = fixtures.generateUser({})
    const manuscript = fixtures.generateManuscript({
      journalId: journal.id,
      status: mockedModels.Manuscript.Statuses.heInvited,
    })
    const manuscriptTeam = fixtures.generateTeam({
      role: 'handlingEditor',
      manuscriptId: manuscript.id,
    })
    manuscript.teams.push(manuscriptTeam)
    manuscriptTeam.manuscript = manuscript

    const teamMember = fixtures.generateTeamMember({
      userId: handlingEditor.id,
      teamId: manuscriptTeam.id,
      status: 'pending',
    })

    teamMember.user = handlingEditor
    teamMember.team = manuscriptTeam
    manuscriptTeam.members.push(teamMember)
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    await declineHandlingEditorInvitationUseCase
      .initialize({ notificationService, models: mockedModels, logEvent })
      .execute({
        input: {
          teamMemberId: teamMember.id,
          reason: `I don't wanna do this.`,
        },
      })

    expect(teamMember.status).toEqual('declined')
    expect(
      notificationService.notifyEiCAboutHEInvitationDecisionEmail,
    ).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(
      mockedModels.Manuscript.Statuses.submitted,
    )
  })
})
