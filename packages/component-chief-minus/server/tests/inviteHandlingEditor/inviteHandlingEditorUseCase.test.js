process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')

const chance = new Chance()
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const useCases = require('../../src/use-cases/inviteHandlingEditor')

const { inviteHandlingEditorUseCase } = useCases

const invitationsService = {
  sendInvitationToHandlingEditor: jest.fn(),
}
const emailJobsService = {
  scheduleEmailsWhenHandlingEditorIsInvited: jest.fn(),
}
const removalJobsService = {
  scheduleRemovalJobForHandlingEditor: jest.fn(),
}

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_sent: 'invitation_sent',
}
logEvent.objectType = { user: 'user' }

describe('Invite handling editor', () => {
  it('adds a user in the handling editor team', async () => {
    const mockedModels = models.build(fixtures)

    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const handlingEditorUser = fixtures.generateUser({})
    const manuscript = fixtures.generateManuscript({ journalId: journal.id })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    await inviteHandlingEditorUseCase
      .initialize({
        invitationsService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
        useCases,
      })
      .execute({
        submissionId: manuscript.submissionId,
        userId: handlingEditorUser.id,
        reqUserId: editorInChief.id,
      })

    const heTeam = manuscript.teams.find(
      team => team.role === mockedModels.Team.Role.handlingEditor,
    )

    expect(heTeam.members).toHaveLength(1)
    expect(heTeam.members[0].userId).toEqual(handlingEditorUser.id)
    expect(heTeam.members[0].status).toEqual(
      mockedModels.TeamMember.Statuses.pending,
    )

    expect(
      invitationsService.sendInvitationToHandlingEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.scheduleEmailsWhenHandlingEditorIsInvited,
    ).toHaveBeenCalledTimes(1)
    expect(
      removalJobsService.scheduleRemovalJobForHandlingEditor,
    ).toHaveBeenCalledTimes(1)
  })

  it('creates a team if one does not already exist', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const handlingEditor = fixtures.generateUser({})
    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    const result = inviteHandlingEditorUseCase
      .initialize({
        invitationsService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
        useCases,
      })
      .execute({
        submissionId: manuscript.submissionId,
        userId: handlingEditor.id,
        reqUserId: editorInChief.id,
      })

    expect(result).resolves.not.toThrow()
    expect(
      invitationsService.sendInvitationToHandlingEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.scheduleEmailsWhenHandlingEditorIsInvited,
    ).toHaveBeenCalledTimes(1)
    expect(
      removalJobsService.scheduleRemovalJobForHandlingEditor,
    ).toHaveBeenCalledTimes(1)
  })

  it('return an error if the user is already invited', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const manuscript = fixtures.generateManuscript({ journalId: journal.id })

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    try {
      const result = await inviteHandlingEditorUseCase
        .initialize({
          invitationsService,
          models: mockedModels,
          logEvent,
          emailJobsService,
          removalJobsService,
          useCases,
        })
        .execute({
          submissionId: manuscript.submissionId,
          userId: handlingEditor.userId,
          reqUserId: editorInChief.id,
        })

      expect(result).not.toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual(`The handling editor is already invited.`)
    }
  })
  it('should return an error if there are no manuscripts found', async () => {
    const mockedModels = models.build(fixtures)
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const result = inviteHandlingEditorUseCase
      .initialize({
        invitationsService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
        useCases,
      })
      .execute({
        submissionId: chance.guid(),
        userId: chance.guid(),
        reqUserId: editorInChief.id,
      })

    return expect(result).rejects.toThrow('No manuscript has been found.')
  })
  it('return an error if the user is removed or declined on older versions', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 1,
    })

    const secondVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 2,
    })

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: chance.pickone(['removed', 'declined']) },
      role: 'handlingEditor',
    })

    await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    try {
      const result = await inviteHandlingEditorUseCase
        .initialize({
          invitationsService,
          models: mockedModels,
          logEvent,
          emailJobsService,
          removalJobsService,
          useCases,
        })
        .execute({
          submissionId,
          userId: handlingEditor.userId,
          reqUserId: editorInChief.id,
        })

      expect(result).not.toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual(`The handling editor can't be invited.`)
    }
  })
  it('adds an expired user to the second version', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })
    const editorInChief = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 1,
    })

    const secondVersion = fixtures.generateManuscript({
      journalId: journal.id,
      submissionId,
      version: 2,
    })

    await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: 'removed' },
      role: 'handlingEditor',
    })

    await dataService.createUserOnManuscript({
      manuscript: firstVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const mockedModels = models.build(fixtures)

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript: secondVersion,
      fixtures,
      input: { status: 'expired' },
      role: 'handlingEditor',
    })

    await inviteHandlingEditorUseCase
      .initialize({
        invitationsService,
        models: mockedModels,
        logEvent,
        emailJobsService,
        removalJobsService,
        useCases,
      })
      .execute({
        submissionId,
        userId: handlingEditor.userId,
        reqUserId: editorInChief.id,
      })

    const heTeam = secondVersion.teams.find(
      team => team.role === mockedModels.Team.Role.handlingEditor,
    )

    expect(heTeam.members).toHaveLength(1)
    expect(heTeam.members[0].status).toEqual('pending')
  })
})
