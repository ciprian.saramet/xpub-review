process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const Chance = require('chance')

const chance = new Chance()

const { removeHandlingEditorUseCase } = require('../../src/use-cases')

const jobsService = { cancelReviewersJobs: jest.fn() }

const notificationService = {
  notifyAuthorWhenHERemoved: jest.fn(),
  notifyInvitedHEWhenRemoved: jest.fn(),
  notifyReviewersWhenHERemoved: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  he_removed: 'he_removed',
}
logEvent.objectType = { user: 'user' }

describe('Revoke handling editor', () => {
  it('removes the handling editor from the manuscript', async () => {
    const mockedModels = models.build(fixtures)

    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorialAssistant',
    })

    await dataService.createUserOnJournal({
      journal,
      fixtures,
      role: 'admin',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({
      journalId: journal.id,
      status: mockedModels.Manuscript.Statuses.heAssigned,
    })

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    const review = fixtures.generateReview({
      teamMemberId: handlingEditor.id,
      manuscriptId: manuscript.id,
      recommendation: 'minor',
    })
    const comment = fixtures.generateComment({
      reviewId: review.id,
      type: 'public',
      content: chance.sentence(),
      files: [],
    })
    review.comments = [comment]

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    await removeHandlingEditorUseCase
      .initialize({
        notificationService,
        models: mockedModels,
        logEvent,
        jobsService,
      })
      .execute({ teamMemberId: handlingEditor.id })

    expect(notificationService.notifyAuthorWhenHERemoved).toHaveBeenCalledTimes(
      1,
    )
    expect(
      notificationService.notifyInvitedHEWhenRemoved,
    ).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyReviewersWhenHERemoved,
    ).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(
      mockedModels.Manuscript.Statuses.submitted,
    )
  })
})
