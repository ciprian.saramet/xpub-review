const { withAuthsomeMiddleware } = require('helper-service')
const { merge } = require('lodash')
const useCases = require('./use-cases')

const inviteHandlingEditorResolver = require('./use-cases/inviteHandlingEditor/resolver')
const inviteReviewersResolver = require('./use-cases/inviteReviewers/resolver')
const makeDecisionAsEicResolver = require('./use-cases/makeDecisionAsEiC/resolver')
const recommendAsHEResolver = require('./use-cases/recommendAsHE/resolver')
const submitReviewResolver = require('./use-cases/submitReview/resolver')
const submitRevisionResolver = require('./use-cases/submitRevision/resolver')

const mergedResolvers = merge(
  inviteHandlingEditorResolver,
  inviteReviewersResolver,
  makeDecisionAsEicResolver,
  recommendAsHEResolver,
  submitReviewResolver,
  submitRevisionResolver,
)

module.exports = withAuthsomeMiddleware(mergedResolvers, useCases)
