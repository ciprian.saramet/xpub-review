const config = require('config')

const journalName = config.get('journal.name')

const getEmailCopy = ({ emailType, titleText, expectedDate }) => {
  let upperContent, manuscriptText, subText, lowerContent, paragraph
  const hasLink = true
  const hasIntro = true
  const hasSignature = true
  let resend = false
  switch (emailType) {
    case 'reviewer-resend-invitation-first-reminder':
      resend = true
      upperContent = `On ${expectedDate} I invited you to review ${titleText}, submitted for possible publication in ${journalName}.<br/><br/>
        We'd be grateful if you could submit a decision on whether or not you will be able to review this manuscript using the link below.`
      lowerContent = `Thank you in advance for your help with the evaluation of this manuscript.<br/><br/>
        We look forward to hearing from you.`
      break
    case 'reviewer-resend-invitation-after-major-revision-reminder':
    case 'reviewer-resend-invitation-second-reminder':
    case 'reviewer-resend-invitation-third-reminder':
      resend = true
      upperContent = `We sent you a request to review ${titleText}; however we have not yet received your decision.
        We would appreciate it if you could visit the following link to let us know whether or not you will be able to review this manuscript:`
      lowerContent = `Please do not hesitate to contact me if you have any problems with the system.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    resend,
    hasLink,
    subText,
    hasIntro,
    paragraph,
    hasSignature,
    upperContent,
    lowerContent,
    manuscriptText,
  }
}

module.exports = {
  getEmailCopy,
}
