const getEmailCopy = ({ emailType, titleText, removalDay }) => {
  let paragraph
  let hasLink = true
  let hasIntro = true
  const hasSignature = true
  switch (emailType) {
    case 'reviewer-submit-report-first-reminder':
      hasIntro = true
      hasLink = true
      paragraph = `This is a reminder regarding ${titleText}. Because we aim to provide a fast peer review process, please use the link below to check the manuscript and submit your report.<br/><br/>
        Thank you for reviewing this manuscript.`
      break
    case 'reviewer-submit-report-second-reminder':
      hasLink = false
      paragraph = `Please note that your review report on ${titleText} has not yet been completed. Please inform us if you have any difficulties submitting it so that we can assist you.<br/><br/>
      In order to not delay the review process of this manuscript, please use the link below to submit your report as soon as possible.<br/><br/>
      Please do not hesitate to contact me if you encounter any problems with the system.<br/><br/>`
      break
    case 'reviewer-submit-report-third-reminder':
      hasIntro = true
      paragraph = `We are currently still awaiting your reviewer report on ${titleText}. We would appreciate if you could submit your reviewer report as soon as possible using the link below.<br/><br/>
      If you have any difficulties submitting your report, please let us know so we can assist you.<br/><br/>
      In order not to delay the authors further, we will have to remove you as a reviewer and invite someone else to review the manuscript if we haven’t received your report by ${removalDay}.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    hasLink,
    hasIntro,
    paragraph,
    hasSignature,
  }
}

module.exports = {
  getEmailCopy,
}
