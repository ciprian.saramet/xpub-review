const config = require('config')

const { name: journalName, staffEmail } = config.get('journal')

const daysList = config.get('reminders.reviewer.acceptInvitation.days')
const acceptedReviewerDaysList = config.get(
  'reminders.reviewer.submitReport.days',
)
const { getExpectedDate } = require('../../dateService/dateService')
const {
  getInvitationEmailProps,
} = require('../../invitationsService/reviewer/getInvitationEmailProps')
const {
  getNotificationEmailProps,
} = require('../../notificationService/reviewer/getNotificationEmailProps')
const reminders = require('./reminders')

const { get } = require('lodash')

const initialize = ({ logEvent, Email, Job }) => ({
  async scheduleEmailsWhenReviewerIsInvited({
    reviewer,
    manuscript,
    submittingAuthorName,
  }) {
    const { title, customId, id: manuscriptId } = manuscript
    const { created, id: invitationId, userId } = reviewer

    const expectedDate = getExpectedDate({
      timestamp: new Date(created).getTime(),
      daysExpected: 0,
    })
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailProps = getInvitationEmailProps({ manuscript, reviewer })
    emailProps.content.subject = `${customId}: Review reminder`

    const {
      scheduleFirstReviewerReminder,
      scheduleSecondReviewerReminder,
      scheduleThirdReviewerReminder,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduleFirstReviewerReminder({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      firstDate: daysList.first,
      teamMemberId: invitationId,
    })
    await scheduleSecondReviewerReminder({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      secondDate: daysList.second,
      teamMemberId: invitationId,
    })
    await scheduleThirdReviewerReminder({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      thirdDate: daysList.third,
      teamMemberId: invitationId,
    })
  },
  async scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision({
    reviewer,
    manuscript,
    submittingAuthorName,
  }) {
    const { title, customId, id: manuscriptId } = manuscript
    const { created, id: invitationId, userId } = reviewer

    const expectedDate = getExpectedDate({
      timestamp: new Date(created).getTime(),
      daysExpected: 0,
    })
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailProps = getInvitationEmailProps({ manuscript, reviewer })
    emailProps.content.subject = `${customId}: Review reminder`

    const {
      scheduledFirstReviewerReminderAfterMajorRevision,
      scheduledSecondReviewerReminderAfterMajorRevision,
      scheduledThirdReviewerReminderAfterMajorRevision,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduledFirstReviewerReminderAfterMajorRevision({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      firstDate: daysList.first,
      teamMemberId: invitationId,
    })
    await scheduledSecondReviewerReminderAfterMajorRevision({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      teamMemberId: invitationId,
      secondDate: daysList.second,
    })
    await scheduledThirdReviewerReminderAfterMajorRevision({
      userId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      thirdDate: daysList.third,
      teamMemberId: invitationId,
    })
  },
  async scheduleEmailsWhenReviewerAcceptsInvitation({
    reviewer,
    manuscript,
    submittingAuthorName,
    editorialAssistant,
  }) {
    const { title, customId, id: manuscriptId } = manuscript
    const { id: invitationId, userId } = reviewer
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const editorialAssistantEmail = get(editorialAssistant, 'alias.email')

    const removalDay = getExpectedDate({
      timestamp: reviewer.responded,
      daysExpected: 24,
    })

    const emailProps = getNotificationEmailProps({
      manuscript,
      toUser: reviewer,
      fromEmail: `${journalName} <${editorialAssistantEmail || staffEmail}>`,
      removalDay,
    })

    const {
      scheduledFirstAcceptedReviewerReminder,
      scheduledSecondAcceptedReviewerReminder,
      scheduledThirdAcceptedReviewerReminder,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduledFirstAcceptedReviewerReminder({
      userId,
      customId,
      titleText,
      emailProps,
      manuscriptId,
      firstDate: acceptedReviewerDaysList.first,
      teamMemberId: invitationId,
    })
    await scheduledSecondAcceptedReviewerReminder({
      userId,
      titleText,
      customId,
      emailProps,
      manuscriptId,
      teamMemberId: invitationId,
      secondDate: acceptedReviewerDaysList.second,
    })
    await scheduledThirdAcceptedReviewerReminder({
      userId,
      titleText,
      customId,
      emailProps,
      manuscriptId,
      thirdDate: acceptedReviewerDaysList.third,
      teamMemberId: invitationId,
      removalDay,
    })
  },
})

module.exports = {
  initialize,
}
