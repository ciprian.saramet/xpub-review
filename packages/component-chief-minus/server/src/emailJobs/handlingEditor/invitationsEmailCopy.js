const config = require('config')

const journalName = config.get('journal.name')

const getInvitationsEmailCopy = ({ emailType, titleText, expectedDate }) => {
  let upperContent, lowerContent
  const hasLink = true
  const hasIntro = true
  const hasSignature = true
  switch (emailType) {
    case 'handling-editor-resend-invitation-first-reminder':
      upperContent = `On ${expectedDate}, we sent you a request to handle ${titleText}, submitted for possible publication in ${journalName}.<br/><br/>
        We'd be grateful if you could submit a decision on whether or not you will be able to handle this manuscript using the link below:`
      lowerContent = `Thank you in advance for your help with the evaluation of this manuscript.<br/><br/>
        We look forward to hearing from you.`
      break
    case 'handling-editor-resend-invitation-second-reminder':
      upperContent = `We sent you a request to handle ${titleText}; however we have not yet received your decision. We would appreciate it if you could visit the following link to let us know whether or not you will be able to handle this manuscript:`
      lowerContent = `Thank you in advance for your help with the evaluation of this manuscript.<br/><br/>
        We look forward to hearing from you.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    hasLink,
    hasIntro,
    upperContent,
    lowerContent,
    hasSignature,
  }
}

module.exports = {
  getInvitationsEmailCopy,
}
