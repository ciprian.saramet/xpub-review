const moment = require('moment-business-days')
const config = require('config')

const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')
const businessDays = config.get('reminders.businessDays')

const { getInvitationsEmailCopy } = require('./invitationsEmailCopy')
const { getNotificationsEmailCopy } = require('./notificationsEmailCopy')

const initialize = ({ Job, Email, logEvent }) => {
  const jobHandler = async job => {
    try {
      const {
        userId,
        action,
        emailProps,
        manuscriptId,
        logActivity = true,
      } = job.data

      const email = new Email(emailProps)
      await email.sendEmail()
      if (logActivity) {
        logEvent({
          userId: null,
          manuscriptId,
          action,
          objectType: logEvent.objectType.user,
          objectId: userId,
        })
      }
      return job.done()
    } catch (e) {
      throw new Error(e)
    }
  }

  return {
    async scheduleFirstHandlingEditorReminder({
      userId,
      titleText,
      firstDate,
      emailProps,
      expectedDate,
      manuscriptId,
      invitationId,
    }) {
      const executionDate = businessDays
        ? moment()
            .businessAdd(firstDate)
            .toISOString()
        : moment()
            .add(firstDate, timeUnit)
            .toISOString()

      const { ...bodyProps } = getInvitationsEmailCopy({
        emailType: `handling-editor-resend-invitation-first-reminder`,
        titleText,
        expectedDate,
      })

      emailProps.bodyProps = bodyProps

      const params = {
        action: logEvent.actions.reminder_invitation_first,
        userId,
        timeUnit,
        manuscriptId,
        emailProps,
      }

      await Job.schedule({
        params,
        executionDate,
        jobHandler,
        teamMemberId: invitationId,
      })
    },
    async scheduleFirstHEInviteReviewerReminder({
      userId,
      firstDate,
      timeUnit,
      titleText,
      emailProps,
      manuscriptId,
      invitationId,
    }) {
      let executionDate
      if (process.env.NODE_ENV !== 'production') {
        executionDate = moment()
          .add(firstDate, timeUnit)
          .toISOString()
      } else {
        executionDate = moment()
          .businessAdd(firstDate)
          .toISOString()
      }

      const action = logEvent.actions.reminder_invite_reviewer_first

      const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
        emailType: 'he-reviewer-invitation-first-reminder',
        titleText,
      })
      emailProps.bodyProps = bodyProps
      emailProps.content.paragraph = paragraph

      const params = {
        action,
        userId,
        timeUnit,
        emailProps,
        manuscriptId,
        executionDate,
      }

      await Job.schedule({
        params,
        jobHandler,
        executionDate,
        teamMemberId: invitationId,
      })
    },
    async scheduleSecondHandlingEditorReminder({
      userId,
      titleText,
      secondDate,
      emailProps,
      expectedDate,
      manuscriptId,
      invitationId,
    }) {
      const executionDate = businessDays
        ? moment()
            .businessAdd(secondDate)
            .toISOString()
        : moment()
            .add(secondDate, timeUnit)
            .toISOString()

      const { ...bodyProps } = getInvitationsEmailCopy({
        emailType: `handling-editor-resend-invitation-second-reminder`,
        titleText,
        expectedDate,
      })

      emailProps.bodyProps = bodyProps

      const params = {
        action: logEvent.actions.reminder_invitation_second,
        userId,
        timeUnit,
        manuscriptId,
        executionDate,
        emailProps,
      }

      await Job.schedule({
        params,
        jobHandler,
        executionDate,
        teamMemberId: invitationId,
      })
    },
    async scheduleSecondHEInviteReviewerReminder({
      userId,
      timeUnit,
      titleText,
      emailProps,
      secondDate,
      manuscriptId,
      invitationId,
    }) {
      const executionDate = businessDays
        ? moment()
            .businessAdd(secondDate)
            .toISOString()
        : moment()
            .add(secondDate, timeUnit)
            .toISOString()

      const action = logEvent.actions.reminder_invite_reviewer_second

      const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
        emailType: 'he-reviewer-invitation-second-reminder',
        titleText,
      })
      emailProps.bodyProps = bodyProps
      emailProps.content.paragraph = paragraph
      const params = {
        action,
        userId,
        timeUnit,
        emailProps,
        manuscriptId,
        executionDate,
      }

      await Job.schedule({
        params,
        executionDate,
        jobHandler,
        teamMemberId: invitationId,
      })
    },
    async scheduleEAInviteReviewerReminder({
      userId,
      timeUnit,
      titleText,
      thirdDate,
      emailProps,
      manuscriptId,
      invitationId,
    }) {
      const executionDate = businessDays
        ? moment()
            .businessAdd(thirdDate)
            .toISOString()
        : moment()
            .add(thirdDate, timeUnit)
            .toISOString()

      const action = logEvent.actions.reminder_not_invited_enough_reviewers

      const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
        emailType: 'ea-he-not-invited-enough-reviewers',
        titleText,
      })
      emailProps.bodyProps = bodyProps
      emailProps.content.paragraph = paragraph

      const params = {
        action,
        userId,
        timeUnit,
        emailProps,
        manuscriptId,
        executionDate,
      }

      await Job.schedule({
        params,
        jobHandler,
        manuscriptId,
        executionDate,
        teamMemberId: invitationId,
      })
    },
    async scheduleHandlingEditorRemovalNotificationForHE({
      days,
      invitationId,
      titleText,
      emailProps,
      expectedDate,
      manuscriptId,
    }) {
      const executionDate = businessDays
        ? moment()
            .businessAdd(days)
            .toISOString()
        : moment()
            .add(days, timeUnit)
            .toISOString()

      const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
        emailType: `he-he-removed`,
        titleText,
        expectedDate,
      })

      emailProps.bodyProps = bodyProps
      emailProps.content.paragraph = paragraph

      const params = {
        invitationId,
        manuscriptId,
        emailProps,
        logActivity: false,
      }

      await Job.schedule({
        params,
        jobHandler,
        executionDate,
        teamMemberId: invitationId,
      })
    },
    async scheduleHandlingEditorRemovalNotificationForEiC({
      days,
      invitationId,
      titleText,
      emailProps,
      expectedDate,
      manuscriptId,
      targetUserName,
    }) {
      const executionDate = businessDays
        ? moment()
            .businessAdd(days)
            .toISOString()
        : moment()
            .add(days, timeUnit)
            .toISOString()

      const { paragraph, ...bodyProps } = getNotificationsEmailCopy({
        emailType: `eic-he-removed`,
        titleText,
        expectedDate,
        targetUserName,
      })

      emailProps.bodyProps = bodyProps
      emailProps.content.paragraph = paragraph

      const params = {
        invitationId,
        manuscriptId,
        emailProps,
        logActivity: false,
      }

      await Job.schedule({
        params,
        jobHandler,
        executionDate,
        teamMemberId: invitationId,
      })
    },
  }
}

module.exports = { initialize }
