const config = require('config')
const { get } = require('lodash')

const daysListAcceptInvitation = config.get(
  'reminders.handlingEditor.acceptInvitation.days',
)
const removalDayAcceptInvitation = config.get(
  'reminders.handlingEditor.acceptInvitation.remove',
)
const dayListInviteReviewers = config.get(
  'reminders.handlingEditor.inviteReviewers.days',
)
const timeUnitInviteReviewers = config.get(
  'reminders.handlingEditor.inviteReviewers.timeUnit',
)
const { name: journalName, staffEmail } = config.get('journal')

const { getExpectedDate } = require('../../dateService/dateService')
const {
  getInvitationEmailProps,
} = require('../../invitationsService/handlingEditor/getInvitationEmailProps')
const {
  getNotificationEmailProps,
} = require('../../notificationService/handlingEditor/getNotificationEmailProps')

const reminders = require('./reminders')

const initialize = ({ logEvent, Email, Job }) => ({
  async scheduleEmailsWhenHandlingEditorIsInvited({
    journal,
    manuscript,
    handlingEditor,
    submittingAuthor,
    editorInChief,
  }) {
    const { title, customId, id: manuscriptId } = manuscript
    const { created, id: invitationId } = handlingEditor
    const submittingAuthorName = submittingAuthor.getName()
    const editorialAssistant = journal.getEditorialAssistant()
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const expectedDate = getExpectedDate({
      timestamp: new Date(created).getTime(),
      daysExpected: 0,
    })
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailProps = getInvitationEmailProps({
      manuscript,
      handlingEditor,
      editorInChiefName: editorInChief.getName(),
      editorialAssistant,
      subject: `${customId}: Invitation to edit a manuscript reminder`,
    })

    const {
      scheduleFirstHandlingEditorReminder,
      scheduleSecondHandlingEditorReminder,
      scheduleHandlingEditorRemovalNotificationForHE,
      scheduleHandlingEditorRemovalNotificationForEiC,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduleFirstHandlingEditorReminder({
      invitationId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      userId: handlingEditor.userId,
      firstDate: daysListAcceptInvitation.first,
    })
    await scheduleSecondHandlingEditorReminder({
      invitationId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      secondDate: daysListAcceptInvitation.second,
      userId: handlingEditor.userId,
    })

    const notificationTitleText = `the manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`

    await scheduleHandlingEditorRemovalNotificationForHE({
      titleText: notificationTitleText,
      emailProps: getNotificationEmailProps({
        manuscript,
        handlingEditor,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
        subject: `${customId}: The editor in chief removed you from ${title}`,
        editorInChief: editorInChief.getName(),
        toUser: handlingEditor,
        editorialAssistant,
      }),
      manuscriptId,
      invitationId,
      expectedDate,
      days: removalDayAcceptInvitation,
    })

    await scheduleHandlingEditorRemovalNotificationForEiC({
      titleText: notificationTitleText,
      targetUserName: handlingEditor.getName(),
      emailProps: getNotificationEmailProps({
        manuscript,
        handlingEditor,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
        subject: `${customId}: Editor Declined`,
        editorInChiefName: editorInChief.getName(),
        toUser: editorInChief,
        editorialAssistant,
      }),
      manuscriptId,
      invitationId,
      expectedDate,
      days: removalDayAcceptInvitation,
    })
  },
  async sendHERemindersToInviteReviewers({
    user,
    admin,
    manuscript,
    editorialAssistant,
    submittingAuthorName,
  }) {
    const { title, id: manuscriptId, customId, articleType } = manuscript
    const titleText = `${articleType} titled "${title}" by ${submittingAuthorName}`

    const editorialAssistantOrAdmin = editorialAssistant || admin
    const editorialAssistantEmail =
      get(editorialAssistantOrAdmin, 'alias.email') || staffEmail

    const emailProps = getNotificationEmailProps({
      toUser: user,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      subject: `${customId}: Inviting Reviewers Reminder`,
      signatureName: editorialAssistantOrAdmin.getName(),
      manuscript,
    })

    const {
      scheduleFirstHEInviteReviewerReminder,
      scheduleSecondHEInviteReviewerReminder,
      scheduleEAInviteReviewerReminder,
    } = reminders.initialize({
      logEvent,
      Email,
      Job,
    })

    await scheduleFirstHEInviteReviewerReminder({
      userId: user.userId,
      timeUnit: timeUnitInviteReviewers,
      titleText,
      emailProps,
      manuscriptId,
      firstDate: dayListInviteReviewers.first,
      invitationId: user.id,
    })

    await scheduleSecondHEInviteReviewerReminder({
      userId: user.userId,
      timeUnit: timeUnitInviteReviewers,
      titleText,
      emailProps,
      manuscriptId,
      secondDate: dayListInviteReviewers.second,
      invitationId: user.id,
    })

    await scheduleEAInviteReviewerReminder({
      userId: editorialAssistantOrAdmin.userId,
      timeUnit: timeUnitInviteReviewers,
      titleText,
      manuscriptId,
      invitationId: editorialAssistantOrAdmin.id,
      thirdDate: dayListInviteReviewers.third,
      emailProps: getNotificationEmailProps({
        toUser: editorialAssistantOrAdmin,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
        subject: `${customId}: Reviewers needed`,
        signatureName: editorialAssistantOrAdmin.getName(),
        manuscript,
      }),
    })
  },
})

module.exports = {
  initialize,
}
