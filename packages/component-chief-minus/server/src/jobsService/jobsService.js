const { Promise } = require('bluebird')

module.exports.initialize = ({ models: { TeamMember, Job } }) => ({
  async deletePendingReviewersFromQueue({ reviewerTeam }) {
    const pendingReviewers = reviewerTeam.members.filter(
      m => m.status === TeamMember.Statuses.pending,
    )

    return Promise.each(pendingReviewers, async reviewer =>
      Promise.each(reviewer.jobs, async job => {
        await Job.cancel(job.id)
        return job.delete()
      }),
    )
  },
  async cancelReviewersJobs({ reviewers }) {
    await Promise.each(reviewers, async reviewer => {
      try {
        if (reviewer.jobs) {
          await Promise.each(reviewer.jobs, async job => {
            await Job.cancel(job.id)
            return job.delete()
          })
        }
      } catch (e) {
        throw new Error(e)
      }
    })
  },
  async cancelAcceptedHandlingEditorJobs({ handlingEditor }) {
    try {
      if (handlingEditor.jobs) {
        await Promise.each(handlingEditor.jobs, async job => {
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      throw new Error(e)
    }
  },
  async cancelEditorialAssistantOrAdminJobs({
    editorialAssistantOrAdmin,
    manuscript,
  }) {
    try {
      if (editorialAssistantOrAdmin.jobs) {
        await Promise.each(editorialAssistantOrAdmin.jobs, async job => {
          if (job.manuscriptId !== manuscript.id) {
            return
          }
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      throw new Error(e)
    }
  },
})
