const config = require('config')

const { getEmailCopy } = require('./emailCopy')
const { getExpectedDate } = require('../../dateService/dateService')

const { name: journalName } = config.get('journal')
const {
  getNotificationEmailProps: getReviewerNotificationEmailProps,
} = require('./getNotificationEmailProps')
const { getEANameAndEmail } = require('../handlingEditor/notifications')

const initialize = ({ Email }) => ({
  async notifyHEAboutReviewerInvitationDecision({
    manuscript,
    user,
    isAccepted = true,
    editorInChief,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { title } = manuscript

    const titleText = `the manuscript titled "${title}" by ${submittingAuthor.getName()}`

    const handlingEditor = manuscript.getHandlingEditor()

    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })

    const emailType = isAccepted ? 'reviewer-accepted' : 'reviewer-declined'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      journalName,
      expectedDate: getExpectedDate({
        timestamp: Date.now(),
        daysExpected: 14,
      }),
      targetUserName: user.alias.surname,
    })

    const emailProps = getReviewerNotificationEmailProps({
      manuscript,
      toUser: handlingEditor,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: isAccepted
        ? `${manuscript.customId}: A reviewer has agreed`
        : `${manuscript.customId}: A reviewer has declined`,
      paragraph,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyReviewerOnCancelOrAcceptInvitation({
    user,
    manuscript,
    submittingAuthor,
    isCanceled = false,
    editorialAssistant,
  }) {
    const { title } = manuscript

    const titleText = `the manuscript titled "${title}" by ${submittingAuthor.getName()}`

    const handlingEditor = manuscript.getHandlingEditor()

    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: handlingEditor.getName(),
    })

    const emailType = isCanceled
      ? 'reviewer-cancel-invitation'
      : 'reviewer-thank-you'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      journalName,
      editorialAssistantEmail,
      expectedDate: getExpectedDate({
        timestamp: new Date(),
        daysExpected: 14,
      }),
    })

    const emailProps = getReviewerNotificationEmailProps({
      manuscript,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: user,
      subject: isCanceled
        ? `${manuscript.customId}: Review no longer required`
        : `${manuscript.customId}: Thank you for agreeing to review`,
      paragraph,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
