const config = require('config')
const { get } = require('lodash')

const urlService = require('../../urlService/urlService')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const { name: journalName } = config.get('journal')

module.exports = {
  getNotificationEmailProps({
    toUser,
    subject,
    manuscript,
    signatureName,
    paragraph,
    fromEmail,
  }) {
    return {
      type: 'user',
      templateType: 'notification',
      fromEmail,
      toUser: {
        email: get(toUser, 'alias.email', ''),
        name: get(toUser, 'alias.surname', ''),
      },
      content: {
        subject,
        signatureJournal: journalName,
        signatureName,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),

        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: toUser.userId,
            token: get(toUser, 'user.unsubscribeToken'),
          },
        }),
      },
    }
  },
}
