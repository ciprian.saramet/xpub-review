const config = require('config')
const { get } = require('lodash')
const { Promise } = require('bluebird')

const { getEmailCopy } = require('./emailCopy')

const { name: journalName, staffEmail } = config.get('journal')
const {
  getNotificationEmailProps: getHENotificationEmailProps,
} = require('./getNotificationEmailProps')

const getEANameAndEmail = ({ editorialAssistant, fallBackName }) => {
  const editorialAssistantName = editorialAssistant
    ? editorialAssistant.getName()
    : fallBackName
  const editorialAssistantEmail =
    get(editorialAssistant, 'alias.email') || staffEmail
  return {
    editorialAssistantName,
    editorialAssistantEmail,
  }
}

const initialize = ({ Email }) => ({
  async notifyHEWhenInvitationDeclined({
    user,
    manuscript,
    editorInChief,
    submittingAuthor,
    editorialAssistant,
  }) {
    const submittingAuthorName = submittingAuthor.getName()

    const titleText = `the manuscript titled "${get(
      manuscript,
      'title',
      '',
    )}" by ${submittingAuthorName}`

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      journalName,
      targetUserName: editorInChief.getName(),
      emailType: 'he-revoked',
    })
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({ editorialAssistant, fallBackName: journalName })

    const emailProps = getHENotificationEmailProps({
      paragraph,
      manuscript,
      subject: `${manuscript.customId}: Editor invitation cancelled`,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: user,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyAuthorWhenHERemoved({
    manuscript,
    submittingAuthor,
    editorInChief,
    editorialAssistant,
  }) {
    const titleText = get(manuscript, 'title', '')
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      journalName,
      emailType: 'author-he-removed',
      editorialAssistantEmail,
    })

    const emailProps = getHENotificationEmailProps({
      paragraph,
      manuscript,
      subject: `${get(
        manuscript,
        'customId',
        '',
      )}:  Your manuscript's editor was changed`,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: submittingAuthor,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyInvitedHEWhenRemoved({
    user,
    manuscript,
    editorialAssistant,
    editorInChief,
  }) {
    const titleText = manuscript.title
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      journalName,
      emailType: 'he-he-removed',
      editorialAssistantEmail,
    })

    const emailProps = getHENotificationEmailProps({
      paragraph,
      toUser: user,
      manuscript,
      subject: `${get(
        manuscript,
        'customId',
        '',
      )}: The editor in chief removed you from ${titleText}`,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyReviewersWhenHERemoved({
    reviewers,
    manuscript,
    editorInChief,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: title,
      journalName,
      emailType: 'reviewer-he-removed',
    })
    const filteredReviewers = reviewers.filter(r => r.status !== 'declined')
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })

    await Promise.each(filteredReviewers, async reviewer => {
      const emailProps = getHENotificationEmailProps({
        paragraph,
        manuscript,
        subject: `${customId}: The handling editor of a manuscript that you were reviewing was changed`,
        toUser: reviewer,
        fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
        signatureName: editorialAssistantName,
      })
      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },

  async notifyEiCAboutHEInvitationDecisionEmail({
    user,
    isAccepted = true,
    manuscript,
    submittingAuthor,
    reason,
    editorInChief,
    editorialAssistant,
  }) {
    const { title, customId } = manuscript
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: journalName,
    })
    const submittingAuthorName = submittingAuthor.getName()
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailType = isAccepted ? 'he-accepted' : 'he-declined'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      journalName,
      comments: reason ? `Reason: "${reason}"` : '',
      targetUserName: `${get(user, 'alias.surname', '')}`,
    })

    const emailProps = getHENotificationEmailProps({
      manuscript,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: editorInChief,
      subject: isAccepted
        ? `${customId}: Editor invitation accepted`
        : `${customId}: Editor invitation declined`,
      paragraph,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyHEAfterAcceptedInvitation({ manuscript, user }) {
    const emailType = 'he-after-accepted-invitation'
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      journalName,
    })
    const emailProps = getHENotificationEmailProps({
      manuscript,
      paragraph,
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: user,
      subject: `${get(manuscript, 'customId', '')}:  Inviting Reviewers`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyEiCWhenHEMakesRecommendation({
    review,
    editorInChief,
    manuscript,
    commentForEiC,
    handlingEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    let emailType, subject
    switch (review.recommendation) {
      case 'minor':
      case 'major':
        emailType = 'eic-request-revision-from-he'
        subject = `${manuscript.customId}: Revision requested`
        break
      case 'reject':
        emailType = 'eic-recommend-to-reject-from-he'
        subject = `${manuscript.customId}: Recommendation to reject`
        break
      case 'publish':
        emailType = 'eic-recommend-to-publish-from-he'
        subject = `${manuscript.customId}: Recommendation to publish`
        break
      default:
        throw new Error(`undefined recommendation: ${review.recommendation} `)
    }

    const comments = commentForEiC
      ? `The editor provided the following comments: "${commentForEiC}"`
      : ''
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: journalName,
    })

    const { paragraph, ...bodyProps } = getEmailCopy({
      comments,
      journalName,
      emailType,
      titleText: `the manuscript titled "${
        manuscript.title
      }" by ${submittingAuthor.getName()} `,
      targetUserName: handlingEditor.getLastName(),
    })

    const emailProps = getHENotificationEmailProps({
      manuscript,
      toUser: editorInChief,
      paragraph,
      subject,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyReviewersWhenHEMakesRecommendation({
    submittingAuthor,
    manuscript,
    reviewers,
    editorialAssistant,
    handlingEditor,
  }) {
    const titleText = `the manuscript titled "${
      manuscript.title
    }" by ${submittingAuthor.getName()}`
    const acceptedReviewers = reviewers.filter(
      reviewer => reviewer.status === 'accepted',
    )
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: handlingEditor.getName(),
    })

    const acceptedReviewersEmailBody = getEmailCopy({
      emailType: 'accepted-reviewers-after-recommendation',
      titleText,
      journalName,
    })

    const pendingReviewers = reviewers.filter(
      reviewer => reviewer.status === 'pending',
    )

    const pendingReviewersEmailBody = getEmailCopy({
      emailType: 'pending-reviewers-after-recommendation',
      titleText,
      journalName,
      editorialAssistantEmail,
    })

    const buildSendEmailFunction = emailBodyProps => async reviewer => {
      const { paragraph, ...bodyProps } = emailBodyProps
      const emailProps = getHENotificationEmailProps({
        manuscript,
        toUser: reviewer,
        fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
        subject: `${manuscript.customId}: Review no longer required`,
        paragraph,
        signatureName: editorialAssistantName,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    }

    return Promise.all([
      ...acceptedReviewers.map(
        buildSendEmailFunction(acceptedReviewersEmailBody),
      ),
      ...pendingReviewers.map(
        buildSendEmailFunction(pendingReviewersEmailBody),
      ),
    ])
  },

  async notifySAWhenHERequestsRevision({
    manuscript,
    commentForAuthor,
    submittingAuthor,
    editorialAssistant,
    handlingEditor,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'author-request-to-revision',
      titleText: `your submission "${manuscript.title}" to ${journalName}`,
      comments: commentForAuthor,
      journalName,
    })
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: handlingEditor.getName(),
    })

    const emailProps = getHENotificationEmailProps({
      manuscript,
      toUser: submittingAuthor,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      paragraph,
      subject: `${manuscript.customId}: Revision requested`,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyHeWhenRevisionSubmitted({
    draft,
    submittingAuthor,
    handlingEditor,
    editorialAssistant,
  }) {
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: journalName,
    })
    const titleText = get(draft, 'title', '')
    const targetUserName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      journalName,
      emailType: 'revision-submitted',
      targetUserName,
    })

    const emailProps = getHENotificationEmailProps({
      manuscript: draft,
      subject: `${draft.customId}: Revision submitted`,
      toUser: handlingEditor,
      paragraph,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyHEWhenReviewerSubmitsReview({
    reviewer,
    manuscript,
    handlingEditor,
    submittingAuthor,
    editorialAssistant,
    editorInChief,
  }) {
    const submittingAuthorName = submittingAuthor.getName()

    const titleText = `the manuscript titled "${get(
      manuscript,
      'title',
      '',
    )}" by ${submittingAuthorName}`
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      journalName,
      targetUserName: `${get(reviewer, 'alias.surname', '')}`,
      emailType: 'reviewer-submitted-review',
    })

    const emailProps = getHENotificationEmailProps({
      manuscript,
      toUser: handlingEditor,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: `${manuscript.customId}: A review has been submitted`,
      paragraph,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
  getEANameAndEmail,
}
