const getEmailCopy = ({
  comments,
  emailType,
  titleText,
  authorName,
  targetUserName,
  manuscriptTitle,
  journalName,
  staffEmail,
  editorialAssistantEmail,
  baseUrl,
}) => {
  let upperContent, manuscriptText, subText, lowerContent, paragraph
  let hasLink = true
  let hasIntro = true
  let hasSignature = true
  switch (emailType) {
    case 'he-accepted':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} agreed to serve as the Handling Editor on ${titleText}.
        Please click on the link below to access the manuscript.`
      break
    case 'he-declined':
      paragraph = `Dr. ${targetUserName} has declined to serve as the Handling Editor on ${titleText}.<br/><br/>
        ${comments}<br/><br/>
        To invite another Handling Editor, please click the link below.`
      hasIntro = false
      hasSignature = false
      break
    case 'he-revoked':
      hasIntro = false
      hasLink = false
      hasSignature = false
      paragraph = `${targetUserName} has removed you from the role of Handling Editor for ${titleText}.<br/><br/>
        The manuscript will no longer appear in your dashboard. Please contact ${staffEmail} if you have any questions about this change.`
      break
    case 'author-he-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `We had to replace the handling editor of your manuscript ${titleText}. We apologise for any inconvenience, but it was necessary in order to move your manuscript forward.<br/><br/>
        If you have questions please email them to ${editorialAssistantEmail}.<br/><br/>
        Thank you for your submission to ${journalName}.`
      break
    case 'he-he-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `The editor in chief removed you from the manuscript "${titleText}".<br/><br/>
        If you have any questions regarding this action, please let us know at ${editorialAssistantEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
      break
    case 'reviewer-he-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `We had to replace the handling editor of the manuscript "${titleText}". We apologise for any inconvenience this may cause.<br/><br/>
        If you have started the review process please email the content to ${editorialAssistantEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
      break
    case 'author-request-to-revision':
      paragraph = `In order for ${titleText} to proceed to the review process, there needs to be a revision. <br/><br/>
        ${comments}<br/><br/>
        For more information about what is required, please click the link below.<br/><br/>`
      break
    case 'eic-recommend-to-publish-from-he':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} has recommended accepting ${titleText} for publication.<br/>
        ${comments}<br/>
        To review this decision, please visit the manuscript details page.`
      break
    case 'eic-recommend-to-reject-from-he':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} has recommended rejecting ${titleText}.<br/><br/>
        ${comments}<br/><br/>
        To review this decision, please visit the manuscript details page.`
      break
    case 'eic-request-revision-from-he':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} has asked the authors to submit a revised version of ${titleText}.<br/><br/>
        No action is required at this time. To see the requested changes, please visit the manuscript details page.`
      break
    case 'revision-submitted':
      hasIntro = false
      hasSignature = false
      paragraph = `The authors of the manuscript titled "${titleText}" by ${targetUserName} have submitted a revised version. <br/><br/>
      To review this new submission and proceed with the review process, please visit the manuscript details page.`
      break
    case 'reviewer-submitted-review':
      paragraph = `We are pleased to inform you that Dr. ${targetUserName} has submitted a review for ${titleText}.<br/><br/>
      To see the full report, please visit the manuscript details page.`
      break
    case 'accepted-reviewers-after-recommendation':
      hasLink = false
      paragraph = `I appreciate any time you may have spent reviewing ${titleText}. However, I am prepared to make an editorial decision and your review is no longer required at this time. I apologize for any inconvenience. <br/><br/>
        If you have comments on this manuscript you believe I should see, please email them to ${staffEmail} as soon as possible. <br/><br/>
        Thank you for your interest and I hope you will consider reviewing for ${journalName} again.`
      break
    case 'pending-reviewers-after-recommendation':
      hasLink = false
      paragraph = `I appreciate any time you may have spent reviewing ${titleText}. However, I am prepared to make an editorial decision and your review is no longer required at this time. I apologize for any inconvenience. <br/><br/>
        If you have comments on this manuscript you believe I should see, please email them to ${editorialAssistantEmail} as soon as possible. <br/><br/>
        Thank you for your interest and I hope you will consider reviewing for ${journalName} again.`
      break
    case 'he-after-accepted-invitation':
      paragraph = `Thank you for agreeing to organize the review process for this manuscript. Please check the manuscript to see if it is of reasonable quality and novelty. If so, we ask that you invite five external reviewers and make an editorial recommendation based on their reports. If not, you may reject the manuscript right away without sending it out for review. <div>&nbsp;</div>
    You can view the submitted manuscript and take any of the above actions using the link below.<div>&nbsp;</div>
    Once you have assigned reviewers, they will be automatically contacted by the Manuscript Tracking System, and you will be notified once these reviewers agree or decline to review the manuscript. As we aim to provide a fast review process, please invite reviewers for this manuscript within one week.<div>&nbsp;</div>
If a potential conflict of interest exists between yourself and either the authors or the subject of the manuscript, please decline to handle the manuscript. If a conflict becomes apparent during the review process, please let us know at the earliest possible opportunity. For more information about our conflicts of interest policies, please see:
<a style="color:#007e92; text-decoration: none;" href="https://www.hindawi.com/ethics/#coi">https://www.hindawi.com/ethics/#coi</a>.<div>&nbsp;</div>
Thank you very much for your contribution to the journal.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    hasLink,
    subText,
    hasIntro,
    paragraph,
    upperContent,
    lowerContent,
    hasSignature,
    manuscriptText,
  }
}

module.exports = {
  getEmailCopy,
}
