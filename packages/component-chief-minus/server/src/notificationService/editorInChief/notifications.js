const config = require('config')
const { chain, get } = require('lodash')

const { getEmailCopy } = require('./emailCopy')

const { name: journalName, staffEmail } = config.get('journal')
const {
  getNotificationEmailProps: getEiCNotificationEmailProps,
} = require('./getNotificationEmailProps')
const { getEANameAndEmail } = require('../handlingEditor/notifications')

const initialize = ({ Email }) => ({
  async notifySAWhenEiCRequestsRevision({
    review,
    manuscript,
    commentTypes,
    editorInChief,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { title, customId } = manuscript
    const submittingAuthorName = submittingAuthor.getName()
    const {
      editorialAssistantName,
      editorialAssistantEmail,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })
    const authorNoteContent = chain(review.comments)
      .find(comm => comm.type === commentTypes.public)
      .get('content')
      .value()

    const authorNoteText = authorNoteContent
      ? `Reason & Details: "${authorNoteContent}"`
      : ''

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'author-request-to-revision-from-eic',
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      comments: authorNoteText,
      journalName,
    })

    const emailProps = getEiCNotificationEmailProps({
      manuscript,
      toUser: submittingAuthor,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: `${customId}: Revision requested`,
      paragraph,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyEiCWhenRevisionSubmitted({
    draft,
    editorInChief,
    submittingAuthor,
    editorialAssistant,
  }) {
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: get(draft, 'title', ''),
      emailType: 'revision-submitted',
      targetUserName: submittingAuthor.getName(),
      journalName,
    })
    const emailProps = getEiCNotificationEmailProps({
      manuscript: draft,
      toUser: editorInChief,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      subject: `${draft.customId}: Revision submitted`,
      paragraph,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyEQAWhenEiCMakesDecisionToPublish({
    editorInChief,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()
    const {
      editorialAssistantEmail,
      editorialAssistantName,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'EQA-manuscript-accepted-by-eic',
      titleText: `The manuscript titled "${title}" by ${submittingAuthorName}`,
      eicName: editorInChief.getName(),
    })
    const emailProps = getEiCNotificationEmailProps({
      manuscript,
      toUser: editorialAssistant,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: `${customId}: Manuscript decision finalised`,
      paragraph,
      signatureName: editorInChief.getName(),
    })
    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyAuthorsWhenEiCMakesDecisionToRejectBeforePeerReview({
    authors,
    comment: { content },
    manuscript,
    editorialAssistant,
    editorInChief,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'authors-manuscript-rejected-before-peer-review',
      comments: content,
      journalName,
    })
    const {
      editorialAssistantEmail,
      editorialAssistantName,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })
    authors.forEach(async author => {
      const emailProps = getEiCNotificationEmailProps({
        manuscript,
        toUser: author,
        fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
        subject: `${manuscript.customId}: Manuscript rejected`,
        paragraph,
        signatureName: editorialAssistantName,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },

  async notifyAuthorsWhenEiCMakesDecisionToRejectAfterPeerReview({
    authors,
    comment: { content },
    editorInChief,
    manuscript,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: title,
      emailType: 'authors-manuscript-rejected-after-peer-review',
      comments: content,
      journalName,
    })
    const {
      editorialAssistantEmail,
      editorialAssistantName,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })

    authors.forEach(async author => {
      const emailProps = getEiCNotificationEmailProps({
        manuscript,
        toUser: author,
        fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
        subject: `${customId}: Manuscript rejected`,
        paragraph,
        signatureName: editorialAssistantName,
      })
      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },

  async notifyHEWhenEICRejectsManuscript({
    manuscript,
    editorInChief,
    handlingEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()
    const {
      editorialAssistantEmail,
      editorialAssistantName,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })
    const { paragraph, ...bodyProps } = getEmailCopy({
      targetUserName: editorInChief.getName(),
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'he-manuscript-rejected-after-he-recommendation',
      journalName,
    })
    const emailProps = getEiCNotificationEmailProps({
      manuscript,
      toUser: handlingEditor,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: `${customId}: Editorial decision confirmed`,
      paragraph,
      signatureName: editorialAssistantName,
    })
    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },

  async notifyReviewersWhenEICRejectsManuscript({
    reviewers,
    editorInChief,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()
    const {
      editorialAssistantEmail,
      editorialAssistantName,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'reviewer-manuscript-rejected-after-eic-decision',
      editorialAssistantEmail,
      journalName,
    })

    reviewers.forEach(async reviewer => {
      const emailProps = getEiCNotificationEmailProps({
        manuscript,
        toUser: reviewer,
        fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
        subject: `${customId}: A manuscript you reviewed has been rejected.`,
        paragraph,
        signatureName: editorialAssistantName,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },

  async notifyHEWhenEICReturnsManuscript({
    submittingAuthor,
    handlingEditor,
    editorInChief,
    manuscript,
    comment: { content },
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()
    const {
      editorialAssistantEmail,
      editorialAssistantName,
    } = getEANameAndEmail({
      editorialAssistant,
      fallBackName: editorInChief.getName(),
    })

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'he-manuscript-returned-with-comments',
      comments: content,
      targetUserName: editorInChief.getName(),
      journalName,
    })
    const emailProps = getEiCNotificationEmailProps({
      manuscript,
      toUser: handlingEditor,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      subject: `${customId}: Editorial decision returned with comments`,
      paragraph,
      signatureName: editorialAssistantName,
    })
    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
