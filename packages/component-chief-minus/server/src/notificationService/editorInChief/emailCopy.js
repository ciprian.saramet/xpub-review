const getEmailCopy = ({
  eicName,
  comments,
  titleText,
  emailType,
  journalName,
  targetUserName,
  editorialAssistantEmail,
}) => {
  let paragraph
  let hasLink = true
  let hasIntro = true
  let hasSignature = true
  switch (emailType) {
    case 'author-request-to-revision-from-eic':
      paragraph = `In order for ${titleText} to proceed to the review process, there needs to be a revision. <br/><br/>
        ${comments}<br/><br/>
        For more information about what is required, please click the link below.<br/><br/>`
      break
    case 'revision-submitted':
      paragraph = `The authors of the manuscript titled "${titleText}" by ${targetUserName} have submitted a revised version. <br/><br/>
      To review this new submission and proceed with the review process, please visit the manuscript details page.`
      break
    case 'EQA-manuscript-accepted-by-eic':
      hasSignature = false
      paragraph = `${titleText} has been accepted for publication by ${eicName}.<br/><br/>
      Please complete QA screening so that manuscript can be sent to production.<br/><br/>
      To review this decision, please visit the manuscript details page <br/><br/>`
      break
    case 'authors-manuscript-rejected-before-peer-review':
      hasLink = false
      paragraph = `I regret to inform you that your manuscript has been rejected for publication in ${journalName} for the following reason:<br/><br/>
        ${comments}<br/><br/>
        Thank you for your submission, and please do consider submitting again in the future.`
      break
    case 'authors-manuscript-rejected-after-peer-review':
      hasLink = false
      paragraph = `The peer review of your manuscript titled "${titleText}" has now been completed.<br/><br/>
      Please find our editorial comments below.<br/><br/>
      ${comments}<br/><br/>
      Thank you for your submission to ${journalName}.<br/><br/>`
      break
    case 'he-manuscript-rejected-after-he-recommendation':
      hasIntro = false
      paragraph = `${targetUserName} has confirmed your decision to reject ${titleText}.<br/><br/>
      No further action is required at this time. To review this decision, please visit the manuscript details page.<br/><br/>
      Thank you for handling this manuscript on behalf of ${journalName}.`
      break
    case 'reviewer-manuscript-rejected-after-eic-decision':
      paragraph = `Thank you for your review of ${titleText} for ${journalName}. After taking into account the reviews and the recommendation of the Handling Editor, I can confirm this article has now been rejected. <br/><br/>
      No further action is required at this time. To see more details about this decision please view the manuscript details page. <br/><br/>
      If you have any questions about this decision, please email them to ${editorialAssistantEmail} as soon as possible. Thank you for reviewing for ${journalName}.<br/><br/>`
      break
    case 'he-manuscript-returned-with-comments':
      hasIntro = false
      hasSignature = false
      paragraph = `${targetUserName} has responded with comments regarding your editorial recommendation on ${titleText}.<br/><br/>
      ${comments}<br/><br/>
      Please review these comments and take action on the manuscript details page. <br/><br/>`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }
  return { paragraph, hasLink, hasIntro, hasSignature }
}

module.exports = {
  getEmailCopy,
}
