const uuid = require('uuid')
const config = require('config')

const removalDays = config.get('reminders.reviewer.acceptInvitation.remove')
const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')

const initialize = ({
  invitationsService,
  models: {
    Team,
    TeamMember,
    Manuscript,
    Identity,
    User,
    ReviewerSuggestion,
    Journal,
    Job,
  },
  logEvent,
  emailJobsService,
  removalJobsService,
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      'teams.[members.[user.[identities], jobs]]',
    )
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }
    const submittingAuthor = manuscript.getSubmittingAuthor()

    const userIdentity = await Identity.findOneByEmail(
      input.email,
      'user.[identities]',
    )
    let user = userIdentity ? userIdentity.user : undefined
    let identity

    if (!user) {
      user = new User({
        defaultIdentity: 'local',
        isActive: true,
        isSubscribedToEmails: true,
        confirmationToken: uuid.v4(),
        unsubscribeToken: uuid.v4(),
        passwordResetToken: uuid.v4(),
        agreeTc: true,
      })

      identity = new Identity({
        type: 'local',
        isConfirmed: false,
        passwordHash: null,
        givenNames: input.givenNames,
        surname: input.surname,
        email: input.email,
        aff: input.aff || '',
        userId: user.id,
        country: input.country || '',
      })
      await user.assignIdentity(identity)
      await user.saveRecursively()
    }

    let manuscriptReviewersTeam = manuscript.teams.find(
      team => team.role === Team.Role.reviewer,
    )

    let teamMember
    if (!manuscriptReviewersTeam) {
      manuscriptReviewersTeam = new Team({
        manuscriptId,
        role: Team.Role.reviewer,
      })
      manuscript.updateStatus(Manuscript.Statuses.reviewersInvited)
      await manuscript.save()
    } else {
      teamMember = manuscriptReviewersTeam.members.find(
        reviewer => reviewer.userId === user.id,
      )
    }

    if (teamMember && teamMember.status === TeamMember.Statuses.expired) {
      teamMember.updateProperties({ status: TeamMember.Statuses.pending })
      await teamMember.save()
    } else {
      teamMember = manuscriptReviewersTeam.addMember(user, {
        alias: input,
      })
      await manuscriptReviewersTeam.saveRecursively()
    }

    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: input.email,
        manuscriptId,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = true
      await reviewerSuggestion.save()
    }

    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members.jobs',
    })
    const admin = adminTeam.members[0]
    const journal = await Journal.find(
      manuscript.journalId,
      'teams.members.jobs',
    )
    const editorialAssistant = journal.getEditorialAssistant()

    const heTeam = manuscript.teams.find(
      t => t.role === Team.Role.handlingEditor,
    )
    const handlingEditor = heTeam.members.find(
      member => member.status === TeamMember.Statuses.accepted,
    )

    const editorialAssistantOrAdmin = editorialAssistant || admin
    if (
      manuscriptReviewersTeam &&
      manuscriptReviewersTeam.members.length >=
        config.mininumNumberOfInvitedReviewers
    ) {
      try {
        if (handlingEditor.jobs) {
          await Promise.each(handlingEditor.jobs, async job => {
            await Job.cancel(job.id)
            return job.delete()
          })
        }
        if (editorialAssistantOrAdmin.jobs) {
          await Promise.each(editorialAssistantOrAdmin.jobs, async job => {
            if (job.manuscriptId !== manuscript.id) {
              return
            }
            await Job.cancel(job.id)
            return job.delete()
          })
        }
      } catch (e) {
        logger.error(e)
        throw new Error(e)
      }
    }
    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.reviewer_invited,
      objectType: logEvent.objectType.user,
      objectId: teamMember.user.id,
    })

    invitationsService.sendInvitationToReviewer({
      editorialAssistant,
      manuscript,
      submittingAuthorName: submittingAuthor.getName(),
      reviewer: teamMember,
    })

    await emailJobsService.scheduleEmailsWhenReviewerIsInvited({
      manuscript,
      reviewer: teamMember,
      submittingAuthorName: submittingAuthor.getName(),
    })

    await removalJobsService.scheduleRemovalJob({
      timeUnit,
      days: removalDays,
      invitationId: teamMember.id,
      manuscriptId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
