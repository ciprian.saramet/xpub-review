const { get } = require('lodash')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')

const initialize = ({
  notificationService,
  models: { TeamMember, Team, Manuscript, ReviewerSuggestion, Journal, Job },
  logEvent,
}) => ({
  async execute({ input: { manuscriptId, teamMemberId }, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      '[jobs, team.[members,manuscript.teams.members]]',
    )

    try {
      if (teamMember.jobs) {
        await Promise.each(teamMember.jobs, async job => {
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    const { team } = teamMember
    const manuscript = get(team, 'manuscript')

    if (!manuscript) {
      throw new NotFoundError('Manuscript does not exist')
    }
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }
    const submittingAuthor = manuscript.getSubmittingAuthor()
    team.removeMember(teamMemberId)
    await team.saveRecursively()

    const manuscriptReviewersTeam = await Team.findOneBy({
      queryObject: { manuscriptId, role: Team.Role.reviewer },
      eagerLoadRelations: 'members',
    })

    const pendingOrAcceptedReviewersTeam = manuscriptReviewersTeam.members.find(
      member =>
        ![TeamMember.Statuses.declined, TeamMember.Statuses.expired].includes(
          member.status,
        ),
    )

    if (!pendingOrAcceptedReviewersTeam) {
      await manuscript.updateStatus(Manuscript.Statuses.heAssigned)
      await manuscript.save()
    }
    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: teamMember.alias.email,
        manuscriptId: manuscript.id,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = false
      await reviewerSuggestion.save()
    }

    if (team.members.length === 0) {
      await team.delete()
    }

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorialAssistant = journal.getEditorialAssistant()

    notificationService.notifyReviewerOnCancelOrAcceptInvitation({
      manuscript,
      user: teamMember,
      submittingAuthor,
      isCanceled: true,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.invitation_revoked,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
