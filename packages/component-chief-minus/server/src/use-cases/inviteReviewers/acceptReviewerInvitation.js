const { get, flatMap, head } = require('lodash')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')

const initialize = ({
  notificationService,
  emailJobsService,
  models: { TeamMember, Manuscript, Review, Comment, User, Team, Job },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      '[jobs, user.identities, team.[manuscript.[teams.[members.[user.[identities]]],journal.[teams.[members]]]]]',
    )

    try {
      if (teamMember.jobs) {
        await Promise.each(teamMember.jobs, async job => {
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    if (get(teamMember, 'status') === TeamMember.Statuses.expired) {
      throw new ValidationError('Your invitation has expired')
    }

    if (get(teamMember, 'status') !== TeamMember.Statuses.pending) {
      throw new ValidationError('User already responded to the invitation.')
    }

    // TODO check logged in user to be the one associated with the teamMember

    const manuscript = get(teamMember, 'team.manuscript', {})
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const journal = get(manuscript, 'journal', {})
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()

    const manuscriptVersions = await getAllManuscriptVersions({
      manuscript,
      Manuscript,
    })
    const reviewerTeams = await getReviewerTeams({ manuscriptVersions, Team })
    const acceptedSubmittedReviewers = await getAcceptedSubmittedReviewers({
      reviewerTeams,
      TeamMember,
    })
    await setReviewerNumber({
      acceptedSubmittedReviewers,
      teamMember,
      userId,
      manuscript,
      Manuscript,
    })
    teamMember.updateProperties({ status: TeamMember.Statuses.accepted })
    await teamMember.save()

    const user = await User.find(teamMember.userId, 'identities')
    const localIdentity = user.identities.find(i => i.type === 'local')
    localIdentity.updateProperties({
      isConfirmed: true,
    })
    await localIdentity.save()

    const draftReview = new Review({
      teamMemberId: teamMember.id,
      manuscriptId: manuscript.id,
    })

    draftReview.addComment({
      type: Comment.Types.public,
    })

    draftReview.addComment({
      type: Comment.Types.private,
    })

    await draftReview.saveRecursively()

    await emailJobsService.scheduleEmailsWhenReviewerAcceptsInvitation({
      manuscript,
      reviewer: teamMember,
      submittingAuthorName: submittingAuthor.getName(),
      editorialAssistant,
    })

    notificationService.notifyReviewerOnCancelOrAcceptInvitation({
      manuscript,
      user: teamMember,
      submittingAuthor,
      editorialAssistant,
    })

    notificationService.notifyHEAboutReviewerInvitationDecision({
      manuscript,
      editorInChief,
      user: teamMember,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.reviewer_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
const getAllManuscriptVersions = async ({ manuscript, Manuscript }) => {
  const { submissionId } = manuscript
  const manuscriptVersions = await Manuscript.findAll({
    queryObject: { submissionId },
    eagerLoadRelations: 'teams.members',
    orderByField: 'version',
    order: 'desc',
  })
  return manuscriptVersions
}
const getReviewerTeams = async ({ manuscriptVersions, Team }) => {
  const reviewerTeams = manuscriptVersions.map(manuscript =>
    manuscript.teams.find(t => t.role === Team.Role.reviewer),
  )
  return reviewerTeams
}
const getAcceptedSubmittedReviewers = async ({ reviewerTeams, TeamMember }) => {
  const allReviewerMembers = flatMap(
    reviewerTeams,
    team => team && team.members,
  )
  const acceptedSubmittedReviewers = allReviewerMembers.filter(
    teamMember =>
      teamMember &&
      [TeamMember.Statuses.accepted, TeamMember.Statuses.submitted].includes(
        teamMember.status,
      ),
  )
  return acceptedSubmittedReviewers
}

const setReviewerNumber = async ({
  acceptedSubmittedReviewers,
  teamMember,
  userId,
  manuscript,
  Manuscript,
}) => {
  const responded = new Date()

  const oldReviewer = acceptedSubmittedReviewers.filter(
    m => m.userId === userId,
  )
  const uniqueReviewers = [
    ...new Set(acceptedSubmittedReviewers.map(user => user.userId)),
  ]

  let reviewerNumber
  if (oldReviewer) {
    reviewerNumber = head(oldReviewer.map(nr => nr.reviewerNumber))
    teamMember.updateProperties({ reviewerNumber, responded })
  }

  if (!teamMember.reviewerNumber) {
    if (acceptedSubmittedReviewers.length === 0) {
      teamMember.updateProperties({ reviewerNumber: 1, responded })
      manuscript.updateStatus(Manuscript.Statuses.underReview)
      await manuscript.save()
    } else {
      reviewerNumber = uniqueReviewers.length + 1
      teamMember.updateProperties({ reviewerNumber, responded })
    }
  }

  return reviewerNumber
}
