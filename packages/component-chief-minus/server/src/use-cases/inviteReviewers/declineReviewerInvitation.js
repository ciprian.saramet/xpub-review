const { get } = require('lodash')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')

const initialize = ({
  notificationService,
  models: { TeamMember, Manuscript, Journal, Job },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    let teamMember
    try {
      teamMember = await TeamMember.find(
        teamMemberId,
        '[jobs, team.[manuscript.[teams.[members.[user.[identities]]]]]]',
      )
    } catch (e) {
      throw new Error('Something went wrong.')
    }

    try {
      if (teamMember.jobs) {
        await Promise.each(teamMember.jobs, async job => {
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    if (get(teamMember, 'status') !== TeamMember.Statuses.pending) {
      throw new ConflictError('User already responded to the invitation.')
    }
    const responded = new Date()
    teamMember.updateProperties({
      status: TeamMember.Statuses.declined,
      responded,
    })
    await teamMember.save()

    const manuscript = get(teamMember, 'team.manuscript')
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()

    notificationService.notifyHEAboutReviewerInvitationDecision({
      manuscript,
      editorInChief,
      submittingAuthor,
      user: teamMember,
      isAccepted: false,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.reviewer_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})
const authsomePolicies = []
module.exports = {
  initialize,
  authsomePolicies,
}
