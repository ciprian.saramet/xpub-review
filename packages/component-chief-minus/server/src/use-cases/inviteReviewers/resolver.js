const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { logEvent } = require('component-activity-log/server')

const useCases = require('./index')
const notificationsReviewer = require('../../notificationService/reviewer/notifications')
const emailJobs = require('../../emailJobs/reviewer/emailJobs')
const removalJobs = require('../../removalJobs/reviewersInvitationRemovalJobs')
const invitations = require('../../invitationsService/invitations')

const resolver = {
  Query: {
    async getReviewersInvited(_, { manuscriptId }, ctx) {
      return useCases.getReviewersInvitedUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async inviteReviewer(_, { manuscriptId, input }, ctx) {
      const { TeamMember, ReviewerSuggestion, Job } = models

      const emailJobsService = emailJobs.initialize({ logEvent, Email, Job })
      const invitationsService = invitations.initialize({ Email })
      const removalJobsService = removalJobs.initialize({
        logEvent,
        Job,
        TeamMember,
        ReviewerSuggestion,
      })

      return useCases.inviteReviewerUseCase
        .initialize({
          invitationsService,
          models,
          logEvent,
          emailJobsService,
          removalJobsService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async cancelReviewerInvitation(_, input, ctx) {
      const notificationService = notificationsReviewer.initialize({ Email })
      return useCases.cancelReviewerInvitationUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
        })
        .execute({ input, userId: ctx.user })
    },
    async acceptReviewerInvitation(_, { teamMemberId }, ctx) {
      const { TeamMember, Job } = models

      const notificationService = notificationsReviewer.initialize({ Email })
      const emailJobsService = emailJobs.initialize({ logEvent, Email, Job })
      const removalJobsService = removalJobs.initialize({
        logEvent,
        Job,
        TeamMember,
      })
      return useCases.acceptReviewerInvitationUseCase
        .initialize({
          notificationService,
          emailJobsService,
          removalJobsService,
          models,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async declineReviewerInvitation(_, { teamMemberId }, ctx) {
      const notificationService = notificationsReviewer.initialize({ Email })
      return useCases.declineReviewerInvitationUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
  },
}

module.exports = resolver
