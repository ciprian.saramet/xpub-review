const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Email = require('@pubsweet/component-email-templating')

const useCases = require('./index')
const notificationsHandlingEditor = require('../../notificationService/handlingEditor/notifications')

const JobsService = require('../../jobsService/jobsService')

const resolver = {
  Query: {},
  Mutation: {
    async recommendToRejectAsHE(_, { manuscriptId, input }, ctx) {
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      const jobsService = JobsService.initialize({ models })

      return useCases.recommendToRejectAsHEUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          jobsService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async requestRevisionAsHE(_, { manuscriptId, content, type }, ctx) {
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      const jobsService = JobsService.initialize({ models })

      return useCases.requestRevisionAsHEUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          jobsService,
        })
        .execute({ manuscriptId, content, userId: ctx.user, type })
    },
    async recommendToPublishAsHE(_, { manuscriptId, input }, ctx) {
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      const jobsService = JobsService.initialize({ models })

      return useCases.recommendToPublishAsHEUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          jobsService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
  },
}

module.exports = resolver
