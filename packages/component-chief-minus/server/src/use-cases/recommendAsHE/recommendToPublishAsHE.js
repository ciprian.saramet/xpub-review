const initialize = ({
  notificationService,
  models: { Review, Manuscript, Journal, Comment, Team, TeamMember },
  logEvent,
  jobsService,
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      'teams.[members.jobs]',
    )

    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }
    const handlingEditor = manuscript.getHandlingEditor()

    const review = new Review({
      recommendation: Review.Recommendations.publish,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: handlingEditor.id,
    })
    await review.save()

    const authorComment = new Comment({
      content: input.forAuthor,
      reviewId: review.id,
      type: 'public',
    })
    await authorComment.save()

    if (input.forEiC) {
      const eicComment = new Comment({
        content: input.forEiC,
        reviewId: review.id,
        type: 'private',
      })
      await eicComment.save()
    }

    manuscript.updateStatus(Manuscript.Statuses.pendingApproval)
    await manuscript.save()

    const journal = await Journal.find(
      manuscript.journalId,
      'teams.members.jobs',
    )
    const editorialAssistant = journal.getEditorialAssistant()
    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members.jobs',
    })
    const admin = adminTeam.members[0]
    const editorialAssistantOrAdmin = editorialAssistant || admin

    jobsService.cancelAcceptedHandlingEditorJobs({
      handlingEditor,
    })
    jobsService.cancelEditorialAssistantOrAdminJobs({
      editorialAssistantOrAdmin,
      manuscript,
    })

    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const reviewerTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )
    let reviewerTeamMembers = []

    if (reviewerTeam) {
      reviewerTeamMembers = reviewerTeam.members
      const acceptedReviewers = reviewerTeamMembers.filter(
        reviewer => reviewer.status === TeamMember.Statuses.accepted,
      )
      jobsService.deletePendingReviewersFromQueue({ reviewerTeam })
      jobsService.cancelReviewersJobs({ reviewers: acceptedReviewers })
    }

    notificationService.notifyEiCWhenHEMakesRecommendation({
      review,
      manuscript,
      editorInChief,
      handlingEditor,
      submittingAuthor,
      editorialAssistant,
      commentForEiC: input.forEiC,
    })

    notificationService.notifyReviewersWhenHEMakesRecommendation({
      manuscript,
      handlingEditor,
      submittingAuthor,
      editorialAssistant,
      reviewers: reviewerTeamMembers,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.recommendation_accept,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
