const { createVersion } = require('../createVersion/createVersion')

const initialize = ({
  notificationService,
  models: { Review, Manuscript, Journal, Comment, File, Team, TeamMember },
  logEvent,
  jobsService,
}) => ({
  async execute({ manuscriptId, content, type, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.[user, jobs], files, reviews]',
    )
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }
    const heMember = manuscript.getHandlingEditor()
    const review = new Review({
      recommendation: Review.Recommendations[type],
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: heMember.id,
    })
    await review.save()

    const authorComment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await authorComment.save()

    manuscript.updateStatus(Manuscript.Statuses.revisionRequested)
    await manuscript.save()

    const journal = await Journal.find(
      manuscript.journalId,
      'teams.members.jobs',
    )
    const editorialAssistant = journal.getEditorialAssistant()
    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members.jobs',
    })
    const admin = adminTeam.members[0]
    const editorialAssistantOrAdmin = editorialAssistant || admin

    jobsService.cancelAcceptedHandlingEditorJobs({
      handlingEditor: heMember,
    })
    jobsService.cancelEditorialAssistantOrAdminJobs({
      editorialAssistantOrAdmin,
      manuscript,
    })

    await createVersion({
      Manuscript,
      Review,
      Comment,
      File,
      Team,
      TeamMember,
      manuscript,
    })

    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const reviewerTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )
    let reviewerTeamMembers = []

    if (reviewerTeam) {
      reviewerTeamMembers = reviewerTeam.members
      const acceptedReviewers = reviewerTeamMembers.filter(
        reviewer => reviewer.status === TeamMember.Statuses.accepted,
      )
      jobsService.deletePendingReviewersFromQueue({ reviewerTeam })
      jobsService.cancelReviewersJobs({ reviewers: acceptedReviewers })
    }

    notificationService.notifyEiCWhenHEMakesRecommendation({
      review,
      manuscript,
      editorInChief,
      submittingAuthor,
      editorialAssistant,
      handlingEditor: heMember,
    })

    notificationService.notifySAWhenHERequestsRevision({
      manuscript,
      submittingAuthor,
      editorialAssistant,
      commentForAuthor: content,
      handlingEditor: heMember,
    })

    if (review.recommendation === Review.Recommendations.minor) {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.revision_requested_minor,
        objectType: logEvent.objectType.manuscript,
        objectId: manuscriptId,
      })
    }

    if (review.recommendation === Review.Recommendations.major) {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.revision_requested_major,
        objectType: logEvent.objectType.manuscript,
        objectId: manuscriptId,
      })
    }

    notificationService.notifyReviewersWhenHEMakesRecommendation({
      manuscript,
      submittingAuthor,
      editorialAssistant,
      handlingEditor: heMember,
      reviewers: reviewerTeamMembers,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
