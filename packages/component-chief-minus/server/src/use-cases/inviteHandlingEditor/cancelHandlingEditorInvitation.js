const { get, last } = require('lodash')
const logger = require('@pubsweet/logger')

const initialize = ({
  logEvent,
  notificationService,
  models: { TeamMember, Journal, Manuscript, Team, Job },
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      '[jobs, user, team.[members,manuscript.[teams.[members.[user.[identities]]]]]]',
    )

    try {
      if (teamMember.jobs && teamMember.jobs.length > 0) {
        await Promise.all(
          teamMember.jobs.map(async job => {
            await Job.cancel(job.id)
            return job.delete()
          }),
        )
      }
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    const manuscript = get(teamMember, 'team.manuscript')
    const submissionId = get(teamMember, 'team.manuscript.submissionId')

    const manuscripts = await Manuscript.findBy(
      { submissionId },
      'teams.members',
    )
    if (last(manuscripts).status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }
    await Promise.all(
      manuscripts.map(async manuscript => {
        const heTeam = manuscript.teams.find(
          team => team.role === Team.Role.handlingEditor,
        )
        await heTeam.delete()
      }),
    )
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()

    const submittingAuthor = manuscript.getSubmittingAuthor()

    manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    notificationService.notifyHEWhenInvitationDeclined({
      manuscript,
      editorInChief,
      user: teamMember,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_revoked,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasGlobalRole']

module.exports = {
  initialize,
  authsomePolicies,
}
