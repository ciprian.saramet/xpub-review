const { get, flatMap, last } = require('lodash')
const Promise = require('bluebird')
const { mininumNumberOfInvitedReviewers } = require('config')
const logger = require('@pubsweet/logger')

const initialize = ({
  notificationService,
  models: { TeamMember, Journal, Manuscript, Team, Review, Job },
  logEvent,
  jobsService,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(teamMemberId, [
      'team.[members, manuscript.[teams.members.jobs, reviewerSuggestions]]',
      'user',
    ])

    const manuscript = get(teamMember, 'team.manuscript')
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }
    const allVersionsOfManuscript = await Manuscript.findBy(
      { submissionId: manuscript.submissionId },
      'teams.members',
    )
    if (last(allVersionsOfManuscript).status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    const reviewersTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )

    let reviewers = []
    if (reviewersTeam) {
      reviewers = reviewersTeam.members
      await reviewersTeam.delete()
    }

    const heRecommendation = await Review.findOneBy({
      queryObject: {
        manuscriptId: manuscript.id,
        teamMemberId,
      },
    })

    if (heRecommendation) {
      await heRecommendation.delete()
    }

    const allHETeams = allVersionsOfManuscript.map(manuscript =>
      manuscript.teams.find(t => t.role === Team.Role.handlingEditor),
    )

    const allHEMembers = flatMap(allHETeams, t => t.members)
    await Promise.each(allHEMembers, member => {
      member.status = TeamMember.Statuses.removed
      return member.save()
    })

    const reviewerSuggestions = get(manuscript, 'reviewerSuggestions')
    if (reviewerSuggestions) {
      await Promise.each(reviewerSuggestions, async reviewerSuggestion => {
        reviewerSuggestion.updateProperties({ isInvited: false })
        await reviewerSuggestion.save()
      })
    }

    const draftManuscript = allVersionsOfManuscript.find(
      m => m.status === Manuscript.Statuses.draft,
    )

    if (draftManuscript) {
      await draftManuscript.delete()
    }

    manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()
    const journal = await Journal.find(
      manuscript.journalId,
      'teams.members.jobs',
    )
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members.jobs',
    })
    const admin = adminTeam.members[0]

    const reviewersTeamMembers = manuscript.getReviewers()
    const acceptedReviewers = reviewersTeamMembers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.accepted,
    )
    const pendingReviewers = reviewersTeamMembers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.pending,
    )

    jobsService.cancelReviewersJobs({ reviewers: acceptedReviewers })
    jobsService.cancelReviewersJobs({ reviewers: pendingReviewers })

    const editorialAssistantOrAdmin = editorialAssistant || admin
    if (
      !reviewersTeam ||
      reviewersTeam.members.length < mininumNumberOfInvitedReviewers
    ) {
      try {
        if (teamMember.jobs) {
          await Promise.each(teamMember.jobs, async job => {
            await Job.cancel(job.id)
            return job.delete()
          })
        }
        if (editorialAssistantOrAdmin.jobs) {
          await Promise.each(editorialAssistantOrAdmin.jobs, async job => {
            if (job.manuscriptId !== manuscript.id) {
              return
            }
            await Job.cancel(job.id)
            return job.delete()
          })
        }
      } catch (e) {
        logger.error(e)
        throw new Error(e)
      }
    }

    notificationService.notifyAuthorWhenHERemoved({
      manuscript,
      editorInChief,
      submittingAuthor,
      editorialAssistant,
    })

    notificationService.notifyInvitedHEWhenRemoved({
      manuscript,
      editorInChief,
      user: teamMember,
      editorialAssistant,
    })

    notificationService.notifyReviewersWhenHERemoved({
      reviewers,
      manuscript,
      editorInChief,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.he_removed,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasGlobalRole']

module.exports = {
  initialize,
  authsomePolicies,
}
