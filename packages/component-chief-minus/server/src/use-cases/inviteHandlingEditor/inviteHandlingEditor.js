const { isEmpty, chain } = require('lodash')
const config = require('config')

const timeUnit = config.get(
  'reminders.handlingEditor.acceptInvitation.timeUnit',
)
const removalDays = config.get(
  'reminders.handlingEditor.acceptInvitation.remove',
)

const initialize = ({
  models,
  logEvent,
  emailJobsService,
  removalJobsService,
  invitationsService,
}) => ({
  async execute({ submissionId, userId, reqUserId }) {
    const { Journal, Manuscript, User, Team, TeamMember } = models
    const manuscripts = await Manuscript.findAll({
      queryObject: { submissionId },
      eagerLoadRelations: 'teams.members.user.identities',
      orderByField: 'version',
      order: 'desc',
    })
    if (isEmpty(manuscripts)) {
      throw new Error('No manuscript has been found.')
    }

    // check if invited user is HE on latest manuscript
    const latestHETeam = manuscripts[0].teams.find(
      t => t.role === Team.Role.handlingEditor,
    )
    if (latestHETeam && latestHETeam.members.length > 0) {
      const existingHe = latestHETeam.members.find(
        m =>
          m.userId === userId &&
          [TeamMember.Statuses.accepted, TeamMember.Statuses.pending].includes(
            m.status,
          ),
      )
      if (existingHe) {
        throw Error(`The handling editor is already invited.`)
      }
    }

    // check if HE has been removed or declined on any version
    if (
      chain(manuscripts)
        .flatMap(m => m.teams)
        .filter(t => t.role === Team.Role.handlingEditor)
        .flatMap(t => t.members)
        .filter(m => m.userId === userId)
        .map(m => m.status)
        .some(s =>
          [TeamMember.Statuses.removed, TeamMember.Statuses.declined].includes(
            s,
          ),
        )
        .value()
    ) {
      throw Error(`The handling editor can't be invited.`)
    }

    const latestManuscript = manuscripts[0]
    const journal = await Journal.find(
      manuscripts[0].journalId,
      'teams.members.user',
    )
    const submittingAuthor = latestManuscript.getSubmittingAuthor()
    const editorInChief = journal.getEditorInChief()

    const user = await User.find(userId, '[identities,teamMemberships.team]')

    await Promise.all(
      manuscripts.map(async manuscript => {
        const heTeam = manuscript.teams.find(
          t => t.role === Team.Role.handlingEditor,
        )
        if (!heTeam) {
          await addHandlingEditorToNewTeam({
            Team,
            user,
            manuscript,
            TeamMember,
          })
        } else {
          await addHandlingEditorToExistingTeam({
            heTeam,
            user,
            TeamMember,
          })
          await heTeam.save()
        }
      }),
    )

    latestManuscript.updateStatus(Manuscript.Statuses.heInvited)
    await latestManuscript.save()

    const handlingEditor = latestManuscript.getHandlingEditorByStatus(
      TeamMember.Statuses.pending,
    )

    invitationsService.sendInvitationToHandlingEditor({
      manuscript: latestManuscript,
      submittingAuthor,
      handlingEditor,
      editorInChief,
    })

    await emailJobsService.scheduleEmailsWhenHandlingEditorIsInvited({
      journal,
      manuscript: latestManuscript,
      submittingAuthor,
      handlingEditor,
      editorInChief,
    })

    await removalJobsService.scheduleRemovalJobForHandlingEditor({
      days: removalDays,
      timeUnit,
      invitationId: handlingEditor.id,
      manuscriptId: latestManuscript.id,
    })

    logEvent({
      userId: reqUserId,
      manuscriptId: latestManuscript.id,
      action: logEvent.actions.invitation_sent,
      objectType: logEvent.objectType.user,
      objectId: user.id,
    })
  },
})

const authsomePolicies = [
  'authenticatedUser',
  'hasGlobalRole',
  'hasAccessToManuscriptVersions',
]

module.exports = {
  initialize,
  authsomePolicies,
}

const addHandlingEditorToNewTeam = async ({
  Team,
  user,
  manuscript,
  TeamMember,
}) => {
  const heTeam = new Team({
    manuscriptId: manuscript.id,
    role: Team.Role.handlingEditor,
  })
  manuscript.assignTeam(heTeam)
  await heTeam.save()

  const teamMember = heTeam.addMember(user, {
    status: TeamMember.Statuses.pending,
  })
  teamMember.userId = user.id
  teamMember.teamId = heTeam.id
  await teamMember.save()

  return teamMember
}

const addHandlingEditorToExistingTeam = async ({
  heTeam,
  user,
  TeamMember,
}) => {
  let handlingEditor = heTeam.members.find(member => member.userId === user.id)
  if (!handlingEditor) {
    handlingEditor = heTeam.addMember(user, {
      status: TeamMember.Statuses.pending,
    })
    handlingEditor.userId = user.id
    handlingEditor.teamId = heTeam.id
    await handlingEditor.save()

    return handlingEditor
  }

  handlingEditor.updateProperties({
    status: TeamMember.Statuses.pending,
  })
  await handlingEditor.save()

  return handlingEditor
}
