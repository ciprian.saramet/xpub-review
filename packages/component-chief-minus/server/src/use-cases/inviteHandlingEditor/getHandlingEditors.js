const { chain } = require('lodash')

const initialize = ({ TeamMember, Team, Manuscript }) => ({
  async execute(manuscriptId) {
    const globalHeTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.handlingEditor,
        manuscriptId: null,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
    })
    if (!globalHeTeam) return []

    const manuscript = await Manuscript.find(manuscriptId, 'teams.members')
    const manuscriptHeTeam = manuscript.teams.find(
      t => t.role === Team.Role.handlingEditor,
    )
    let declinedHeMembersIds = []
    if (manuscriptHeTeam) {
      declinedHeMembersIds = manuscriptHeTeam.members
        .filter(m => m.status === TeamMember.Statuses.declined)
        .map(m => m.userId)
    }

    const manuscripts = await Manuscript.findBy(
      { submissionId: manuscript.submissionId },
      'teams.members',
    )
    const allManuscriptsHeTeams = chain(manuscripts)
      .flatMap(m => m.teams)
      .filter(t => t.role === Team.Role.handlingEditor)
      .value()
    let removedHeMembersIds = []
    if (allManuscriptsHeTeams) {
      removedHeMembersIds = chain(allManuscriptsHeTeams)
        .flatMap(t => t.members)
        .filter(m => m.status === TeamMember.Statuses.removed)
        .map(m => m.userId)
        .value()
    }

    const declinedAndRemovedMemberIds = [
      ...declinedHeMembersIds,
      ...removedHeMembersIds,
    ]
    if (declinedAndRemovedMemberIds.length > 0) {
      return globalHeTeam.members
        .filter(
          gmember => !declinedAndRemovedMemberIds.includes(gmember.userId),
        )
        .map(m => m.toDTO())
    }

    return globalHeTeam.members.map(m => m.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser', 'hasGlobalRole']

module.exports = {
  initialize,
  authsomePolicies,
}
