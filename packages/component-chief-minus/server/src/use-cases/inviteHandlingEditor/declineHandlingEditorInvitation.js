const { get, last } = require('lodash')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')

const initialize = ({
  notificationService,
  models: { TeamMember, Journal, Manuscript, Team, Job },
  logEvent,
}) => ({
  async execute({ input: { teamMemberId, reason }, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      '[jobs, team.[manuscript.[teams.[members.[user.[identities]]]]]]',
    )
    teamMember.updateProperties({ status: 'declined' })

    const manuscript = get(teamMember, 'team.manuscript')
    const submissionId = get(teamMember, 'team.manuscript.submissionId')

    const manuscripts = await Manuscript.findBy(
      { submissionId },
      'teams.members',
    )
    if (last(manuscripts).status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    await Promise.all(
      manuscripts.map(async manuscript => {
        const heTeam = manuscript.teams.find(
          team => team.role === Team.Role.handlingEditor,
        )
        const heMember = heTeam.members[0]
        heMember.updateProperties({ status: 'declined' })
        await heMember.save()
      }),
    )
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    try {
      if (teamMember.jobs) {
        await Promise.each(teamMember.jobs, async job => {
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    notificationService.notifyEiCAboutHEInvitationDecisionEmail({
      reason,
      manuscript,
      editorInChief,
      user: teamMember,
      submittingAuthor,
      isAccepted: false,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
