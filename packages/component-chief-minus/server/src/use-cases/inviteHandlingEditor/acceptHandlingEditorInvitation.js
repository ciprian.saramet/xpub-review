const { get, chain, last } = require('lodash')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')

const initialize = ({
  notificationService,
  models: { TeamMember, Journal, Manuscript, Team, Job },
  logEvent,
  emailJobsService,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      '[jobs, team.manuscript.teams.members, user]',
    )

    try {
      if (teamMember.jobs) {
        await Promise.each(teamMember.jobs, async job => {
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    const manuscript = get(teamMember, 'team.manuscript', {})
    const manuscripts = await Manuscript.findBy(
      {
        submissionId: manuscript.submissionId,
      },
      'teams.members',
    )

    if (last(manuscripts).status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    const heTeams = manuscripts.map(m =>
      m.teams.find(t => t.role === Team.Role.handlingEditor),
    )

    const teamMembers = chain(heTeams)
      .flatMap(t => t.members)
      .filter(m => m.userId === teamMember.userId)
      .value()

    await Promise.all(
      teamMembers.map(async member => {
        member.updateProperties({ status: TeamMember.Statuses.accepted })
        await member.save()
      }),
    )

    manuscript.updateStatus(Manuscript.Statuses.heAssigned)
    await manuscript.save()

    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members',
    })
    const admin = adminTeam.members[0]

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()

    const submittingAuthor = manuscript.getSubmittingAuthor()

    notificationService.notifyEiCAboutHEInvitationDecisionEmail({
      manuscript,
      editorInChief,
      user: teamMember,
      submittingAuthor,
      editorialAssistant,
    })

    notificationService.notifyHEAfterAcceptedInvitation({
      user: teamMember,
      manuscript,
    })

    emailJobsService.sendHERemindersToInviteReviewers({
      submittingAuthorName: submittingAuthor.getName(),
      user: teamMember,
      manuscript,
      admin,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
