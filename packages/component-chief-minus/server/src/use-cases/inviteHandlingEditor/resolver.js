const useCases = require('./index')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')

const { logEvent } = require('component-activity-log/server')
const emailJobs = require('../../emailJobs/handlingEditor/emailJobs')
const removalJobs = require('../../removalJobs/handlingEditorRemovalJobs')
const invitations = require('../../invitationsService/invitations')
const notificationsHandlingEditor = require('../../notificationService/handlingEditor/notifications')
const JobsService = require('../../jobsService/jobsService')

const resolver = {
  Query: {
    async getHandlingEditors(_, { manuscriptId }, ctx) {
      return useCases.getHandlingEditorsUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async inviteHandlingEditor(_, { submissionId, userId }, ctx) {
      const { TeamMember, Manuscript, Job } = models

      const emailJobsService = emailJobs.initialize({
        logEvent,
        Email,
        Job,
      })
      const removalJobsService = removalJobs.initialize({
        Job,
        TeamMember,
        Manuscript,
        logEvent,
      })
      const invitationsService = invitations.initialize({ Email })
      return useCases.inviteHandlingEditorUseCase
        .initialize({
          useCases,
          models,
          logEvent,
          invitationsService,
          emailJobsService,
          removalJobsService,
        })
        .execute({ submissionId, userId, reqUserId: ctx.user })
    },
    async cancelHandlingEditorInvitation(_, { teamMemberId }, ctx) {
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      return useCases.cancelHandlingEditorInvitationUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async acceptHandlingEditorInvitation(_, { teamMemberId }, ctx) {
      const emailJobsService = emailJobs.initialize({
        logEvent,
        Email,
        Job: models.Job,
      })
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      return useCases.acceptHandlingEditorInvitationUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          emailJobsService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async declineHandlingEditorInvitation(_, input, ctx) {
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      return useCases.declineHandlingEditorInvitationUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
        })
        .execute({ input, userId: ctx.user })
    },
    async removeHandlingEditor(_, { teamMemberId }, ctx) {
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      const jobsService = JobsService.initialize({ models })

      return useCases.removeHandlingEditorUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          jobsService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
  },
}

module.exports = resolver
