const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Email = require('@pubsweet/component-email-templating')
const notificationsHandlingEditor = require('../../notificationService/handlingEditor/notifications')
const JobsService = require('../../jobsService/jobsService')

const resolver = {
  Query: {},
  Mutation: {
    async updateDraftReview(_, { reviewId, input }, ctx) {
      return useCases.updateDraftReviewUseCase
        .initialize({ models })
        .execute({ reviewId, input, userId: ctx.user })
    },
    async submitReview(_, { reviewId }, ctx) {
      const notificationService = notificationsHandlingEditor.initialize({
        Email,
      })
      const jobsService = JobsService.initialize({ models })
      return useCases.submitReviewUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          jobsService,
        })
        .execute({ reviewId, userId: ctx.user })
    },
  },
}

module.exports = resolver
