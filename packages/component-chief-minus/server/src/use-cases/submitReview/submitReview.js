const { Promise } = require('bluebird')
const { mininumNumberOfSubmittedReports } = require('config')
const { isEmpty } = require('lodash')

const initialize = ({
  notificationService,
  models: { Review, Manuscript, Journal, Job, Team, Comment },
  logEvent,
  jobsService,
}) => ({
  async execute({ reviewId, userId }) {
    const review = await Review.find(
      reviewId,
      '[member.jobs, manuscript.[reviews.member.jobs, teams.members.[user, jobs]], comments.files]',
    )

    if (review.member.userId !== userId)
      throw new AuthorizationError('User can not update selected review')
    const hasPublicComment = review.hasPublicComment()
    if (!hasPublicComment)
      throw new ValidationError('Cannot submit an empty review')

    const { manuscript } = review
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    review.updateProperties({
      submitted: new Date(),
      open: false,
    })
    await review.save()

    review.manuscript.updateProperties({
      status: Manuscript.Statuses.reviewCompleted,
    })
    await review.manuscript.save()

    review.member.updateProperties({
      status: 'submitted',
    })
    await review.member.save()

    const privateComment = review.comments.find(
      comm => comm.type === Comment.Types.private && isEmpty(comm.content),
    )
    if (privateComment) {
      await privateComment.delete()
    }

    const reviewer = review.member
    const journal = await Journal.find(
      manuscript.journalId,
      'teams.members.jobs',
    )
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const handlingEditor = manuscript.getHandlingEditor()
    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members.jobs',
    })
    const admin = adminTeam.members[0]
    const editorialAssistantOrAdmin = editorialAssistant || admin

    if (manuscript.reviews.length >= mininumNumberOfSubmittedReports) {
      jobsService.cancelAcceptedHandlingEditorJobs({
        handlingEditor,
      })
      jobsService.cancelEditorialAssistantOrAdminJobs({
        editorialAssistantOrAdmin,
        manuscript,
      })
    }

    try {
      if (reviewer.jobs) {
        await Promise.each(reviewer.jobs, async job => {
          await Job.cancel(job.id)
          return job.delete()
        })
      }
    } catch (e) {
      throw new Error(e)
    }

    notificationService.notifyHEWhenReviewerSubmitsReview({
      reviewer,
      manuscript,
      editorInChief,
      handlingEditor,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.review_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
