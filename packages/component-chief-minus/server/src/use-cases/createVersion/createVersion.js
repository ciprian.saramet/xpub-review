const uuid = require('uuid')
const { s3service } = require('component-files/server')

module.exports = {
  async createVersion({
    Manuscript,
    Review,
    Comment,
    File,
    Team,
    TeamMember,
    manuscript,
  }) {
    const newManuscript = new Manuscript({
      ...manuscript,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
      version: manuscript.version + 1,
      status: Manuscript.Statuses.draft,
    })
    delete newManuscript.id

    await newManuscript.save()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    const review = new Review({
      manuscriptId: newManuscript.id,
      recommendation: Review.Recommendations.responseToRevision,
      teamMemberId: submittingAuthor.id,
    })
    await review.save()

    const comment = new Comment({
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await comment.save()

    uploadNewFiles({
      files: manuscript.files,
      newManuscriptId: newManuscript.id,
      File,
    })

    const teams = await Promise.all(
      manuscript.teams
        .filter(team => team.role !== Team.Role.reviewer)
        .map(async team => {
          const newTeam = new Team({
            ...team,
            created: new Date().toISOString(),
            updated: new Date().toISOString(),
            manuscriptId: newManuscript.id,
          })
          delete newTeam.id
          await newTeam.save()

          return newTeam
        }),
    )
    const authorTeam = teams.find(team => team.role === Team.Role.author)
    const heTeam = teams.find(team => team.role === Team.Role.handlingEditor)

    if (manuscript.hasHandlingEditor()) {
      const handlingEditor = manuscript.getHandlingEditor()
      const newHe = new TeamMember({
        ...handlingEditor,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: heTeam.id,
      })
      delete newHe.id
      await newHe.save()
    }

    const authors = manuscript.getAuthors()
    authors.map(async author => {
      const newAuthor = new TeamMember({
        ...author,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: authorTeam.id,
      })
      delete newAuthor.id
      return newAuthor.save()
    })
  },
}

const uploadNewFiles = async ({ files, File, newManuscriptId }) => {
  files.map(async file => {
    const newKey = `${newManuscriptId}/${uuid.v4()}`
    await s3service.copyObject({ prevKey: file.providerKey, newKey })

    const newFile = new File({
      ...file,
      providerKey: newKey,
      manuscriptId: newManuscriptId,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    })
    delete newFile.id
    return newFile.save()
  })
}
