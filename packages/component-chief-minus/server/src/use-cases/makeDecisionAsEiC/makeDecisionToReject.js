const { isEmpty } = require('lodash')

const initialize = ({
  notificationService,
  models: { Review, Manuscript, Journal, User, Team, TeamMember, Comment, Job },
  logEvent,
  jobsService,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[files, reviews.[comments.files,member.team], teams.members.jobs]',
    )
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    const statuses = Manuscript.Statuses
    const notAllowedStatuses = [
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
    ]

    if (notAllowedStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot reject a manuscript in the current status.`,
      )
    }

    const user = await User.find(userId, 'teamMemberships.team')
    const globalRoleMember = user.teamMemberships.find(member =>
      [
        Team.Role.editorInChief,
        Team.Role.admin,
        Team.Role.editorialAssistant,
      ].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.reject,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: globalRoleMember.id,
    })
    await review.save()

    const comment = new Comment({
      content,
      type: Comment.Types.public,
      reviewId: review.id,
    })
    await comment.save()

    manuscript.assignReview(review)
    manuscript.updateStatus(Manuscript.Statuses.rejected)
    await manuscript.save()

    const authors = manuscript.getAuthors()
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()
    const handlingEditor = manuscript.getHandlingEditor()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    const reviewers = manuscript.getReviewers()
    const submittingReviewers = reviewers.filter(
      r => r.status === TeamMember.Statuses.submitted,
    )
    const HERecommendation = manuscript.getLatestHERecommendation()

    const acceptedReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.accepted,
    )
    const pendingReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.pending,
    )

    jobsService.cancelReviewersJobs({ reviewers: acceptedReviewers })
    jobsService.cancelReviewersJobs({ reviewers: pendingReviewers })

    if (!isEmpty(submittingReviewers)) {
      notificationService.notifyReviewersWhenEICRejectsManuscript({
        manuscript,
        editorInChief,
        submittingAuthor,
        editorialAssistant,
        reviewers: submittingReviewers,
      })
      notificationService.notifyAuthorsWhenEiCMakesDecisionToRejectAfterPeerReview(
        {
          authors,
          comment,
          manuscript,
          editorInChief,
          editorialAssistant,
        },
      )
    } else {
      notificationService.notifyAuthorsWhenEiCMakesDecisionToRejectBeforePeerReview(
        {
          authors,
          comment,
          manuscript,
          editorInChief,
          editorialAssistant,
        },
      )
    }
    if (HERecommendation) {
      notificationService.notifyHEWhenEICRejectsManuscript({
        manuscript,
        editorInChief,
        handlingEditor,
        submittingAuthor,
        editorialAssistant,
      })
    }

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_rejected,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasGlobalRole']

module.exports = {
  initialize,
  authsomePolicies,
}
