const makeDecisionToRejectAsEiCUseCase = require('./makeDecisionToReject')
const makeDecisionToReturnAsEiCUseCase = require('./makeDecisionToReturn')
const makeDecisionToPublishAsEiCUseCase = require('./makeDecisionToPublish')
const requestRevisionAsEiCUseCase = require('./requestRevision')

module.exports = {
  makeDecisionToRejectAsEiCUseCase,
  makeDecisionToReturnAsEiCUseCase,
  makeDecisionToPublishAsEiCUseCase,
  requestRevisionAsEiCUseCase,
}
