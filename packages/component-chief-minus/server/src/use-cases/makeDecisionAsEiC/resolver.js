const useCases = require('./index')
const { sendPackage } = require('component-mts-package')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const notificationsEditorInChief = require('../../notificationService/editorInChief/notifications')
const { logEvent } = require('component-activity-log/server')
const JobsService = require('../../jobsService/jobsService')

const resolver = {
  Query: {},
  Mutation: {
    async makeDecisionToPublishAsEiC(_, { manuscriptId }, ctx) {
      const notificationService = notificationsEditorInChief.initialize({
        Email,
      })
      return useCases.makeDecisionToPublishAsEiCUseCase
        .initialize({
          notificationService,
          models,
          sendPackage,
          logEvent,
        })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async makeDecisionToReturnAsEiC(_, { manuscriptId, content }, ctx) {
      const notificationService = notificationsEditorInChief.initialize({
        Email,
      })
      return useCases.makeDecisionToReturnAsEiCUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
        })
        .execute({ manuscriptId, content, userId: ctx.user })
    },
    async requestRevisionAsEiC(_, { manuscriptId, content }, ctx) {
      const notificationService = notificationsEditorInChief.initialize({
        Email,
      })
      return useCases.requestRevisionAsEiCUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
        })
        .execute({ manuscriptId, content, userId: ctx.user })
    },
    async makeDecisionToRejectAsEiC(_, { manuscriptId, content }, ctx) {
      const notificationService = notificationsEditorInChief.initialize({
        Email,
      })
      const jobsService = JobsService.initialize({ models })

      return useCases.makeDecisionToRejectAsEiCUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          jobsService,
        })
        .execute({ manuscriptId, content, userId: ctx.user })
    },
  },
}

module.exports = resolver
