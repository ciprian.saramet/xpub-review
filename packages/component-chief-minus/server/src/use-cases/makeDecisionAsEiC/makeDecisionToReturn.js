const initialize = ({
  notificationService,
  models: { Review, Manuscript, Journal, Comment, User, Team },
  logEvent,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[reviews.comments.files, teams.members]',
    )
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    const allowedStatuses = [Manuscript.Statuses.pendingApproval]

    if (!allowedStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot return a manuscript in the current status.`,
      )
    }

    const user = await User.find(userId, 'teamMemberships.team')
    const globalRoleMember = user.teamMemberships.find(member =>
      [
        Team.Role.editorInChief,
        Team.Role.admin,
        Team.Role.editorialAssistant,
      ].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.returnToHE,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: globalRoleMember.id,
    })

    const comment = review.addComment({
      content,
      type: Comment.Types.public,
    })

    manuscript.assignReview(review)
    manuscript.updateStatus(Manuscript.Statuses.reviewCompleted)
    await manuscript.saveRecursively()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()
    const handlingEditor = manuscript.getHandlingEditor()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    notificationService.notifyHEWhenEICReturnsManuscript({
      comment,
      manuscript,
      editorInChief,
      handlingEditor,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_returned,
      objectType: logEvent.objectType.user,
      objectId: handlingEditor.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasGlobalRole']

module.exports = {
  initialize,
  authsomePolicies,
}
