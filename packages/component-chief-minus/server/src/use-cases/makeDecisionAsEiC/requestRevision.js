const { createVersion } = require('../createVersion/createVersion')

const initialize = ({
  notificationService,
  fileUploadService,
  models: {
    Review,
    Manuscript,
    User,
    Team,
    Comment,
    File,
    Journal,
    TeamMember,
  },
  logEvent,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, files, reviews]',
    )

    if (manuscript.hasHandlingEditor()) {
      throw new ValidationError(
        'Cannot make a request to revision after a Handling Editor has been assigned.',
      )
    }
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    await createVersion({
      Manuscript,
      Review,
      Comment,
      File,
      Team,
      TeamMember,
      manuscript,
    })
    manuscript.updateStatus(Manuscript.Statuses.revisionRequested)

    await manuscript.save()

    const user = await User.find(userId, 'teamMemberships.team')
    const globalRoleMember = user.teamMemberships.find(member =>
      [
        Team.Role.editorInChief,
        Team.Role.admin,
        Team.Role.editorialAssistant,
      ].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.revision,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: globalRoleMember.id,
    })

    review.addComment({
      content,
      type: Comment.Types.public,
    })

    await review.saveRecursively()

    const submittingAuthor = manuscript.getSubmittingAuthor()
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()

    notificationService.notifySAWhenEiCRequestsRevision({
      review,
      manuscript,
      editorInChief,
      submittingAuthor,
      editorialAssistant,
      commentTypes: {
        public: Comment.Types.public,
        private: Comment.Types.private,
      },
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.revision_requested,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasGlobalRole']

module.exports = {
  initialize,
  authsomePolicies,
}
