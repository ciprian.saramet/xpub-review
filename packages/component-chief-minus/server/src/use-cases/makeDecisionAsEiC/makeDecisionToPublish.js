const uuid = require('uuid')

const initialize = ({
  notificationService,
  models: { Manuscript, User, Team, Review, Journal },
  sendPackage,
  logEvent,
}) => ({
  async execute({ manuscriptId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, 'teams.members.user')

    if (manuscript.status !== Manuscript.Statuses.pendingApproval) {
      throw new ValidationError(
        `Cannot publish a manuscript in the current status.`,
      )
    }

    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    const user = await User.find(userId, 'teamMemberships.team')
    const globalRoleMember = user.teamMemberships.find(member =>
      [
        Team.Role.editorInChief,
        Team.Role.admin,
        Team.Role.editorialAssistant,
      ].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.publish,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: globalRoleMember.id,
    })

    await review.save()
    manuscript.assignReview(review)

    if (manuscript.hasPassedEqa) {
      manuscript.updateStatus(Manuscript.Statuses.accepted)
    } else {
      manuscript.updateProperties({
        status: Manuscript.Statuses.inQA,
        technicalCheckToken: uuid.v4(),
      })
    }
    await manuscript.save()

    const manuscriptVersions = await Manuscript.findManuscriptsBySubmissionId({
      submissionId: manuscript.submissionId,
      excludedStatus: Manuscript.Statuses.deleted,
      eagerLoadRelations:
        '[files,teams.members,reviews.[comments,member.team]]',
    })
    await Promise.all(
      manuscriptVersions.map(async manuscript =>
        sendPackage({ manuscript, isEQA: true }),
      ),
    )

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    notificationService.notifyEQAWhenEiCMakesDecisionToPublish({
      manuscript,
      editorInChief,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_accepted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasGlobalRole']

module.exports = {
  initialize,
  authsomePolicies,
}
