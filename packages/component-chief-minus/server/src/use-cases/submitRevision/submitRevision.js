const { last } = require('lodash')
const config = require('config')

const noReviewers = require('./strategies/noReviewers')
const minorWithReviewers = require('./strategies/minorWithReviewers')
const majorWithReviewers = require('./strategies/majorWithReviewers')
const eicRevision = require('./strategies/eicRevision')

const removalDays = config.get('reminders.reviewer.acceptInvitation.remove')
const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')

const initialize = ({
  invitationsService,
  notificationService,
  models: { Manuscript, Journal, Review, TeamMember },
  logEvent,
  emailJobsService,
  removalJobsService,
}) => ({
  async execute({ submissionId }) {
    const manuscripts = await Manuscript.findAll({
      queryObject: { submissionId },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: [
        'reviews.member.team',
        'teams.members.user.identities',
      ],
    })
    const journal = await Journal.find(
      manuscripts[0].journalId,
      'teams.members',
    )
    const editorialAssistant = journal.getEditorialAssistant()

    const latestManuscriptVersion = manuscripts.find(
      manuscript => manuscript.status !== Manuscript.Statuses.olderVersion,
    )

    const draft = last(manuscripts)
    const beforeDraft = manuscripts[manuscripts.length - 2]

    beforeDraft.updateStatus(Manuscript.Statuses.olderVersion)
    await beforeDraft.save()

    const editorReview = beforeDraft.getLatestEditorReview()

    const submittingAuthor = draft.getSubmittingAuthor()

    const responseToRevision = draft.reviews.find(
      review =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )

    if (responseToRevision) {
      responseToRevision.setSubmitted(new Date().toISOString())
      await responseToRevision.save()
    }

    const strategies = {
      noReviewers,
      minor: minorWithReviewers,
      major: majorWithReviewers,
      revision: eicRevision,
    }

    let strategy = editorReview.recommendation

    const reviewers = beforeDraft.getReviewers()
    if (
      reviewers.length === 0 &&
      editorReview.recommendation !== Review.Recommendations.revision
    ) {
      strategy = 'noReviewers'
    }

    const handlingEditor = beforeDraft.getHandlingEditor()

    await strategies[strategy].execute({
      draft,
      Journal,
      timeUnit,
      logEvent,
      Manuscript,
      TeamMember,
      removalDays,
      beforeDraft,
      submissionId,
      handlingEditor,
      emailJobsService,
      submittingAuthor,
      invitationsService,
      notificationService,
      editorialAssistant,
      removalJobsService,
    })

    logEvent({
      userId: submittingAuthor.userId,
      manuscriptId: latestManuscriptVersion.id,
      action: logEvent.actions.revision_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: submissionId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
