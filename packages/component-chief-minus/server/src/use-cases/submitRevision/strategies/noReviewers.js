module.exports = {
  execute: async ({
    draft,
    Manuscript,
    submissionId,
    handlingEditor,
    submittingAuthor,
    editorialAssistant,
    notificationService,
  }) => {
    draft.updateStatus(Manuscript.Statuses.heAssigned)
    await draft.save()

    notificationService.notificationsHandlingEditor.notifyHeWhenRevisionSubmitted(
      {
        draft,
        submissionId,
        handlingEditor,
        submittingAuthor,
        editorialAssistant,
      },
    )
  },
}
