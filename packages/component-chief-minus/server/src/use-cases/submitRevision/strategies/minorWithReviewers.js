module.exports = {
  execute: async ({
    draft,
    TeamMember,
    Manuscript,
    beforeDraft,
    handlingEditor,
    submittingAuthor,
    notificationService,
    editorialAssistant,
  }) => {
    const pendingReviewers = beforeDraft.getReviewersByStatus(
      TeamMember.Statuses.pending,
    )
    await Promise.all(pendingReviewers.map(async pr => pr.delete()))

    draft.updateStatus(Manuscript.Statuses.reviewCompleted)
    await draft.saveRecursively()

    notificationService.notificationsHandlingEditor.notifyHeWhenRevisionSubmitted(
      {
        draft,
        handlingEditor,
        submittingAuthor,
        editorialAssistant,
      },
    )
  },
}
