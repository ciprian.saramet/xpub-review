const { Promise } = require('bluebird')

module.exports = {
  execute: async ({
    draft,
    logEvent,
    timeUnit,
    Manuscript,
    TeamMember,
    beforeDraft,
    removalDays,
    submissionId,
    handlingEditor,
    emailJobsService,
    submittingAuthor,
    notificationService,
    invitationsService,
    editorialAssistant,
    removalJobsService,
  }) => {
    const reviewers = beforeDraft.getReviewers()

    const pendingReviewers = beforeDraft.getReviewersByStatus(
      TeamMember.Statuses.pending,
    )
    await Promise.all(pendingReviewers.map(async pr => pr.delete()))

    const submittedReviewers = reviewers.filter(
      r => r.status === TeamMember.Statuses.submitted,
    )
    draft.addReviewers(submittedReviewers)
    const newVersionReviewers = draft.getReviewers()

    draft.updateStatus(Manuscript.Statuses.underReview)
    await draft.saveRecursively()

    invitationsService.sendInvitationToReviewerAfterMajorRevision({
      newVersionReviewers,
      manuscript: draft,
      submittingAuthor,
      editorialAssistant,
    })
    notificationService.notificationsHandlingEditor.notifyHeWhenRevisionSubmitted(
      {
        draft,
        submissionId,
        handlingEditor,
        submittingAuthor,
        editorialAssistant,
      },
    )
    await Promise.each(newVersionReviewers, reviewers =>
      logEvent({
        userId: null,
        manuscriptId: draft.id,
        action: logEvent.actions.reviewer_invited,
        objectType: logEvent.objectType.user,
        objectId: reviewers.userId,
      }),
    )

    await Promise.each(newVersionReviewers, async reviewer => {
      await emailJobsService.scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision(
        {
          manuscript: draft,
          reviewer,
          submittingAuthorName: submittingAuthor.getName(),
        },
      )
      await removalJobsService.scheduleRemovalJob({
        timeUnit,
        days: removalDays,
        invitationId: reviewer.id,
        manuscriptId: draft.id,
      })
    })
  },
}
