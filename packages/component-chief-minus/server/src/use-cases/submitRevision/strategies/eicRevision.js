module.exports = {
  execute: async ({
    draft,
    Journal,
    Manuscript,
    submittingAuthor,
    notificationService,
  }) => {
    const journal = await Journal.find(draft.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const editorialAssistant = journal.getEditorialAssistant()

    draft.updateStatus(Manuscript.Statuses.submitted)
    await draft.save()

    notificationService.notificationsEditorInChief.notifyEiCWhenRevisionSubmitted(
      {
        draft,
        editorInChief,
        submittingAuthor,
        editorialAssistant,
      },
    )
  },
}
