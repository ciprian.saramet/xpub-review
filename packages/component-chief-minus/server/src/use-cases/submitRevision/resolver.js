const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Email = require('@pubsweet/component-email-templating')
const notificationsEditorInChief = require('../../notificationService/editorInChief/notifications')
const notificationsHandlingEditor = require('../../notificationService/handlingEditor/notifications')
const invitationsReviewer = require('../../invitationsService/invitations')
const emailJobs = require('../../emailJobs/reviewer/emailJobs')
const removalJobs = require('../../removalJobs/reviewersInvitationRemovalJobs')

const resolver = {
  Query: {},
  Mutation: {
    async submitRevision(_, { submissionId }, ctx) {
      const invitationsService = invitationsReviewer.initialize({ Email })
      const notificationService = {
        notificationsHandlingEditor: notificationsHandlingEditor.initialize({
          Email,
        }),
        notificationsEditorInChief: notificationsEditorInChief.initialize({
          Email,
        }),
      }
      const { TeamMember, ReviewerSuggestion, Job } = models

      const emailJobsService = emailJobs.initialize({ logEvent, Email, Job })
      const removalJobsService = removalJobs.initialize({
        logEvent,
        Job,
        TeamMember,
        ReviewerSuggestion,
      })
      return useCases.submitRevisionUseCase
        .initialize({
          notification: {
            notificationsHandlingEditor,
            notificationsEditorInChief,
            invitationsReviewer,
          },
          emailJobsService,
          removalJobsService,
          models,
          logEvent,
          notificationService,
          invitationsService,
        })
        .execute({ submissionId })
    },
    async updateDraftRevision(_, { manuscriptId, autosaveInput }, ctx) {
      return useCases.updateDraftRevisionUseCase
        .initialize(models)
        .execute({ manuscriptId, autosaveInput })
    },
  },
}

module.exports = resolver
