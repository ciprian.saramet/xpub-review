const config = require('config')
const { get, concat } = require('lodash')

const urlService = require('../../urlService/urlService')

const acceptUrl = config.get('accept-handling-editor.url')
const declineUrl = config.get('decline-handling-editor.url')
const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const { staffEmail, name: journalName } = config.get('journal')

module.exports = {
  getInvitationEmailProps({
    manuscript,
    handlingEditor,
    subject,
    editorInChiefName,
    editorialAssistant,
  }) {
    const { title, abstract, submissionId, id: manuscriptId } = manuscript
    const authorTeam = get(manuscript, 'teams').find(t => t.role === 'author')
    const authorTeamMembers = get(authorTeam, 'members', [])
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const agreeLink = urlService.createUrl({
      baseUrl,
      slug: acceptUrl,
      queryParams: {
        teamMemberId: handlingEditor.id,
        manuscriptId,
        submissionId,
      },
    })

    const declineLink = urlService.createUrl({
      baseUrl,
      slug: declineUrl,
      queryParams: {
        teamMemberId: handlingEditor.id,
      },
    })

    const authorsNameList = authorTeamMembers
      .map((author, index) => `${author.getName()}<sup>${index + 1}</sup>`)
      .join(', ')

    const authorAffiliationsList = authorTeamMembers
      .map(
        (author, index) =>
          `<div><sup style="color:#242424">${index +
            1}</sup><span style="color: #586971;">${get(
            author,
            'alias.aff',
          )}.</span></div>`,
      )
      .join(' ')

    const authorsList = concat(authorsNameList, authorAffiliationsList)

    return {
      type: 'user',
      templateType: 'invitation',
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(handlingEditor, 'alias.email'),
        name: get(handlingEditor, 'alias.surname'),
      },
      content: {
        title,
        abstract,
        agreeLink,
        declineLink,
        signatureJournal: journalName,
        signatureName: editorInChiefName,
        authorsList,
        subject,
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: handlingEditor.userId,
            token: handlingEditor.user.unsubscribeToken,
          },
        }),
      },
    }
  },
}
