const config = require('config')

const { getExpectedDate } = require('../dateService/dateService')

const { getEmailCopy: getReviewerEmailCopy } = require('./reviewer/emailCopy')
const {
  getEmailCopy: getHandlingEditorEmailCopy,
} = require('./handlingEditor/emailCopy')

const {
  getInvitationEmailProps: getReviewerInvitationProps,
} = require('./reviewer/getInvitationEmailProps')
const {
  getInvitationEmailProps: getHandlingEditorInvitationProps,
} = require('./handlingEditor/getInvitationEmailProps')

const baseUrl = config.get('pubsweet-client.baseUrl')

const initialize = ({ Email }) => ({
  async sendInvitationToReviewer({
    reviewer,
    manuscript,
    editorialAssistant,
    submittingAuthorName,
  }) {
    const emailProps = getReviewerInvitationProps({
      editorialAssistant,
      manuscript,
      reviewer,
    })

    const { title } = manuscript

    const emailType = 'reviewer-invitation'
    const titleText = `A manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`

    const { paragraph, ...bodyProps } = getReviewerEmailCopy({
      emailType,
      titleText,
      expectedDate: getExpectedDate({
        timestamp: new Date(),
        daysExpected: 14,
      }),
    })
    emailProps.bodyProps = bodyProps

    const email = new Email(emailProps)

    await email.sendEmail()
  },
  async sendInvitationToReviewerAfterMajorRevision({
    newVersionReviewers,
    manuscript,
    submittingAuthor,
  }) {
    const submittingAuthorName = submittingAuthor.getName()

    const { title, customId } = manuscript

    newVersionReviewers.forEach(reviewerMember => {
      const emailProps = getReviewerInvitationProps({
        reviewer: reviewerMember,
        manuscript,
      })

      const { paragraph, ...bodyProps } = getReviewerEmailCopy({
        emailType: 'reviewer-invitation-after-revision',
        titleText: `the manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`,
        expectedDate: getExpectedDate({
          timestamp: new Date(),
          daysExpected: 14,
        }),
      })

      emailProps.bodyProps = bodyProps
      emailProps.content.subject = `${customId}: Review invitation: New Version`

      const email = new Email(emailProps)

      return email.sendEmail()
    })
  },
  async sendInvitationToHandlingEditor({
    handlingEditor,
    manuscript,
    editorInChief,
    editorialAssistant,
    submittingAuthor,
  }) {
    const { customId, title } = manuscript
    const emailProps = getHandlingEditorInvitationProps({
      manuscript,
      handlingEditor,
      editorInChiefName: editorInChief.getName(),
      editorialAssistant,
      subject: `${customId}: Invitation to edit a manuscript`,
    })
    const targetUserName = editorInChief.getName()
    const { paragraph, ...bodyProps } = getHandlingEditorEmailCopy({
      baseUrl,
      targetUserName,
      manuscriptTitle: title,
      emailType: 'he-assigned',
      authorName: submittingAuthor.getName(),
    })
    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)

    return email.sendEmail()
  },
})
module.exports = { initialize }
