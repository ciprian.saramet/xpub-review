const config = require('config')
const { get, concat } = require('lodash')

const urlService = require('../../urlService/urlService')

const acceptReviewPath = config.get('accept-review.url')
const acceptReviewNewUserPath = config.get('accept-review-new-user.url')
const declineReviewPath = config.get('decline-review.url')
const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const { staffEmail, name: journalName } = config.get('journal')

module.exports = {
  getInvitationEmailProps({ manuscript, reviewer, editorialAssistant }) {
    const {
      title,
      abstract,
      submissionId,
      id: manuscriptId,
      customId,
    } = manuscript

    const authorTeam = get(manuscript, 'teams').find(t => t.role === 'author')
    const authorTeamMembers = get(authorTeam, 'members', [])

    const subjectText = `${customId}: Review invitation`

    const declineLink = urlService.createUrl({
      baseUrl,
      slug: declineReviewPath,
      queryParams: {
        invitationId: reviewer.id,
        manuscriptId,
      },
    })

    let agreeLink = urlService.createUrl({
      baseUrl,
      slug: acceptReviewPath,
      queryParams: {
        invitationId: reviewer.id,
        submissionId,
        manuscriptId,
      },
    })
    const reviewerIdentity = reviewer.user.getDefaultIdentity()
    if (!reviewerIdentity.isConfirmed) {
      agreeLink = urlService.createUrl({
        baseUrl,
        slug: acceptReviewNewUserPath,
        queryParams: {
          invitationId: reviewer.id,
          submissionId: manuscript.submissionId,
          manuscriptId: manuscript.id,
          email: get(reviewer, 'alias.email', ''),
          givenNames: get(reviewer, 'alias.givenNames', ''),
          surname: get(reviewer, 'alias.surname', ''),
          aff: get(reviewer, 'alias.aff', ''),
          country: get(reviewer, 'alias.country', ''),
          token: reviewer.user.passwordResetToken,
        },
      })
    }

    const authorsNameList = authorTeamMembers
      .map((author, index) => `${author.getName()}<sup>${index + 1}</sup>`)
      .join(', ')

    const authorAffiliationsList = authorTeamMembers
      .map(
        (author, index) =>
          `<div><sup style="color:#242424">${index +
            1}</sup><span style="color: #586971;">${get(
            author,
            'alias.aff',
          )}.</span></div>`,
      )
      .join(' ')

    const authorsList = concat(authorsNameList, authorAffiliationsList)

    const handlingEditor = manuscript.getHandlingEditor()
    const handlingEditorName = handlingEditor.getName()
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : handlingEditorName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    return {
      type: 'user',
      templateType: 'invitation',
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(reviewer, 'alias.email'),
        name: get(reviewer, 'alias.surname'),
      },
      content: {
        title,
        abstract,
        agreeLink,
        declineLink,
        signatureJournal: journalName,
        signatureName: handlingEditorName,
        authorsList,
        subject: subjectText,
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: reviewer.user.id,
            token: reviewer.user.unsubscribeToken,
          },
        }),
      },
    }
  },
}
