const JournalArticleType = require('./src/entities/journalArticleType')

module.exports = {
  model: JournalArticleType,
  modelName: 'JournalArticleType',
}
