import gql from 'graphql-tag'
import { auditLogEventsFragment } from './fragments'

export const getAuditLogEvents = gql`
  query getAuditLogEvents($manuscriptId: String!) {
    getAuditLogEvents(manuscriptId: $manuscriptId) {
      ...auditLogEvents
    }
  }
  ${auditLogEventsFragment}
`
