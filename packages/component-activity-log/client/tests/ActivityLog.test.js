import React from 'react'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { theme } from '@hindawi/ui'
import { Router } from 'react-router'
import { createBrowserHistory } from 'history'
import {
  cleanup,
  fireEvent,
  render as rtlRender,
  wait,
} from 'react-testing-library'
import { ThemeProvider } from 'styled-components'

import ActivityLog from '../components/ActivityLog'

const history = createBrowserHistory()

const render = ui => {
  const Component = () => (
    <ThemeProvider theme={theme}>
      <Router history={history}>{ui}</Router>
    </ThemeProvider>
  )

  const utils = rtlRender(<Component />)

  return {
    ...utils,
    selectOption: value => {
      fireEvent.click(utils.container.querySelector(`button[type=button]`))
      wait(() => utils.getByText(value))
      fireEvent.click(utils.getByText(value))
    },
  }
}

const events = [
  {
    version: 1,
    logs: [
      {
        id: '1',
        action: 'sent reviewer invite to ',
        user: {
          email: 'handlingEditor@hindawieditorial.com',
          role: 'handlingEditor',
        },
        target: {
          email: 'allie.bryan@gmail.com',
          role: '',
        },
        created: 1550629860963,
      },
      {
        id: '2',
        action: 'sent reviewer invite to',
        created: 1550629871097,
        user: {
          email: 'author@hindawieditorial.com',
          role: 'author',
        },
        target: {
          email: 'mike-knight@educentralmanchester.co.uk',
          role: '',
        },
      },
      {
        id: '3',
        action: 'sent reviewer invite to',
        created: 1550629880332,
        user: {
          email: 'editorInChief@hindawieditorial.com',
          role: 'editorInChief',
        },
        target: {
          email: 'charlie.bennett@gmail.com',
          role: '',
        },
      },
      {
        id: '4',
        action: 'accepted invitation to review',
        created: 1550629888928,
        user: {
          email: 'reviewer1@hindawi.com',
          role: 'reviewer',
        },
        target: null,
      },
      {
        id: '5',
        action: 'sent automatic invitiation to review to',
        created: 1550629902715,
        user: null,
        target: {
          email: 'curt.weber@facultyofamazingwork.com ',
          role: 'reviewer',
        },
      },
    ],
  },
]

describe('Should filter events by role.', () => {
  afterEach(cleanup)

  it('Should filter by handling editor.', () => {
    const { selectOption, getByText } = render(
      <ActivityLog events={events} expanded />,
    )
    selectOption('Handling Editor')
    expect(getByText('handlingEditor@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('Handling Editor')).toBeInTheDocument()
    expect(getByText('HE')).toBeInTheDocument()
  })

  it('Should filter by editor in chief.', () => {
    const { selectOption, getByText } = render(
      <ActivityLog events={events} expanded />,
    )
    selectOption('Editor In Chief')
    expect(getByText('editorInChief@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('Editor In Chief')).toBeInTheDocument()
    expect(getByText('EIC')).toBeInTheDocument()
  })

  it('Should filter by reviewer.', () => {
    const { selectOption, getByText } = render(
      <ActivityLog events={events} expanded />,
    )
    selectOption('Reviewer')
    expect(getByText('reviewer1@hindawi.com')).toBeInTheDocument()
    expect(getByText('Reviewer')).toBeInTheDocument()
  })

  it('Should filter by author.', () => {
    const { selectOption, getByText } = render(
      <ActivityLog events={events} expanded />,
    )
    selectOption('Author')
    expect(getByText('author@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('Author')).toBeInTheDocument()
    expect(getByText('SA')).toBeInTheDocument()
  })
  it('Should filter by all.', () => {
    const { selectOption, getByText } = render(
      <ActivityLog events={events} expanded />,
    )
    selectOption('All')

    expect(getByText('All')).toBeInTheDocument()
    expect(getByText('handlingEditor@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('HE')).toBeInTheDocument()
    expect(getByText('reviewer1@hindawi.com')).toBeInTheDocument()
    expect(getByText('Reviewer')).toBeInTheDocument()
    expect(getByText('author@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('System')).toBeInTheDocument()
    expect(getByText('SA')).toBeInTheDocument()
    expect(getByText('editorInChief@hindawieditorial.com')).toBeInTheDocument()
    expect(getByText('EIC')).toBeInTheDocument()
  })
})
